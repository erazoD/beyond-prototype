$(document).ready(function(){
	var vm = new Vue({
		el:'#app',
		data: {
			id: 1,
			mouthData:1,
			plaqueData:1,
			evoData:1,
			maxId:14,
			minId:1,
			thumbnails:[],
			selected:false,
			unselected:true
		},
		methods:{
			//permite el salto entre pantallas desde el menu de navegacion
			jumpSlide:function(jumpVal, index){
				this.id = jumpVal.id;
				!this.selected;
				!this.unselected;
				this.closeNav();
				this.checkId(this.id);
			   $('.default').removeClass('active-menu');
               $('.menu'+ index).addClass('active-menu');
			},
			//Interaccion textos de diapositiva 13
			enterProduct:function(num){
                $("#" + num).addClass('appear');
			},
			exitProduct:function(num){
				$('#' + num).removeClass('appear');
			},
			//Marca la diapositiva actual en el menu de navegacion
			//despliega el menu de navegacion
			openNav:function(){
				if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
					$("#myNav").css('width', '100%');
				}else{
					$("#myNav").css('width', '20%');
				}
			},
			//cierra el menu de navegacion
			closeNav:function(){
				$("#myNav").css('width', '0%');
			},
			//Permite avanzar entre las pantallas
			forwardSlide:function(){
				this.id++;
				$('.default').removeClass('active-menu');
				$('.menu' + (this.id-1)).addClass('active-menu'); 
				if(this.id == this.maxId){
					$('.forward').css('display', 'none');
				}else{
					$('.forward').css('display', 'inline-block');
				}
				this.checkId(this.id);
			},
			//Permite volver entre las pantllas
			backwardSlide:function(){
				this.id--;
			    $('.default').removeClass('active-menu');
				$('.menu' + (this.id-1)).addClass('active-menu'); 
				if(this.id == this.minId){
					$('.back').css('display', 'none');
				}else{
					$('.back').css('display', 'inline-block');
				}
				this.checkId(this.id);
			},
			//Inicializa la aplicacion en la pantalla 2
			beginShow:function(){
				addShadow();
				$('ul.line-products li.cepillo-line').addClass('active');
			},
			//Muestra la informacion en la pantalla 2
			getTip:function(event){
				var tool  = event.currentTarget;
				var image = tool.getAttribute("data-image");
				var clase = tool.getAttribute('class');
				var adapt = clase.split(' ');
				addShadow();
				$('ul.line-products li.lines').removeClass('show active');
				$("."+adapt[0]).addClass('show active');
				if(image != "finish"){
					$('img.boca-imagen').attr('src', 'img/'+image+'.png');
				}else{
					$('img.boca-imagen').attr('src', 'img/boca-d.png');
					$('ul.line-products li.lines').removeClass('show active');
					$('.shadow').removeClass('active');
				}
			},
		   //Realiza el cambio de las graficas para las pantallas 7, 9, 11
		   switchData(event){
		   	var showImg = event.currentTarget;
		   	var graphId = showImg.getAttribute("graph-id");
		   	var graphClass = showImg.getAttribute('class').split(' ');
                //console.log(graphClass[1]);
                if(graphClass[0] == 'mouth-btn'){
                	this.mouthData = graphId;
                	$('.mouth-btn').removeClass('active');
                	$('.' + graphClass[1]).addClass('active');
                }else if(graphClass[0] == 'selector-round'){
                	this.plaqueData = graphId;
                	$('.selector-round').removeClass('active');
                	$('.' + graphClass[1]).addClass('active');
                }else if(graphClass[0] == 'evo-round'){
                	this.evoData = graphId;
                	$('.selector-round').removeClass('active');
                	$('.' + graphClass[2]).addClass('active');
                }
            },
            checkId:function(id){
            	var currentId = id;
                 //console.log('Este es el id actual:' +  currentId);
                 if(currentId < 7 || currentId > 7){
                 	this.mouthData =1;
                 }
                 if(currentId < 9 || currentId > 9){ 
                 	this.plaqueData = 1; 
                 }
                 if(currentId < 11 || currentId > 11){
                 	this.evoData = 1;
                 }
                 if(currentId == 13){
                 	$('body').css('background-color', '#51b989');
                 }else{
                 	$('body').css('background-color','#169fbf');
                 }
                 if(currentId == 14){
                 	$('body').css('background-color', '#fff');
                 }
             }
         },
         created: function(){
         	fetch("json/data.json").then(r=>r.json()).then(json =>{
         		vm.thumbnails = json;
         	});
         },
         mounted: function() {
           $('#loader').css('display', 'none');
           $('#app').css('display', 'block');
        }
     });
function addShadow(){
	$('.interactive .shadow').addClass('active');
}
});