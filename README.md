# Beyond Prototype

* Prototype for Landing/Presentation showcasing Listerine Products

### What is this repository for? ###
* Project for Listerine "Beyond"
*3.0

### How do I get set up? ###

* This is a front-end project implemented with Vue, just download in htdocs folder of your localhost and browse it normally.
* Some of the slides are interactive, be sure to interact with them!

### Who do I talk to? ###
ErazD

