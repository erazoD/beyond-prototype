(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1100,
	height: 700,
	fps: 30,
	color: "#FFFFFF",
	opacity: 0.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib._03oscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AjgSJMAAAgkRIHBAAMAAAAkRg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-116.1,45,232.3);


(lib._03claro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AjgYpMAAAgxRIHBAAMAAAAxRg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-157.7,45,315.5);


(lib._03azul = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AjgSJMAAAgkRIHBAAMAAAAkRg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-116.1,45,232.3);


(lib._036meses = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgYBwQgMgEgIgHQgJgIgEgLQgEgLAAgNIAAgQIAtAAIAAAQQAAAHAEAFQAEAFAIAAQAFAAAEgEQAEgFAAgIQAAgLgJgKIgUgWIgPgOIgOgOQgHgIgEgKQgEgKAAgLQAAgMAEgMQAEgMAIgIQAIgJAMgFQAMgFAOAAQAMAAALAEQALAEAHAJQAIAHAEALQAEAKAAAMIAAAPIgtAAIAAgQQAAgHgDgEQgEgDgFAAQgEAAgEADQgEAEAAAHQAAAHACAFIAHALIAIAJIALALIAQAPIAOAOQAGAJAEAJQAEAKAAANQAAAMgEAMQgFAMgIAIQgIAIgLAFQgMAFgOAAQgNAAgLgEg");
	this.shape.setTransform(38.5,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag1BvIAAjdIBrAAIAAArIg4AAIAAAuIAkAAIAAAlIgkAAIAAAyIA4AAIAAAtg");
	this.shape_1.setTransform(25.7,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgYBwQgMgEgIgHQgJgIgEgLQgEgLAAgNIAAgQIAtAAIAAAQQAAAHAEAFQAEAFAIAAQAFAAAEgEQAEgFAAgIQAAgLgJgKIgUgWIgPgOIgOgOQgHgIgEgKQgEgKAAgLQAAgMAEgMQAEgMAIgIQAIgJAMgFQAMgFAOAAQAMAAALAEQALAEAHAJQAIAHAEALQAEAKAAAMIAAAPIgtAAIAAgQQAAgHgDgEQgEgDgFAAQgEAAgEADQgEAEAAAHQAAAHACAFIAHALIAIAJIALALIAQAPIAOAOQAGAJAEAJQAEAKAAANQAAAMgEAMQgFAMgIAIQgIAIgLAFQgMAFgOAAQgNAAgLgEg");
	this.shape_2.setTransform(12.3,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag1BvIAAjdIBrAAIAAArIg3AAIAAAuIAjAAIAAAlIgjAAIAAAyIA3AAIAAAtg");
	this.shape_3.setTransform(-0.4,-1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAnBvIAAiUIgKA1IgQBfIgfAAIgQhfIgIg1IAACUIgtAAIAAjdIBEAAIAQBXIADAmIAGgmIAPhXIBDAAIAADdg");
	this.shape_4.setTransform(-16.9,-1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgXBwQgLgEgHgIQgIgIgFgKQgDgLAAgNIAAhzQAAgNADgLQAFgKAIgIQAHgIAMgEQAKgEAMAAQAOAAAKAEQALAFAGAIQAHAHAEAKQAEAJAAAKIAAAVIgqAAIAAgQQAAgIgDgEQgEgDgGAAQgFAAgEADQgDAEAAAGIAAA0QAFgHAGgEQAHgEAMAAQATAAALAKQAEAGADAGQACAGABAJIAAArQAAANgFALQgEAKgIAIQgHAIgLAEQgLAEgNAAQgMAAgLgEgAgIAMQgDADAAAFIAAAqQAAAFADADQAEADAEAAQAGAAADgDQADgDAAgFIAAgqQAAgFgDgDQgDgDgGAAQgEAAgEADg");
	this.shape_5.setTransform(-38.4,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-47.5,-18.7,94.9,37.6);


(lib._02oscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AjgRCMAAAgiDIHBAAMAAAAiDg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-109,45,218.1);


(lib._02claro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AjgX5MAAAgvxIHBAAMAAAAvxg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-152.9,45,305.9);


(lib._02azul = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AjgUwMAAAgpfIHBAAMAAAApfg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-132.8,45,265.7);


(lib._023meses = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgYBwQgMgEgIgHQgJgIgEgLQgEgLAAgNIAAgQIAtAAIAAAQQAAAHAEAFQAEAFAIAAQAFAAAEgEQAEgFAAgIQAAgLgJgKIgUgWIgPgOIgOgOQgHgIgEgKQgEgKAAgLQAAgMAEgMQAEgMAIgIQAIgJAMgFQAMgFAOAAQAMAAALAEQALAEAHAJQAIAHAEALQAEAKAAAMIAAAPIgtAAIAAgQQAAgHgDgEQgEgDgFAAQgEAAgEADQgEAEAAAHQAAAHACAFIAHALIAIAJIALALIAQAPIAOAOQAGAJAEAJQAEAKAAANQAAAMgEAMQgFAMgIAIQgIAIgLAFQgMAFgOAAQgNAAgLgEg");
	this.shape.setTransform(38.2,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag1BvIAAjdIBrAAIAAArIg3AAIAAAuIAjAAIAAAlIgjAAIAAAyIA3AAIAAAtg");
	this.shape_1.setTransform(25.4,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgYBwQgMgEgIgHQgJgIgEgLQgEgLAAgNIAAgQIAtAAIAAAQQAAAHAEAFQAEAFAIAAQAFAAAEgEQAEgFAAgIQAAgLgJgKIgUgWIgPgOIgOgOQgHgIgEgKQgEgKAAgLQAAgMAEgMQAEgMAIgIQAIgJAMgFQAMgFAOAAQAMAAALAEQALAEAHAJQAIAHAEALQAEAKAAAMIAAAPIgtAAIAAgQQAAgHgDgEQgEgDgFAAQgEAAgEADQgEAEAAAHQAAAHACAFIAHALIAIAJIALALIAQAPIAOAOQAGAJAEAJQAEAKAAANQAAAMgEAMQgFAMgIAIQgIAIgLAFQgMAFgOAAQgNAAgLgEg");
	this.shape_2.setTransform(12,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag1BvIAAjdIBrAAIAAArIg4AAIAAAuIAkAAIAAAlIgkAAIAAAyIA4AAIAAAtg");
	this.shape_3.setTransform(-0.7,-1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAmBvIAAiUIgJA1IgRBfIgeAAIgQhfIgIg1IAACUIgtAAIAAjdIBEAAIAPBXIAEAmIAGgmIAOhXIBEAAIAADdg");
	this.shape_4.setTransform(-17.2,-1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgZBwQgLgFgGgIQgIgHgDgKQgDgJgBgKIAAgWIArAAIAAASQABAHADAEQADADAHAAQAEAAAEgDQADgEAAgGIAAgiQAAgJgJAAIgPAAIAAgiIAPAAQAJAAAAgKIAAggQAAgGgDgEQgEgDgEAAQgOAAAAAPIAAARIgrAAIAAgWQABgKADgJQADgKAIgHQAGgIALgFQAKgEANAAQAMAAALAEQAMAEAHAHQAJAHAEAKQAEAKAAAMIAAAXQABAPgIAKQgGAKgNADQAMABAHAKQAHAJAAAPIAAAbQAAAMgEAKQgEAKgJAHQgHAIgMADQgLAEgMAAQgNAAgKgEg");
	this.shape_5.setTransform(-38.5,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-47.2,-18.7,94.3,37.6);


(lib._01oscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("EgDgAhUMAAAhCnIHBAAMAAABCng");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-213.2,45,426.5);


(lib._01claro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AjgZvMAAAgzdIHBAAMAAAAzdg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-164.8,45,329.6);


(lib._01BASELINE = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag1BvIAAjdIBrAAIAAArIg3AAIAAAuIAjAAIAAAlIgjAAIAAAyIA3AAIAAAtg");
	this.shape.setTransform(44.6,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAXBvIgehSIgMgdIAABvIgtAAIAAjdIAtAAIAbBTIANAeIAAhxIAtAAIAADdg");
	this.shape_1.setTransform(30.4,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgYBvIAAjdIAxAAIAADdg");
	this.shape_2.setTransform(19,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag0BvIAAjdIA0AAIAACxIA1AAIAAAsg");
	this.shape_3.setTransform(9.8,-1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ag1BvIAAjdIBrAAIAAArIg3AAIAAAuIAjAAIAAAlIgjAAIAAAyIA3AAIAAAtg");
	this.shape_4.setTransform(-2.7,-1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgYBwQgMgEgIgHQgJgIgEgLQgEgLAAgNIAAgQIAtAAIAAAQQAAAHAEAFQAEAFAIAAQAFAAAEgEQAEgFAAgIQAAgLgJgKIgUgWIgPgOIgOgOQgHgIgEgKQgEgKAAgLQAAgMAEgMQAEgMAIgIQAIgJAMgFQAMgFAOAAQAMAAALAEQALAEAHAJQAIAHAEALQAEAKAAAMIAAAPIgtAAIAAgQQAAgHgDgEQgEgDgFAAQgEAAgEADQgEAEAAAHQAAAHACAFIAHALIAIAJIALALIAQAPIAOAOQAGAJAEAJQAEAKAAANQAAAMgEAMQgFAMgIAIQgIAIgLAFQgMAFgOAAQgNAAgLgEg");
	this.shape_5.setTransform(-16.1,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAQBvIgDghIgdAAIgFAhIguAAIAnjdIA5AAIAnDdgAgKAnIASAAIgIhPg");
	this.shape_6.setTransform(-29.8,-1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Ag7BvIAAjdIBIAAQAKAAAIACQAIADAHAGQAHAFADAJQAEAIAAAMIAAAeIgBALIgEAKQgDAFgEADQgFAEgIACQAOACAGAJQAFAJAAAMIAAAiQAAAMgEAIQgDAJgHAFQgHAGgIADQgIACgKAAgAgIBHIALAAQAJAAAAgLIAAggQAAgLgJAAIgLAAgAgIgQIALAAQAJAAAAgLIAAggQAAgLgJAAIgLAAg");
	this.shape_7.setTransform(-43.6,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.8,-18.7,105.6,37.6);


(lib._01azul = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AjgcjMAAAg5FIHBAAMAAAA5Fg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.5,-182.8,45,365.6);


(lib._00vertical = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgAEAn/MAAAhP9IAJAAMAAABP9g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-256,1,512);


(lib._00textooscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("Ag2BOIBjikIAKAIIhiClgAAUBOQgDgDgCgFQgCgEAAgGIAAgiQAAgGACgFQACgEADgEQAHgHALAAQAMAAAGAHQAEAEACAEQABAFAAAGIAAAiQABAMgIAGQgGAIgMAAQgLAAgHgIgAAhAQQgCADAAAEIAAAoQAAADACADQACADADgBQAEABACgDQADgDgBgDIAAgoQABgEgDgDQgCgBgEAAQgDAAgCABgAgwgBQgEgCgDgDQgIgHABgMIAAgiQAAgFABgFQACgFAEgEIAHgEQAFgCAGAAQAFAAAFACIAIAEQADAEACAFQACAFAAAFIAAAiQAAAMgHAHQgEADgEACQgFABgFAAQgGAAgFgBgAgrhEQgCACAAAEIAAApQAAADACACQADACADABQADgBACgCQACgCAAgDIAAgpQAAgEgCgCQgCgCgDgBQgDABgDACg");
	this.shape.setTransform(74.2,-0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgpBTIAAgIQgBgKACgIQACgIAEgJQADgJAHgJIAPgWIAJgKIAGgLIAEgKIABgJQAAgFgEgEQgCgCgEAAQgDAAgDADQgCADAAAFIAAAQIggAAIAAgTQAAgHACgIQADgGAFgGQAFgGAIgDQAHgEAJABQALgBAJAEQAIAEAFAFQAFAHACAIQACAHAAAKIgBANIgEAOQgDAIgGAFQgFAHgKAJIgJAMIgEAJIgCAHIAAAHIAsAAIAAAeg");
	this.shape_1.setTransform(62.2,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgQBRIAAiSQAJAAADgFQAEgFgBgGIASAAIAACig");
	this.shape_2.setTransform(54.1,-0.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgMARIAAghIAZAAIAAAhg");
	this.shape_3.setTransform(48.8,5.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#256379").s().p("AgRBSQgIgDgFgGQgGgFgDgIQgDgIAAgKIAAhUQAAgJADgIQADgIAGgGQAFgFAIgDQAIgDAJAAQAKAAAIADQAIADAGAFQAFAGADAIQADAIAAAJIAABUQAAAKgDAIQgDAIgFAFQgGAGgIADQgIADgKAAQgJAAgIgDgAgFgxQgDACAAADIAABYQAAAEADADQACACADAAQAEAAACgCQADgDAAgEIAAhYQAAgDgDgCQgCgDgEAAQgDAAgCADg");
	this.shape_4.setTransform(41.6,-0.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#256379").s().p("AAMBRIgDgYIgUAAIgDAYIgjAAIAdiiIAqAAIAcCigAgHAdIANAAIgGg6g");
	this.shape_5.setTransform(27.8,-0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#256379").s().p("AARBRIgVg8IgKgVIAABRIghAAIAAiiIAhAAIAUA+IAJAVIAAhTIAhAAIAACig");
	this.shape_6.setTransform(17.2,-0.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#256379").s().p("AgRBRIAAiiIAkAAIAACig");
	this.shape_7.setTransform(8.9,-0.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#256379").s().p("AgtBRIAAiiIAtAAQALAAAIADQAIAEAHAFQAFAGAEAIQAEAJgBAJIAABOQABAJgEAIQgEAHgFAGQgHAFgIACQgIADgLAAgAgIAzIAIAAQADAAADgDQACgEAAgGIAAhLQAAgGgCgDQgDgDgDAAIgIAAg");
	this.shape_8.setTransform(0.9,-0.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#256379").s().p("AgSBRIAAiiIAlAAIAACig");
	this.shape_9.setTransform(-7.2,-0.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#256379").s().p("AAMBRIgMgyIgPAyIgjAAIAdhRIgchRIAnAAIALA1IAOg1IAkAAIgdBPIAcBTg");
	this.shape_10.setTransform(-15.2,-0.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#256379").s().p("AgmBRIAAiiIBOAAIAAAhIgpAAIAAAhIAaAAIAAAbIgaAAIAAAkIApAAIAAAhg");
	this.shape_11.setTransform(-24.9,-0.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#256379").s().p("AAKBRIAAg+IgUAAIAAA+IglAAIAAiiIAlAAIAABFIAUAAIAAhFIAnAAIAACig");
	this.shape_12.setTransform(-35.3,-0.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#256379").s().p("AAQBRQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBgBIgCgGIgCgIIAAgJIAAgeQAAgDgDgDQgCgDgCgBIgIAAIAABBIgmAAIAAiiIAzAAQAHAAAGACQAHADAFAEQAGAEACAHQADAGABAJIAAAcQgBAHgEAFQgEAGgGACQAGADAFAHQAFAGAAAGIAAAeIABAPIADAMIAAAAgAgIgJIAIAAQAGABAAgJIAAgaQAAgIgGAAIgIAAg");
	this.shape_13.setTransform(-46,-0.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#256379").s().p("AgRBSQgJgDgGgGQgGgFgEgIQgEgIAAgKIAAhUQAAgJAEgIQAEgIAGgFQAGgGAJgDQAIgDAJAAQAJAAAJADQAJADAGAGQAGAFAEAIQAEAIAAAJIAABUQAAAKgEAIQgEAIgGAFQgGAGgJADQgJADgJAAQgJAAgIgDgAgEgyQgDACAAAEIAABYQAAAFADACQADABABAAQADAAADgBQADgCgBgFIAAhYQABgEgDgCQgDgCgDAAQgBAAgDACg");
	this.shape_14.setTransform(-56.9,-0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#256379").s().p("AglBRIAAiiIAlAAIAACCIAnAAIAAAgg");
	this.shape_15.setTransform(-66.4,-0.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#256379").s().p("AgQBSQgIgDgHgGQgGgGgEgIQgEgJAAgKIAAhPQAAgKAEgIQAEgJAGgGQAHgFAIgEQAJgDAJAAQALAAAIADQAHADAGAGQAGAFADAIQADAIAAAKIAAAVIghAAIAAgWQAAgEgDgDQgDgDgFAAQgDAAgCADQgDADAAAEIAABVQAAAEADADQACADADAAQAFAAADgDQADgDAAgEIAAgWIAhAAIAAAWQAAAJgDAIQgDAIgGAGQgGAFgHADQgIADgLAAQgJAAgJgDg");
	this.shape_16.setTransform(-76.3,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-83.6,-14.2,167,28.7);


(lib._00textoclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AglBRIAAiiIAlAAIAACCIAmAAIAAAgg");
	this.shape.setTransform(32,-0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B2F2F8").s().p("AgRBSQgIgDgHgGQgGgFgEgIQgEgIAAgKIAAhUQAAgJAEgIQAEgIAGgFQAHgGAIgDQAJgDAIAAQAJAAAKADQAIADAHAGQAFAFAFAIQADAIAAAJIAABUQAAAKgDAIQgFAIgFAFQgHAGgIADQgKADgJAAQgIAAgJgDgAgFgyQgCACAAAEIAABYQAAAFACACQADABACAAQADAAADgBQACgCABgFIAAhYQgBgEgCgCQgDgCgDAAQgCAAgDACg");
	this.shape_1.setTransform(21.9,-0.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B2F2F8").s().p("AAQBRQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBgBIgCgGIgCgIIAAgJIAAgeQgBgDgCgDQgCgDgCgBIgIAAIAABBIgmAAIAAiiIAzAAQAHAAAHACQAGADAFAEQAGAEACAHQADAGAAAJIAAAcQAAAHgDAFQgFAGgGACQAGADAFAHQAFAGAAAGIAAAeIABAPIADAMIAAAAgAgIgJIAIAAQAGABgBgJIAAgaQABgIgGAAIgIAAg");
	this.shape_2.setTransform(11.5,-0.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B2F2F8").s().p("AgSBRIAAiBIgaAAIAAghIBZAAIAAAhIgbAAIAACBg");
	this.shape_3.setTransform(1.2,-0.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#B2F2F8").s().p("AARBRIgVg8IgJgVIAABRIgiAAIAAiiIAiAAIATA+IAJAVIAAhTIAhAAIAACig");
	this.shape_4.setTransform(-9.1,-0.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B2F2F8").s().p("AgRBSQgJgDgGgGQgHgFgDgIQgEgIAAgKIAAhUQAAgJAEgIQADgIAHgFQAGgGAJgDQAJgDAIAAQAKAAAJADQAIADAGAGQAHAFADAIQAEAIAAAJIAABUQAAAKgEAIQgDAIgHAFQgGAGgIADQgJADgKAAQgIAAgJgDgAgEgyQgDACAAAEIAABYQAAAFADACQADABABAAQADAAADgBQADgCAAgFIAAhYQAAgEgDgCQgDgCgDAAQgBAAgDACg");
	this.shape_5.setTransform(-20.2,-0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B2F2F8").s().p("AgPBSQgKgDgGgGQgHgGgDgIQgEgJAAgKIAAhPQAAgKAEgIQADgJAHgGQAGgFAKgEQAJgDAIAAQALAAAHADQAJADAFAGQAGAFADAIQADAIAAAKIAAAVIghAAIAAgWQAAgEgDgDQgDgDgFAAQgDAAgDADQgCADAAAEIAABVQAAAEACADQADADADAAQAFAAADgDQADgDAAgEIAAgWIAhAAIAAAWQAAAJgDAIQgDAIgGAGQgFAFgJADQgHADgLAAQgIAAgJgDg");
	this.shape_6.setTransform(-30.8,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.1,-14.2,76.4,28.7);


(lib._00textoazul = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AgRBSQgJgDgGgFQgGgFgDgIQgDgJAAgKIAAgLIAgAAIAAALQAAAGAEAEQACADAGAAQAEAAACgDQADgDAAgHQAAgIgHgIIgNgPIgLgKIgKgKQgFgGgEgHQgCgIAAgIQAAgJACgIQAEgJAFgGQAGgHAJgEQAJgDAKAAQAJAAAIADQAHADAGAGQAFAFADAJQADAHABAJIAAALIghAAIAAgMQgBgFgCgDQgDgDgDAAQgCABgEACQgDADAAAFQAAAFACAEIAEAHIAGAIIAIAIIAMAKIALAKQAEAHADAHQACAHABAJQAAAKgEAIQgDAIgGAHQgFAGgJADQgIAEgLAAQgJAAgIgDg");
	this.shape.setTransform(75.3,-0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AgmBRIAAiiIBOAAIAAAhIgpAAIAAAhIAaAAIAAAbIgaAAIAAAkIApAAIAAAhg");
	this.shape_1.setTransform(65.9,-0.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7BD7F8").s().p("AgmBRIAAiiIAmAAIAACCIAnAAIAAAgg");
	this.shape_2.setTransform(57,-0.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7BD7F8").s().p("AAMBRIgDgYIgUAAIgDAYIgjAAIAdiiIAqAAIAcCigAgHAdIANAAIgGg6g");
	this.shape_3.setTransform(47.2,-0.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7BD7F8").s().p("AgSBRIAAiiIAlAAIAACig");
	this.shape_4.setTransform(39.6,-0.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7BD7F8").s().p("AgPBSQgKgDgGgGQgGgGgEgIQgEgJAAgKIAAhPQAAgKAEgIQAEgJAGgGQAGgFAKgEQAIgDAJAAQAKAAAJADQAIADAFAGQAGAFADAIQADAIAAAKIAAAVIghAAIAAgWQAAgEgDgDQgCgDgGAAQgDAAgDADQgCADAAAEIAABVQAAAEACADQADADADAAQAGAAACgDQADgDAAgEIAAgWIAhAAIAAAWQAAAJgDAIQgDAIgGAGQgFAFgIADQgJADgKAAQgJAAgIgDg");
	this.shape_5.setTransform(31.7,-0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#7BD7F8").s().p("AARBRIgVg8IgJgVIAABRIgiAAIAAiiIAiAAIATA+IAJAVIAAhTIAhAAIAACig");
	this.shape_6.setTransform(20.8,-0.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#7BD7F8").s().p("AgnBRIAAiiIBOAAIAAAhIgnAAIAAAhIAZAAIAAAbIgZAAIAAAkIAnAAIAAAhg");
	this.shape_7.setTransform(10.6,-0.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#7BD7F8").s().p("AgSBSQgIgDgGgFQgGgFgDgIQgEgJAAgKIAAgLIAiAAIAAALQAAAGACAEQADADAGAAQAEAAACgDQADgDAAgHQAAgIgGgIIgOgPIgLgKIgKgKQgGgGgCgHQgEgIAAgIQAAgJAEgIQACgJAHgGQAFgHAJgEQAIgDALAAQAIAAAIADQAJADAFAGQAGAFADAJQACAHAAAJIAAALIggAAIAAgMQAAgFgDgDQgCgDgEAAQgDABgCACQgEADAAAFQAAAFACAEIAEAHIAGAIIAJAIIALAKIAKAKQAFAHADAHQACAHAAAJQAAAKgDAIQgDAIgGAHQgFAGgJADQgJAEgKAAQgJAAgJgDg");
	this.shape_8.setTransform(0.8,-0.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#7BD7F8").s().p("AgnBRIAAiiIBOAAIAAAhIgnAAIAAAhIAZAAIAAAbIgZAAIAAAkIAnAAIAAAhg");
	this.shape_9.setTransform(-8.6,-0.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#7BD7F8").s().p("AgSBSQgIgDgGgFQgGgFgDgIQgDgJgBgKIAAgLIAhAAIAAALQAAAGADAEQAEADAFAAQAEAAACgDQADgDAAgHQAAgIgGgIIgOgPIgLgKIgKgKQgGgGgCgHQgDgIAAgIQAAgJADgIQACgJAHgGQAFgHAJgEQAJgDAKAAQAIAAAJADQAHADAGAGQAFAFADAJQADAHAAAJIAAALIggAAIAAgMQgBgFgCgDQgCgDgEAAQgDABgDACQgDADAAAFQAAAFACAEIAEAHIAGAIIAIAIIAMAKIALAKQAEAHADAHQADAHAAAJQAAAKgEAIQgDAIgGAHQgFAGgJADQgIAEgLAAQgJAAgJgDg");
	this.shape_10.setTransform(-22,-0.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#7BD7F8").s().p("AgmBRIAAiiIBOAAIAAAhIgoAAIAAAhIAZAAIAAAbIgZAAIAAAkIAoAAIAAAhg");
	this.shape_11.setTransform(-31.4,-0.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#7BD7F8").s().p("AgSBRIAAiBIgaAAIAAghIBZAAIAAAhIgbAAIAACBg");
	this.shape_12.setTransform(-40.9,-0.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#7BD7F8").s().p("AgRBRIAAiiIAkAAIAACig");
	this.shape_13.setTransform(-48.3,-0.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#7BD7F8").s().p("AgnBRIAAiiIBOAAIAAAhIgnAAIAAAhIAZAAIAAAbIgZAAIAAAkIAnAAIAAAhg");
	this.shape_14.setTransform(-55.5,-0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#7BD7F8").s().p("AgPBSQgKgDgGgGQgGgGgEgIQgEgJAAgKIAAhPQAAgKAEgIQAEgJAGgGQAGgFAKgEQAIgDAJAAQAKAAAIADQAJADAFAGQAGAFADAIQADAIAAAKIAAAVIghAAIAAgWQAAgEgDgDQgCgDgGAAQgDAAgDADQgCADAAAEIAABVQAAAEACADQADADADAAQAGAAACgDQADgDAAgEIAAgWIAhAAIAAAWQAAAJgDAIQgDAIgGAGQgFAFgJADQgIADgKAAQgJAAgIgDg");
	this.shape_15.setTransform(-65.5,-0.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#7BD7F8").s().p("AAMBRIgDgYIgUAAIgDAYIgjAAIAdiiIAqAAIAcCigAgHAdIANAAIgGg6g");
	this.shape_16.setTransform(-75.6,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-82.6,-14.2,165.2,28.7);


(lib._00horizontal3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgTAFIAAgJIAnAAIAAAJg");
	this.shape.setTransform(451.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EBEWAAFIAAgJIBHAAIAAAJgEBCLAAFIAAgJIBFAAIAAAJgEA/+AAFIAAgJIBHAAIAAAJgEA9zAAFIAAgJIBFAAIAAAJgEA7nAAFIAAgJIBGAAIAAAJgEA5aAAFIAAgJIBGAAIAAAJgEA3PAAFIAAgJIBGAAIAAAJgEA1CAAFIAAgJIBGAAIAAAJgEAy3AAFIAAgJIBGAAIAAAJgEAwqAAFIAAgJIBHAAIAAAJgEAufAAFIAAgJIBFAAIAAAJgEAsSAAFIAAgJIBHAAIAAAJgEAqHAAFIAAgJIBFAAIAAAJgEAn7AAFIAAgJIBGAAIAAAJgEAluAAFIAAgJIBGAAIAAAJgEAjjAAFIAAgJIBGAAIAAAJgEAhWAAFIAAgJIBGAAIAAAJgAfLAFIAAgJIBGAAIAAAJgAc+AFIAAgJIBHAAIAAAJgAazAFIAAgJIBFAAIAAAJgAYmAFIAAgJIBHAAIAAAJgAWbAFIAAgJIBFAAIAAAJgAUPAFIAAgJIBGAAIAAAJgASCAFIAAgJIBGAAIAAAJgAP3AFIAAgJIBGAAIAAAJgANqAFIAAgJIBGAAIAAAJgALfAFIAAgJIBGAAIAAAJgAJSAFIAAgJIBHAAIAAAJgAHHAFIAAgJIBFAAIAAAJgAE6AFIAAgJIBHAAIAAAJgACvAFIAAgJIBFAAIAAAJgAAjAFIAAgJIBGAAIAAAJgAhoAFIAAgJIBGAAIAAAJgAjzAFIAAgJIBGAAIAAAJgAmAAFIAAgJIBGAAIAAAJgAoLAFIAAgJIBGAAIAAAJgAqYAFIAAgJIBGAAIAAAJgAsjAFIAAgJIBFAAIAAAJgAuwAFIAAgJIBHAAIAAAJgAw7AFIAAgJIBFAAIAAAJgAzIAFIAAgJIBHAAIAAAJgA1UAFIAAgJIBGAAIAAAJgA3fAFIAAgJIBGAAIAAAJgA5sAFIAAgJIBGAAIAAAJgA73AFIAAgJIBGAAIAAAJgA+EAFIAAgJIBGAAIAAAJgEggPAAFIAAgJIBFAAIAAAJgEgicAAFIAAgJIBHAAIAAAJgEgknAAFIAAgJIBFAAIAAAJgEgm0AAFIAAgJIBHAAIAAAJgEgpAAAFIAAgJIBGAAIAAAJgEgrLAAFIAAgJIBGAAIAAAJgEgtYAAFIAAgJIBGAAIAAAJgEgvjAAFIAAgJIBGAAIAAAJgEgxwAAFIAAgJIBGAAIAAAJgEgz7AAFIAAgJIBFAAIAAAJgEg2IAAFIAAgJIBHAAIAAAJgEg4TAAFIAAgJIBFAAIAAAJgEg6gAAFIAAgJIBHAAIAAAJgEg8sAAFIAAgJIBGAAIAAAJgEg+3AAFIAAgJIBGAAIAAAJgEhBEAAFIAAgJIBGAAIAAAJgEhDPAAFIAAgJIBGAAIAAAJgEhFcAAFIAAgJIBGAAIAAAJg");
	this.shape_1.setTransform(1,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOAFIAAgJIAcAAIAAAJg");
	this.shape_2.setTransform(-452,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-453.5,-0.5,907,1);


(lib._00horizontal2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgTAFIAAgJIAnAAIAAAJg");
	this.shape.setTransform(451.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EBEWAAFIAAgJIBHAAIAAAJgEBCLAAFIAAgJIBFAAIAAAJgEA/+AAFIAAgJIBHAAIAAAJgEA9zAAFIAAgJIBFAAIAAAJgEA7nAAFIAAgJIBGAAIAAAJgEA5aAAFIAAgJIBGAAIAAAJgEA3PAAFIAAgJIBGAAIAAAJgEA1CAAFIAAgJIBGAAIAAAJgEAy3AAFIAAgJIBGAAIAAAJgEAwqAAFIAAgJIBHAAIAAAJgEAufAAFIAAgJIBFAAIAAAJgEAsSAAFIAAgJIBHAAIAAAJgEAqHAAFIAAgJIBFAAIAAAJgEAn7AAFIAAgJIBGAAIAAAJgEAluAAFIAAgJIBGAAIAAAJgEAjjAAFIAAgJIBGAAIAAAJgEAhWAAFIAAgJIBGAAIAAAJgAfLAFIAAgJIBGAAIAAAJgAc+AFIAAgJIBHAAIAAAJgAazAFIAAgJIBFAAIAAAJgAYmAFIAAgJIBHAAIAAAJgAWbAFIAAgJIBFAAIAAAJgAUPAFIAAgJIBGAAIAAAJgASCAFIAAgJIBGAAIAAAJgAP3AFIAAgJIBGAAIAAAJgANqAFIAAgJIBGAAIAAAJgALfAFIAAgJIBGAAIAAAJgAJSAFIAAgJIBHAAIAAAJgAHHAFIAAgJIBFAAIAAAJgAE6AFIAAgJIBHAAIAAAJgACvAFIAAgJIBFAAIAAAJgAAjAFIAAgJIBGAAIAAAJgAhoAFIAAgJIBGAAIAAAJgAjzAFIAAgJIBGAAIAAAJgAmAAFIAAgJIBGAAIAAAJgAoLAFIAAgJIBGAAIAAAJgAqYAFIAAgJIBGAAIAAAJgAsjAFIAAgJIBFAAIAAAJgAuwAFIAAgJIBHAAIAAAJgAw7AFIAAgJIBFAAIAAAJgAzIAFIAAgJIBHAAIAAAJgA1UAFIAAgJIBGAAIAAAJgA3fAFIAAgJIBGAAIAAAJgA5sAFIAAgJIBGAAIAAAJgA73AFIAAgJIBGAAIAAAJgA+EAFIAAgJIBGAAIAAAJgEggPAAFIAAgJIBFAAIAAAJgEgicAAFIAAgJIBHAAIAAAJgEgknAAFIAAgJIBFAAIAAAJgEgm0AAFIAAgJIBHAAIAAAJgEgpAAAFIAAgJIBGAAIAAAJgEgrLAAFIAAgJIBGAAIAAAJgEgtYAAFIAAgJIBGAAIAAAJgEgvjAAFIAAgJIBGAAIAAAJgEgxwAAFIAAgJIBGAAIAAAJgEgz7AAFIAAgJIBFAAIAAAJgEg2IAAFIAAgJIBHAAIAAAJgEg4TAAFIAAgJIBFAAIAAAJgEg6gAAFIAAgJIBHAAIAAAJgEg8sAAFIAAgJIBGAAIAAAJgEg+3AAFIAAgJIBGAAIAAAJgEhBEAAFIAAgJIBGAAIAAAJgEhDPAAFIAAgJIBGAAIAAAJgEhFcAAFIAAgJIBGAAIAAAJg");
	this.shape_1.setTransform(1,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOAFIAAgJIAcAAIAAAJg");
	this.shape_2.setTransform(-452,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-453.5,-0.5,907,1);


(lib._00horizontal1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhLhAAFIAAgJMCXDAAAIAAAJg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-483.5,-0.5,967,1);


(lib._00circulooscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AhvBxQgvgvAAhCQAAhBAvguQAugvBBAAQBCAAAvAvQAuAuAABBQAABCguAvQgvAuhCAAQhBAAgugug");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16,-16,32,32);


(lib._00circuloclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AhvBxQgvgvAAhCQAAhBAvguQAugvBBAAQBCAAAvAvQAuAuAABBQAABCguAvQgvAuhCAAQhBAAgugug");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16,-16,32,32);


(lib._00circuloazul = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AhvBxQgvgvAAhCQAAhBAvguQAugvBBAAQBCAAAuAvQAvAuAABBQAABCgvAvQguAuhCAAQhBAAgugug");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.9,-16,32,32);


(lib._00250 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgXBwQgLgEgIgIQgIgIgEgLQgEgKAAgNIAAhzQAAgNAEgLQAEgLAIgHQAIgHALgFQALgEAMAAQANAAALAEQAMAFAHAHQAIAHAEALQAEALAAANIAABzQAAANgEAKQgEALgIAIQgHAIgMAEQgLAEgNAAQgMAAgLgEgAgIhFQgEAEABAFIAAB5QgBAFAEADQAEADAEAAQAGAAADgDQADgDAAgFIAAh5QAAgFgDgEQgDgCgGAAQgEAAgEACg");
	this.shape.setTransform(16,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgaBtQgKgFgHgHQgGgIgEgJQgEgJABgKIAAgVIAqAAIAAAQQAAAIAEADQADAEAHAAQAEAAADgEQAEgDAAgGIAAgyQAAgGgEgCQgDgDgEAAQgDAAgEADQgEABABAEIgrgFIAHhxIBmAAQgCAWgFALQgHAKgLAAIgnAAIgDAvQAGgIAHgEQAJgEALAAQAQAAAJALQAGAGACAHQADAHAAAIIAAA7QAAANgFAKQgFAKgHAHQgJAHgKAEQgMAEgLAAQgOAAgLgFg");
	this.shape_1.setTransform(2.4,-0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgSAdQAIAAAGgFQAEgFAAgMIgSAAIAAgtIAkAAIAAApQAAAHgDAHQgCAGgFAGQgFAFgFADQgHADgJAAg");
	this.shape_2.setTransform(-7,9.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag5ByIAAgMQAAgNACgLQACgMAFgLQAFgMAJgNIAWgdIAMgQIAIgOIAFgNIABgMQAAgJgEgDQgDgEgHAAQgEAAgEAEQgDAEAAAIIAAAVIgrAAIAAgaQAAgKAEgKQADgJAHgIQAHgHAKgFQAKgFANAAQAPAAAMAFQALAFAHAIQAHAIADALQADALAAAMIgCATQgBAJgFAKQgEAJgIAJQgIAKgMAMIgMAQIgGAMIgDAKIgBAIIA9AAIAAArg");
	this.shape_3.setTransform(-16.5,-1.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.1,-18.7,50.3,37.6);


(lib._00150 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgXBwQgLgEgIgIQgIgIgEgLQgEgKAAgNIAAhzQAAgNAEgLQAEgLAIgHQAIgHALgFQAKgEANAAQANAAAMAEQAKAFAIAHQAIAHAEALQAEALAAANIAABzQAAANgEAKQgEALgIAIQgIAIgKAEQgMAEgNAAQgNAAgKgEgAgIhFQgEAEAAAFIAAB5QAAAFAEADQADADAFAAQAGAAADgDQADgDAAgFIAAh5QAAgFgDgEQgDgCgGAAQgFAAgDACg");
	this.shape.setTransform(13.8,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgZBtQgLgFgGgHQgIgIgDgJQgDgJgBgKIAAgVIArAAIAAAQQABAIADADQADAEAHAAQAEAAAEgEQADgDAAgGIAAgyQAAgGgDgCQgFgDgDAAQgDAAgEADQgEABAAAEIgpgFIAFhxIBnAAQgBAWgHALQgFAKgNAAIgmAAIgEAvQAHgIAHgEQAIgEALAAQAQAAAKALQAFAGADAHQADAHgBAIIAAA7QAAANgEAKQgEAKgJAHQgHAHgMAEQgLAEgMAAQgNAAgKgFg");
	this.shape_1.setTransform(0.2,-0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgRAdQAHAAAFgFQAFgFAAgMIgRAAIAAgtIAkAAIAAApQAAAHgDAHQgDAGgFAGQgFAFgFADQgHADgIAAg");
	this.shape_2.setTransform(-9.2,9.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgWBvIAAjIQALAAAFgGQAFgHAAgIIAYAAIAADdg");
	this.shape_3.setTransform(-16.4,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.8,-18.7,45.8,37.6);


// stage content:
(lib.grafica_17_v1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_139 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(139).call(this.frame_139).wait(1));

	// 00 mascara (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_105 = new cjs.Graphics().p("EhGTAoTMAAAhP+MCXFAAAMAAABP+g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(105).to({graphics:mask_graphics_105,x:517,y:258}).wait(35));

	// 03 claro
	this.instance = new lib._03claro();
	this.instance.setTransform(948.9,673.7);
	this.instance._off = true;

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(105).to({_off:false},0).wait(1).to({y:673.2},0).wait(1).to({y:671.9},0).wait(1).to({y:669.5},0).wait(1).to({y:666},0).wait(1).to({y:661.3},0).wait(1).to({y:655.3},0).wait(1).to({y:647.7},0).wait(1).to({y:638.6},0).wait(1).to({y:627.6},0).wait(1).to({y:614.8},0).wait(1).to({y:599.9},0).wait(1).to({y:583.1},0).wait(1).to({y:564.4},0).wait(1).to({y:544.1},0).wait(1).to({y:522.9},0).wait(1).to({y:501.3},0).wait(1).to({y:480},0).wait(1).to({y:459.9},0).wait(1).to({y:441.4},0).wait(1).to({y:424.8},0).wait(1).to({y:410.4},0).wait(1).to({y:398},0).wait(1).to({y:387.5},0).wait(1).to({y:379},0).wait(1).to({y:372.1},0).wait(1).to({y:366.8},0).wait(1).to({y:362.9},0).wait(1).to({y:360.2},0).wait(1).to({y:358.7},0).wait(1).to({y:358.3},0).wait(5));

	// 00 mascara (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_103 = new cjs.Graphics().p("EhGTAoTMAAAhP+MCXFAAAMAAABP+g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(103).to({graphics:mask_1_graphics_103,x:517,y:258}).wait(37));

	// 03 oscuro
	this.instance_1 = new lib._03oscuro();
	this.instance_1.setTransform(880.9,632.1);
	this.instance_1._off = true;

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(103).to({_off:false},0).wait(1).to({y:631.7},0).wait(1).to({y:630.7},0).wait(1).to({y:629},0).wait(1).to({y:626.4},0).wait(1).to({y:623},0).wait(1).to({y:618.5},0).wait(1).to({y:613},0).wait(1).to({y:606.2},0).wait(1).to({y:598.2},0).wait(1).to({y:588.7},0).wait(1).to({y:577.8},0).wait(1).to({y:565.4},0).wait(1).to({y:551.6},0).wait(1).to({y:536.7},0).wait(1).to({y:521.1},0).wait(1).to({y:505.1},0).wait(1).to({y:489.5},0).wait(1).to({y:474.7},0).wait(1).to({y:461.1},0).wait(1).to({y:448.9},0).wait(1).to({y:438.2},0).wait(1).to({y:429.1},0).wait(1).to({y:421.4},0).wait(1).to({y:415.1},0).wait(1).to({y:410.1},0).wait(1).to({y:406.2},0).wait(1).to({y:403.3},0).wait(1).to({y:401.3},0).wait(1).to({y:400.2},0).wait(1).to({y:399.9},0).wait(7));

	// 00 mascara (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_101 = new cjs.Graphics().p("EhGTAoTMAAAhP+MCXFAAAMAAABP+g");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(101).to({graphics:mask_2_graphics_101,x:517,y:258}).wait(39));

	// 03 azul
	this.instance_2 = new lib._03azul();
	this.instance_2.setTransform(813.9,632.1);
	this.instance_2._off = true;

	this.instance_2.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(101).to({_off:false},0).wait(1).to({y:631.7},0).wait(1).to({y:630.7},0).wait(1).to({y:629},0).wait(1).to({y:626.4},0).wait(1).to({y:623},0).wait(1).to({y:618.5},0).wait(1).to({y:613},0).wait(1).to({y:606.2},0).wait(1).to({y:598.2},0).wait(1).to({y:588.7},0).wait(1).to({y:577.8},0).wait(1).to({y:565.4},0).wait(1).to({y:551.6},0).wait(1).to({y:536.7},0).wait(1).to({y:521.1},0).wait(1).to({y:505.1},0).wait(1).to({y:489.5},0).wait(1).to({y:474.7},0).wait(1).to({y:461.1},0).wait(1).to({y:448.9},0).wait(1).to({y:438.2},0).wait(1).to({y:429.1},0).wait(1).to({y:421.4},0).wait(1).to({y:415.1},0).wait(1).to({y:410.1},0).wait(1).to({y:406.2},0).wait(1).to({y:403.3},0).wait(1).to({y:401.3},0).wait(1).to({y:400.2},0).wait(1).to({y:399.9},0).wait(9));

	// 03 6 meses
	this.instance_3 = new lib._036meses();
	this.instance_3.setTransform(881.4,609.1);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(99).to({_off:false},0).wait(1).to({regX:0.2,regY:-1,x:881.6,y:608,alpha:0.001},0).wait(1).to({y:607.8,alpha:0.006},0).wait(1).to({y:607.4,alpha:0.013},0).wait(1).to({y:606.9,alpha:0.024},0).wait(1).to({y:606.2,alpha:0.039},0).wait(1).to({y:605.2,alpha:0.059},0).wait(1).to({y:604.1,alpha:0.083},0).wait(1).to({y:602.7,alpha:0.112},0).wait(1).to({y:601,alpha:0.148},0).wait(1).to({y:599,alpha:0.189},0).wait(1).to({y:596.7,alpha:0.236},0).wait(1).to({y:594.1,alpha:0.29},0).wait(1).to({y:591.3,alpha:0.349},0).wait(1).to({y:588.2,alpha:0.413},0).wait(1).to({y:585,alpha:0.481},0).wait(1).to({y:581.7,alpha:0.549},0).wait(1).to({y:578.5,alpha:0.616},0).wait(1).to({y:575.4,alpha:0.679},0).wait(1).to({y:572.6,alpha:0.738},0).wait(1).to({y:570.1,alpha:0.79},0).wait(1).to({y:568,alpha:0.835},0).wait(1).to({y:566.1,alpha:0.875},0).wait(1).to({y:564.5,alpha:0.907},0).wait(1).to({y:563.2,alpha:0.934},0).wait(1).to({y:562.2,alpha:0.956},0).wait(1).to({y:561.4,alpha:0.973},0).wait(1).to({y:560.8,alpha:0.985},0).wait(1).to({y:560.4,alpha:0.994},0).wait(1).to({y:560.1,alpha:0.998},0).wait(1).to({regX:0,regY:0,x:881.4,y:561.1,alpha:1},0).wait(11));

	// 00 mascara (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	var mask_3_graphics_75 = new cjs.Graphics().p("EhGTAoTMAAAhP+MCXFAAAMAAABP+g");

	this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(75).to({graphics:mask_3_graphics_75,x:517,y:258}).wait(65));

	// 02 claro
	this.instance_4 = new lib._02claro();
	this.instance_4.setTransform(652.5,668.9);
	this.instance_4._off = true;

	this.instance_4.mask = mask_3;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(75).to({_off:false},0).wait(1).to({y:668.5},0).wait(1).to({y:667},0).wait(1).to({y:664.6},0).wait(1).to({y:661},0).wait(1).to({y:656.2},0).wait(1).to({y:650},0).wait(1).to({y:642.3},0).wait(1).to({y:633.1},0).wait(1).to({y:622.1},0).wait(1).to({y:609.4},0).wait(1).to({y:594.8},0).wait(1).to({y:578.4},0).wait(1).to({y:560.5},0).wait(1).to({y:541.3},0).wait(1).to({y:521.3},0).wait(1).to({y:501},0).wait(1).to({y:481.1},0).wait(1).to({y:462.1},0).wait(1).to({y:444.6},0).wait(1).to({y:428.8},0).wait(1).to({y:414.8},0).wait(1).to({y:402.7},0).wait(1).to({y:392.4},0).wait(1).to({y:383.9},0).wait(1).to({y:377.1},0).wait(1).to({y:371.7},0).wait(1).to({y:367.8},0).wait(1).to({y:365.1},0).wait(1).to({y:363.5},0).wait(1).to({y:363.1},0).wait(35));

	// 00 mascara (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	var mask_4_graphics_73 = new cjs.Graphics().p("EhGTAoTMAAAhP+MCXFAAAMAAABP+g");

	this.timeline.addTween(cjs.Tween.get(mask_4).to({graphics:null,x:0,y:0}).wait(73).to({graphics:mask_4_graphics_73,x:517,y:258}).wait(67));

	// 02 oscuro
	this.instance_5 = new lib._02oscuro();
	this.instance_5.setTransform(584.5,625);
	this.instance_5._off = true;

	this.instance_5.mask = mask_4;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(73).to({_off:false},0).wait(1).to({y:624.7},0).wait(1).to({y:623.7},0).wait(1).to({y:621.9},0).wait(1).to({y:619.4},0).wait(1).to({y:615.9},0).wait(1).to({y:611.5},0).wait(1).to({y:606.1},0).wait(1).to({y:599.5},0).wait(1).to({y:591.6},0).wait(1).to({y:582.5},0).wait(1).to({y:572.2},0).wait(1).to({y:560.5},0).wait(1).to({y:547.7},0).wait(1).to({y:534},0).wait(1).to({y:519.7},0).wait(1).to({y:505.3},0).wait(1).to({y:491.1},0).wait(1).to({y:477.6},0).wait(1).to({y:465.1},0).wait(1).to({y:453.8},0).wait(1).to({y:443.8},0).wait(1).to({y:435.2},0).wait(1).to({y:427.9},0).wait(1).to({y:421.8},0).wait(1).to({y:417},0).wait(1).to({y:413.1},0).wait(1).to({y:410.3},0).wait(1).to({y:408.4},0).wait(1).to({y:407.3},0).wait(1).to({y:407},0).wait(37));

	// 00 mascara (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	var mask_5_graphics_71 = new cjs.Graphics().p("EhGTAoTMAAAhP+MCXFAAAMAAABP+g");

	this.timeline.addTween(cjs.Tween.get(mask_5).to({graphics:null,x:0,y:0}).wait(71).to({graphics:mask_5_graphics_71,x:517,y:258}).wait(69));

	// 02 azul
	this.instance_6 = new lib._02azul();
	this.instance_6.setTransform(517.5,648.9);
	this.instance_6._off = true;

	this.instance_6.mask = mask_5;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(71).to({_off:false},0).wait(1).to({y:648.5},0).wait(1).to({y:647.2},0).wait(1).to({y:645.1},0).wait(1).to({y:642},0).wait(1).to({y:637.8},0).wait(1).to({y:632.4},0).wait(1).to({y:625.8},0).wait(1).to({y:617.7},0).wait(1).to({y:608.2},0).wait(1).to({y:597.1},0).wait(1).to({y:584.5},0).wait(1).to({y:570.3},0).wait(1).to({y:554.7},0).wait(1).to({y:538},0).wait(1).to({y:520.6},0).wait(1).to({y:503},0).wait(1).to({y:485.7},0).wait(1).to({y:469.2},0).wait(1).to({y:454},0).wait(1).to({y:440.2},0).wait(1).to({y:428.1},0).wait(1).to({y:417.6},0).wait(1).to({y:408.7},0).wait(1).to({y:401.3},0).wait(1).to({y:395.3},0).wait(1).to({y:390.7},0).wait(1).to({y:387.3},0).wait(1).to({y:384.9},0).wait(1).to({y:383.6},0).wait(1).to({y:383.2},0).wait(39));

	// 02 3 meses
	this.instance_7 = new lib._023meses();
	this.instance_7.setTransform(585,609.1);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(69).to({_off:false},0).wait(1).to({regX:0.1,regY:-1,x:585.1,y:608,alpha:0.001},0).wait(1).to({y:607.8,alpha:0.006},0).wait(1).to({y:607.4,alpha:0.013},0).wait(1).to({y:606.9,alpha:0.024},0).wait(1).to({y:606.2,alpha:0.039},0).wait(1).to({y:605.2,alpha:0.059},0).wait(1).to({y:604.1,alpha:0.083},0).wait(1).to({y:602.7,alpha:0.112},0).wait(1).to({y:601,alpha:0.148},0).wait(1).to({y:599,alpha:0.189},0).wait(1).to({y:596.7,alpha:0.236},0).wait(1).to({y:594.1,alpha:0.29},0).wait(1).to({y:591.3,alpha:0.349},0).wait(1).to({y:588.2,alpha:0.413},0).wait(1).to({y:585,alpha:0.481},0).wait(1).to({y:581.7,alpha:0.549},0).wait(1).to({y:578.5,alpha:0.616},0).wait(1).to({y:575.4,alpha:0.679},0).wait(1).to({y:572.6,alpha:0.738},0).wait(1).to({y:570.1,alpha:0.79},0).wait(1).to({y:568,alpha:0.835},0).wait(1).to({y:566.1,alpha:0.875},0).wait(1).to({y:564.5,alpha:0.907},0).wait(1).to({y:563.2,alpha:0.934},0).wait(1).to({y:562.2,alpha:0.956},0).wait(1).to({y:561.4,alpha:0.973},0).wait(1).to({y:560.8,alpha:0.985},0).wait(1).to({y:560.4,alpha:0.994},0).wait(1).to({y:560.1,alpha:0.998},0).wait(1).to({regX:0,regY:0,x:585,y:561.1,alpha:1},0).wait(41));

	// 00 mascara (mask)
	var mask_6 = new cjs.Shape();
	mask_6._off = true;
	var mask_6_graphics_45 = new cjs.Graphics().p("EhGTAoTMAAAhP+MCXFAAAMAAABP+g");

	this.timeline.addTween(cjs.Tween.get(mask_6).to({graphics:null,x:0,y:0}).wait(45).to({graphics:mask_6_graphics_45,x:517,y:258}).wait(95));

	// 01 claro
	this.instance_8 = new lib._01claro();
	this.instance_8.setTransform(356.1,680.8);
	this.instance_8._off = true;

	this.instance_8.mask = mask_6;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(45).to({_off:false},0).wait(1).to({y:680.3},0).wait(1).to({y:678.9},0).wait(1).to({y:676.4},0).wait(1).to({y:672.7},0).wait(1).to({y:667.8},0).wait(1).to({y:661.4},0).wait(1).to({y:653.4},0).wait(1).to({y:643.7},0).wait(1).to({y:632.2},0).wait(1).to({y:618.6},0).wait(1).to({y:603},0).wait(1).to({y:585.3},0).wait(1).to({y:565.7},0).wait(1).to({y:544.6},0).wait(1).to({y:522.4},0).wait(1).to({y:499.9},0).wait(1).to({y:477.8},0).wait(1).to({y:456.9},0).wait(1).to({y:437.7},0).wait(1).to({y:420.5},0).wait(1).to({y:405.4},0).wait(1).to({y:392.6},0).wait(1).to({y:381.7},0).wait(1).to({y:372.8},0).wait(1).to({y:365.7},0).wait(1).to({y:360.1},0).wait(1).to({y:356.1},0).wait(1).to({y:353.3},0).wait(1).to({y:351.7},0).wait(1).to({y:351.2},0).wait(65));

	// 00 mascara (mask)
	var mask_7 = new cjs.Shape();
	mask_7._off = true;
	var mask_7_graphics_43 = new cjs.Graphics().p("EhGTAoTMAAAhP+MCXFAAAMAAABP+g");

	this.timeline.addTween(cjs.Tween.get(mask_7).to({graphics:null,x:0,y:0}).wait(43).to({graphics:mask_7_graphics_43,x:517,y:258}).wait(97));

	// 01 oscuro
	this.instance_9 = new lib._01oscuro();
	this.instance_9.setTransform(288.1,729.3);
	this.instance_9._off = true;

	this.instance_9.mask = mask_7;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(43).to({_off:false},0).wait(1).to({y:728.7},0).wait(1).to({y:726.8},0).wait(1).to({y:723.6},0).wait(1).to({y:718.8},0).wait(1).to({y:712.4},0).wait(1).to({y:704.1},0).wait(1).to({y:693.8},0).wait(1).to({y:681.3},0).wait(1).to({y:666.3},0).wait(1).to({y:648.8},0).wait(1).to({y:628.6},0).wait(1).to({y:605.7},0).wait(1).to({y:580.4},0).wait(1).to({y:553},0).wait(1).to({y:524.3},0).wait(1).to({y:495.1},0).wait(1).to({y:466.6},0).wait(1).to({y:439.5},0).wait(1).to({y:414.7},0).wait(1).to({y:392.4},0).wait(1).to({y:372.9},0).wait(1).to({y:356.3},0).wait(1).to({y:342.3},0).wait(1).to({y:330.7},0).wait(1).to({y:321.5},0).wait(1).to({y:314.3},0).wait(1).to({y:309},0).wait(1).to({y:305.4},0).wait(1).to({y:303.4},0).wait(1).to({y:302.8},0).wait(67));

	// 00 mascara (mask)
	var mask_8 = new cjs.Shape();
	mask_8._off = true;
	var mask_8_graphics_41 = new cjs.Graphics().p("EhGTAoTMAAAhP+MCXFAAAMAAABP+g");

	this.timeline.addTween(cjs.Tween.get(mask_8).to({graphics:null,x:0,y:0}).wait(41).to({graphics:mask_8_graphics_41,x:517,y:258}).wait(99));

	// 01 azul
	this.instance_10 = new lib._01azul();
	this.instance_10.setTransform(221.1,698.8);
	this.instance_10._off = true;

	this.instance_10.mask = mask_8;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(41).to({_off:false},0).wait(1).to({y:698.3},0).wait(1).to({y:696.7},0).wait(1).to({y:693.9},0).wait(1).to({y:689.9},0).wait(1).to({y:684.4},0).wait(1).to({y:677.3},0).wait(1).to({y:668.4},0).wait(1).to({y:657.7},0).wait(1).to({y:644.9},0).wait(1).to({y:629.8},0).wait(1).to({y:612.5},0).wait(1).to({y:592.9},0).wait(1).to({y:571.2},0).wait(1).to({y:547.7},0).wait(1).to({y:523.1},0).wait(1).to({y:498.1},0).wait(1).to({y:473.6},0).wait(1).to({y:450.4},0).wait(1).to({y:429.1},0).wait(1).to({y:410.1},0).wait(1).to({y:393.4},0).wait(1).to({y:379.1},0).wait(1).to({y:367.1},0).wait(1).to({y:357.2},0).wait(1).to({y:349.3},0).wait(1).to({y:343.1},0).wait(1).to({y:338.6},0).wait(1).to({y:335.5},0).wait(1).to({y:333.8},0).wait(1).to({y:333.2},0).wait(69));

	// 01 BASELINE
	this.instance_11 = new lib._01BASELINE();
	this.instance_11.setTransform(288.6,609.1);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(39).to({_off:false},0).wait(1).to({regX:0.1,regY:-1,x:288.7,y:608,alpha:0.001},0).wait(1).to({y:607.8,alpha:0.006},0).wait(1).to({y:607.4,alpha:0.013},0).wait(1).to({y:606.9,alpha:0.024},0).wait(1).to({y:606.2,alpha:0.039},0).wait(1).to({y:605.2,alpha:0.059},0).wait(1).to({y:604.1,alpha:0.083},0).wait(1).to({y:602.7,alpha:0.112},0).wait(1).to({y:601,alpha:0.148},0).wait(1).to({y:599,alpha:0.189},0).wait(1).to({y:596.7,alpha:0.236},0).wait(1).to({y:594.1,alpha:0.29},0).wait(1).to({y:591.3,alpha:0.349},0).wait(1).to({y:588.2,alpha:0.413},0).wait(1).to({y:585,alpha:0.481},0).wait(1).to({y:581.7,alpha:0.549},0).wait(1).to({y:578.5,alpha:0.616},0).wait(1).to({y:575.4,alpha:0.679},0).wait(1).to({y:572.6,alpha:0.738},0).wait(1).to({y:570.1,alpha:0.79},0).wait(1).to({y:568,alpha:0.835},0).wait(1).to({y:566.1,alpha:0.875},0).wait(1).to({y:564.5,alpha:0.907},0).wait(1).to({y:563.2,alpha:0.934},0).wait(1).to({y:562.2,alpha:0.956},0).wait(1).to({y:561.4,alpha:0.973},0).wait(1).to({y:560.8,alpha:0.985},0).wait(1).to({y:560.4,alpha:0.994},0).wait(1).to({y:560.1,alpha:0.998},0).wait(1).to({regX:0,regY:0,x:288.6,y:561.1,alpha:1},0).wait(71));

	// 00 mascara (mask)
	var mask_9 = new cjs.Shape();
	mask_9._off = true;
	var mask_9_graphics_9 = new cjs.Graphics().p("EhGTAsOMAAAhP/MCXFAAAMAAABP/g");

	this.timeline.addTween(cjs.Tween.get(mask_9).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_9_graphics_9,x:517,y:283}).wait(131));

	// 00 horizontal 3
	this.instance_12 = new lib._00horizontal3();
	this.instance_12.setTransform(-386.5,186.5);
	this.instance_12._off = true;

	this.instance_12.mask = mask_9;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(9).to({_off:false},0).wait(1).to({x:-385.1},0).wait(1).to({x:-380.6},0).wait(1).to({x:-372.8},0).wait(1).to({x:-361.3},0).wait(1).to({x:-345.8},0).wait(1).to({x:-325.8},0).wait(1).to({x:-300.9},0).wait(1).to({x:-270.7},0).wait(1).to({x:-234.7},0).wait(1).to({x:-192.6},0).wait(1).to({x:-144.3},0).wait(1).to({x:-89.8},0).wait(1).to({x:-29.8},0).wait(1).to({x:34.6},0).wait(1).to({x:101.6},0).wait(1).to({x:168.9},0).wait(1).to({x:234.2},0).wait(1).to({x:295.4},0).wait(1).to({x:351.1},0).wait(1).to({x:400.4},0).wait(1).to({x:442.9},0).wait(1).to({x:478.8},0).wait(1).to({x:508.5},0).wait(1).to({x:532.3},0).wait(1).to({x:550.7},0).wait(1).to({x:564.3},0).wait(1).to({x:573.6},0).wait(1).to({x:578.8},0).wait(1).to({x:580.5},0).wait(102));

	// 00 mascara (mask)
	var mask_10 = new cjs.Shape();
	mask_10._off = true;
	var mask_10_graphics_7 = new cjs.Graphics().p("EhGTAsOMAAAhP/MCXFAAAMAAABP/g");

	this.timeline.addTween(cjs.Tween.get(mask_10).to({graphics:null,x:0,y:0}).wait(7).to({graphics:mask_10_graphics_7,x:517,y:283}).wait(133));

	// 00 horizontal 2
	this.instance_13 = new lib._00horizontal2();
	this.instance_13.setTransform(-386.5,340.5);
	this.instance_13._off = true;

	this.instance_13.mask = mask_10;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(7).to({_off:false},0).wait(1).to({x:-385.1},0).wait(1).to({x:-380.6},0).wait(1).to({x:-372.8},0).wait(1).to({x:-361.3},0).wait(1).to({x:-345.8},0).wait(1).to({x:-325.8},0).wait(1).to({x:-300.9},0).wait(1).to({x:-270.7},0).wait(1).to({x:-234.7},0).wait(1).to({x:-192.6},0).wait(1).to({x:-144.3},0).wait(1).to({x:-89.8},0).wait(1).to({x:-29.8},0).wait(1).to({x:34.6},0).wait(1).to({x:101.6},0).wait(1).to({x:168.9},0).wait(1).to({x:234.2},0).wait(1).to({x:295.4},0).wait(1).to({x:351.1},0).wait(1).to({x:400.4},0).wait(1).to({x:442.9},0).wait(1).to({x:478.8},0).wait(1).to({x:508.5},0).wait(1).to({x:532.3},0).wait(1).to({x:550.7},0).wait(1).to({x:564.3},0).wait(1).to({x:573.6},0).wait(1).to({x:578.8},0).wait(1).to({x:580.5},0).wait(104));

	// 00 mascara (mask)
	var mask_11 = new cjs.Shape();
	mask_11._off = true;
	var mask_11_graphics_5 = new cjs.Graphics().p("EhGTAsOMAAAhP/MCXFAAAMAAABP/g");

	this.timeline.addTween(cjs.Tween.get(mask_11).to({graphics:null,x:0,y:0}).wait(5).to({graphics:mask_11_graphics_5,x:517,y:283}).wait(135));

	// 00 horizontal 1
	this.instance_14 = new lib._00horizontal1();
	this.instance_14.setTransform(-416.5,516.5);
	this.instance_14._off = true;

	this.instance_14.mask = mask_11;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(5).to({_off:false},0).wait(1).to({x:-415.1},0).wait(1).to({x:-410.6},0).wait(1).to({x:-402.8},0).wait(1).to({x:-391.3},0).wait(1).to({x:-375.8},0).wait(1).to({x:-355.8},0).wait(1).to({x:-330.9},0).wait(1).to({x:-300.7},0).wait(1).to({x:-264.7},0).wait(1).to({x:-222.6},0).wait(1).to({x:-174.3},0).wait(1).to({x:-119.8},0).wait(1).to({x:-59.8},0).wait(1).to({x:4.6},0).wait(1).to({x:71.6},0).wait(1).to({x:138.9},0).wait(1).to({x:204.2},0).wait(1).to({x:265.4},0).wait(1).to({x:321.1},0).wait(1).to({x:370.4},0).wait(1).to({x:412.9},0).wait(1).to({x:448.8},0).wait(1).to({x:478.5},0).wait(1).to({x:502.3},0).wait(1).to({x:520.7},0).wait(1).to({x:534.3},0).wait(1).to({x:543.6},0).wait(1).to({x:548.8},0).wait(1).to({x:550.5},0).wait(106));

	// 00 mascara (mask)
	var mask_12 = new cjs.Shape();
	mask_12._off = true;
	var mask_12_graphics_5 = new cjs.Graphics().p("EhGTAsOMAAAhP/MCXFAAAMAAABP/g");

	this.timeline.addTween(cjs.Tween.get(mask_12).to({graphics:null,x:0,y:0}).wait(5).to({graphics:mask_12_graphics_5,x:517,y:283}).wait(135));

	// 00 vertical
	this.instance_15 = new lib._00vertical();
	this.instance_15.setTransform(136.5,822);
	this.instance_15._off = true;

	this.instance_15.mask = mask_12;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(5).to({_off:false},0).wait(1).to({y:821.2},0).wait(1).to({y:818.9},0).wait(1).to({y:814.7},0).wait(1).to({y:808.7},0).wait(1).to({y:800.4},0).wait(1).to({y:789.9},0).wait(1).to({y:776.7},0).wait(1).to({y:760.7},0).wait(1).to({y:741.6},0).wait(1).to({y:719.4},0).wait(1).to({y:693.8},0).wait(1).to({y:664.9},0).wait(1).to({y:633.1},0).wait(1).to({y:599},0).wait(1).to({y:563.6},0).wait(1).to({y:527.9},0).wait(1).to({y:493.4},0).wait(1).to({y:460.9},0).wait(1).to({y:431.5},0).wait(1).to({y:405.4},0).wait(1).to({y:382.9},0).wait(1).to({y:363.8},0).wait(1).to({y:348.1},0).wait(1).to({y:335.5},0).wait(1).to({y:325.8},0).wait(1).to({y:318.6},0).wait(1).to({y:313.7},0).wait(1).to({y:310.9},0).wait(1).to({y:310},0).wait(106));

	// 00 2,50
	this.instance_16 = new lib._00250();
	this.instance_16.setTransform(89.6,185.1,0.029,0.029);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(24).to({_off:false},0).wait(1).to({regX:-0.2,regY:0.4,scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.06,scaleY:0.06,y:185.2},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.13,scaleY:0.13,x:89.5},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.26,scaleY:0.26},0).wait(1).to({scaleX:0.34,scaleY:0.34,y:185.3},0).wait(1).to({scaleX:0.44,scaleY:0.44},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.64,scaleY:0.64,x:89.4,y:185.4},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.82,scaleY:0.82,y:185.5},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:89.6,y:185.1},0).wait(97));

	// 00 1,50
	this.instance_17 = new lib._00150();
	this.instance_17.setTransform(87.4,341.6,0.032,0.032);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(22).to({_off:false},0).wait(1).to({regX:0.5,regY:0.4,scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.07,scaleY:0.07,y:341.7},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.19,scaleY:0.19,x:87.5},0).wait(1).to({scaleX:0.26,scaleY:0.26},0).wait(1).to({scaleX:0.34,scaleY:0.34,y:341.8},0).wait(1).to({scaleX:0.44,scaleY:0.44,x:87.6},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.64,scaleY:0.64,x:87.7,y:341.9},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.82,scaleY:0.82,x:87.8,y:342},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96,x:87.9},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:87.3,y:341.6},0).wait(99));

	// 00 texto claro
	this.instance_18 = new lib._00textoclaro();
	this.instance_18.setTransform(851.6,677.1,1,1,0,0,0,0.1,0.1);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(14).to({_off:false},0).wait(1).to({regX:0.2,regY:-0.7,x:851.7,y:676.2,alpha:0.001},0).wait(1).to({y:676,alpha:0.006},0).wait(1).to({y:675.6,alpha:0.014},0).wait(1).to({y:675.1,alpha:0.026},0).wait(1).to({y:674.3,alpha:0.042},0).wait(1).to({y:673.4,alpha:0.063},0).wait(1).to({y:672.2,alpha:0.089},0).wait(1).to({y:670.7,alpha:0.12},0).wait(1).to({y:669,alpha:0.158},0).wait(1).to({y:666.9,alpha:0.202},0).wait(1).to({y:664.6,alpha:0.252},0).wait(1).to({y:662,alpha:0.309},0).wait(1).to({y:659,alpha:0.372},0).wait(1).to({y:655.9,alpha:0.439},0).wait(1).to({y:652.7,alpha:0.509},0).wait(1).to({y:649.5,alpha:0.579},0).wait(1).to({y:646.3,alpha:0.646},0).wait(1).to({y:643.4,alpha:0.709},0).wait(1).to({y:640.7,alpha:0.766},0).wait(1).to({y:638.4,alpha:0.817},0).wait(1).to({y:636.4,alpha:0.86},0).wait(1).to({y:634.7,alpha:0.897},0).wait(1).to({y:633.3,alpha:0.927},0).wait(1).to({y:632.2,alpha:0.951},0).wait(1).to({y:631.3,alpha:0.97},0).wait(1).to({y:630.7,alpha:0.984},0).wait(1).to({y:630.2,alpha:0.993},0).wait(1).to({y:630,alpha:0.998},0).wait(1).to({regX:0.1,regY:0.1,x:851.6,y:630.7,alpha:1},0).wait(97));

	// 00 circulo claro
	this.instance_19 = new lib._00circuloclaro();
	this.instance_19.setTransform(786.2,630,0.038,0.038);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(12).to({_off:false},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.12,scaleY:0.12},0).wait(1).to({scaleX:0.15,scaleY:0.15},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.23,scaleY:0.23},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.4,scaleY:0.4},0).wait(1).to({scaleX:0.46,scaleY:0.46},0).wait(1).to({scaleX:0.53,scaleY:0.53},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.66,scaleY:0.66},0).wait(1).to({scaleX:0.72,scaleY:0.72},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.82,scaleY:0.82},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(99));

	// 00 texto oscuro
	this.instance_20 = new lib._00textooscuro();
	this.instance_20.setTransform(669.4,677.1,1,1,0,0,0,-0.1,0.1);
	this.instance_20.alpha = 0;
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(8).to({_off:false},0).wait(1).to({regX:-0.2,regY:-0.7,x:669.3,y:676.2,alpha:0.001},0).wait(1).to({y:676,alpha:0.006},0).wait(1).to({y:675.6,alpha:0.014},0).wait(1).to({y:675.1,alpha:0.026},0).wait(1).to({y:674.3,alpha:0.042},0).wait(1).to({y:673.4,alpha:0.063},0).wait(1).to({y:672.2,alpha:0.089},0).wait(1).to({y:670.7,alpha:0.12},0).wait(1).to({y:669,alpha:0.158},0).wait(1).to({y:666.9,alpha:0.202},0).wait(1).to({y:664.6,alpha:0.252},0).wait(1).to({y:662,alpha:0.309},0).wait(1).to({y:659,alpha:0.372},0).wait(1).to({y:655.9,alpha:0.439},0).wait(1).to({y:652.7,alpha:0.509},0).wait(1).to({y:649.5,alpha:0.579},0).wait(1).to({y:646.3,alpha:0.646},0).wait(1).to({y:643.4,alpha:0.709},0).wait(1).to({y:640.7,alpha:0.766},0).wait(1).to({y:638.4,alpha:0.817},0).wait(1).to({y:636.4,alpha:0.86},0).wait(1).to({y:634.7,alpha:0.897},0).wait(1).to({y:633.3,alpha:0.927},0).wait(1).to({y:632.2,alpha:0.951},0).wait(1).to({y:631.3,alpha:0.97},0).wait(1).to({y:630.7,alpha:0.984},0).wait(1).to({y:630.2,alpha:0.993},0).wait(1).to({y:630,alpha:0.998},0).wait(1).to({regX:-0.1,regY:0.1,x:669.4,y:630.7,alpha:1},0).wait(103));

	// 00 circulo oscuro
	this.instance_21 = new lib._00circulooscuro();
	this.instance_21.setTransform(556.8,630,0.038,0.038);
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(6).to({_off:false},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.12,scaleY:0.12},0).wait(1).to({scaleX:0.15,scaleY:0.15},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.23,scaleY:0.23},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.4,scaleY:0.4},0).wait(1).to({scaleX:0.46,scaleY:0.46},0).wait(1).to({scaleX:0.53,scaleY:0.53},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.66,scaleY:0.66},0).wait(1).to({scaleX:0.72,scaleY:0.72},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.82,scaleY:0.82},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1,x:556.9},0).wait(105));

	// 00 texto azul
	this.instance_22 = new lib._00textoazul();
	this.instance_22.setTransform(434.3,677.1,1,1,0,0,0,-0.1,0.1);
	this.instance_22.alpha = 0;
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(2).to({_off:false},0).wait(1).to({regX:-0.4,regY:-0.7,x:434,y:676.2,alpha:0.001},0).wait(1).to({y:676,alpha:0.006},0).wait(1).to({y:675.6,alpha:0.014},0).wait(1).to({y:675.1,alpha:0.026},0).wait(1).to({y:674.3,alpha:0.042},0).wait(1).to({y:673.4,alpha:0.063},0).wait(1).to({y:672.2,alpha:0.089},0).wait(1).to({y:670.7,alpha:0.12},0).wait(1).to({y:669,alpha:0.158},0).wait(1).to({y:666.9,alpha:0.202},0).wait(1).to({y:664.6,alpha:0.252},0).wait(1).to({y:662,alpha:0.309},0).wait(1).to({y:659,alpha:0.372},0).wait(1).to({y:655.9,alpha:0.439},0).wait(1).to({y:652.7,alpha:0.509},0).wait(1).to({y:649.5,alpha:0.579},0).wait(1).to({y:646.3,alpha:0.646},0).wait(1).to({y:643.4,alpha:0.709},0).wait(1).to({y:640.7,alpha:0.766},0).wait(1).to({y:638.4,alpha:0.817},0).wait(1).to({y:636.4,alpha:0.86},0).wait(1).to({y:634.7,alpha:0.897},0).wait(1).to({y:633.3,alpha:0.927},0).wait(1).to({y:632.2,alpha:0.951},0).wait(1).to({y:631.3,alpha:0.97},0).wait(1).to({y:630.7,alpha:0.984},0).wait(1).to({y:630.2,alpha:0.993},0).wait(1).to({y:630,alpha:0.998},0).wait(1).to({regX:-0.1,regY:0.1,x:434.3,y:630.7,alpha:1},0).wait(109));

	// 00 circulo azul
	this.instance_23 = new lib._00circuloazul();
	this.instance_23.setTransform(324.6,630,0.038,0.038);

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.12,scaleY:0.12},0).wait(1).to({scaleX:0.15,scaleY:0.15},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.23,scaleY:0.23},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.4,scaleY:0.4},0).wait(1).to({scaleX:0.46,scaleY:0.46},0).wait(1).to({scaleX:0.53,scaleY:0.53},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.66,scaleY:0.66},0).wait(1).to({scaleX:0.72,scaleY:0.72},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.82,scaleY:0.82},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(111));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(874,979.4,1.2,1.2);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;