(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 580,
	height: 360,
	fps: 30,
	color: "#FFFFFF",
	opacity: 0.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib._02puntoblanco5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,4.9,5);


(lib._02puntoblanco4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,5,4.9);


(lib._02puntoblanco3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgIAAgJQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAJgHAIQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,5,5);


(lib._02puntoblanco2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgIAHgIQAHgHAJAAQAKAAAHAHQAHAIAAAIQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.5,4.9,5);


(lib._02puntoblanco1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,4.9,4.9);


(lib._02lineaoscura = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AQnMQIw1lKQgGgCgFgFIwnykQgHgIABgLQAAgKAIgHQAIgHAKABQALAAAGAIIQgSfIQxFIQAKADAEAJQAFAKgDAJQgCAIgHAFQgGAFgJAAIgHgBg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-109.6,-78.5,219.2,157);


(lib._02lineaclara = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AQcGyIwik9Iwnn2QgKgFgDgKQgEgJAFgKQAEgJAKgEQAKgDAJAEIQgH1IQiE8QAKADAFAJQAFAJgDAKQgCAIgHAFQgHAFgIAAIgHgBg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-108.5,-43.5,217,87);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("Ah0DLQhVgrglhYQgmhYAehcQAihmBggxQBggxBkAiQBnAhAwBgQAxBgghBlQgdBZhQAyQhOAxhagLIhDA8g");
	this.shape.setTransform(26,28.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,57.9);


(lib.Path_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AiODDQhQg1gahcQgbhbAphYQAthiBlglQBjglBiAtQBiAuAlBlQAlBlguBfQgnBWhVAoQhUAohXgWIhKAzg");
	this.shape_1.setTransform(26,28.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,56.5);


(lib.Path_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B2F2F8").s().p("AhlDPQhXglgrhWQgshUAYheQAbhpBdg2QBdg3BmAaQBoAbA3BdQA3BcgbBnQgXBbhMA3QhLA2hagFIg/BAg");
	this.shape_2.setTransform(26,29.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,58.4);


(lib.Path_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B2F2F8").s().p("AhlDPQhYgmgqhWQgrhVAYheQAbhoBdg2QBcg3BnAbQBoAbA3BdQA2BdgbBmQgYBbhMA3QhLA2hagGIg/BAg");
	this.shape_3.setTransform(26,29.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,58.4);


(lib.Path_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7BD7F8").s().p("AhWEPQhlgkguhhQgvhhAlhkQAfhYBRgwQBQgvBZANIBFg6IAQBWQBUAuAjBZQAjBYghBcQgkBlhhAuQg3Abg4AAQgpAAgtgRg");
	this.shape_4.setTransform(26,28.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.2,57.6);


(lib._02franja = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgxFbIAAq1IBjAAIAAK1g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5,-34.7,10,69.5);


(lib._02562 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhDBgIB5jJIAOAKIh5DJgAAZBgQgEgEgDgGQgCgFAAgIIAAgpQAAgHACgGQADgGAEgEQAJgIANAAQANAAAJAIQAEAEADAGQACAGAAAHIAAApQAAAPgJAIQgJAJgNAAQgNAAgJgJgAAoAVQgCACAAAEIAAAyQAAAFACADQADACAEAAQAEAAADgCQADgDAAgFIAAgyQAAgEgDgCQgDgDgEAAQgEAAgDADgAg6gBQgGgCgEgFQgJgIAAgOIAAgqQAAgHADgGQACgFAEgFQAEgEAGgCQAFgCAHAAQAHAAAFACQAGACAEAEQAEAFADAFQACAGAAAHIAAAqQAAAOgJAIQgEAFgGACQgFABgHAAQgHAAgFgBgAg1hTQgDADAAAEIAAAyQAAAEADADQADACAEAAQAEAAADgCQACgDAAgEIAAgyQAAgEgCgDQgDgDgEAAQgEAAgDADg");
	this.shape.setTransform(20.5,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgzBmIAAgLQAAgLACgLQACgKAEgKQAFgLAIgLIATgbIALgNIAHgNIAFgLIABgLQAAgIgEgEQgDgDgFAAQgFAAgDAEQgDAEAAAHIAAATIgmAAIAAgYQAAgJADgJQADgIAGgGQAGgIAKgEQAJgEALAAQAOAAAKAEQAKAFAGAHQAHAHACAKQADAKAAALIgBARQgCAIgEAJQgEAIgHAIQgHAJgLALIgKAOIgGALIgCAJIgBAHIA2AAIAAAmg");
	this.shape_1.setTransform(5.8,-1.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgPAVIAAgpIAfAAIAAApg");
	this.shape_2.setTransform(-2.6,7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgUBkQgKgDgHgHQgHgHgEgKQgEgJAAgMIAAhnQAAgMAEgJQAEgKAHgHQAHgHAKgDQAKgEAKAAQANAAAJAEQAJAEAGAHQAHAHADAIQADAJAAAJIAAATIglAAIAAgPQAAgHgDgDQgDgEgGAAQgEAAgEADQgDAEAAAFIAAAvQAFgGAFgEQAGgEALAAQARAAAJAKQAFAFACAFQADAFAAAIIAAAnQAAAMgEAJQgEAKgHAHQgGAHgKADQgKAEgMAAQgLAAgJgEgAgHALQgDADAAAEIAAAmQAAAEADADQADADAEAAQAFAAADgDQADgDAAgEIAAgmQAAgEgDgDQgDgDgFAAQgEAAgDADg");
	this.shape_3.setTransform(-11.1,-0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgWBiQgJgFgHgGQgGgHgDgJQgEgHAAgKIAAgSIAnAAIAAAOQAAAHADADQADAEAGgBQADAAAEgCQADgEAAgFIAAguQAAgEgDgCQgDgCgEAAQgDAAgDACQgDABAAAEIglgFIAFhlIBcAAQgCAUgGAJQgEAJgMAAIgiAAIgDArQAGgIAGgDQAIgDAJgBQAPAAAIAKQAFAFADAHQABAGAAAHIAAA1QAAALgDAJQgFAJgHAGQgGAHgLADQgKAEgKAAQgMAAgJgEg");
	this.shape_4.setTransform(-23.2,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.1,-17,62.4,34.1);


(lib._01lineaoscura = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AP5E+Iv5kCIwGlIQgJgDgFgKQgFgJADgKQADgKAJgEQAKgFAJADIQFFJIP3EAQAKADAFAJQAGAJgDAKQgCAIgHAFQgHAFgIAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-104.8,-31.9,209.8,63.8);


(lib._01lineaclara = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AhBBeIwIiLQgKgBgGgJQgGgIABgLQABgKAJgGQAIgHAKACIQGCKISBgbQALgBAHAIQAIAHAAAKQAAALgHAIQgHAIgKAAIyFAdIgDgCg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-111.9,-9.7,224,19.4);


(lib.Path_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#256379").s().p("AgqEEQhqgTg+hXQg3hNAHhbQAHhdBAhDIgJhZIBRAkQBVgrBbAXQBeAWA4BQQA/BXgSBpQgSBqhYA+QhEAxhPAAQgVAAgYgEg");
	this.shape_5.setTransform(26,26.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,52.9);


(lib.Path_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#256379").s().p("AhsEFQhigtglhmQgghYAfhWQAghYBOgvIAQhZIBEA4QBbgTBUAuQBVAvAhBbQAlBkgtBhQgtBihlAlQguAQgrAAQg2AAg2gYg");
	this.shape_6.setTransform(26,28.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,57.2);


(lib.Path_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B2F2F8").s().p("AhlDPQhXglgrhWQgshUAYheQAbhpBdg2QBdg3BmAaQBoAbA3BdQA3BcgbBnQgXBbhMA3QhLA2hagFIg/BAg");
	this.shape_7.setTransform(26,29.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,58.4);


(lib.Path_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#B2F2F8").s().p("AhlDPQhYgmgqhWQgrhVAYheQAbhoBdg2QBcg3BnAbQBoAbA3BdQA2BdgbBmQgYBbhMA3QhLA2hagGIg/BAg");
	this.shape_8.setTransform(26,29.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,58.4);


(lib.Path_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#7BD7F8").s().p("Ah6DJQhUgtgjhZQgjhZAhhbQAkhlBhgvQBhguBkAkQBlAlAvBhQAuBhglBkQgfBYhRAwQhQAvhZgOIhFA6g");
	this.shape_9.setTransform(26,28.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.1,52.2,57.6);


(lib._01franja = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgxFbIAAq1IBjAAIAAK1g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5,-34.7,10,69.5);


(lib._01circuloblanco5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,4.9,4.9);


(lib._01circuloblanco4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.5,4.9,5);


(lib._01circuloblanco3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,5,5);


(lib._01circuloblanco2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,4.9,4.9);


(lib._01circuloblanco1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,4.9,5);


(lib._01324 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhDBgIB5jJIAOAKIh5DJgAAZBgQgEgEgDgGQgCgFAAgIIAAgpQAAgHACgGQADgGAEgEQAJgIANAAQANAAAJAIQAEAEADAGQACAGAAAHIAAApQAAAPgJAIQgJAJgNAAQgNAAgJgJgAAoAVQgCACAAAEIAAAyQAAAFACADQADACAEAAQAEAAADgCQADgDAAgFIAAgyQAAgEgDgCQgDgDgEAAQgEAAgDADgAg6gBQgGgCgEgFQgJgIAAgOIAAgqQAAgHADgGQACgFAEgFQAEgEAGgCQAFgCAHAAQAHAAAFACQAGACAEAEQAEAFADAFQACAGAAAHIAAAqQAAAOgJAIQgEAFgGACQgFABgHAAQgHAAgFgBgAg1hTQgDADAAAEIAAAyQAAAEADADQADACAEAAQAEAAADgCQACgDAAgEIAAgyQAAgEgCgDQgDgDgEAAQgEAAgDADg");
	this.shape.setTransform(20.4,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AABBkIAAgpIg4AAIAAgeIA7iAIAlAAIAAB9IAPAAIAAAhIgPAAIAAApgAgSAaIATAAIAAgwg");
	this.shape_1.setTransform(5.8,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgPAVIAAgpIAfAAIAAApg");
	this.shape_2.setTransform(-2.7,7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgzBmIAAgLQAAgLACgLQACgKAEgKQAFgLAIgLIATgbIALgNIAHgNIAFgLIABgLQAAgIgEgEQgDgDgFAAQgFAAgDAEQgDAEAAAHIAAATIgmAAIAAgYQAAgJADgJQADgIAGgGQAGgIAKgEQAJgEALAAQAOAAAKAEQAKAFAGAHQAHAHACAKQADAKAAALIgBARQgCAIgEAJQgEAIgHAIQgHAJgLALIgKAOIgGALIgCAJIgBAHIA2AAIAAAmg");
	this.shape_3.setTransform(-11.2,-1.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgWBkQgJgEgHgHQgGgHgDgIQgDgIAAgJIAAgUIAmAAIAAAPQAAAHADADQADAEAGAAQADAAAEgDQADgEAAgFIAAgfQAAgIgIAAIgNAAIAAgeIANAAQAIAAAAgJIAAgdQAAgFgDgEQgEgDgDAAQgMAAAAAOIAAAPIgmAAIAAgTQAAgJADgJQADgIAGgHQAHgHAJgEQAIgEANAAQAKAAALADQAJAEAHAGQAHAHAFAJQADAJAAALIAAAUQABANgHAJQgFAKgMACQAKABAHAJQAGAIAAAOIAAAXQAAALgDAJQgFAJgHAHQgHAGgJAEQgLADgKAAQgNAAgIgEg");
	this.shape_4.setTransform(-23.1,-0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31,-17,62.2,34.1);


(lib.Group = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgERgMAAAgi/IAIAAMAAAAi/g");
	this.shape.setTransform(0.5,112);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1,224);


(lib.Group_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgERgMAAAgi/IAIAAMAAAAi/g");
	this.shape_1.setTransform(0.5,112);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1,224);


(lib.Group_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgERgMAAAgi/IAIAAMAAAAi/g");
	this.shape_2.setTransform(0.5,112);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1,224);


(lib._00vertical1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgOR0MAAAgjnIAdAAMAAAAjng");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-114,3,228);


(lib._00titulo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AATB+IgEgmIghAAIgFAmIg1AAIAsj7IBBAAIAsD7gAgMAtIAWAAIgKhag");
	this.shape.setTransform(91.4,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgZB+QgOgEgKgKQgKgIgFgOQgGgMAAgQIAAh7QAAgQAGgMQAFgNAKgKQAKgIAOgFQAOgGAPAAQAPAAANAFQAMAFAJAIQAJAKAFAMQAEALAAAQIAAAhIgzAAIAAgjQAAgGgEgFQgEgDgIAAQgGAAgEADQgEAFAAAGIAACEQAAAHAEAEQAEAEAGAAQAIAAAEgEQAEgEAAgHIAAgiIAzAAIAAAhQAAAPgEAMQgFAMgJAKQgJAIgMAFQgNAFgPAAQgPAAgOgGg");
	this.shape_1.setTransform(76.1,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASB+IgDgmIghAAIgFAmIg1AAIAsj7IBCAAIArD7gAgLAtIAUAAIgJhag");
	this.shape_2.setTransform(60.5,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag7B+IAAj7IA7AAIAADIIA8AAIAAAzg");
	this.shape_3.setTransform(46.4,-1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhDB+IAAj7IBQAAQAMAAAKADQAKAEAIAHQAHAHAEAKQAFALgBANIAAAyQABAOgFAIQgEALgHAHQgIAGgKAEQgKAEgMAAIgWAAIAABcgAgJgGIAMAAQALAAAAgMIAAgxQAAgMgLAAIgMAAg");
	this.shape_4.setTransform(31.7,-1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ag9B+IAAj7IB7AAIAAAxIhAAAIAAA0IAoAAIAAAqIgoAAIAAA4IBAAAIAAA0g");
	this.shape_5.setTransform(11.2,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhHB+IAAj7IBIAAQAQAAANAFQANAEAKAKQAJAJAFAMQAFANAAAPIAAB4QAAAPgFAMQgFAMgJAHQgKAJgNADQgNAFgQAAgAgNBOIAMAAQAGAAAEgFQAEgEABgKIAAh1QgBgJgEgFQgEgFgGAAIgMAAg");
	this.shape_6.setTransform(-4.5,-1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Ag9B+IAAj7IB6AAIAAAxIg/AAIAAA0IApAAIAAAqIgpAAIAAA4IA/AAIAAA0g");
	this.shape_7.setTransform(-25.3,-1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgZB+QgOgEgKgKQgKgIgFgOQgGgMAAgQIAAh7QAAgQAGgMQAFgNAKgKQAKgIAOgFQAOgGAPAAQAPAAANAFQAMAFAJAIQAJAKAFAMQAEALAAAQIAAAhIgzAAIAAgjQAAgGgEgFQgEgDgIAAQgGAAgEADQgEAFAAAGIAACEQAAAHAEAEQAEAEAGAAQAIAAAEgEQAEgEAAgHIAAgiIAzAAIAAAhQAAAPgEAMQgFAMgJAKQgJAIgMAFQgNAFgPAAQgPAAgOgGg");
	this.shape_8.setTransform(-40.7,-1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgcB+IAAj7IA5AAIAAD7g");
	this.shape_9.setTransform(-53,-1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhHB+IAAj7IBJAAQAPAAANAFQANAEAJAKQAKAJAFAMQAFANAAAPIAAB4QAAAPgFAMQgFAMgKAHQgJAJgNADQgNAFgPAAgAgMBOIALAAQAGAAAEgFQAFgEAAgKIAAh1QAAgJgFgFQgEgFgGAAIgLAAg");
	this.shape_10.setTransform(-65.4,-1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAbB+IgihdIgPgiIAAB/Ig0AAIAAj7IA0AAIAgBeIANAiIAAiAIAzAAIAAD7g");
	this.shape_11.setTransform(-82.5,-1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgiCgIAAj7IA4AAIAAD7gAgThpIANg2IApAAIgfA2g");
	this.shape_12.setTransform(-94.7,-4.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101.5,-20.7,203.1,42);


(lib._00leyenda = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAMBUIgCgZIgWAAIgDAZIgkAAIAeioIArAAIAeCogAgHAeIANAAIgGg8g");
	this.shape.setTransform(30.9,-0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgvBUIAAioIAwAAQAKAAAJAEQAJADAGAGQAGAGAEAIQADAJAAAKIAABQQAAAKgDAIQgEAIgGAGQgGAFgJADQgJACgKAAgAgIA1IAIAAQADAAADgEQADgDAAgHIAAhOQAAgGgDgDQgDgDgDAAIgIAAg");
	this.shape_1.setTransform(20.4,-0.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASBUIgXg+IgJgWIAABUIgjAAIAAioIAjAAIAVA/IAJAXIAAhWIAiAAIAACog");
	this.shape_2.setTransform(8.9,-0.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgoBUIAAioIBRAAIAAAiIgqAAIAAAjIAbAAIAAAbIgbAAIAAAlIAqAAIAAAjg");
	this.shape_3.setTransform(-1.6,-0.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgTBUIAAhGIgihiIAoAAIAOA9IAOg9IAnAAIgkBiIAABGg");
	this.shape_4.setTransform(-12,-0.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgoBUIAAioIBRAAIAAAiIgqAAIAAAjIAbAAIAAAbIgbAAIAAAlIAqAAIAAAjg");
	this.shape_5.setTransform(-22.1,-0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgnBUIAAioIAnAAIAACGIAoAAIAAAig");
	this.shape_6.setTransform(-31.3,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.2,-14.7,76.6,29.6);


(lib._00horizontal7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A41AFIAAgJMAxrAAAIAAAJg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib.Group_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_3.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib.Group_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_4.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib.Group_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_5.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib.Group_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_6.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib.Group_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_7.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib._00horizontal1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A5DAPIAAgdMAyHAAAIAAAdg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-160.5,-1.5,321,3);


(lib._00CPC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgFgEQgFgFgCgGQgDgGAAgHIAAg4QAAgIADgGQACgGAFgEQAFgEAGgDQAHgCAFAAQAIAAAFACQAGADAEADQAEAEADAGQACAGAAAHIAAAPIgYAAIAAgQQAAgDgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAA8QAAAEACACQACACABAAQAEAAACgCQACgCAAgEIAAgPIAYAAIAAAPQAAAHgCAGQgDAFgEAFQgEAEgGACQgFACgIAAQgFAAgHgCg");
	this.shape.setTransform(7.5,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgfA6IAAh0IAlAAQAFAAAFADQAFABADADQAEAEACAEQACAFAAAGIAAAYQAAAGgCADQgCAFgEADQgDAEgFABIgKACIgJAAIAAAqgAgDgCIAEAAQAFAAAAgFIAAgYQAAgFgFAAIgEAAg");
	this.shape_1.setTransform(0.1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgFgEQgFgFgCgGQgDgGAAgHIAAg4QAAgIADgGQACgGAFgEQAFgEAGgDQAHgCAFAAQAIAAAFACQAGADAEADQAEAEADAGQACAGAAAHIAAAPIgYAAIAAgQQAAgDgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAA8QAAAEACACQACACABAAQAEAAACgCQACgCAAgEIAAgPIAYAAIAAAPQAAAHgCAGQgDAFgEAFQgEAEgGACQgFACgIAAQgFAAgHgCg");
	this.shape_2.setTransform(-7.3,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.1,-10.8,26.5,21.7);


(lib._00circulooscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AhKBLQgfgfAAgsQAAgrAfgfQAfgfArAAQAsAAAfAfQAfAfAAArQAAAsgfAfQgfAfgsAAQgrAAgfgfg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.7,-10.6,21.4,21.3);


(lib._00circuloclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AhKBLQgfgfAAgsQAAgrAfgfQAggfAqAAQAsAAAfAfQAfAfAAArQAAArgfAgQgfAfgsAAQgqAAgggfg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.7,-10.6,21.4,21.4);


(lib._00Baseline = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIARAAIAAASIgRAAIAAAbIAcAAIAAAXg");
	this.shape.setTransform(23.5,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAMA6IgPgrIgGgPIAAA6IgYAAIAAh0IAYAAIANAsIAHAPIAAg7IAYAAIAAB0g");
	this.shape_1.setTransform(16,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMA6IAAh0IAZAAIAAB0g");
	this.shape_2.setTransform(10,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IAbAAIAABdIAcAAIAAAXg");
	this.shape_3.setTransform(5.2,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIARAAIAAASIgRAAIAAAbIAdAAIAAAXg");
	this.shape_4.setTransform(-1.4,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgCgFQgCgGgBgIIAAgIIAYAAIAAAIQAAAEACADQACACAEAAQADAAACgCQACgCAAgFQAAgFgGgGIgJgLIgIgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQABgGAEgFQAFgFAGgCQAHgDAFAAQAIAAAFACQAGADAEAEQAEAEACAFQACAGAAAGIAAAIIgYAAIAAgJQAAgDgBgCQgDgCgDAAQAAAAgCACQgCACgBAEIACAGIADAGIAEAEIAFAHIAJAHIAHAHQADAEADAGQACAFAAAGQgBAHgCAGQgCAGgEAFQgFAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape_5.setTransform(-8.4,-0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAIA6IgCgRIgOAAIgCARIgZAAIAVh0IAdAAIAVB0gAgFAUIAJAAIgEgog");
	this.shape_6.setTransform(-15.7,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgfA6IAAh0IAmAAQAFAAAEACQAFACADACQAEADACAFQACAFAAAFIAAAQIgBAGIgCAFIgEAFIgGACQAHABADAFQADAFAAAGIAAASQAAAGgCAEQgCAFgEADQgDADgFABQgEABgFAAgAgDAlIAEAAQAFAAAAgGIAAgQQAAgGgFAAIgEAAgAgDgIIAEAAQAFAAAAgGIAAgRQAAgFgFAAIgEAAg");
	this.shape_7.setTransform(-23,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-28.7,-10.8,57.7,21.7);


(lib._00Aceitesesenciales = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgDgFQgCgGAAgIIAAgIIAYAAIAAAIQAAAEACADQACACAEAAQACAAACgCQADgCAAgFQgBgFgFgGIgIgLIgIgIIgIgGQgEgFgBgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAGgDAGAAQAHAAAGACQAFADAEAEQAEAEADAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgDgCgDAAQAAAAgDACQgCACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQADAEACAGQACAFABAGQAAAHgDAGQgCAGgEAFQgFAEgGACQgFADgIAAQgGAAgGgCg");
	this.shape.setTransform(53.9,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIASAAIAAASIgSAAIAAAbIAcAAIAAAXg");
	this.shape_1.setTransform(47.2,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IAbAAIAABdIAcAAIAAAXg");
	this.shape_2.setTransform(40.8,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAIA6IgCgRIgOAAIgCARIgZAAIAVh0IAdAAIAVB0gAgFAUIAJAAIgEgog");
	this.shape_3.setTransform(33.8,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgMA6IAAh0IAZAAIAAB0g");
	this.shape_4.setTransform(28.2,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgFgEQgFgFgCgGQgDgGAAgHIAAg4QAAgIADgGQACgGAFgEQAFgEAGgDQAHgCAFAAQAIAAAFACQAGADAEADQAEAEADAGQACAGAAAHIAAAPIgYAAIAAgQQAAgDgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAA8QAAAEACACQACACABAAQAEAAACgCQACgCAAgEIAAgPIAYAAIAAAPQAAAHgCAGQgDAFgEAFQgEAEgGACQgFACgIAAQgFAAgHgCg");
	this.shape_5.setTransform(22.6,-0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAMA6IgPgrIgGgPIAAA6IgZAAIAAh0IAZAAIANAsIAGAPIAAg7IAZAAIAAB0g");
	this.shape_6.setTransform(14.8,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIARAAIAAASIgRAAIAAAbIAdAAIAAAXg");
	this.shape_7.setTransform(7.5,-0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgCgFQgDgGAAgIIAAgIIAYAAIAAAIQAAAEACADQADACADAAQACAAADgCQACgCAAgFQAAgFgGgGIgJgLIgIgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAHgDAFAAQAIAAAFACQAFADAFAEQAEAEACAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgCgCgEAAQAAAAgCACQgDACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQAEAEABAGQADAFAAAGQAAAHgDAGQgCAGgEAFQgFAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape_8.setTransform(0.5,-0.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIARAAIAAASIgRAAIAAAbIAcAAIAAAXg");
	this.shape_9.setTransform(-6.3,-0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgDgFQgCgGAAgIIAAgIIAYAAIAAAIQAAAEACADQACACAEAAQACAAACgCQADgCAAgFQgBgFgFgGIgIgLIgJgIIgHgGQgEgFgBgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAGgDAGAAQAHAAAGACQAFADAEAEQAEAEADAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgDgCgDAAQAAAAgDACQgCACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQADAEACAGQACAFABAGQAAAHgDAGQgCAGgEAFQgFAEgGACQgFADgIAAQgGAAgGgCg");
	this.shape_10.setTransform(-15.8,-0.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIASAAIAAASIgSAAIAAAbIAcAAIAAAXg");
	this.shape_11.setTransform(-22.6,-0.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgMA6IAAhcIgTAAIAAgYIA/AAIAAAYIgTAAIAABcg");
	this.shape_12.setTransform(-29.4,-0.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgMA6IAAh0IAZAAIAAB0g");
	this.shape_13.setTransform(-34.8,-0.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIASAAIAAASIgSAAIAAAbIAdAAIAAAXg");
	this.shape_14.setTransform(-39.9,-0.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgFgEQgFgFgCgGQgDgGAAgHIAAg4QAAgIADgGQACgGAFgEQAFgEAGgDQAHgCAFAAQAIAAAFACQAGADAEADQAEAEADAGQACAGAAAHIAAAPIgYAAIAAgQQAAgDgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAA8QAAAEACACQACACABAAQAEAAACgCQACgCAAgEIAAgPIAYAAIAAAPQAAAHgCAGQgDAFgEAFQgEAEgGACQgFACgIAAQgFAAgHgCg");
	this.shape_15.setTransform(-47,-0.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAIA6IgCgRIgOAAIgCARIgZAAIAVh0IAdAAIAVB0gAgFAUIAJAAIgEgog");
	this.shape_16.setTransform(-54.3,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-59.9,-10.8,119.6,21.7);


(lib._006meses = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgCgFQgDgGAAgIIAAgIIAYAAIAAAIQAAAEACADQADACADAAQACAAADgCQACgCAAgFQAAgFgGgGIgIgLIgJgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAHgDAFAAQAIAAAFACQAFADAFAEQAEAEACAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgCgCgEAAQAAAAgCACQgDACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQAEAEABAGQADAFAAAGQAAAHgDAGQgCAGgEAFQgFAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape.setTransform(20.3,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIASAAIAAASIgSAAIAAAbIAcAAIAAAXg");
	this.shape_1.setTransform(13.5,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMA7QgHgCgEgEQgEgEgDgFQgCgGAAgIIAAgIIAYAAIAAAIQAAAEADADQABACAEAAQACAAACgCQACgCAAgFQAAgFgEgGIgJgLIgIgIIgIgGQgEgFgCgFQgCgEAAgHQAAgGACgGQACgGAEgFQAEgFAHgCQAGgDAHAAQAGAAAGACQAFADAEAEQAFAEACAFQACAGAAAGIAAAIIgXAAIAAgJQgBgDgCgCQgCgCgCAAQgBAAgDACQgCACAAAEIABAGIAEAGIADAEIAHAHIAIAHIAHAHQADAEACAGQACAFAAAGQAAAHgCAGQgCAGgFAFQgDAEgHACQgFADgIAAQgGAAgGgCg");
	this.shape_2.setTransform(6.5,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIASAAIAAASIgSAAIAAAbIAdAAIAAAXg");
	this.shape_3.setTransform(-0.3,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUA6IAAhNIgFAbIgIAyIgPAAIgJgyIgEgbIAABNIgYAAIAAh0IAkAAIAIAvIABATIADgTIAIgvIAjAAIAAB0g");
	this.shape_4.setTransform(-9,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgEgEQgEgFgCgFQgDgGAAgHIAAg7QAAgHADgGQACgGAEgEQAEgDAGgDQAGgCAFAAQAHAAAGACQAFADAEAEQAEAEABAFQACAFAAAFIAAALIgVAAIAAgJQAAgEgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAAcQADgDACgCQAEgDAGAAQAKAAAFAFQADADACADIABAIIAAAWQAAAHgCAGQgDAFgEAFQgEAEgFACQgGACgHAAQgGAAgFgCgAgDAGQgBAAAAABQgBABAAAAQAAABAAAAQAAABAAAAIAAAXQAAAAAAABQAAAAAAABQAAAAABABQAAABABAAQACACABgBQADABACgCQAAAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAIAAgXQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAgBAAAAQgCgCgDAAQgBAAgCACg");
	this.shape_5.setTransform(-20.3,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.9,-10.8,52,21.7);


(lib._0030 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgEgEQgEgFgCgFQgDgFAAgIIAAg7QAAgHADgGQACgGAEgEQAEgEAGgCQAFgCAGAAQAHAAAGACQAFACAFAEQAEAEACAGQACAGAAAHIAAA7QAAAIgCAFQgCAFgEAFQgFAEgFACQgGACgHAAQgGAAgFgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQADACABAAQADAAACgCQAAAAAAgBQABAAAAgBQAAAAAAgBQABAAAAgBIAAg/QAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQgCgCgDAAQgBAAgDACg");
	this.shape.setTransform(5,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAPQAEAAACgDQACgCAAgHIgIAAIAAgWIASAAIAAAUQgBAEgBADIgEAHQgCACgCACQgEABgEAAg");
	this.shape_1.setTransform(-0.2,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNA6QgFgCgEgEQgDgEgCgEQgCgFAAgGIAAgMIAXAAIAAAJQAAAFABABQACACADAAQACAAACgCQACgBAAgEIAAgSQAAgEgFAAIgHAAIAAgRIAHAAQAFAAAAgGIAAgRQAAgDgCgBQgCgCgCAAQgGAAAAAHIAAAKIgXAAIAAgMQAAgFACgFQACgFADgEQAEgEAFgDQAGgCAHAAQAFAAAGACQAGACAEAEQAEAEADAFQACAFAAAHIAAAMQAAAHgEAGQgDAFgHABQAGAAAEAGQAEAEAAAJIAAAOQAAAGgCAFQgDAGgEAEQgEADgGACQgGACgFAAQgHAAgGgDg");
	this.shape_2.setTransform(-5.1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.6,-10.8,21.5,21.7);


(lib._003meses = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgEgEQgFgEgCgFQgCgGAAgIIAAgIIAXAAIAAAIQAAAEADADQACACADAAQADAAACgCQABgCAAgFQAAgFgEgGIgKgLIgIgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQABgGAFgFQAEgFAGgCQAHgDAFAAQAIAAAFACQAGADAEAEQADAEADAFQACAGAAAGIAAAIIgYAAIAAgJQAAgDgCgCQgBgCgEAAQAAAAgCACQgCACAAAEIABAGIADAGIAEAEIAFAHIAJAHIAHAHQAEAEACAGQABAFAAAGQAAAHgCAGQgCAGgFAFQgEAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape.setTransform(20.1,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIARAAIAAASIgRAAIAAAbIAdAAIAAAXg");
	this.shape_1.setTransform(13.4,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgCgFQgDgGAAgIIAAgIIAYAAIAAAIQAAAEACADQADACADAAQACAAADgCQACgCAAgFQAAgFgGgGIgIgLIgJgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAHgDAFAAQAIAAAFACQAFADAFAEQAEAEACAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgCgCgEAAQAAAAgCACQgDACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQAEAEABAGQADAFAAAGQAAAHgDAGQgCAGgEAFQgFAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape_2.setTransform(6.3,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIASAAIAAASIgSAAIAAAbIAcAAIAAAXg");
	this.shape_3.setTransform(-0.4,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUA6IAAhNIgFAbIgIAyIgPAAIgJgyIgEgbIAABNIgYAAIAAh0IAkAAIAIAvIABATIADgTIAIgvIAjAAIAAB0g");
	this.shape_4.setTransform(-9.1,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgNA6QgFgCgEgEQgDgEgCgEQgCgFAAgGIAAgMIAXAAIAAAJQAAAFABABQACACADAAQACAAACgCQACgBAAgEIAAgSQAAgEgFAAIgHAAIAAgRIAHAAQAFAAAAgGIAAgRQAAgDgCgBQgCgCgCAAQgGAAAAAHIAAAKIgXAAIAAgMQAAgFACgFQACgFADgEQAEgEAFgDQAGgCAHAAQAFAAAGACQAGACAEAEQAEAEADAFQACAFAAAHIAAAMQAAAHgEAGQgDAFgHABQAGAAAEAGQAEAEAAAJIAAAOQAAAGgCAFQgDAGgEAEQgEADgGACQgGACgFAAQgHAAgGgDg");
	this.shape_5.setTransform(-20.3,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.8,-10.8,51.7,21.7);


(lib._0025 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNA5QgFgCgEgEQgDgEgCgFQgCgFAAgFIAAgLIAXAAIAAAIQAAAEABACQACACADAAQACAAACgCQACgBAAgEIAAgaQAAgBAAgBQAAAAgBgBQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAgBgBQAAAAAAAAIgDABQAAAAgBAAQAAAAAAABQAAAAgBABQAAAAAAABIgWgDIADg7IA2AAQgBAMgEAFQgDAGgGAAIgUAAIgBAZQADgEADgDQAEgCAGAAQAJAAAFAGIAEAHQABAEAAADIAAAfQAAAHgCAFQgDAFgEAEQgEAEgGACQgGACgFAAQgHAAgGgDg");
	this.shape.setTransform(5,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAPQAEAAADgDQACgCAAgHIgJAAIAAgWIASAAIAAAUQABAEgCADIgEAHQgDACgBACQgEABgFAAg");
	this.shape_1.setTransform(0.1,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeA8IAAgGIABgNQABgGAEgGQACgHAEgGIAMgQIAGgHIAEgIIACgGIABgHQAAgEgCgCQgCgCgDAAQgBAAgDACQgBACAAAEIAAALIgXAAIAAgOIACgKQACgFADgEQAEgEAFgCQAGgDAGAAQAIAAAGADQAFACAEAFQAEAEACAGQABAFAAAHIAAAKIgEAKQgCAFgEADIgKAMIgHAJIgDAGIgBAFIgBAEIAgAAIAAAXg");
	this.shape_2.setTransform(-4.9,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.4,-10.8,21.1,21.7);


(lib._0020 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgFgCgEgEQgFgFgCgFQgCgFAAgIIAAg7QAAgHACgGQACgGAFgEQAEgEAFgCQAHgCAFAAQAHAAAGACQAGACADAEQAEAEADAGQACAGAAAHIAAA7QAAAIgCAFQgDAFgEAFQgDAEgGACQgGACgHAAQgFAAgHgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQACACACAAQADAAABgCQABAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBIAAg/QAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBgCgDAAQgCAAgCACg");
	this.shape.setTransform(5,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAPQAFAAACgDQACgCAAgHIgJAAIAAgWIASAAIAAAUQAAAEgBADIgEAHQgDACgCACQgDABgFAAg");
	this.shape_1.setTransform(-0.1,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeA8IAAgGIABgNQACgGADgGQACgHAFgGIALgQIAGgHIAEgIIADgGIAAgHQAAgEgCgCQgCgCgDAAQgCAAgCACQgBACAAAEIAAALIgWAAIAAgOIABgKQACgFADgEQAEgEAFgCQAGgDAGAAQAIAAAGADQAFACAEAFQAEAEABAGQACAFAAAHIgBAKIgDAKQgCAFgFADIgJAMIgHAJIgDAGIAAAFIgCAEIAfAAIAAAXg");
	this.shape_2.setTransform(-5.1,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.6,-10.8,21.6,21.7);


(lib._0015 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNA5QgFgCgEgEQgDgEgCgFQgCgFAAgFIAAgLIAXAAIAAAIQAAAEABACQACACADAAQACAAACgCQACgBAAgEIAAgaQAAgBAAgBQAAAAgBgBQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAgBgBQAAAAAAAAIgDABQAAAAgBAAQAAAAAAABQgBAAAAABQAAAAAAABIgWgDIADg7IA2AAQgBAMgEAFQgDAGgGAAIgUAAIgBAZQADgEADgDQAEgCAGAAQAJAAAFAGIAEAHQABAEAAADIAAAfQAAAHgCAFQgDAFgEAEQgEAEgGACQgGACgFAAQgHAAgGgDg");
	this.shape.setTransform(3.9,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAPQAFAAACgDQACgCAAgHIgJAAIAAgWIASAAIAAAUQAAAEgBADIgEAHQgDACgCACQgDABgFAAg");
	this.shape_1.setTransform(-1,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLA6IAAhoQAGAAADgEQACgDAAgFIAMAAIAAB0g");
	this.shape_2.setTransform(-4.8,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.2,-10.8,18.7,21.7);


(lib._0010 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgFgCgEgEQgFgFgBgFQgDgFAAgIIAAg7QAAgHADgGQABgGAFgEQAEgEAFgCQAGgCAGAAQAHAAAGACQAFACAEAEQAEAEADAGQACAGAAAHIAAA7QAAAIgCAFQgDAFgEAFQgEAEgFACQgGACgHAAQgGAAgGgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQADACABAAQADAAABgCQABAAAAgBQABAAAAgBQAAAAAAgBQABAAAAgBIAAg/QAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBgCgDAAQgBAAgDACg");
	this.shape.setTransform(3.9,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAPQAEAAADgDQACgCAAgHIgJAAIAAgWIATAAIAAAUQAAAEgCADIgEAHQgDACgCACQgDABgFAAg");
	this.shape_1.setTransform(-1.3,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLA6IAAhoQAGAAADgEQACgDAAgFIAMAAIAAB0g");
	this.shape_2.setTransform(-5.1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.4,-10.8,19.2,21.7);


(lib._0005 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNA5QgFgCgEgEQgDgEgCgFQgCgFAAgFIAAgLIAXAAIAAAIQAAAEABACQACACADAAQACAAACgCQACgBAAgEIAAgaQAAgBAAgBQgBAAAAgBQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAgBgBQAAAAAAAAIgDABQAAAAgBAAQAAAAAAABQAAAAgBABQAAAAAAABIgWgDIADg7IA2AAQgBAMgEAFQgDAGgGAAIgUAAIgBAZQADgEADgDQAEgCAGAAQAJAAAFAGIAEAHQABAEAAADIAAAfQAAAHgCAFQgDAFgEAEQgEAEgGACQgGACgFAAQgHAAgGgDg");
	this.shape.setTransform(5.2,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAPQAEAAACgDQACgCAAgHIgIAAIAAgWIASAAIAAAUQgBAEgBADIgEAHQgCACgCACQgEABgEAAg");
	this.shape_1.setTransform(0.3,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMA7QgFgCgEgEQgFgFgCgFQgCgFAAgIIAAg7QAAgHACgGQACgGAFgEQAEgEAFgCQAHgCAFAAQAHAAAGACQAGACADAEQAEAEADAGQACAGAAAHIAAA7QAAAIgCAFQgDAFgEAFQgDAEgGACQgGACgHAAQgFAAgHgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQACACACAAQADAAABgCQABAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBIAAg/QAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBgCgDAAQgCAAgCACg");
	this.shape_2.setTransform(-4.9,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.6,-10.8,21.4,21.7);


(lib._0000 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgFgCgEgEQgFgFgCgFQgCgFAAgIIAAg7QAAgHACgGQACgGAFgEQAEgEAFgCQAHgCAFAAQAHAAAGACQAGACADAEQAEAEADAGQACAGAAAHIAAA7QAAAIgCAFQgDAFgEAFQgDAEgGACQgGACgHAAQgFAAgHgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQACACACAAQADAAABgCQABAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBIAAg/QAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBgCgDAAQgCAAgCACg");
	this.shape.setTransform(0.1,-0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.6,-10.8,11.6,21.7);


(lib._02globooscuro2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AABBeIAAgmIg1AAIAAgcIA4h5IAjAAIAAB1IAOAAIAAAgIgOAAIAAAmgAgRAYIASAAIAAgtg");
	this.shape.setTransform(11.5,-3.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgiBeQAAgVAEgUQAEgWAGgUQAGgSAJgRQAFgSAJgNIg5AAIAAgmIBhAAIAAAQQgKAWgIAUQgHAUgFATQgGAVgCAWQgDAXAAAYg");
	this.shape_1.setTransform(1.8,-3.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-6.2,4.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTBeQgKgDgGgHQgHgGgDgJQgEgJAAgLIAAhhQAAgLAEgJQADgJAHgHQAGgGAKgEQAJgDAKAAQAMAAAIADQAKAEAGAGQAHAHADAJQAEAJAAALIAABhQAAALgEAJQgDAJgHAGQgGAHgKADQgIAEgMAAQgKAAgJgEgAgHg6QgCADAAAEIAABmQAAAEACADQAEADADAAQAFAAADgDQACgDAAgEIAAhmQAAgEgCgDQgDgCgFAAQgDAAgEACg");
	this.shape_3.setTransform(-14.5,-3.9);

	this.instance = new lib.Path();
	this.instance.setTransform(-1,-1,1,1,0,0,0,26,28.9);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-29.9,60,64);


(lib._02globooscuro1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AABBeIAAgmIg1AAIAAgdIA4h4IAjAAIAAB1IAOAAIAAAgIgOAAIAAAmgAgRAYIASAAIAAgtg");
	this.shape.setTransform(8.1,-2.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgSBeIAAipQAJAAAEgGQAEgFAAgHIAUAAIAAC7g");
	this.shape_1.setTransform(-1.3,-2.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-7.5,6.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTBeIAAipQAKAAAEgGQAEgFAAgHIAVAAIAAC7g");
	this.shape_3.setTransform(-13.5,-2.4);

	this.instance = new lib.Path_1();
	this.instance.setTransform(-1,-1,1,1,0,0,0,26,28.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-29.3,60,64);


(lib._02globoclaro2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgVBeQgJgEgGgGQgHgHgDgIQgDgIAAgIIAAgOIAjAAIAAAJQAAAHADADQADAEAHAAQAGAAACgEQADgDAAgFIAAgpQgEAGgGAEQgEAEgLAAQgQAAgIgKQgFgEgCgGQgDgEAAgIIAAgnQAAgLAEgJQADgJAHgHQAGgGAKgEQAJgDAKAAQALAAAJADQAJAEAHAGQAGAHAEAJQAEAJAAALIAABhQAAALgEAJQgEAJgHAGQgHAHgKADQgJAEgJAAQgMAAgJgEgAgHg6QgCACAAAEIAAAoQAAAEACADQAEACADAAQAFAAACgCQADgDAAgEIAAgoQAAgEgDgCQgCgDgFAAQgDAAgEADg");
	this.shape.setTransform(11,-4.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgTBfQgJgEgHgGQgHgHgDgJQgEgJAAgLIAAhhQAAgLAEgJQAEgJAGgGQAHgHAJgEQAKgEAJAAQAMAAAJAFQAIAEAGAGQAGAHADAIQADAHAAAJIAAARIgjAAIAAgNQAAgHgDgDQgDgDgFAAQgEAAgDADQgDADAAAFIAAAsQAEgFAFgEQAGgDAKgBQAQAAAJAJQAEAFACAFQADAFAAAHIAAAlQAAALgEAJQgEAJgGAHQgGAGgKAEQgJADgLAAQgKAAgJgDgAgGAKQgDACAAAFIAAAjQAAAFADACQACADAEAAQAFAAACgDQADgCAAgFIAAgjQAAgFgDgCQgCgCgFgBQgEABgCACg");
	this.shape_1.setTransform(-0.6,-4.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-8.8,4.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgTBeIAAipQAKAAAEgGQAEgFAAgHIAVAAIAAC7g");
	this.shape_3.setTransform(-14.9,-4.6);

	this.instance = new lib.Path_2();
	this.instance.setTransform(-1,-1,1,1,0,0,0,26,29.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-30.2,60,66);


(lib._02globoclaro1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgTBeIAAiqQAKAAAEgEQAEgGAAgHIAVAAIAAC7g");
	this.shape.setTransform(11,-3.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgVBeQgJgEgGgGQgGgHgDgIQgEgIAAgIIAAgOIAjAAIAAAJQAAAHADADQADAEAHAAQAGAAADgEQACgDAAgFIAAgpQgEAGgGAEQgFAEgKAAQgPAAgKgKQgEgEgCgGQgDgEAAgIIAAgnQAAgLAEgJQADgJAHgHQAHgGAJgEQAJgDAKAAQALAAAJADQAJAEAHAGQAHAHADAJQAEAJAAALIAABhQAAALgEAJQgEAJgHAGQgGAHgLADQgJAEgJAAQgMAAgJgEgAgHg6QgCACAAAEIAAAoQAAAEACADQAEACADAAQAFAAADgCQACgDAAgEIAAgoQAAgEgCgCQgDgDgFAAQgDAAgEADg");
	this.shape_1.setTransform(1.3,-3.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-7,5.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgSBeIAAiqQAJAAAEgEQAEgGAAgHIAUAAIAAC7g");
	this.shape_3.setTransform(-13,-3.3);

	this.instance = new lib.Path_3();
	this.instance.setTransform(-0.9,-0.9,1,1,0,0,0,26.1,29.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-30.2,60,66);


(lib._02globoazul = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgVBcQgJgEgGgGQgGgHgCgIQgDgIAAgIIAAgRIAkAAIAAANQAAAGACAEQADADAGAAQADAAADgDQADgDAAgFIAAgrQAAgFgDgBQgDgCgDAAQgCAAgEACQgCABgBADIgjgEIAFhfIBXAAQgCASgFAJQgFAIgLAAIggAAIgCApQAFgHAFgEQAIgDAJAAQANAAAJAJQAEAFACAGQACAGABAHIAAAyQgBAKgEAJQgDAIgHAGQgHAHgJADQgKADgJAAQgMAAgIgEg");
	this.shape.setTransform(12.3,3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgiBeQAAgVAEgVQAEgVAGgUQAGgSAJgRQAFgSAJgNIg5AAIAAgmIBhAAIAAARQgLAUgHAVQgIAUgEATQgFAWgDAWQgCAWAAAYg");
	this.shape_1.setTransform(1.5,2.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-6.5,11.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgwBgIAAgKQAAgLACgJQACgKAEgKQAEgKAIgLIASgYIAKgNIAHgMIAEgLIABgLQAAgHgDgDQgDgDgFAAQgEAAgDADQgDAEAAAGIAAASIgkAAIAAgWQAAgIADgIQADgIAGgHQAFgGAJgEQAJgEAKAAQANAAAKAEQAJAEAGAHQAGAHADAJQACAJAAAKIgBARQgBAHgEAIQgEAIgHAHQgGAJgKALIgLANIgFAKIgCAIIgBAHIAzAAIAAAkg");
	this.shape_3.setTransform(-14.5,2.5);

	this.instance = new lib.Path_4();
	this.instance.setTransform(-0.9,-0.9,1,1,0,0,0,26.1,28.8);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.1,-29.7,60,64);


(lib._01globooscuro2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgiBeQAAgVAEgVQAEgVAGgUQAGgSAJgRQAFgSAJgNIg5AAIAAgmIBhAAIAAARQgLAUgHAVQgIAUgEATQgFAWgDAWQgCAWAAAYg");
	this.shape.setTransform(11.8,-1.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgwBgIAAgKQAAgLACgJQACgKAEgKQAEgKAIgLIASgYIAKgNIAHgMIAEgLIABgLQAAgHgDgDQgDgDgFAAQgEAAgDADQgDAEAAAGIAAASIgkAAIAAgWQAAgIADgIQADgIAGgHQAFgGAJgEQAJgEAKAAQANAAAKAEQAJAEAGAHQAGAHADAJQACAJAAAKIgBARQgBAHgEAIQgEAIgHAHQgGAJgKALIgLANIgFAKIgCAIIgBAHIAzAAIAAAkg");
	this.shape_1.setTransform(0.4,-1.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-7.5,7.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgSBeIAAiqQAJABAEgGQAEgFAAgHIAUAAIAAC7g");
	this.shape_3.setTransform(-13.6,-1.3);

	this.instance = new lib.Path_5();
	this.instance.setTransform(-0.9,-1,1,1,0,0,0,26.1,26.4);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-27.4,60,60);


(lib._01globooscuro1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgwBgIAAgKQAAgLACgJQACgKAEgKQAEgKAIgLIASgYIAKgNIAHgMIAEgLIABgLQAAgHgDgDQgDgDgFAAQgEAAgDADQgDAEAAAGIAAASIgkAAIAAgWQAAgIADgIQADgIAGgHQAFgGAJgEQAJgEAKAAQANAAAKAEQAJAEAGAHQAGAHADAJQACAJAAAKIgBARQgBAHgEAIQgEAIgHAHQgGAJgKALIgLANIgFAKIgCAIIgBAHIAzAAIAAAkg");
	this.shape.setTransform(11.7,1.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgTBfQgJgEgHgGQgHgHgDgJQgEgJAAgLIAAhhQAAgLAEgJQAEgJAGgGQAHgHAJgEQAKgEAJAAQAMAAAJAFQAIAEAGAGQAGAHADAIQADAHAAAJIAAARIgjAAIAAgNQAAgHgDgDQgDgDgFAAQgEAAgDADQgDADAAAFIAAAsQAEgFAFgEQAGgDAKgBQAQAAAJAJQAEAFACAFQADAFAAAHIAAAlQAAALgEAJQgEAJgGAHQgGAGgKAEQgJADgLAAQgKAAgJgDgAgGAKQgDACAAAFIAAAjQAAAFADACQACADAEAAQAFAAACgDQADgCAAgFIAAgjQAAgFgDgCQgCgCgFgBQgEABgCACg");
	this.shape_1.setTransform(0.5,2.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-7.8,10.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgSBeIAAipQAJAAAEgGQAEgFAAgHIAUAAIAAC7g");
	this.shape_3.setTransform(-13.8,2.1);

	this.instance = new lib.Path_6();
	this.instance.setTransform(-1,-0.9,1,1,0,0,0,26,28.6);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-29.5,60,64);


(lib._01globoclaro2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgTBfQgKgDgGgHQgHgFgEgJQgEgIAAgLIAAgXQAAgKAFgIQAEgIAIgDQgIgDgEgJQgFgIAAgKIAAgVQAAgKAEgJQAEgJAHgFQAGgGAKgEQAJgEAKAAQALAAAKAEQAJAEAHAGQAGAFAEAJQAEAJAAAKIAAAVQAAAKgEAIQgEAJgJADQAJADAEAIQAEAIAAAKIAAAXQAAALgEAIQgDAJgHAFQgHAHgJADQgKADgLAAQgKAAgJgDgAgHAMQgDACAAAGIAAAfQAAAFADADQAEADADAAQAFAAADgDQADgDAAgFIAAgfQAAgGgDgCQgDgDgFgBQgDABgEADgAgHg6QgDADAAAFIAAAeQAAAFADADQAEADADAAQAFAAADgDQADgDAAgFIAAgeQAAgFgDgDQgDgDgFAAQgDAAgEADg");
	this.shape.setTransform(11.4,-3.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgTBfQgKgDgGgHQgHgFgEgJQgEgIAAgLIAAgXQAAgKAFgIQAEgIAIgDQgIgDgEgJQgFgIAAgKIAAgVQAAgKAEgJQAEgJAHgFQAGgGAKgEQAJgEAKAAQALAAAKAEQAJAEAHAGQAGAFAEAJQAEAJAAAKIAAAVQAAAKgEAIQgEAJgJADQAJADAEAIQAEAIAAAKIAAAXQAAALgEAIQgDAJgHAFQgHAHgJADQgKADgLAAQgKAAgJgDgAgHAMQgDACAAAGIAAAfQAAAFADADQAEADADAAQAFAAADgDQADgDAAgFIAAgfQAAgGgDgCQgDgDgFgBQgDABgEADgAgHg6QgDADAAAFIAAAeQAAAFADADQAEADADAAQAFAAADgDQADgDAAgFIAAgeQAAgFgDgDQgDgDgFAAQgDAAgEADg");
	this.shape_1.setTransform(-0.5,-3.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-8.8,5.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgTBeIAAipQAKAAAEgGQAEgFAAgHIAVAAIAAC7g");
	this.shape_3.setTransform(-14.9,-3.6);

	this.instance = new lib.Path_7();
	this.instance.setTransform(-1,-1,1,1,0,0,0,26,29.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-30.2,60,66);


(lib._01globoclaro1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgWBcQgIgEgGgGQgFgHgDgIQgDgIgBgIIAAgRIAkAAIAAANQABAGADAEQADADAFAAQADAAADgDQADgDAAgFIAAgrQAAgFgDgBQgDgCgDAAQgDAAgDACQgCABAAADIgkgEIAFhfIBXAAQgCASgFAJQgFAIgKAAIghAAIgCApQAFgHAFgEQAIgDAJAAQANAAAJAJQAEAFACAGQACAGAAAHIAAAyQAAAKgDAJQgEAIgHAGQgGAHgKADQgJADgKAAQgMAAgJgEg");
	this.shape.setTransform(12.1,-4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgTBfQgKgDgGgGQgHgHgEgIQgEgJAAgKIAAgXQAAgKAFgIQAEgIAIgDQgIgEgEgHQgFgJAAgKIAAgVQAAgKAEgJQAEgJAHgFQAGgHAKgDQAJgEAKAAQALAAAKAEQAJADAHAHQAGAFAEAJQAEAJAAAKIAAAVQAAAKgEAJQgEAHgJAEQAJADAEAIQAEAIAAAKIAAAXQAAAKgEAJQgDAIgHAHQgHAGgJADQgKAEgLgBQgKABgJgEgAgHAMQgDACAAAGIAAAfQAAAFADADQAEADADAAQAFAAADgDQADgDAAgFIAAgfQAAgGgDgCQgDgEgFAAQgDAAgEAEgAgHg6QgDADAAAFIAAAeQAAAFADADQAEADADAAQAFAAADgDQADgDAAgFIAAgeQAAgFgDgDQgDgDgFAAQgDAAgEADg");
	this.shape_1.setTransform(0.5,-4.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-7.8,4.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgTBeIAAipQAKAAAEgGQAEgFAAgHIAVAAIAAC7g");
	this.shape_3.setTransform(-13.9,-4.2);

	this.instance = new lib.Path_8();
	this.instance.setTransform(-0.9,-0.9,1,1,0,0,0,26.1,29.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-30.2,60,66);


(lib._01globoazul = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgWBcQgIgEgGgGQgFgHgDgIQgEgIAAgIIAAgRIAkAAIAAANQAAAGAEAEQADADAFAAQADAAADgDQADgDAAgFIAAgrQAAgFgDgBQgDgCgDAAQgCAAgDACQgDABAAADIgkgEIAFhfIBXAAQgCASgFAJQgFAIgKAAIghAAIgDApQAGgHAGgEQAHgDAJAAQANAAAJAJQAEAFADAGQABAGAAAHIAAAyQAAAKgDAJQgEAIgHAGQgGAHgKADQgJADgKAAQgMAAgJgEg");
	this.shape.setTransform(13.2,-3.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgTBeQgKgDgGgHQgHgGgEgJQgDgJAAgLIAAhhQAAgLADgJQAEgJAHgHQAGgGAKgEQAJgDAKAAQALAAAKADQAJAEAGAGQAHAHAEAJQADAJAAALIAABhQAAALgDAJQgEAJgHAGQgGAHgJADQgKAEgLAAQgKAAgJgEgAgHg6QgCADAAAEIAABmQAAAEACADQAEADADAAQAFAAACgDQADgDABgEIAAhmQgBgEgDgDQgCgCgFAAQgDAAgEACg");
	this.shape_1.setTransform(1.7,-3.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-6.6,5.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgwBgIAAgKQAAgLACgJQACgKAEgKQAEgKAIgLIASgYIAKgNIAHgMIAEgLIABgLQAAgHgDgDQgDgDgFAAQgEAAgDADQgDAEAAAGIAAASIgkAAIAAgWQAAgIADgIQADgIAGgHQAFgGAJgEQAJgEAKAAQANAAAKAEQAJAEAGAHQAGAHADAJQACAJAAAKIgBARQgBAHgEAIQgEAIgHAHQgGAJgKALIgLANIgFAKIgCAIIgBAHIAzAAIAAAkg");
	this.shape_3.setTransform(-14.7,-3.6);

	this.instance = new lib.Path_9();
	this.instance.setTransform(-0.9,-1,1,1,0,0,0,26.1,28.7);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.1,-29.8,60,64);


(lib._00vertical4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group();
	this.instance.setTransform(0,0,1,1,0,0,0,0.5,112);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-112,1,224);


(lib._00vertical3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_1();
	this.instance.setTransform(0,0,1,1,0,0,0,0.5,112);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-112,1,224);


(lib._00vertical2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_2();
	this.instance.setTransform(0,0,1,1,0,0,0,0.5,112);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-112,1,224);


(lib._00horizontal6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_3();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib._00horizontal5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_4();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib._00horizontal4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_5();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib._00horizontal3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_6();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib._00horizontal2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_7();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


// stage content:
(lib.grafica_16_v1_rv = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// 02 56,2%
	this.instance = new lib._02562();
	this.instance.setTransform(403,226.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(38).to({regY:-0.9,x:403.1,y:225.3,alpha:0.999},0).wait(1).to({x:403.6,alpha:0.994},0).wait(1).to({x:404.3,alpha:0.986},0).wait(1).to({x:405.5,alpha:0.975},0).wait(1).to({x:407,alpha:0.96},0).wait(1).to({x:408.9,alpha:0.94},0).wait(1).to({x:411.4,alpha:0.916},0).wait(1).to({x:414.3,alpha:0.886},0).wait(1).to({x:417.8,alpha:0.851},0).wait(1).to({x:421.9,alpha:0.809},0).wait(1).to({x:426.7,alpha:0.761},0).wait(1).to({x:432,alpha:0.707},0).wait(1).to({x:437.9,alpha:0.648},0).wait(1).to({x:444.3,alpha:0.584},0).wait(1).to({x:450.9,alpha:0.517},0).wait(1).to({x:457.7,alpha:0.449},0).wait(1).to({x:464.3,alpha:0.382},0).wait(1).to({x:470.5,alpha:0.319},0).wait(1).to({x:476.3,alpha:0.262},0).wait(1).to({x:481.4,alpha:0.21},0).wait(1).to({x:485.9,alpha:0.164},0).wait(1).to({x:489.8,alpha:0.125},0).wait(1).to({x:493,alpha:0.093},0).wait(1).to({x:495.7,alpha:0.066},0).wait(1).to({x:497.8,alpha:0.044},0).wait(1).to({x:499.5,alpha:0.027},0).wait(1).to({x:500.7,alpha:0.015},0).wait(1).to({x:501.6,alpha:0.006},0).wait(1).to({x:502,alpha:0.002},0).wait(1).to({regY:0,x:502.2,y:226.2,alpha:0},0).to({_off:true},1).wait(42));

	// 02 franja
	this.instance_1 = new lib._02franja();
	this.instance_1.setTransform(364.5,223.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(38).to({scaleY:1},0).wait(1).to({scaleY:0.99},0).wait(1).to({scaleY:0.99},0).wait(1).to({scaleY:0.98},0).wait(1).to({scaleY:0.96},0).wait(1).to({scaleY:0.94},0).wait(1).to({scaleY:0.92},0).wait(1).to({scaleY:0.89},0).wait(1).to({scaleY:0.85},0).wait(1).to({scaleY:0.81},0).wait(1).to({scaleY:0.77},0).wait(1).to({scaleY:0.71},0).wait(1).to({scaleY:0.65},0).wait(1).to({scaleY:0.59},0).wait(1).to({scaleY:0.52},0).wait(1).to({scaleY:0.46},0).wait(1).to({scaleY:0.39},0).wait(1).to({scaleY:0.33},0).wait(1).to({scaleY:0.27},0).wait(1).to({scaleY:0.22},0).wait(1).to({scaleY:0.18},0).wait(1).to({scaleY:0.14},0).wait(1).to({scaleY:0.11},0).wait(1).to({scaleY:0.08},0).wait(1).to({scaleY:0.06},0).wait(1).to({scaleY:0.04},0).wait(1).to({scaleY:0.03},0).wait(1).to({scaleY:0.02},0).wait(1).to({scaleY:0.02},0).wait(1).to({scaleY:0.02},0).to({_off:true},1).wait(42));

	// 02 globo claro 2
	this.instance_2 = new lib._02globoclaro2();
	this.instance_2.setTransform(324.9,161.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(38).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.86,scaleY:0.86},0).wait(1).to({scaleX:0.8,scaleY:0.8},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.64,scaleY:0.64},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).to({_off:true},1).wait(52));

	// 02 globo claro 1
	this.instance_3 = new lib._02globoclaro1();
	this.instance_3.setTransform(218.9,129.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(28).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.86,scaleY:0.86},0).wait(1).to({scaleX:0.8,scaleY:0.8},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.64,scaleY:0.64},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).to({_off:true},1).wait(62));

	// 02 globo oscuro 2
	this.instance_4 = new lib._02globooscuro2();
	this.instance_4.setTransform(329.1,232.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(28).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.86,scaleY:0.86},0).wait(1).to({scaleX:0.8,scaleY:0.8},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.64,scaleY:0.64},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).to({_off:true},1).wait(62));

	// 02 globo oscuro 1
	this.instance_5 = new lib._02globooscuro1();
	this.instance_5.setTransform(224.5,199.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(15).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.86,scaleY:0.86},0).wait(1).to({scaleX:0.8,scaleY:0.8},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.64,scaleY:0.64},0).wait(1).to({scaleX:0.55,scaleY:0.55},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).to({_off:true},1).wait(75));

	// 02 globo azul
	this.instance_6 = new lib._02globoazul();
	this.instance_6.setTransform(93.8,137.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.86,scaleY:0.86},0).wait(1).to({scaleX:0.8,scaleY:0.8},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.64,scaleY:0.64},0).wait(1).to({scaleX:0.55,scaleY:0.55},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).to({_off:true},1).wait(89));

	// 02 punto blanco 5
	this.instance_7 = new lib._02puntoblanco5();
	this.instance_7.setTransform(316.4,189.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(38).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.63,scaleY:0.63},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).to({_off:true},1).wait(62));

	// 02 punto blanco 4
	this.instance_8 = new lib._02puntoblanco4();
	this.instance_8.setTransform(210.1,157.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(28).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.63,scaleY:0.63},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).to({_off:true},1).wait(72));

	// 02 punto blanco 3
	this.instance_9 = new lib._02puntoblanco3();
	this.instance_9.setTransform(318.4,260.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(28).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.63,scaleY:0.63},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).to({_off:true},1).wait(72));

	// 02 punto blanco 2
	this.instance_10 = new lib._02puntoblanco2();
	this.instance_10.setTransform(210.1,227.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(15).to({scaleX:0.99,scaleY:0.99,y:227},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.63,scaleY:0.63},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).to({_off:true},1).wait(85));

	// 02 punto blanco 1
	this.instance_11 = new lib._02puntoblanco1();
	this.instance_11.setTransform(104,106.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(19).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.63,scaleY:0.63},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).to({_off:true},1).wait(81));

	// 00 mascara (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AxQX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_14 = new cjs.Graphics().p("AxQX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_15 = new cjs.Graphics().p("AxOX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_16 = new cjs.Graphics().p("AxJX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_17 = new cjs.Graphics().p("AxAX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_18 = new cjs.Graphics().p("Aw0X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_19 = new cjs.Graphics().p("AwiX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_20 = new cjs.Graphics().p("AwMX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_21 = new cjs.Graphics().p("AvwX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_22 = new cjs.Graphics().p("AvNX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_23 = new cjs.Graphics().p("AukX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_24 = new cjs.Graphics().p("At0X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_25 = new cjs.Graphics().p("As8X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_26 = new cjs.Graphics().p("Ar8X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_27 = new cjs.Graphics().p("Aq2X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_28 = new cjs.Graphics().p("AprX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_29 = new cjs.Graphics().p("AodX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_30 = new cjs.Graphics().p("AnPX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_31 = new cjs.Graphics().p("AmEX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_32 = new cjs.Graphics().p("Ak+X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_33 = new cjs.Graphics().p("Aj+X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_34 = new cjs.Graphics().p("AjGX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_35 = new cjs.Graphics().p("AiWX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_36 = new cjs.Graphics().p("AhtX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_37 = new cjs.Graphics().p("AhKX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_38 = new cjs.Graphics().p("AguX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_39 = new cjs.Graphics().p("AgYX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_40 = new cjs.Graphics().p("AgGX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_41 = new cjs.Graphics().p("AAEX+MAAAgjlMAyKAAAMAAAAjlg");
	var mask_graphics_42 = new cjs.Graphics().p("AANX+MAAAgjlMAyKAAAMAAAAjlg");
	var mask_graphics_43 = new cjs.Graphics().p("AASX+MAAAgjlMAyKAAAMAAAAjlg");
	var mask_graphics_44 = new cjs.Graphics().p("AATX+MAAAgjlMAyLAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:210.5,y:153.5}).wait(14).to({graphics:mask_graphics_14,x:210.5,y:153.5}).wait(1).to({graphics:mask_graphics_15,x:210.7,y:153.5}).wait(1).to({graphics:mask_graphics_16,x:211.2,y:153.5}).wait(1).to({graphics:mask_graphics_17,x:212,y:153.5}).wait(1).to({graphics:mask_graphics_18,x:213.3,y:153.5}).wait(1).to({graphics:mask_graphics_19,x:215,y:153.5}).wait(1).to({graphics:mask_graphics_20,x:217.3,y:153.5}).wait(1).to({graphics:mask_graphics_21,x:220.1,y:153.5}).wait(1).to({graphics:mask_graphics_22,x:223.5,y:153.5}).wait(1).to({graphics:mask_graphics_23,x:227.6,y:153.5}).wait(1).to({graphics:mask_graphics_24,x:232.5,y:153.5}).wait(1).to({graphics:mask_graphics_25,x:238.1,y:153.5}).wait(1).to({graphics:mask_graphics_26,x:244.4,y:153.5}).wait(1).to({graphics:mask_graphics_27,x:251.4,y:153.5}).wait(1).to({graphics:mask_graphics_28,x:258.9,y:153.5}).wait(1).to({graphics:mask_graphics_29,x:266.8,y:153.5}).wait(1).to({graphics:mask_graphics_30,x:274.6,y:153.5}).wait(1).to({graphics:mask_graphics_31,x:282.1,y:153.5}).wait(1).to({graphics:mask_graphics_32,x:289.1,y:153.5}).wait(1).to({graphics:mask_graphics_33,x:295.4,y:153.5}).wait(1).to({graphics:mask_graphics_34,x:301,y:153.5}).wait(1).to({graphics:mask_graphics_35,x:305.9,y:153.5}).wait(1).to({graphics:mask_graphics_36,x:310,y:153.5}).wait(1).to({graphics:mask_graphics_37,x:313.4,y:153.5}).wait(1).to({graphics:mask_graphics_38,x:316.2,y:153.5}).wait(1).to({graphics:mask_graphics_39,x:318.5,y:153.5}).wait(1).to({graphics:mask_graphics_40,x:320.2,y:153.5}).wait(1).to({graphics:mask_graphics_41,x:321.5,y:153.5}).wait(1).to({graphics:mask_graphics_42,x:322.3,y:153.5}).wait(1).to({graphics:mask_graphics_43,x:322.8,y:153.5}).wait(1).to({graphics:mask_graphics_44,x:323,y:153.5}).wait(66));

	// 02 linea clara
	this.instance_12 = new lib._02lineaclara();
	this.instance_12.setTransform(210,148.1);

	this.instance_12.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({_off:true},45).wait(65));

	// 00 mascara (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_0 = new cjs.Graphics().p("AxLX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_4 = new cjs.Graphics().p("AxLX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_5 = new cjs.Graphics().p("AxJX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_6 = new cjs.Graphics().p("AxEX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_7 = new cjs.Graphics().p("Aw8X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_8 = new cjs.Graphics().p("AwvX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_9 = new cjs.Graphics().p("AweX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_10 = new cjs.Graphics().p("AwIX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_11 = new cjs.Graphics().p("AvtX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_12 = new cjs.Graphics().p("AvLX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_13 = new cjs.Graphics().p("AuiX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_14 = new cjs.Graphics().p("AtzX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_15 = new cjs.Graphics().p("As8X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_16 = new cjs.Graphics().p("Ar+X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_17 = new cjs.Graphics().p("Aq5X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_18 = new cjs.Graphics().p("ApvX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_19 = new cjs.Graphics().p("AoiX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_20 = new cjs.Graphics().p("AnWX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_21 = new cjs.Graphics().p("AmMX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_22 = new cjs.Graphics().p("AlHX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_23 = new cjs.Graphics().p("AkJX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_24 = new cjs.Graphics().p("AjSX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_25 = new cjs.Graphics().p("AiiX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_26 = new cjs.Graphics().p("Ah6X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_27 = new cjs.Graphics().p("AhYX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_28 = new cjs.Graphics().p("Ag8X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_29 = new cjs.Graphics().p("AgmX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_30 = new cjs.Graphics().p("AgVX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_31 = new cjs.Graphics().p("AgJX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_32 = new cjs.Graphics().p("AAAX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_33 = new cjs.Graphics().p("AADX+MAAAgjlMAyKAAAMAAAAjlg");
	var mask_1_graphics_34 = new cjs.Graphics().p("AAEX+MAAAgjlMAyKAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:mask_1_graphics_0,x:211,y:153.5}).wait(4).to({graphics:mask_1_graphics_4,x:211,y:153.5}).wait(1).to({graphics:mask_1_graphics_5,x:211.1,y:153.5}).wait(1).to({graphics:mask_1_graphics_6,x:211.6,y:153.5}).wait(1).to({graphics:mask_1_graphics_7,x:212.5,y:153.5}).wait(1).to({graphics:mask_1_graphics_8,x:213.7,y:153.5}).wait(1).to({graphics:mask_1_graphics_9,x:215.4,y:153.5}).wait(1).to({graphics:mask_1_graphics_10,x:217.6,y:153.5}).wait(1).to({graphics:mask_1_graphics_11,x:220.4,y:153.5}).wait(1).to({graphics:mask_1_graphics_12,x:223.8,y:153.5}).wait(1).to({graphics:mask_1_graphics_13,x:227.8,y:153.5}).wait(1).to({graphics:mask_1_graphics_14,x:232.6,y:153.5}).wait(1).to({graphics:mask_1_graphics_15,x:238.1,y:153.5}).wait(1).to({graphics:mask_1_graphics_16,x:244.3,y:153.5}).wait(1).to({graphics:mask_1_graphics_17,x:251.2,y:153.5}).wait(1).to({graphics:mask_1_graphics_18,x:258.6,y:153.5}).wait(1).to({graphics:mask_1_graphics_19,x:266.2,y:153.5}).wait(1).to({graphics:mask_1_graphics_20,x:273.9,y:153.5}).wait(1).to({graphics:mask_1_graphics_21,x:281.3,y:153.5}).wait(1).to({graphics:mask_1_graphics_22,x:288.2,y:153.5}).wait(1).to({graphics:mask_1_graphics_23,x:294.4,y:153.5}).wait(1).to({graphics:mask_1_graphics_24,x:299.9,y:153.5}).wait(1).to({graphics:mask_1_graphics_25,x:304.6,y:153.5}).wait(1).to({graphics:mask_1_graphics_26,x:308.7,y:153.5}).wait(1).to({graphics:mask_1_graphics_27,x:312.1,y:153.5}).wait(1).to({graphics:mask_1_graphics_28,x:314.8,y:153.5}).wait(1).to({graphics:mask_1_graphics_29,x:317,y:153.5}).wait(1).to({graphics:mask_1_graphics_30,x:318.7,y:153.5}).wait(1).to({graphics:mask_1_graphics_31,x:320,y:153.5}).wait(1).to({graphics:mask_1_graphics_32,x:320.8,y:153.5}).wait(1).to({graphics:mask_1_graphics_33,x:321.3,y:153.5}).wait(1).to({graphics:mask_1_graphics_34,x:321.5,y:153.5}).wait(76));

	// 02 linea oscura
	this.instance_13 = new lib._02lineaoscura();
	this.instance_13.setTransform(211.1,184.1);

	this.instance_13.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({_off:true},35).wait(75));

	// 01 32,4%
	this.instance_14 = new lib._01324();
	this.instance_14.setTransform(500.8,193,1,1,0,0,0,0.1,0.1);
	this.instance_14.alpha = 0;
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(62).to({_off:false},0).wait(1).to({regX:0,regY:-0.9,x:500.6,y:192,alpha:0.001},0).wait(1).to({x:500.2,alpha:0.005},0).wait(1).to({x:499.5,alpha:0.013},0).wait(1).to({x:498.5,alpha:0.023},0).wait(1).to({x:497.1,alpha:0.037},0).wait(1).to({x:495.3,alpha:0.055},0).wait(1).to({x:493.1,alpha:0.078},0).wait(1).to({x:490.4,alpha:0.106},0).wait(1).to({x:487.1,alpha:0.139},0).wait(1).to({x:483.3,alpha:0.178},0).wait(1).to({x:478.9,alpha:0.222},0).wait(1).to({x:473.9,alpha:0.274},0).wait(1).to({x:468.3,alpha:0.331},0).wait(1).to({x:462.2,alpha:0.393},0).wait(1).to({x:455.7,alpha:0.46},0).wait(1).to({x:449,alpha:0.529},0).wait(1).to({x:442.3,alpha:0.597},0).wait(1).to({x:435.9,alpha:0.662},0).wait(1).to({x:430,alpha:0.723},0).wait(1).to({x:424.6,alpha:0.778},0).wait(1).to({x:419.9,alpha:0.826},0).wait(1).to({x:415.9,alpha:0.867},0).wait(1).to({x:412.5,alpha:0.902},0).wait(1).to({x:409.7,alpha:0.931},0).wait(1).to({x:407.4,alpha:0.954},0).wait(1).to({x:405.7,alpha:0.971},0).wait(1).to({x:404.4,alpha:0.985},0).wait(1).to({x:403.5,alpha:0.993},0).wait(1).to({x:403.1,alpha:0.998},0).wait(1).to({regX:0.1,regY:0.1,x:403,y:193,alpha:1},0).wait(18));

	// 01 franja
	this.instance_15 = new lib._01franja();
	this.instance_15.setTransform(364.5,190.3,1,0.015);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(62).to({_off:false},0).wait(1).to({scaleY:0.02},0).wait(1).to({scaleY:0.02},0).wait(1).to({scaleY:0.03},0).wait(1).to({scaleY:0.04},0).wait(1).to({scaleY:0.05},0).wait(1).to({scaleY:0.07},0).wait(1).to({scaleY:0.09},0).wait(1).to({scaleY:0.12},0).wait(1).to({scaleY:0.15},0).wait(1).to({scaleY:0.19},0).wait(1).to({scaleY:0.23},0).wait(1).to({scaleY:0.28},0).wait(1).to({scaleY:0.34},0).wait(1).to({scaleY:0.4},0).wait(1).to({scaleY:0.47},0).wait(1).to({scaleY:0.54},0).wait(1).to({scaleY:0.6},0).wait(1).to({scaleY:0.67},0).wait(1).to({scaleY:0.73},0).wait(1).to({scaleY:0.78},0).wait(1).to({scaleY:0.83},0).wait(1).to({scaleY:0.87},0).wait(1).to({scaleY:0.9},0).wait(1).to({scaleY:0.93},0).wait(1).to({scaleY:0.95},0).wait(1).to({scaleY:0.97},0).wait(1).to({scaleY:0.99},0).wait(1).to({scaleY:0.99},0).wait(1).to({scaleY:1},0).wait(1).to({scaleY:1},0).wait(18));

	// 01 globo claro 2
	this.instance_16 = new lib._01globoclaro2();
	this.instance_16.setTransform(330.7,128.5,0.018,0.018);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(62).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.23,scaleY:0.23},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.39,scaleY:0.39},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.77,scaleY:0.77},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(28));

	// 01 globo claro 1
	this.instance_17 = new lib._01globoclaro1();
	this.instance_17.setTransform(212.9,131,0.02,0.02);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(52).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.12,scaleY:0.12},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.23,scaleY:0.23},0).wait(1).to({scaleX:0.31,scaleY:0.31},0).wait(1).to({scaleX:0.39,scaleY:0.39},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.77,scaleY:0.77},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(38));

	// 01 globo oscuro 2
	this.instance_18 = new lib._01globooscuro2();
	this.instance_18.setTransform(328.3,232.1,0.02,0.02);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(52).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.12,scaleY:0.12},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.23,scaleY:0.23},0).wait(1).to({scaleX:0.31,scaleY:0.31},0).wait(1).to({scaleX:0.4,scaleY:0.4},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.77,scaleY:0.77},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(38));

	// 01 globo oscuro 1
	this.instance_19 = new lib._01globooscuro1();
	this.instance_19.setTransform(220.7,208.9,0.019,0.019);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(39).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.23,scaleY:0.23},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.39,scaleY:0.39},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.77,scaleY:0.77},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(51));

	// 01 globo azul
	this.instance_20 = new lib._01globoazul();
	this.instance_20.setTransform(114.6,117.5,0.018,0.018);
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(25).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.23,scaleY:0.23},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.39,scaleY:0.39},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.77,scaleY:0.77},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(65));

	// 01 circulo blanco 5
	this.instance_21 = new lib._01circuloblanco5();
	this.instance_21.setTransform(321.9,156.6,0.224,0.224);
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(62).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.76,scaleY:0.76},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(38));

	// 01 circulo blanco 4
	this.instance_22 = new lib._01circuloblanco4();
	this.instance_22.setTransform(206.6,159.3,0.224,0.224);
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(52).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.76,scaleY:0.76},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(48));

	// 01 circulo blanco 3
	this.instance_23 = new lib._01circuloblanco3();
	this.instance_23.setTransform(307.6,204.8,0.224,0.224);
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(52).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.76,scaleY:0.76},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(48));

	// 01 circulo blanco 2
	this.instance_24 = new lib._01circuloblanco2();
	this.instance_24.setTransform(207.7,179.6,0.224,0.224);
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(39).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.76,scaleY:0.76},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(61));

	// 01 circulo blanco 1
	this.instance_25 = new lib._01circuloblanco1();
	this.instance_25.setTransform(103,145.1,0.224,0.224);
	this.instance_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(25).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.76,scaleY:0.76},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(75));

	// 00 mascara (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_39 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_2_graphics_40 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_41 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_42 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_2_graphics_43 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_2_graphics_44 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_2_graphics_45 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_46 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_2_graphics_47 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_48 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_49 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_2_graphics_50 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_51 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_52 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_53 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_54 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_55 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_56 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_57 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_58 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_59 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_60 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_2_graphics_61 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_62 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_63 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_2_graphics_64 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_65 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_2_graphics_66 = new cjs.Graphics().p("A5BX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_67 = new cjs.Graphics().p("A44X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_68 = new cjs.Graphics().p("A4zX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_2_graphics_69 = new cjs.Graphics().p("A4yX+MAAAgjlMAyIAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(39).to({graphics:mask_2_graphics_39,x:-61.3,y:153.5}).wait(1).to({graphics:mask_2_graphics_40,x:-61,y:153.5}).wait(1).to({graphics:mask_2_graphics_41,x:-60,y:153.5}).wait(1).to({graphics:mask_2_graphics_42,x:-58.3,y:153.5}).wait(1).to({graphics:mask_2_graphics_43,x:-55.7,y:153.5}).wait(1).to({graphics:mask_2_graphics_44,x:-52.2,y:153.5}).wait(1).to({graphics:mask_2_graphics_45,x:-47.7,y:153.5}).wait(1).to({graphics:mask_2_graphics_46,x:-42.1,y:153.5}).wait(1).to({graphics:mask_2_graphics_47,x:-35.2,y:153.5}).wait(1).to({graphics:mask_2_graphics_48,x:-27,y:153.5}).wait(1).to({graphics:mask_2_graphics_49,x:-17.3,y:153.5}).wait(1).to({graphics:mask_2_graphics_50,x:-6.1,y:153.5}).wait(1).to({graphics:mask_2_graphics_51,x:6.6,y:153.5}).wait(1).to({graphics:mask_2_graphics_52,x:20.7,y:153.5}).wait(1).to({graphics:mask_2_graphics_53,x:35.7,y:153.5}).wait(1).to({graphics:mask_2_graphics_54,x:51.4,y:153.5}).wait(1).to({graphics:mask_2_graphics_55,x:67,y:153.5}).wait(1).to({graphics:mask_2_graphics_56,x:82.1,y:153.5}).wait(1).to({graphics:mask_2_graphics_57,x:96.1,y:153.5}).wait(1).to({graphics:mask_2_graphics_58,x:108.8,y:153.5}).wait(1).to({graphics:mask_2_graphics_59,x:120,y:153.5}).wait(1).to({graphics:mask_2_graphics_60,x:129.7,y:153.5}).wait(1).to({graphics:mask_2_graphics_61,x:138,y:153.5}).wait(1).to({graphics:mask_2_graphics_62,x:144.8,y:153.5}).wait(1).to({graphics:mask_2_graphics_63,x:150.5,y:153.5}).wait(1).to({graphics:mask_2_graphics_64,x:155,y:153.5}).wait(1).to({graphics:mask_2_graphics_65,x:158.4,y:153.5}).wait(1).to({graphics:mask_2_graphics_66,x:160.8,y:153.5}).wait(1).to({graphics:mask_2_graphics_67,x:161.6,y:153.5}).wait(1).to({graphics:mask_2_graphics_68,x:162.1,y:153.5}).wait(1).to({graphics:mask_2_graphics_69,x:162.3,y:153.5}).wait(41));

	// 01 linea clara
	this.instance_26 = new lib._01lineaclara();
	this.instance_26.setTransform(212.4,152.3);
	this.instance_26._off = true;

	this.instance_26.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(39).to({_off:false},0).wait(71));

	// 00 mascara (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	var mask_3_graphics_29 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_30 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_3_graphics_31 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_32 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_33 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_34 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_35 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_36 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_37 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_38 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_39 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_40 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_3_graphics_41 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_42 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_43 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_3_graphics_44 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_3_graphics_45 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_3_graphics_46 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_47 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_48 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_49 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_50 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_51 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_3_graphics_52 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_53 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_54 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_55 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_3_graphics_56 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_3_graphics_57 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_3_graphics_58 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_3_graphics_59 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(29).to({graphics:mask_3_graphics_29,x:-61.3,y:153.5}).wait(1).to({graphics:mask_3_graphics_30,x:-61,y:153.5}).wait(1).to({graphics:mask_3_graphics_31,x:-60.1,y:153.5}).wait(1).to({graphics:mask_3_graphics_32,x:-58.4,y:153.5}).wait(1).to({graphics:mask_3_graphics_33,x:-56,y:153.5}).wait(1).to({graphics:mask_3_graphics_34,x:-52.8,y:153.5}).wait(1).to({graphics:mask_3_graphics_35,x:-48.6,y:153.5}).wait(1).to({graphics:mask_3_graphics_36,x:-43.3,y:153.5}).wait(1).to({graphics:mask_3_graphics_37,x:-36.8,y:153.5}).wait(1).to({graphics:mask_3_graphics_38,x:-29.1,y:153.5}).wait(1).to({graphics:mask_3_graphics_39,x:-20,y:153.5}).wait(1).to({graphics:mask_3_graphics_40,x:-9.5,y:153.5}).wait(1).to({graphics:mask_3_graphics_41,x:2.4,y:153.5}).wait(1).to({graphics:mask_3_graphics_42,x:15.6,y:153.5}).wait(1).to({graphics:mask_3_graphics_43,x:29.7,y:153.5}).wait(1).to({graphics:mask_3_graphics_44,x:44.4,y:153.5}).wait(1).to({graphics:mask_3_graphics_45,x:59,y:153.5}).wait(1).to({graphics:mask_3_graphics_46,x:73.1,y:153.5}).wait(1).to({graphics:mask_3_graphics_47,x:86.3,y:153.5}).wait(1).to({graphics:mask_3_graphics_48,x:98.2,y:153.5}).wait(1).to({graphics:mask_3_graphics_49,x:108.7,y:153.5}).wait(1).to({graphics:mask_3_graphics_50,x:117.8,y:153.5}).wait(1).to({graphics:mask_3_graphics_51,x:125.5,y:153.5}).wait(1).to({graphics:mask_3_graphics_52,x:132,y:153.5}).wait(1).to({graphics:mask_3_graphics_53,x:137.3,y:153.5}).wait(1).to({graphics:mask_3_graphics_54,x:141.5,y:153.5}).wait(1).to({graphics:mask_3_graphics_55,x:144.7,y:153.5}).wait(1).to({graphics:mask_3_graphics_56,x:147.1,y:153.5}).wait(1).to({graphics:mask_3_graphics_57,x:148.8,y:153.5}).wait(1).to({graphics:mask_3_graphics_58,x:149.7,y:153.5}).wait(1).to({graphics:mask_3_graphics_59,x:150,y:153.5}).wait(51));

	// 01 linea oscura
	this.instance_27 = new lib._01lineaoscura();
	this.instance_27.setTransform(205.3,175.5);
	this.instance_27._off = true;

	this.instance_27.mask = mask_3;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(29).to({_off:false},0).wait(81));

	// 00 Aceites esenciales
	this.instance_28 = new lib._00Aceitesesenciales();
	this.instance_28.setTransform(490.5,109.7,1,1,0,0,0,-0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(110));

	// 00 circulo oscuro
	this.instance_29 = new lib._00circulooscuro();
	this.instance_29.setTransform(415.8,108.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(110));

	// 00 CPC
	this.instance_30 = new lib._00CPC();
	this.instance_30.setTransform(443.9,81,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(110));

	// 00 circulo claro
	this.instance_31 = new lib._00circuloclaro();
	this.instance_31.setTransform(415.8,79.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(110));

	// 00 leyenda
	this.instance_32 = new lib._00leyenda();
	this.instance_32.setTransform(445,52.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_32).wait(110));

	// 00 titulo
	this.instance_33 = new lib._00titulo();
	this.instance_33.setTransform(130.2,48.7,1,1,0,0,0,0.1,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(110));

	// 00 horizontal 7
	this.instance_34 = new lib._00horizontal7();
	this.instance_34.setTransform(210,79.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_34).wait(110));

	// 00 horizontal 6
	this.instance_35 = new lib._00horizontal6();
	this.instance_35.setTransform(210,115.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(110));

	// 00 horizontal 5
	this.instance_36 = new lib._00horizontal5();
	this.instance_36.setTransform(210,153.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_36).wait(110));

	// 00 horizontal 4
	this.instance_37 = new lib._00horizontal4();
	this.instance_37.setTransform(210,191.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_37).wait(110));

	// 00 horizontal 3
	this.instance_38 = new lib._00horizontal3();
	this.instance_38.setTransform(210,229.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_38).wait(110));

	// 00 horizontal 2
	this.instance_39 = new lib._00horizontal2();
	this.instance_39.setTransform(210,267.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_39).wait(110));

	// 00 horizontal 1
	this.instance_40 = new lib._00horizontal1();
	this.instance_40.setTransform(208.5,305.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_40).wait(110));

	// 00 vertical 4
	this.instance_41 = new lib._00vertical4();
	this.instance_41.setTransform(315.5,192);

	this.timeline.addTween(cjs.Tween.get(this.instance_41).wait(110));

	// 00 vertical 3
	this.instance_42 = new lib._00vertical3();
	this.instance_42.setTransform(209.5,192);

	this.timeline.addTween(cjs.Tween.get(this.instance_42).wait(110));

	// 00 vertical 2
	this.instance_43 = new lib._00vertical2();
	this.instance_43.setTransform(103.5,192);

	this.timeline.addTween(cjs.Tween.get(this.instance_43).wait(110));

	// 00 vertical 1
	this.instance_44 = new lib._00vertical1();
	this.instance_44.setTransform(49.5,193);

	this.timeline.addTween(cjs.Tween.get(this.instance_44).wait(110));

	// 00 6 meses
	this.instance_45 = new lib._006meses();
	this.instance_45.setTransform(315.6,321.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_45).wait(110));

	// 00 3 meses
	this.instance_46 = new lib._003meses();
	this.instance_46.setTransform(209.7,321.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_46).wait(110));

	// 00 Baseline
	this.instance_47 = new lib._00Baseline();
	this.instance_47.setTransform(106.9,321.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_47).wait(110));

	// 00 3,0
	this.instance_48 = new lib._0030();
	this.instance_48.setTransform(32.9,79.6,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_48).wait(110));

	// 00 2,5
	this.instance_49 = new lib._0025();
	this.instance_49.setTransform(32.7,116.6,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_49).wait(110));

	// 00 2,0
	this.instance_50 = new lib._0020();
	this.instance_50.setTransform(32.9,153.6,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_50).wait(110));

	// 00 1,5
	this.instance_51 = new lib._0015();
	this.instance_51.setTransform(31.5,190.6,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_51).wait(110));

	// 00 1,0
	this.instance_52 = new lib._0010();
	this.instance_52.setTransform(31.7,227.6,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_52).wait(110));

	// 00 0,5
	this.instance_53 = new lib._0005();
	this.instance_53.setTransform(32.9,264.6,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_53).wait(110));

	// 00 0,0
	this.instance_54 = new lib._0000();
	this.instance_54.setTransform(34.9,303.9,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_54).wait(110));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(312.2,207.7,528.1,304.7);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;