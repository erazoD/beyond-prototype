(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 640,
	height: 570,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib._03seda = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8CD1E6").s().p("ACACvQgDgCAAgEQAAjCiAhXQgpgbgxgNQgYgGgRgCQgDAAgCgDQgCgCAAgEQAAgJAJABQATACAaAHQAyAOApAcQBOA0AiBYQAaBFAABWQAAAEgCACQgDADgDAAQgDAAgDgDg");
	this.shape.setTransform(0,-9.3,0.909,0.909);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C9EBE").s().p("AgpAqQgRgRAAgZQAAgXARgRQASgSAXAAQAYAAASASQARARAAAXQAAAZgRARQgSARgYAAQgXAAgSgRg");
	this.shape_1.setTransform(0.1,6,0.909,0.909);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#8CD1E6").s().p("AhkBmQgrgqAAg8QAAg6ArgrQAqgqA6AAQA7AAArAqQAqArAAA6QAAA8gqAqQgrAqg7AAQg6AAgqgqg");
	this.shape_2.setTransform(0.1,6,0.909,0.909);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ah6DWIhPhNIAAkRIBPhOID2AAIBOBOIAAERIhOBNg");
	this.shape_3.setTransform(0,6,0.909,0.909);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.3,-25.5,36.8,51.2);


(lib._03listerine = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2B657A").s().p("AhfBrIAAjWIC/AAIAADWg");
	this.shape.setTransform(0,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgtA6IAAgIIAHAAIAAgEIgHAAIAAhnIBbAAIAABnIgHAAIAAAEIAHAAIAAAIg");
	this.shape_1.setTransform(0,-23.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#8AD1E7").s().p("AhoDuIgdgcIAAhGIAQgUIAAjgIgQgQIAAhHIAqgiIA8AAIAAgMIA/AAIAAAMIA8AAIAqAiIAABHIgQAQIAADgIAQAUIAABGIgdAcg");
	this.shape_2.setTransform(0,5.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.4,-29.7,26.9,59.5);


(lib._03franjaoscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AiyN5IAA7xIFlAAIAAbxg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-88.9,35.9,177.9);


(lib._03franjaclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AiyMaIAA4zIFlAAIAAYzg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-79.4,36,158.9);


(lib._03flechaoscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgzCkIgMlJIB/AAIgUFLg");
	this.shape.setTransform(0.8,-5.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AhXhLICvAAIhYCXg");
	this.shape_1.setTransform(0,15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.8,-22.6,17.6,45.2);


(lib._03flechaclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AgzGZIgMszIB/AAIgUM1g");
	this.shape.setTransform(0.2,-5.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AhXhLICvAAIhYCXg");
	this.shape_1.setTransform(0,39.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.8,-46.9,17.7,93.8);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBfAABEBEQBEBEAABeQAABfhEBEQhEBEhfAAQheAAhEhEg");
	this.shape.setTransform(26.5,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3.4,0,46.3,46.3);


(lib.Path_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBfAABEBEQBEBEAABeQAABfhEBEQhEBEhfAAQheAAhEhEg");
	this.shape_1.setTransform(23.1,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,46.3,46.3);


(lib._03cepillo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8CD1E6").s().p("AgmBwIAAinIA1g3IAYAAIAAARIgRAAIgrAtIAACgg");
	this.shape.setTransform(-7.6,-15.7,0.835,0.835);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("Ag7FOIAApSIAvhJIBIAAIAAKbg");
	this.shape_1.setTransform(-7.2,0,0.835,0.835);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgFQADgEAHAAICZAAQAHAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_2.setTransform(4.6,6.7,0.835,0.835);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgFAFgDQADgFAHAAICZAAQAHAAAEAFQAEADAAAFQAAAFgEAFQgEAEgHAAg");
	this.shape_3.setTransform(4.6,0.7,0.835,0.835);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_4.setTransform(4.6,-5.4,0.835,0.835);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_5.setTransform(4.6,-11.4,0.835,0.835);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgHAAg");
	this.shape_6.setTransform(4.6,-17.5,0.835,0.835);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgFAFgEQADgEAHAAICZAAQAHAAAEAEQAEAEAAAFQAAAFgEAFQgEAEgHAAg");
	this.shape_7.setTransform(4.6,9.8,0.835,0.835);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgHAAg");
	this.shape_8.setTransform(4.6,3.7,0.835,0.835);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgFQADgEAHAAICZAAQAHAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_9.setTransform(4.6,-2.4,0.835,0.835);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgHAAg");
	this.shape_10.setTransform(4.6,-8.4,0.835,0.835);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgFQADgEAHAAICZAAQAHAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_11.setTransform(4.6,-14.4,0.835,0.835);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgFAFgEQADgEAHAAICZAAQAHAAAEAEQAEAEAAAFQAAAGgEAEQgEAEgHAAg");
	this.shape_12.setTransform(4.6,-20.5,0.835,0.835);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.2,-27.9,24.5,55.9);


(lib._03baselinelp = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AgiBBIAAiBIApAAQAFAAAGACQAFACAEADQAEAEACAFQACAGAAAHIAAAaQAAAGgCAEQgCAFgEAEQgEAEgFACQgGACgFAAIgLAAIAAAvgAgEgDIAFAAQAGAAAAgFIAAgaQAAgGgGAAIgFAAg");
	this.shape.setTransform(34.6,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AgdBBIAAiBIAdAAIAABnIAfAAIAAAag");
	this.shape_1.setTransform(27.1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7BD7F8").s().p("AgfBBIAAiBIA+AAIAAAZIgfAAIAAAbIATAAIAAAVIgTAAIAAAdIAfAAIAAAbg");
	this.shape_2.setTransform(17,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7BD7F8").s().p("AANBBIgQgwIgIgRIAABBIgaAAIAAiBIAaAAIAQAxIAHAQIAAhBIAaAAIAACBg");
	this.shape_3.setTransform(8.7,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7BD7F8").s().p("AgNBBIAAiBIAbAAIAACBg");
	this.shape_4.setTransform(2.1,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7BD7F8").s().p("AgdBBIAAiBIAdAAIAABnIAfAAIAAAag");
	this.shape_5.setTransform(-3.3,-0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#7BD7F8").s().p("AgeBBIAAiBIA+AAIAAAZIghAAIAAAbIAUAAIAAAVIgUAAIAAAdIAhAAIAAAbg");
	this.shape_6.setTransform(-10.6,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#7BD7F8").s().p("AgOBBQgHgCgFgEQgEgFgDgGQgCgGAAgIIAAgKIAaAAIAAAKQAAAEACADQADADAEAAQADAAACgDQACgCAAgFQAAgHgFgGIgKgMIgJgJIgJgHIgGgLQgCgFgBgHQABgHACgHQACgHAFgFQAFgFAGgDQAIgDAHAAQAHAAAHADQAGACAEAFQAFAEACAGQACAHABAHIAAAIIgbAAIAAgJQAAgEgCgCQgCgDgDAAQgCAAgCADQgCACgBAEQABAEABADIAEAGIADAGIAHAGIAKAJIAIAHIAGALQACAGAAAHQAAAIgCAGQgDAHgFAFQgEAFgHADQgHADgIAAQgHAAgHgDg");
	this.shape_7.setTransform(-18.4,-0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#7BD7F8").s().p("AAKBBIgDgUIgQAAIgCAUIgcAAIAYiBIAhAAIAVCBgAgFAXIAJAAIgEgug");
	this.shape_8.setTransform(-26.5,-0.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#7BD7F8").s().p("AgiBBIAAiBIApAAQAGAAAFACIAJAEQADAEADAFQACAFAAAHIAAARIgBAGIgCAHIgFAEQgCADgFAAQAJABADAGQADAEAAAIIAAATQAAAHgCAGQgDAEgDADQgEAEgFABQgFACgGAAgAgEAqIAFAAQAGAAAAgHIAAgTQAAgGgGAAIgFAAgAgEgJIAFAAQAGAAAAgGIAAgTQAAgGgGAAIgFAAg");
	this.shape_9.setTransform(-34.5,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40.7,-11.7,81.4,23.7);


(lib._03baselinelmg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgKANIAAgZIAVAAIAAAZg");
	this.shape.setTransform(47.4,4.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgRBBQgGgCgFgFQgEgEgCgGQgCgGgBgHIAAhFQAAgGADgGQADgGAFgFQAFgEAHgDQAHgDAHAAQAIAAAGADQAHADAGAEQAEAFADAGQACAFABAHIAAATIgdAAIAAgTQAAgDgCgCQgCgCgEAAQgCAAgCACQgCACAAADIAABFQAAADACACQACACACAAQAEAAACgCQADgCAAgDIAAgQIgJAAIAAgTIAmAAIAABBIgSAAIgDgLQgDAHgGADQgGAEgGAAQgHAAgGgDg");
	this.shape_1.setTransform(41.6,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgKANIAAgZIAVAAIAAAZg");
	this.shape_2.setTransform(32.8,4.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AAWBBIAAhWIgFAeIgKA4IgRAAIgJg4IgFgeIAABWIgaAAIAAiBIAnAAIAKAzIABAWIADgWIAJgzIAnAAIAACBg");
	this.shape_3.setTransform(25.4,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#256379").s().p("AgeBBIAAiBIAeAAIAABnIAeAAIAAAag");
	this.shape_4.setTransform(16.4,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#256379").s().p("AgeBBIAAiBIA9AAIAAAZIggAAIAAAbIAVAAIAAAVIgVAAIAAAdIAgAAIAAAbg");
	this.shape_5.setTransform(6.3,-0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#256379").s().p("AANBBIgQgwIgIgRIAABBIgaAAIAAiBIAaAAIAQAxIAHAQIAAhBIAaAAIAACBg");
	this.shape_6.setTransform(-2,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#256379").s().p("AgOBBIAAiBIAdAAIAACBg");
	this.shape_7.setTransform(-8.6,-0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#256379").s().p("AgeBBIAAiBIAeAAIAABnIAeAAIAAAag");
	this.shape_8.setTransform(-14,-0.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#256379").s().p("AgfBBIAAiBIA+AAIAAAZIgfAAIAAAbIATAAIAAAVIgTAAIAAAdIAfAAIAAAbg");
	this.shape_9.setTransform(-21.3,-0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#256379").s().p("AgNBBQgIgCgEgEQgFgFgCgGQgDgGAAgIIAAgKIAaAAIAAAKQAAAEADADQACADAEAAQADAAACgDQACgCAAgFQAAgHgFgGIgLgMIgJgJIgIgHIgGgLQgCgFAAgHQAAgHACgHQADgHAEgFQAEgFAIgDQAHgDAHAAQAHAAAGADQAHACAEAFQAEAEADAGQACAHAAAHIAAAIIgaAAIAAgJQAAgEgCgCQgCgDgDAAQgBAAgDADQgDACABAEQgBAEACADIAEAGIADAGIAIAGIAJAJIAIAHIAGALQACAGAAAHQAAAIgDAGQgCAHgEAFQgGAFgGADQgHADgIAAQgHAAgGgDg");
	this.shape_10.setTransform(-29.2,-0.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#256379").s().p("AAJBBIgCgUIgQAAIgCAUIgbAAIAWiBIAhAAIAXCBgAgFAXIAKAAIgFgug");
	this.shape_11.setTransform(-37.2,-0.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#256379").s().p("AgiBBIAAiBIAqAAQAFAAAFACIAJAEQADAEADAFQACAFAAAHIAAARIAAAGIgDAHIgEAEQgDADgFAAQAJABADAGQADAEAAAIIAAATQAAAHgCAGQgDAEgDADQgEAEgFABQgFACgFAAgAgEAqIAGAAQAFAAAAgHIAAgTQAAgGgFAAIgGAAgAgEgJIAGAAQAFAAAAgGIAAgTQAAgGgFAAIgGAAg");
	this.shape_12.setTransform(-45.3,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-11.7,102.7,23.7);


(lib.Path_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7BD7F8").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBfAABEBEQBEBEAABeQAABfhEBEQhEBEhfAAQheAAhEhEg");
	this.shape_2.setTransform(23.2,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,46.3,46.3);


(lib.Path_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AiJCLQg6g6AAhRQAAhQA6g5QA5g6BQAAQBRAAA6A6QA5A5AABQQAABRg5A6Qg6A5hRAAQhQAAg5g5g");
	this.shape_3.setTransform(19.7,19.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,39.3,39.3);


(lib._032 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPA5IAAgoIgnAAIAAghIAnAAIAAgnIAfAAIAAAnIAnAAIAAAhIgnAAIAAAog");
	this.shape.setTransform(0,-1.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.4,-21.1,17.1,42.5);


(lib._031 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPA5IAAgoIgnAAIAAghIAnAAIAAgnIAfAAIAAAnIAnAAIAAAhIgnAAIAAAog");
	this.shape.setTransform(0,-1.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.4,-21.1,17.1,42.5);


(lib._02seda = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8CD1E6").s().p("ACACvQgDgCAAgEQAAjCiAhXQgpgbgxgNQgYgGgRgCQgDAAgCgDQgCgCAAgEQAAgJAJABQATACAaAHQAyAOApAcQBOA0AiBYQAaBFAABWQAAAEgCACQgDADgDAAQgDAAgDgDg");
	this.shape.setTransform(0,-10.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C9EBE").s().p("AgpAqQgRgRAAgZQAAgXARgRQASgSAXAAQAYAAASASQARARAAAXQAAAZgRARQgSARgYAAQgXAAgSgRg");
	this.shape_1.setTransform(0,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#8CD1E6").s().p("AhkBmQgrgqAAg8QAAg6ArgrQAqgqA6AAQA7AAArAqQAqArAAA6QAAA8gqAqQgrAqg7AAQg6AAgqgqg");
	this.shape_2.setTransform(0,6.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ah6DWIhPhNIAAkRIBPhOID2AAIBOBOIAAERIhOBNg");
	this.shape_3.setTransform(0,6.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-20.2,-28.1,40.5,56.3);


(lib._02franjaoscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AiyRLMAAAgiVIFlAAMAAAAiVg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-110,36,220);


(lib._02franjaclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AiyWAMAAAgr/IFlAAMAAAAr/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-140.8,35.9,281.7);


(lib.Path_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#256379").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBgAABEBEQBDBEAABeQAABfhDBEQhEBEhgAAQheAAhEhEg");
	this.shape_4.setTransform(26.5,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3.4,0,46.3,46.3);


(lib.Path_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7BD7F8").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBfAABEBEQBEBEAABeQAABfhEBEQhEBEhfAAQheAAhEhEg");
	this.shape_5.setTransform(23.2,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,46.3,46.4);


(lib._02cepillo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8CD1E6").s().p("AgmBwIAAinIA1g3IAYAAIAAARIgRAAIgrAtIAACgg");
	this.shape.setTransform(-9,-18.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("Ag7FOIAApSIAvhJIBIAAIAAKbg");
	this.shape_1.setTransform(-8.6,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgFQADgEAHAAICZAAQAHAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_2.setTransform(5.5,8.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgFAFgDQADgFAHAAICZAAQAHAAAEAFQAEADAAAFQAAAFgEAFQgEAEgHAAg");
	this.shape_3.setTransform(5.5,0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_4.setTransform(5.5,-6.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_5.setTransform(5.5,-13.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgHAAg");
	this.shape_6.setTransform(5.5,-20.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgFAFgEQADgEAHAAICZAAQAHAAAEAEQAEAEAAAFQAAAFgEAFQgEAEgHAAg");
	this.shape_7.setTransform(5.5,11.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgHAAg");
	this.shape_8.setTransform(5.5,4.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgFQADgEAHAAICZAAQAHAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_9.setTransform(5.5,-2.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgHAAg");
	this.shape_10.setTransform(5.5,-10);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgFQADgEAHAAICZAAQAHAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_11.setTransform(5.5,-17.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgFAFgEQADgEAHAAICZAAQAHAAAEAEQAEAEAAAFQAAAGgEAEQgEAEgHAAg");
	this.shape_12.setTransform(5.5,-24.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.6,-33.5,29.4,67);


(lib._02 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPA5IAAgoIgnAAIAAghIAnAAIAAgnIAfAAIAAAnIAnAAIAAAhIgnAAIAAAog");
	this.shape.setTransform(0.1,-1.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.3,-21.1,17,42.5);


(lib._01franjaoscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AiyTdMAAAgm5IFlAAMAAAAm5g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-124.5,35.9,249.1);


(lib._01franjaclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AiyY1MAAAgxpIFlAAMAAAAxpg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-158.9,35.9,317.8);


(lib.Path_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#256379").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBgAABEBEQBDBEAABeQAABfhDBEQhEBEhgAAQheAAhEhEg");
	this.shape_6.setTransform(26.5,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3.4,0,46.3,46.3);


(lib.Path_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#7BD7F8").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBfAABEBEQBEBEAABeQAABfhEBEQhEBEhfAAQheAAhEhEg");
	this.shape_7.setTransform(23.2,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,46.3,46.4);


(lib._01cepillo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8CD1E6").s().p("AgmBwIAAioIA1g2IAYAAIAAARIgRAAIgrAtIAACgg");
	this.shape.setTransform(-9.3,-18.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("Ag7FOIAApQIArhLIBLAAIAAKbg");
	this.shape_1.setTransform(-8.4,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgFAFgDQAEgFAGAAICaAAQAGAAAEAFQAEADAAAFQAAAFgEAFQgEAEgGAAg");
	this.shape_2.setTransform(5.2,8.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_3.setTransform(5.2,1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_4.setTransform(5.2,-6.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_5.setTransform(5.2,-13.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_6.setTransform(5.2,-20.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_7.setTransform(5.2,11.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_8.setTransform(5.2,4.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_9.setTransform(5.2,-2.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_10.setTransform(5.2,-9.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_11.setTransform(5.2,-17);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_12.setTransform(5.2,-24.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.4,-33.5,28.9,67);


(lib._00indicedeplaca = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AAJBBIgCgUIgQAAIgCAUIgbAAIAWiBIAhAAIAXCBgAgFAXIAKAAIgFgug");
	this.shape.setTransform(47,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AgMBBQgHgCgFgFQgGgFgCgGQgEgHAAgIIAAg/QAAgIAEgGQACgHAGgFQAFgFAHgCQAHgDAHAAQAHAAAHADQAGACAFAEQAEAFADAHQADAGAAAHIAAARIgbAAIAAgRQAAgEgDgBQgCgDgDAAQgCAAgDADQgCABAAAEIAABDQAAAEACACQADACACAAQADAAACgCQADgCAAgEIAAgRIAbAAIAAARQAAAHgDAGQgDAHgEAFQgFAEgGACQgHADgHAAQgHAAgHgDg");
	this.shape_1.setTransform(39.1,-0.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7BD7F8").s().p("AAJBBIgBgUIgQAAIgDAUIgbAAIAWiBIAhAAIAWCBgAgFAXIAKAAIgFgug");
	this.shape_2.setTransform(31.1,-0.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7BD7F8").s().p("AgeBBIAAiBIAeAAIAABnIAfAAIAAAag");
	this.shape_3.setTransform(23.8,-0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7BD7F8").s().p("AgiBBIAAiBIAoAAQAHAAAFACQAFACAEAEQADADADAGQACAFAAAHIAAAaQAAAGgCAEQgDAFgDAEQgEAEgFACQgFABgHABIgKAAIAAAvgAgEgCIAGAAQAFgBAAgFIAAgaQAAgGgFAAIgGAAg");
	this.shape_4.setTransform(16.2,-0.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7BD7F8").s().p("AgeBBIAAiBIA+AAIAAAaIggAAIAAAbIATAAIAAAUIgTAAIAAAdIAgAAIAAAbg");
	this.shape_5.setTransform(5.6,-0.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#7BD7F8").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_6.setTransform(-2.5,-0.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#7BD7F8").s().p("AgeBBIAAiBIA+AAIAAAaIghAAIAAAbIAVAAIAAAUIgVAAIAAAdIAhAAIAAAbg");
	this.shape_7.setTransform(-13.2,-0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#7BD7F8").s().p("AgMBBQgIgCgFgFQgFgFgDgGQgCgHAAgIIAAg/QAAgIACgGQADgHAFgFQAFgFAIgCQAHgDAGAAQAJAAAGADQAGACAFAEQAEAFADAHQADAGgBAHIAAARIgaAAIAAgRQAAgEgCgBQgCgDgFAAQgCAAgCADQgCABAAAEIAABDQAAAEACACQACACACAAQAFAAACgCQACgCAAgEIAAgRIAaAAIAAARQABAHgDAGQgDAHgEAFQgFAEgGACQgGADgJAAQgGAAgHgDg");
	this.shape_8.setTransform(-21.1,-0.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#7BD7F8").s().p("AgNBBIAAiBIAbAAIAACBg");
	this.shape_9.setTransform(-27.5,-0.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#7BD7F8").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_10.setTransform(-33.9,-0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#7BD7F8").s().p("AANBBIgQgwIgIgRIAABBIgaAAIAAiBIAaAAIAQAxIAHAQIAAhBIAaAAIAACBg");
	this.shape_11.setTransform(-42.7,-0.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#7BD7F8").s().p("AgRBSIAAiBIAcAAIAACBgAgJg2IAHgbIAUAAIgQAbg");
	this.shape_12.setTransform(-49,-2.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.4,-11.6,106.7,23.7);


(lib._00indicedegingivitis = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgOBBQgGgCgFgEQgFgFgDgGQgCgGAAgIIAAgKIAaAAIAAAKQAAAEADADQACADAEAAQADAAACgDQACgCAAgFQAAgHgFgGIgKgMIgJgJIgJgHIgGgLQgDgFABgHQgBgHADgHQADgHAEgFQAFgFAGgDQAIgDAHAAQAHAAAGADQAHACAEAFQAEAEADAGQADAHAAAHIAAAIIgbAAIAAgJQAAgEgCgCQgCgDgDAAQgCAAgCADQgCACgBAEQABAEABADIAEAGIADAGIAHAGIAKAJIAIAHIAGALQACAGAAAHQAAAIgCAGQgDAHgEAFQgGAFgGADQgHADgIAAQgHAAgHgDg");
	this.shape.setTransform(94.8,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgOBBIAAiBIAdAAIAACBg");
	this.shape_1.setTransform(88.7,-0.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOBBIAAhnIgVAAIAAgaIBHAAIAAAaIgVAAIAABng");
	this.shape_2.setTransform(82.7,-0.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgNBBIAAiBIAbAAIAACBg");
	this.shape_3.setTransform(76.8,-0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#256379").s().p("AgNBBIgaiBIAeAAIAJBOIAMhOIAcAAIgaCBg");
	this.shape_4.setTransform(70.6,-0.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#256379").s().p("AgNBBIAAiBIAbAAIAACBg");
	this.shape_5.setTransform(64.4,-0.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#256379").s().p("AgRBBQgGgCgFgFQgEgEgDgGQgCgGAAgHIAAhFQABgGACgGQADgGAFgFQAFgEAHgDQAHgDAHAAQAIAAAGADQAIADAEAEQAFAFADAGQADAFAAAHIAAATIgeAAIAAgTQAAgDgBgCQgCgCgEAAQgCAAgCACQgCACAAADIAABFQAAADADACQACACABAAQAEAAACgCQACgCAAgDIAAgQIgIAAIAAgTIAmAAIAABBIgRAAIgEgLQgDAHgHADQgGAEgEAAQgIAAgGgDg");
	this.shape_6.setTransform(58,-0.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#256379").s().p("AANBBIgQgwIgIgRIAABBIgaAAIAAiBIAaAAIAQAxIAHAQIAAhBIAaAAIAACBg");
	this.shape_7.setTransform(49.2,-0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#256379").s().p("AgNBBIAAiBIAbAAIAACBg");
	this.shape_8.setTransform(42.6,-0.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#256379").s().p("AgRBBQgHgCgEgFQgEgEgDgGQgCgGAAgHIAAhFQABgGACgGQADgGAFgFQAFgEAHgDQAHgDAHAAQAIAAAHADQAGADAFAEQAFAFADAGQACAFAAAHIAAATIgdAAIAAgTQABgDgCgCQgCgCgEAAQgCAAgCACQgCACAAADIAABFQAAADADACQACACABAAQAEAAACgCQADgCgBgDIAAgQIgIAAIAAgTIAlAAIAABBIgQAAIgEgLQgDAHgHADQgFAEgFAAQgIAAgGgDg");
	this.shape_9.setTransform(36.2,-0.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#256379").s().p("AgeBBIAAiBIA+AAIAAAaIghAAIAAAbIAVAAIAAAUIgVAAIAAAdIAhAAIAAAbg");
	this.shape_10.setTransform(25.5,-0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#256379").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_11.setTransform(17.4,-0.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#256379").s().p("AgNBBQgHgCgFgFQgGgEgCgHQgDgGAAgIIAAhCQAAgIADgGQACgGAGgEQAFgFAHgCQAGgDAHAAQAHAAAHADQAHACAFAFQAFAEADAGQADAGAAAIIAABCQAAAIgDAGQgDAHgFAEQgFAFgHACQgHADgHAAQgHAAgGgDgAgDgoQgDACAAADIAABGQAAADADACQACACABAAQAAAAABAAQABAAAAgBQABAAAAAAQABAAAAgBQACgCAAgDIAAhGQAAgDgCgCQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAgCABg");
	this.shape_12.setTransform(6,-0.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#256379").s().p("AgOBBIAAiBIAdAAIAACBg");
	this.shape_13.setTransform(-0.4,-0.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#256379").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_14.setTransform(-6.8,-0.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#256379").s().p("AgeBBIAAiBIA+AAIAAAaIggAAIAAAbIATAAIAAAUIgTAAIAAAdIAgAAIAAAbg");
	this.shape_15.setTransform(-14.7,-0.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#256379").s().p("AAWBBIAAhWIgFAeIgKA4IgRAAIgJg4IgFgeIAABWIgaAAIAAiBIAnAAIAKAzIABAVIADgVIAJgzIAnAAIAACBg");
	this.shape_16.setTransform(-24.4,-0.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#256379").s().p("AgNBBQgHgCgFgFQgGgEgCgHQgDgGAAgIIAAhCQAAgIADgGQACgGAGgEQAFgFAHgCQAGgDAHAAQAHAAAHADQAHACAFAFQAFAEADAGQADAGAAAIIAABCQAAAIgDAGQgDAHgFAEQgFAFgHACQgHADgHAAQgHAAgGgDgAgDgoQgDACAAADIAABGQAAADADACQACACABAAQAAAAABAAQABAAAAgBQABAAAAAAQABAAAAgBQACgCAAgDIAAhGQAAgDgCgCQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAgCABg");
	this.shape_17.setTransform(-34.5,-0.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#256379").s().p("AAMBBQAAAAgBAAQAAAAgBAAQAAgBAAAAQgBAAAAgBIgCgFIgBgGIgBgHIAAgYIgBgFQgCgCgCAAIgGAAIAAAzIgeAAIAAiBIAoAAQAFAAAGACQAFACAEADQAEAEADAFQACAFAAAHIAAAWQAAAGgDAEQgDAEgGACQAGADADAEQAEAFAAAFIAAAYIABALIACAKIAAABgAgGgHIAGAAQAEABAAgHIAAgVQAAgGgEAAIgGAAg");
	this.shape_18.setTransform(-42.7,-0.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#256379").s().p("AgiBBIAAiBIApAAQAGAAAFACQAFACAEAEQAEADACAGQACAFAAAHIAAAaQAAAGgCAEQgCAFgEAEQgEAEgFACQgFABgGABIgLAAIAAAvgAgEgCIAFAAQAGgBAAgFIAAgaQAAgGgGAAIgFAAg");
	this.shape_19.setTransform(-51,-0.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#256379").s().p("AgfBBIAAiBIA+AAIAAAaIggAAIAAAbIAVAAIAAAUIgVAAIAAAdIAgAAIAAAbg");
	this.shape_20.setTransform(-61.6,-0.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#256379").s().p("AgMBBQgHgCgGgFQgFgFgDgGQgDgHAAgIIAAg/QAAgIADgGQADgHAFgFQAGgFAHgCQAHgDAGAAQAIAAAHADQAGACAFAEQAEAFADAHQACAGAAAHIAAARIgaAAIAAgRQAAgEgDgBQgBgDgFAAQgCAAgCADQgCABAAAEIAABDQAAAEACACQACACACAAQAFAAABgCQADgCAAgEIAAgRIAaAAIAAARQAAAHgCAGQgDAHgEAFQgFAEgGACQgHADgIAAQgGAAgHgDg");
	this.shape_21.setTransform(-69.5,-0.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#256379").s().p("AgOBBIAAiBIAcAAIAACBg");
	this.shape_22.setTransform(-75.9,-0.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#256379").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_23.setTransform(-82.3,-0.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#256379").s().p("AANBBIgQgwIgIgRIAABBIgaAAIAAiBIAaAAIAQAxIAHAQIAAhBIAaAAIAACBg");
	this.shape_24.setTransform(-91.1,-0.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#256379").s().p("AgRBSIAAiBIAcAAIAACBgAgJg2IAHgbIAUAAIgQAbg");
	this.shape_25.setTransform(-97.4,-2.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101.8,-11.6,202.8,23.7);


(lib._00flecha2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgZgdIAzAdIgzAeg");
	this.shape.setTransform(279.3,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgOAEIAAgHIAcAAIAAAHg");
	this.shape_1.setTransform(274.7,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("EApBAAEIAAgHIBFAAIAAAHgEAm0AAEIAAgHIBHAAIAAAHgEAkoAAEIAAgHIBGAAIAAAHgEAidAAEIAAgHIBGAAIAAAHgEAgQAAEIAAgHIBGAAIAAAHgAeFAEIAAgHIBGAAIAAAHgAb4AEIAAgHIBGAAIAAAHgAZtAEIAAgHIBFAAIAAAHgAXgAEIAAgHIBHAAIAAAHgAVVAEIAAgHIBFAAIAAAHgATJAEIAAgHIBGAAIAAAHgAQ8AEIAAgHIBGAAIAAAHgAOxAEIAAgHIBGAAIAAAHgAMkAEIAAgHIBGAAIAAAHgAKZAEIAAgHIBGAAIAAAHgAIMAEIAAgHIBGAAIAAAHgAGBAEIAAgHIBFAAIAAAHgAD0AEIAAgHIBHAAIAAAHgABpAEIAAgHIBFAAIAAAHgAgiAEIAAgHIBFAAIAAAHgAiuAEIAAgHIBGAAIAAAHgAk5AEIAAgHIBGAAIAAAHgAnGAEIAAgHIBGAAIAAAHgApRAEIAAgHIBGAAIAAAHgAreAEIAAgHIBGAAIAAAHgAtpAEIAAgHIBFAAIAAAHgAv2AEIAAgHIBHAAIAAAHgAyBAEIAAgHIBFAAIAAAHgA0OAEIAAgHIBHAAIAAAHgA2aAEIAAgHIBGAAIAAAHgA4lAEIAAgHIBGAAIAAAHgA6yAEIAAgHIBGAAIAAAHgA89AEIAAgHIBGAAIAAAHgA/KAEIAAgHIBGAAIAAAHgEghVAAEIAAgHIBFAAIAAAHgEgjiAAEIAAgHIBHAAIAAAHgEgltAAEIAAgHIBFAAIAAAHgEgn6AAEIAAgHIBHAAIAAAHgEgqGAAEIAAgHIBGAAIAAAHg");
	this.shape_2.setTransform(-2.3,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgOAEIAAgHIAdAAIAAAHg");
	this.shape_3.setTransform(-280.3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-281.8,-3,563.8,6.1);


(lib._00flecha1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AgZgdIAzAdIgzAeg");
	this.shape.setTransform(279.3,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AgOAFIAAgIIAcAAIAAAIg");
	this.shape_1.setTransform(274.7,0.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7BD7F8").s().p("EApBAAFIAAgIIBFAAIAAAIgEAm0AAFIAAgIIBHAAIAAAIgEAkoAAFIAAgIIBGAAIAAAIgEAidAAFIAAgIIBGAAIAAAIgEAgQAAFIAAgIIBGAAIAAAIgAeFAFIAAgIIBGAAIAAAIgAb4AFIAAgIIBGAAIAAAIgAZtAFIAAgIIBFAAIAAAIgAXgAFIAAgIIBHAAIAAAIgAVVAFIAAgIIBFAAIAAAIgATJAFIAAgIIBGAAIAAAIgAQ8AFIAAgIIBGAAIAAAIgAOxAFIAAgIIBGAAIAAAIgAMkAFIAAgIIBGAAIAAAIgAKZAFIAAgIIBGAAIAAAIgAIMAFIAAgIIBGAAIAAAIgAGBAFIAAgIIBFAAIAAAIgAD0AFIAAgIIBHAAIAAAIgABpAFIAAgIIBFAAIAAAIgAgiAFIAAgIIBFAAIAAAIgAiuAFIAAgIIBGAAIAAAIgAk5AFIAAgIIBGAAIAAAIgAnGAFIAAgIIBGAAIAAAIgApRAFIAAgIIBGAAIAAAIgAreAFIAAgIIBGAAIAAAIgAtpAFIAAgIIBFAAIAAAIgAv2AFIAAgIIBHAAIAAAIgAyBAFIAAgIIBFAAIAAAIgA0OAFIAAgIIBHAAIAAAIgA2aAFIAAgIIBGAAIAAAIgA4lAFIAAgIIBGAAIAAAIgA6yAFIAAgIIBGAAIAAAIgA89AFIAAgIIBGAAIAAAIgA/KAFIAAgIIBGAAIAAAIgEghVAAFIAAgIIBFAAIAAAIgEgjiAAFIAAgIIBHAAIAAAIgEgltAAFIAAgIIBFAAIAAAIgEgn6AAFIAAgIIBHAAIAAAIgEgqGAAFIAAgIIBGAAIAAAIg");
	this.shape_2.setTransform(-2.3,0.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7BD7F8").s().p("AgOAFIAAgIIAdAAIAAAIg");
	this.shape_3.setTransform(-280.3,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-281.8,-3,563.8,6.2);


(lib._00circulo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AhZBaQglgmAAg0QAAgzAlgmQAmglAzAAQA0AAAmAlQAlAmAAAzQAAA0glAmQgmAlg0AAQgzAAgmglg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.7,-12.7,25.5,25.5);


(lib._00circulo1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AhZBaQglgmAAg0QAAgzAlgmQAmglAzAAQA0AAAmAlQAlAmAAAzQAAA0glAmQgmAlg0AAQgzAAgmglg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.7,-12.7,25.5,25.5);


(lib._00carte2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgEd/MAAAg79IAJAAMAAAA79g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-192,1,384);


(lib._00carte1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgsCAAFIAAgJMBYFAAAIAAAJg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-282,-0.5,564,1);


(lib._0030 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgNBBQgGgCgFgFQgFgEgDgGQgCgHAAgHIAAhDQAAgIACgGQADgGAFgFQAFgEAGgCQAGgDAHAAQAIAAAGADQAHACAEAEQAFAFACAGQACAGABAIIAABDQgBAHgCAHQgCAGgFAEQgEAFgHACQgGADgIAAQgHAAgGgDgAgEgnQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABIAABGQAAADACACQACACACAAQADAAACgCQABgCABgDIAAhGQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgDAAQgCAAgCACg");
	this.shape.setTransform(5.5,-0.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAUAAIAAAXQAAAFgBAEQgBADgDADIgGAFQgEACgFAAg");
	this.shape_1.setTransform(-0.2,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOBBQgGgCgEgFQgEgEgCgGQgDgGABgFIAAgNIAZAAIAAAKQAAAFACACQACACADAAQACAAACgCQACgCAAgEIAAgUQABgGgGAAIgIAAIAAgTIAIAAQAGAAgBgFIAAgTQAAgDgCgCQgCgCgCAAQgHgBAAAJIAAALIgZAAIAAgOQgBgFADgGQACgFAEgFQAEgEAGgDQAGgDAIAAQAGAAAGACQAHADAEAEQAFAFADAFQACAGAAAHIAAAOQAAAIgEAGQgDAGgIABQAHABAEAFQAEAFAAAKIAAAPQAAAHgCAGQgDAGgFAEQgEAFgHACQgGACgGAAQgIAAgGgDg");
	this.shape_2.setTransform(-5.7,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.5,-11.7,23.4,23.7);


(lib._00273 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgOBBQgGgDgEgEQgEgEgCgGQgCgGgBgFIAAgNIAaAAIAAAKQAAAFACABQACADADAAQACAAADgDQACgBAAgEIAAgUQgBgFgFgBIgIAAIAAgSIAIAAQAFAAABgGIAAgTQAAgEgCgCQgDgCgCAAQgHAAAAAJIAAAKIgaAAIAAgMQABgGACgGQACgFAEgFQAEgEAGgDQAGgDAIAAQAGAAAGADQAHACAFAEQAFAEACAGQACAGABAHIAAANQAAAJgFAGQgEAGgHABQAHAAAEAHQAFAFAAAIIAAAQQgBAHgCAGQgCAGgFAEQgFAEgHACQgGADgGAAQgIAAgGgDg");
	this.shape.setTransform(8.6,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgXBBQAAgOADgPQACgPAFgNQAEgMAFgMQAEgNAGgIIgnAAIAAgbIBDAAIAAAMQgIAOgFAOQgFAPgDAMQgEAPgCAPIgCAgg");
	this.shape_1.setTransform(1.1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQgBAFgBAEQgCADgDADIgFAFQgDACgGAAg");
	this.shape_2.setTransform(-4.4,5.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AghBDIAAgHIABgPIAFgNQADgHAFgIIAMgRIAHgIIAFgJIACgHIABgHQAAgFgCgCQgCgCgEAAQgCAAgCACQgCADAAAEIAAAMIgZAAIAAgPQAAgGACgGQACgFAEgEQAEgFAGgDQAGgCAHAAQAJAAAGACQAHADAEAFQAEAFACAGQACAGAAAIIgBALIgEAKQgCAHgFADIgMAOIgHAJIgDAHIgBAGIAAAFIAiAAIAAAZg");
	this.shape_3.setTransform(-10,-0.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7BD7F8").s().p("Ah+B/Qg1g1AAhKQAAhJA1g1QA1g1BJAAQBKAAA1A1QA1A1AABJQAABKg1A1Qg1A1hKAAQhJAAg1g1g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18,-18,36.1,36.1);


(lib._0025 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgOBAQgGgDgEgEQgEgFgCgFQgDgGAAgFIAAgNIAaAAIAAAJQAAAFACACQACADADgBQADABACgDQACgCAAgEIAAgdQAAgDgCgBQgDgBgCAAQAAAAAAAAQAAAAgBAAQAAABgBAAQgBAAAAAAQgDABAAACIgYgDIAEhCIA7AAQgBAOgEAFQgDAHgHAAIgWAAIgCAbQAEgEADgDQAFgCAHAAQAJAAAFAHQADADACAEQABAEABAEIAAAiQgBAHgCAHQgDAFgEAFQgFAEgGACQgHACgGAAQgIAAgGgCg");
	this.shape.setTransform(5.5,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQgBAFgBAEQgCADgDADIgFAFQgDACgGAAg");
	this.shape_1.setTransform(0.1,5.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AghBDIAAgHIABgPIAFgNQADgHAFgIIAMgRIAHgIIAFgJIACgHIABgHQAAgFgCgCQgCgCgEAAQgCAAgCACQgCADAAAEIAAAMIgZAAIAAgPQAAgGACgGQACgFAEgEQAEgFAGgDQAGgCAHAAQAJAAAGACQAHADAEAFQAEAFACAGQACAGAAAIIgBALIgEAKQgCAHgFADIgMAOIgHAJIgDAHIgBAGIAAAFIAiAAIAAAZg");
	this.shape_2.setTransform(-5.5,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.3,-11.7,22.9,23.7);


(lib._0021 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMBBIAAh1QAGAAADgDQADgEAAgFIANAAIAACBg");
	this.shape.setTransform(5.1,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQAAAFgCAEQgCADgCADIgGAFQgDACgGAAg");
	this.shape_1.setTransform(0.9,5.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AghBDIAAgHIABgPIAFgNQADgHAFgIIAMgRIAHgIIAFgJIACgHIABgHQAAgFgCgCQgCgCgEAAQgCAAgCACQgCADAAAEIAAAMIgZAAIAAgPQAAgGACgGQACgFAEgEQAEgFAGgDQAGgCAHAAQAJAAAGACQAHADAEAFQAEAFACAGQACAGAAAIIgBALIgEAKQgCAHgFADIgMAOIgHAJIgDAHIgBAGIAAAFIAiAAIAAAZg");
	this.shape_2.setTransform(-4.7,-0.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("Ah+B/Qg1g1AAhKQAAhKA1g0QA1g1BJAAQBKAAA1A1QA1A0AABKQAABKg1A1Qg1A1hKAAQhJAAg1g1g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18,-18,36.1,36.1);


(lib._0015 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgOA/QgGgCgEgFQgEgEgCgGQgDgFAAgGIAAgMIAaAAIAAAKQAAAEACACQACADADAAQACAAADgDQABgCAAgDIAAgeQAAgDgBgBQgDgBgCAAQAAAAAAAAQAAAAgBAAQAAABgBAAQgBAAAAAAQgDABAAACIgYgDIADhBIA8AAQgBAMgEAHQgDAFgHAAIgWAAIgCAcQAEgFADgCQAFgCAHAAQAJAAAFAGQAEADABAFQABAEABAEIAAAjQgBAGgCAGQgDAHgEADQgFAFgGACQgHACgGAAQgIAAgGgDg");
	this.shape.setTransform(4.3,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQgBAFgBAEQgCADgDADIgFAFQgDACgGAAg");
	this.shape_1.setTransform(-1.2,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgMBBIAAh1QAGAAADgDQADgEAAgFIANAAIAACBg");
	this.shape_2.setTransform(-5.4,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10,-11.7,20.3,23.7);


(lib._0010 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgNBBQgGgCgFgFQgFgEgCgGQgDgHAAgHIAAhDQAAgIADgGQACgGAFgFQAFgEAGgCQAGgDAHAAQAIAAAGADQAGACAFAEQAEAFADAGQADAGgBAIIAABDQABAHgDAHQgDAGgEAEQgFAFgGACQgGADgIAAQgHAAgGgDgAgEgnQgBAAAAABQAAAAAAABQgBAAAAABQAAAAAAABIAABGQAAADACACQABACADAAQADAAACgCQABgCAAgDIAAhGQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgDAAQgDAAgBACg");
	this.shape.setTransform(4.3,-0.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQAAAFgCAEQgBADgDADIgGAFQgEACgFAAg");
	this.shape_1.setTransform(-1.5,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgMBBIAAh1QAGAAADgDQADgEAAgFIANAAIAACBg");
	this.shape_2.setTransform(-5.7,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.3,-11.7,20.9,23.7);


(lib._0005 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgOA/QgGgCgEgFQgEgEgCgGQgCgFgBgGIAAgMIAaAAIAAAKQAAAEACACQACADADAAQACAAACgDQADgCAAgDIAAgeQAAgDgDgBQgCgBgCAAQAAAAAAAAQAAAAgBAAQAAABgBAAQAAAAgBAAQgCABgBACIgYgDIAEhBIA7AAQgBAMgEAHQgDAFgHAAIgWAAIgCAcQAEgFADgCQAFgCAGAAQAKAAAGAGQACADACAFQACAEAAAEIAAAjQAAAGgDAGQgCAHgFADQgFAFgHACQgGACgGAAQgIAAgGgDg");
	this.shape.setTransform(5.8,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQAAAFgCAEQgBADgDADIgGAFQgEACgFAAg");
	this.shape_1.setTransform(0.3,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgNBBQgHgCgEgFQgFgEgDgGQgCgHAAgHIAAhDQAAgIACgGQADgGAFgFQAEgEAHgCQAGgDAHAAQAIAAAGADQAHACAEAEQAFAFACAGQACAGABAIIAABDQgBAHgCAHQgCAGgFAEQgEAFgHACQgGADgIAAQgHAAgGgDgAgEgnQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABIAABGQAAADACACQACACACAAQADAAACgCQABgCABgDIAAhGQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgDAAQgCAAgCACg");
	this.shape_2.setTransform(-5.4,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.5,-11.7,23.4,23.7);


(lib._0000 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgNBBQgGgCgFgFQgFgEgDgGQgCgHAAgHIAAhDQAAgIACgGQADgGAFgFQAFgEAGgCQAGgDAHAAQAIAAAGADQAHACAEAEQAFAFACAGQACAGAAAIIAABDQAAAHgCAHQgCAGgFAEQgEAFgHACQgGADgIAAQgHAAgGgDgAgEgnQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABIAABGQAAADACACQACACACAAQADAAACgCQABgCABgDIAAhGQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgDAAQgCAAgCACg");
	this.shape.setTransform(0.1,-0.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,-11.7,12.5,23.7);


(lib._03circulooscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AABBVIAAgiIgwAAIAAgaIAyhuIAgAAIAABrIANAAIAAAdIgNAAIAAAigAgQAWIARAAIAAgog");
	this.shape.setTransform(11.9,-1.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AABBVIAAgiIgwAAIAAgaIAyhuIAgAAIAABrIANAAIAAAdIgNAAIAAAigAgQAWIARAAIAAgog");
	this.shape_1.setTransform(1.6,-1.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgDAEgEACQgFACgHAAg");
	this.shape_2.setTransform(-5.6,6.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgRBVIAAiZQAJAAADgFQAFgFAAgHIARAAIAACqg");
	this.shape_3.setTransform(-11.1,-1.4);

	this.instance = new lib.Path();
	this.instance.setTransform(1.7,-1.6,1,1,0,0,0,26.5,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.169)",-3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.4,-24.8,55,54);


(lib._03circuloclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgTBWQgIgEgFgGQgGgGgCgHQgDgHAAgIIAAgRIAhAAIAAAOQAAAFADADQADADAEAAQADAAADgDQACgCABgFIAAgaQAAgIgIAAIgKAAIAAgZIAKAAQAIAAAAgHIAAgZQgBgFgCgDQgDgCgDAAQgKAAAAALIAAAOIghAAIAAgRQAAgIADgHQACgIAGgFQAFgGAIgEQAIgDALAAQAIAAAJADQAIADAHAFQAGAGADAIQADAHAAAKIAAARQAAALgFAIQgGAIgJACQAJABAFAHQAGAHAAAMIAAAUQAAAKgDAHQgDAIgGAGQgHAFgIADQgJADgIAAQgLAAgIgDg");
	this.shape.setTransform(7.7,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRBWIAAiaQAJAAADgFQAFgFAAgGIARAAIAACqg");
	this.shape_1.setTransform(-0.8,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgEAEgDACQgGACgGAAg");
	this.shape_2.setTransform(-6.3,7.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgRBWIAAiaQAJAAAEgFQADgFAAgGIASAAIAACqg");
	this.shape_3.setTransform(-11.8,-0.9);

	this.instance = new lib.Path_1();
	this.instance.setTransform(-1.4,-1.4,1,1,0,0,0,23.2,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.227)",3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.6,-24.6,54,54);


(lib._0352 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag1BMIBgifIALAIIhgCfgAATBMQgDgDgBgFQgCgEAAgGIAAghQAAgGACgFQABgEADgDQAIgHAKAAQALAAAGAHQAEADACAEQABAFAAAGIAAAhQABALgIAHQgGAHgLAAQgKAAgIgHgAAgAQQgCACgBADIAAAoQABADACADQACACADAAQAEAAACgCQACgDAAgDIAAgoQAAgDgCgCQgCgCgEAAQgDAAgCACgAgugBQgFgCgDgDQgHgHAAgLIAAghQABgGABgEQACgFADgDQADgDAFgCQAFgCAFAAQAFAAAEACIAJAFIAEAIQACAEAAAGIAAAhQAAALgGAHQgEADgFACQgEABgFAAQgFAAgFgBgAgqhCQgCACAAAEIAAAnQAAADACADQACACAEAAQADAAACgCQACgDABgDIAAgnQgBgEgCgCQgCgCgDAAQgEAAgCACg");
	this.shape.setTransform(9.6,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgoBRIAAgIQAAgKABgHQACgJADgIQAEgIAGgKIAPgUIAJgLIAGgLIADgJIABgIQAAgGgDgDQgCgDgFABQgDAAgCACQgCADAAAGIAAAPIgfAAIAAgTQAAgGADgIQACgGAFgFQAFgGAHgDQAHgEAJAAQALAAAIAEQAIADAFAGQAFAGACAHQACAIAAAIIgBAPIgEAMIgJANQgGAHgIAJIgJAMIgEAIIgCAHIAAAGIAqAAIAAAeg");
	this.shape_1.setTransform(-2.1,-1.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgRBNQgIgDgFgFQgFgGgCgGQgDgHAAgHIAAgPIAfAAIAAAMQAAAFACADQADACAEAAQADAAACgCQADgDAAgEIAAgkQAAgEgDgBQgCgCgDAAQgBAAgDACQgDABAAACIgegDIAFhQIBJAAQgCAQgEAHQgFAHgIAAIgbAAIgCAiQAEgGAEgCQAHgDAHAAQAMAAAHAIQADADACAGQACAFAAAFIAAAqQAAAJgDAHQgDAHgGAFQgGAFgIADQgIADgHAAQgKAAgHgEg");
	this.shape_2.setTransform(-11.5,-0.9);

	this.instance = new lib.Path_2();
	this.instance.setTransform(-1.4,-1.4,1,1,0,0,0,23.2,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.227)",3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.6,-24.6,54,54);


(lib._0321 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgtBBIBSiIIAJAHIhSCHgAARBBQgDgDgCgEQgBgEAAgFIAAgcQAAgFABgDQACgEADgDQAFgGAJAAQAJAAAGAGQADADACAEQABADAAAFIAAAcQAAAKgGAGQgGAFgJAAQgJAAgFgFgAAbANQgBABAAABQAAAAAAABQgBAAAAABQAAAAAAABIAAAiQAAADACACQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQADAAACgBQACgCAAgDIAAgiQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBgBQgCgBgDAAQAAAAgBAAQgBAAAAAAQgBABAAAAQgBAAAAAAgAgngBQgEgBgCgCQgGgHAAgJIAAgcQAAgFABgEQACgEADgDQACgCAEgBQAEgCAEAAQAFAAAEACIAGADQADADACAEQABAEAAAFIAAAcQAAAJgGAHIgGADQgEABgFAAQgEAAgEgBgAgjg4QgBABAAAAQgBABAAAAQAAABAAABQAAAAAAABIAAAhQAAAEACACQAAAAABAAQAAAAABABQAAAAABAAQABAAAAAAQABAAABAAQAAAAABAAQAAgBABAAQAAAAABAAQACgCAAgEIAAghQAAgBAAAAQAAgBgBgBQAAAAAAgBQgBAAAAgBQgBAAAAAAQgBgBAAAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAg");
	this.shape.setTransform(5.5,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgNBDIAAh5QAHAAADgDQADgEAAgFIAOAAIAACFg");
	this.shape_1.setTransform(-2.9,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgiBFIAAgHIABgPIAFgOQADgHAFgIIANgSIAHgIIAFgJIACgIIABgHQABgFgDgCQgCgDgEAAQgCAAgCADQgCACAAAFIAAANIgaAAIAAgQQAAgGACgGQACgGAEgEQAFgFAGgDQAGgDAHAAQAJAAAHADQAHADAEAFQAEAFACAHQACAGAAAHIgBAMIgDALIgIALIgMANIgHAKIgEAHIgBAGIAAAFIAjAAIAAAag");
	this.shape_2.setTransform(-9.7,-1.1);

	this.instance = new lib.Path_3();
	this.instance.setTransform(-1.2,-1.2,1,1,0,0,0,19.7,19.7);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.227)",3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-20.9,-20.9,46,46);


(lib._02circulooscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRBWIAAiaQAJAAAEgFQADgFAAgGIASAAIAACqg");
	this.shape.setTransform(12.4,-0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRBWQgJgDgGgFQgHgGgDgIQgDgHAAgKIAAgVQAAgJADgHQAEgIAHgCQgHgDgEgHQgDgIAAgJIAAgTQAAgKADgHQADgIAHgGQAGgFAJgDQAIgDAJAAQAKAAAJADQAIADAGAFQAGAGAEAIQADAHAAAKIAAATQAAAJgEAIQgEAHgHADQAHACAEAIQAEAHAAAJIAAAVQAAAKgDAHQgDAIgHAGQgGAFgIADQgJADgKAAQgJAAgIgDgAgGAKQgDADAAAEIAAAdQAAAFADACQADADADAAQAEAAADgDQADgCAAgFIAAgdQAAgEgDgDQgDgCgEAAQgDAAgDACgAgGg1QgDADAAAFIAAAbQAAAEADADQADADADAAQAEAAADgDQADgDAAgEIAAgbQAAgFgDgDQgDgCgEAAQgDAAgDACg");
	this.shape_1.setTransform(3.5,-0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgDAEgEACQgGACgGAAg");
	this.shape_2.setTransform(-4,7.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgQBWIAAiaQAIAAADgFQAFgFAAgGIASAAIAACqg");
	this.shape_3.setTransform(-9.5,-0.8);

	this.instance = new lib.Path_4();
	this.instance.setTransform(1.7,-1.6,1,1,0,0,0,26.5,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.169)",-3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.4,-24.8,55,54);


(lib._02circuloclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgfBWQABgTADgTQAEgUAFgSQAGgQAHgQQAFgQAIgMIgzAAIAAgiIBXAAIAAAPQgJATgHATQgGASgFARQgFAUgCATQgCAUAAAXg");
	this.shape.setTransform(12,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgTBWQgIgEgFgGQgGgGgCgHQgDgHAAgIIAAgRIAhAAIAAAOQAAAFADADQADADAEAAQADAAADgDQACgCABgFIAAgaQAAgIgIAAIgKAAIAAgZIAKAAQAIAAAAgHIAAgZQgBgFgCgDQgDgCgDAAQgKAAAAALIAAAOIghAAIAAgRQAAgIADgHQACgIAGgFQAFgGAIgEQAIgDALAAQAIAAAJADQAIADAHAFQAGAGADAIQADAHAAAKIAAARQAAALgFAIQgGAIgJACQAJABAFAHQAGAHAAAMIAAAUQAAAKgDAHQgDAIgGAGQgHAFgIADQgJADgIAAQgLAAgIgDg");
	this.shape_1.setTransform(1.8,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgDAEgEACQgGACgGAAg");
	this.shape_2.setTransform(-5.3,7.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgsBXIAAgJQAAgKACgIQACgJAEgJQAEgJAGgKIARgXIAJgLIAGgLIAEgKIABgKQAAgGgDgDQgDgDgFAAQgDAAgCAEQgDADAAAFIAAARIghAAIAAgUQAAgIADgHQACgIAGgFQAFgGAIgEQAIgDAJAAQAMAAAJADQAIAEAGAGQAFAGACAJQADAIAAAJIgCAPQgBAHgDAHQgEAIgGAGIgPARIgJANIgFAJIgCAHIAAAHIAuAAIAAAgg");
	this.shape_3.setTransform(-12.6,-1.1);

	this.instance = new lib.Path_5();
	this.instance.setTransform(-1.4,-1.4,1,1,0,0,0,23.2,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.227)",3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.6,-24.6,54,54);


(lib._01circulooscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AABBWIAAgjIgwAAIAAgaIAyhtIAgAAIAABqIANAAIAAAdIgNAAIAAAjgAgQAWIARAAIAAgpg");
	this.shape.setTransform(14.3,-1.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRBWQgJgDgGgGQgGgGgDgIQgDgIAAgLIAAhYQAAgKADgIQADgIAGgGQAGgGAJgDQAIgDAJAAQAKAAAJADQAIADAGAGQAGAGADAIQADAIAAAKIAABYQAAALgDAIQgDAIgGAGQgGAGgIADQgJADgKAAQgJAAgIgDgAgGg0QgCACAAAEIAABcQAAAEACADQADADADAAQAEAAADgDQACgDAAgEIAAhcQAAgEgCgCQgDgDgEAAQgDAAgDADg");
	this.shape_1.setTransform(3.7,-1.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgEAEgDACQgFACgHAAg");
	this.shape_2.setTransform(-3.8,6.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgsBXIAAgJQAAgKACgIQACgJAEgJQAEgJAGgKIARgXIAJgLIAGgLIAEgKIABgKQAAgGgDgDQgDgDgFAAQgDAAgCAEQgDADAAAFIAAARIghAAIAAgUQAAgIADgHQACgIAGgFQAFgGAIgEQAIgDAJAAQAMAAAJADQAIAEAGAGQAFAGACAJQADAIAAAJIgCAPQgBAHgDAHQgEAIgGAGIgPARIgJANIgFAJIgCAHIAAAHIAuAAIAAAgg");
	this.shape_3.setTransform(-11.1,-1.8);

	this.instance = new lib.Path_6();
	this.instance.setTransform(1.7,-1.6,1,1,0,0,0,26.5,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.169)",-3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.4,-24.8,55,54);


(lib._01circuloclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRBVIAAiZQAJAAAEgFQADgFAAgHIASAAIAACqg");
	this.shape.setTransform(10.8,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRBWQgJgDgGgGQgGgGgDgIQgDgJAAgKIAAhYQAAgKADgIQAEgIAFgGQAHgGAIgDQAJgDAIAAQAKAAAIADQAJAEAFAGQAFAFADAIQADAHAAAIIAAAQIggAAIAAgNQAAgGgDgDQgDgCgFAAQgDAAgDACQgCADAAAFIAAAoQADgFAFgEQAFgDAJAAQAPAAAIAIQAEAEACAFQACAEAAAHIAAAhQAAAKgDAJQgEAIgFAGQgGAGgJADQgJADgJAAQgJAAgIgDgAgGAJQgDACABAEIAAAgQgBAEADADQADACADAAQAEAAADgCQACgDAAgEIAAggQAAgEgCgCQgDgCgEAAQgDAAgDACg");
	this.shape_1.setTransform(2.2,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgDAEgEACQgGACgGAAg");
	this.shape_2.setTransform(-5.3,7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgsBXIAAgJQAAgKACgIQACgJAEgJQAEgJAGgKIARgXIAJgLIAGgLIAEgKIABgKQAAgGgDgDQgDgDgFAAQgDAAgCAEQgDADAAAFIAAARIghAAIAAgUQAAgIADgHQACgIAGgFQAFgGAIgEQAIgDAJAAQAMAAAJADQAIAEAGAGQAFAGACAJQADAIAAAJIgCAPQgBAHgDAHQgEAIgGAGIgPARIgJANIgFAJIgCAHIAAAHIAuAAIAAAgg");
	this.shape_3.setTransform(-12.6,-1.2);

	this.instance = new lib.Path_7();
	this.instance.setTransform(-1.4,-1.5,1,1,0,0,0,23.2,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.227)",3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.6,-24.7,54,54);


// stage content:
(lib.grafica_11_v1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_120 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(120).call(this.frame_120).wait(1));

	// 03 baseline lmg
	this.instance = new lib._03baselinelmg();
	this.instance.setTransform(545.9,118,1,1,0,0,0,-0.1,0.1);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(32).to({_off:false},0).wait(1).to({regX:-0.2,regY:-0.5,x:545.8,y:117.4,alpha:0.001},0).wait(1).to({y:117.5,alpha:0.006},0).wait(1).to({y:117.7,alpha:0.013},0).wait(1).to({y:118.1,alpha:0.024},0).wait(1).to({y:118.5,alpha:0.039},0).wait(1).to({y:119.1,alpha:0.058},0).wait(1).to({y:119.7,alpha:0.082},0).wait(1).to({y:120.6,alpha:0.111},0).wait(1).to({y:121.6,alpha:0.146},0).wait(1).to({y:122.8,alpha:0.187},0).wait(1).to({y:124.2,alpha:0.234},0).wait(1).to({y:125.7,alpha:0.287},0).wait(1).to({y:127.5,alpha:0.347},0).wait(1).to({y:129.3,alpha:0.411},0).wait(1).to({y:131.3,alpha:0.478},0).wait(1).to({y:133.3,alpha:0.547},0).wait(1).to({y:135.3,alpha:0.614},0).wait(1).to({y:137.1,alpha:0.678},0).wait(1).to({y:138.9,alpha:0.736},0).wait(1).to({y:140.4,alpha:0.789},0).wait(1).to({y:141.7,alpha:0.835},0).wait(1).to({y:142.9,alpha:0.874},0).wait(1).to({y:143.8,alpha:0.907},0).wait(1).to({y:144.6,alpha:0.934},0).wait(1).to({y:145.3,alpha:0.956},0).wait(1).to({y:145.8,alpha:0.973},0).wait(1).to({y:146.1,alpha:0.985},0).wait(1).to({y:146.4,alpha:0.994},0).wait(1).to({y:146.5,alpha:0.998},0).wait(1).to({regX:-0.1,regY:0.1,x:545.9,y:147.2,alpha:1},0).wait(59));

	// 03 21%
	this.instance_1 = new lib._0321();
	this.instance_1.setTransform(521.7,184.1,0.031,0.031);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(36).to({_off:false},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.26,scaleY:0.26},0).wait(1).to({scaleX:0.31,scaleY:0.31},0).wait(1).to({scaleX:0.37,scaleY:0.37},0).wait(1).to({scaleX:0.43,scaleY:0.43},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.56,scaleY:0.56},0).wait(1).to({scaleX:0.63,scaleY:0.63},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.8,scaleY:0.8},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(55));

	// 03 mascara (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_56 = new cjs.Graphics().p("EAjKAZvIAA0HIHWAAIAAUHg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(56).to({graphics:mask_graphics_56,x:272.1,y:164.7}).wait(65));

	// 03 flecha oscuro
	this.instance_2 = new lib._03flechaoscuro();
	this.instance_2.setTransform(519.6,177.6);
	this.instance_2._off = true;

	this.instance_2.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(56).to({_off:false},0).wait(2).to({y:177.8},0).wait(1).to({y:178},0).wait(1).to({y:178.4},0).wait(1).to({y:178.8},0).wait(1).to({y:179.5},0).wait(1).to({y:180.2},0).wait(1).to({y:181.2},0).wait(1).to({y:182.3},0).wait(1).to({y:183.6},0).wait(1).to({y:185.1},0).wait(1).to({y:186.8},0).wait(1).to({y:188.7},0).wait(1).to({y:190.7},0).wait(1).to({y:192.9},0).wait(1).to({y:195.1},0).wait(1).to({y:197.2},0).wait(1).to({y:199.3},0).wait(1).to({y:201.2},0).wait(1).to({y:202.8},0).wait(1).to({y:204.3},0).wait(1).to({y:205.6},0).wait(1).to({y:206.6},0).wait(1).to({y:207.5},0).wait(1).to({y:208.2},0).wait(1).to({y:208.7},0).wait(1).to({y:209.1},0).wait(1).to({y:209.4},0).wait(1).to({y:209.6},0).wait(36));

	// 03 circulo oscuro
	this.instance_3 = new lib._03circulooscuro();
	this.instance_3.setTransform(554.3,229.9,0.024,0.024);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.31,scaleY:0.31},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.43,scaleY:0.43},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.56,scaleY:0.56},0).wait(1).to({scaleX:0.62,scaleY:0.62},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.79,scaleY:0.79},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(85));

	// 03 mascara (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_4 = new cjs.Graphics().p("EgffAgWMAAAg18MBNCAAAMAAAA18g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(4).to({graphics:mask_1_graphics_4,x:291.6,y:207}).wait(117));

	// 03 franja oscuro
	this.instance_4 = new lib._03franjaoscuro();
	this.instance_4.setTransform(532.2,503);
	this.instance_4._off = true;

	this.instance_4.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).wait(1).to({y:502.8},0).wait(1).to({y:502},0).wait(1).to({y:500.7},0).wait(1).to({y:498.7},0).wait(1).to({y:496.1},0).wait(1).to({y:492.6},0).wait(1).to({y:488.4},0).wait(1).to({y:483.2},0).wait(1).to({y:477},0).wait(1).to({y:469.8},0).wait(1).to({y:461.4},0).wait(1).to({y:451.9},0).wait(1).to({y:441.3},0).wait(1).to({y:429.9},0).wait(1).to({y:417.9},0).wait(1).to({y:405.7},0).wait(1).to({y:393.8},0).wait(1).to({y:382.4},0).wait(1).to({y:372},0).wait(1).to({y:362.6},0).wait(1).to({y:354.5},0).wait(1).to({y:347.5},0).wait(1).to({y:341.6},0).wait(1).to({y:336.8},0).wait(1).to({y:332.9},0).wait(1).to({y:329.9},0).wait(1).to({y:327.7},0).wait(1).to({y:326.2},0).wait(1).to({y:325.4},0).wait(1).to({y:325.1},0).wait(87));

	// 03 baseline lp
	this.instance_5 = new lib._03baselinelp();
	this.instance_5.setTransform(471.3,27,1,1,0,0,0,-0.1,0.1);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(30).to({_off:false},0).wait(1).to({regX:0,regY:-0.5,x:471.4,y:26.4,alpha:0.001},0).wait(1).to({y:26.5,alpha:0.006},0).wait(1).to({y:26.7,alpha:0.013},0).wait(1).to({y:27.1,alpha:0.024},0).wait(1).to({y:27.5,alpha:0.039},0).wait(1).to({y:28.1,alpha:0.058},0).wait(1).to({y:28.7,alpha:0.082},0).wait(1).to({y:29.6,alpha:0.111},0).wait(1).to({y:30.6,alpha:0.146},0).wait(1).to({y:31.8,alpha:0.187},0).wait(1).to({y:33.2,alpha:0.234},0).wait(1).to({y:34.7,alpha:0.287},0).wait(1).to({y:36.5,alpha:0.347},0).wait(1).to({y:38.3,alpha:0.411},0).wait(1).to({y:40.3,alpha:0.478},0).wait(1).to({y:42.3,alpha:0.547},0).wait(1).to({y:44.3,alpha:0.614},0).wait(1).to({y:46.1,alpha:0.678},0).wait(1).to({y:47.9,alpha:0.736},0).wait(1).to({y:49.4,alpha:0.789},0).wait(1).to({y:50.7,alpha:0.835},0).wait(1).to({y:51.9,alpha:0.874},0).wait(1).to({y:52.8,alpha:0.907},0).wait(1).to({y:53.6,alpha:0.934},0).wait(1).to({y:54.3,alpha:0.956},0).wait(1).to({y:54.8,alpha:0.973},0).wait(1).to({y:55.1,alpha:0.985},0).wait(1).to({y:55.4,alpha:0.994},0).wait(1).to({y:55.5,alpha:0.998},0).wait(1).to({regX:-0.1,regY:0.1,x:471.3,y:56.2,alpha:1},0).wait(61));

	// 03 52%
	this.instance_6 = new lib._0352();
	this.instance_6.setTransform(474.3,106.9,0.022,0.022);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(34).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.56,scaleY:0.56},0).wait(1).to({scaleX:0.62,scaleY:0.62},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.79,scaleY:0.79},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(57));

	// 03 mascara (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_54 = new cjs.Graphics().p("AfaUAIAA0EIHXAAIAAUEg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(54).to({graphics:mask_2_graphics_54,x:248.1,y:128.1}).wait(67));

	// 03 flecha claro
	this.instance_7 = new lib._03flechaclaro();
	this.instance_7.setTransform(472.8,72.2);
	this.instance_7._off = true;

	this.instance_7.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(54).to({_off:false},0).wait(1).to({y:72.3},0).wait(1).to({y:72.7},0).wait(1).to({y:73.5},0).wait(1).to({y:74.6},0).wait(1).to({y:76.1},0).wait(1).to({y:78.1},0).wait(1).to({y:80.6},0).wait(1).to({y:83.5},0).wait(1).to({y:87.1},0).wait(1).to({y:91.3},0).wait(1).to({y:96.1},0).wait(1).to({y:101.6},0).wait(1).to({y:107.6},0).wait(1).to({y:114.2},0).wait(1).to({y:121.1},0).wait(1).to({y:128.1},0).wait(1).to({y:135},0).wait(1).to({y:141.6},0).wait(1).to({y:147.6},0).wait(1).to({y:152.9},0).wait(1).to({y:157.6},0).wait(1).to({y:161.7},0).wait(1).to({y:165},0).wait(1).to({y:167.8},0).wait(1).to({y:170},0).wait(1).to({y:171.8},0).wait(1).to({y:173},0).wait(1).to({y:173.9},0).wait(1).to({y:174.4},0).wait(1).to({y:174.6},0).wait(37));

	// 03 circulo claro
	this.instance_8 = new lib._03circuloclaro();
	this.instance_8.setTransform(462.3,249.2,0.022,0.022);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(2).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.56,scaleY:0.56},0).wait(1).to({scaleX:0.62,scaleY:0.62},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.79,scaleY:0.79},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(89));

	// 03 mascara (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("EgffAgWMAAAg18MBNCAAAMAAAA18g");
	mask_3.setTransform(291.6,207);

	// 03 franja claro
	this.instance_9 = new lib._03franjaclaro();
	this.instance_9.setTransform(477.9,493.5);

	this.instance_9.mask = mask_3;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1).to({y:493.3},0).wait(1).to({y:492.6},0).wait(1).to({y:491.4},0).wait(1).to({y:489.7},0).wait(1).to({y:487.3},0).wait(1).to({y:484.2},0).wait(1).to({x:478,y:480.4},0).wait(1).to({y:475.8},0).wait(1).to({y:470.3},0).wait(1).to({y:463.8},0).wait(1).to({x:478.1,y:456.3},0).wait(1).to({y:447.8},0).wait(1).to({x:478.2,y:438.4},0).wait(1).to({y:428.2},0).wait(1).to({x:478.3,y:417.5},0).wait(1).to({y:406.6},0).wait(1).to({x:478.4,y:395.9},0).wait(1).to({y:385.8},0).wait(1).to({x:478.5,y:376.5},0).wait(1).to({y:368.1},0).wait(1).to({x:478.6,y:360.8},0).wait(1).to({y:354.6},0).wait(1).to({y:349.4},0).wait(1).to({y:345},0).wait(1).to({x:478.7,y:341.6},0).wait(1).to({y:338.9},0).wait(1).to({y:336.9},0).wait(1).to({y:335.6},0).wait(1).to({y:334.8},0).wait(1).to({y:334.6},0).wait(91));

	// 03 listerine
	this.instance_10 = new lib._03listerine();
	this.instance_10.setTransform(567.2,463.6,0.018,0.018);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(8).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.56,scaleY:0.56},0).wait(1).to({scaleX:0.62,scaleY:0.62},0).wait(1).to({scaleX:0.68,scaleY:0.68},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.79,scaleY:0.79},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(83));

	// 03 +2
	this.instance_11 = new lib._032();
	this.instance_11.setTransform(541,467,0.029,0.029);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(6).to({_off:false},0).wait(1).to({regY:-1.6,scaleX:0.03,scaleY:0.03,y:466.9},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09,y:466.8},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17,y:466.7},0).wait(1).to({scaleX:0.21,scaleY:0.21,y:466.6},0).wait(1).to({scaleX:0.26,scaleY:0.26},0).wait(1).to({scaleX:0.31,scaleY:0.31,y:466.5},0).wait(1).to({scaleX:0.37,scaleY:0.37,y:466.4},0).wait(1).to({scaleX:0.43,scaleY:0.43,y:466.3},0).wait(1).to({scaleX:0.49,scaleY:0.49,y:466.2},0).wait(1).to({scaleX:0.56,scaleY:0.56,y:466.1},0).wait(1).to({scaleX:0.63,scaleY:0.63,y:466},0).wait(1).to({scaleX:0.69,scaleY:0.69,y:465.9},0).wait(1).to({scaleX:0.74,scaleY:0.74,y:465.8},0).wait(1).to({scaleX:0.8,scaleY:0.8},0).wait(1).to({scaleX:0.84,scaleY:0.84,y:465.7},0).wait(1).to({scaleX:0.88,scaleY:0.88,y:465.6},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94,y:465.5},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99,y:465.4},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,regY:0.1,scaleX:1,scaleY:1,x:541.1,y:467.1},0).wait(85));

	// 03 seda
	this.instance_12 = new lib._03seda();
	this.instance_12.setTransform(508.4,463.9,0.022,0.022);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(4).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3,x:508.5},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.56,scaleY:0.56,x:508.6,y:464},0).wait(1).to({scaleX:0.62,scaleY:0.62},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.79,scaleY:0.79,x:508.7},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.4,regY:0.2,scaleX:1,scaleY:1,x:508.8,y:464.1},0).wait(87));

	// 03 +1
	this.instance_13 = new lib._031();
	this.instance_13.setTransform(473.9,467,0.029,0.029);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(2).to({_off:false},0).wait(1).to({regY:-1.6,scaleX:0.03,scaleY:0.03,y:466.9},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09,y:466.8},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17,y:466.7},0).wait(1).to({scaleX:0.21,scaleY:0.21,y:466.6},0).wait(1).to({scaleX:0.26,scaleY:0.26},0).wait(1).to({scaleX:0.31,scaleY:0.31,y:466.5},0).wait(1).to({scaleX:0.37,scaleY:0.37,y:466.4},0).wait(1).to({scaleX:0.43,scaleY:0.43,y:466.3},0).wait(1).to({scaleX:0.49,scaleY:0.49,y:466.2},0).wait(1).to({scaleX:0.56,scaleY:0.56,x:474,y:466.1},0).wait(1).to({scaleX:0.63,scaleY:0.63,y:466},0).wait(1).to({scaleX:0.69,scaleY:0.69,y:465.9},0).wait(1).to({scaleX:0.74,scaleY:0.74,y:465.8},0).wait(1).to({scaleX:0.8,scaleY:0.8},0).wait(1).to({scaleX:0.84,scaleY:0.84,y:465.7},0).wait(1).to({scaleX:0.88,scaleY:0.88,y:465.6},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94,y:465.5},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99,y:465.4},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,regY:0.1,scaleX:1,scaleY:1,y:467.1},0).wait(89));

	// 03 cepillo
	this.instance_14 = new lib._03cepillo();
	this.instance_14.setTransform(446.5,470,0.02,0.02);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.56,scaleY:0.56},0).wait(1).to({scaleX:0.62,scaleY:0.62},0).wait(1).to({scaleX:0.68,scaleY:0.68},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.79,scaleY:0.79},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,scaleX:1,scaleY:1},0).wait(91));

	// 02 circulo oscuro
	this.instance_15 = new lib._02circulooscuro();
	this.instance_15.setTransform(376.7,190.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(121));

	// 02 mascara (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	mask_4.graphics.p("EgffAgWMAAAg18MBNCAAAMAAAA18g");
	mask_4.setTransform(291.6,207);

	// 02 franja oscuro
	this.instance_16 = new lib._02franjaoscuro();
	this.instance_16.setTransform(354.6,304.1);

	this.instance_16.mask = mask_4;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(121));

	// 02 circulo claro
	this.instance_17 = new lib._02circuloclaro();
	this.instance_17.setTransform(284.7,120.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(121));

	// 02 mascara (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	mask_5.graphics.p("EgffAgWMAAAg18MBNCAAAMAAAA18g");
	mask_5.setTransform(291.6,207);

	// 02 franja claro
	this.instance_18 = new lib._02franjaclaro();
	this.instance_18.setTransform(301.2,273.2);

	this.instance_18.mask = mask_5;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(121));

	// 02 seda
	this.instance_19 = new lib._02seda();
	this.instance_19.setTransform(365.9,462.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(121));

	// 02 +
	this.instance_20 = new lib._02();
	this.instance_20.setTransform(326.4,467.1,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(121));

	// 02 cepillo
	this.instance_21 = new lib._02cepillo();
	this.instance_21.setTransform(292.7,470.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(121));

	// 01 circulo oscuro
	this.instance_22 = new lib._01circulooscuro();
	this.instance_22.setTransform(206.8,166);

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(121));

	// 01 mascara (mask)
	var mask_6 = new cjs.Shape();
	mask_6._off = true;
	mask_6.graphics.p("EgffAgWMAAAg18MBNCAAAMAAAA18g");
	mask_6.setTransform(291.6,207);

	// 01 franja oscuro
	this.instance_23 = new lib._01franjaoscuro();
	this.instance_23.setTransform(184.7,289.5);

	this.instance_23.mask = mask_6;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(121));

	// 01 circulo claro
	this.instance_24 = new lib._01circuloclaro();
	this.instance_24.setTransform(114.8,93.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(121));

	// 01 mascara (mask)
	var mask_7 = new cjs.Shape();
	mask_7._off = true;
	mask_7.graphics.p("EgffAgWMAAAg18MBNCAAAMAAAA18g");
	mask_7.setTransform(291.6,207);

	// 01 franja claro
	this.instance_25 = new lib._01franjaclaro();
	this.instance_25.setTransform(131.3,255.2);

	this.instance_25.mask = mask_7;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(121));

	// 01 cepillo
	this.instance_26 = new lib._01cepillo();
	this.instance_26.setTransform(160.4,469.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(121));

	// 00 indice de gingivitis
	this.instance_27 = new lib._00indicedegingivitis();
	this.instance_27.setTransform(454.2,540.8,1,1,0,0,0,-0.5,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(121));

	// 00 circulo2
	this.instance_28 = new lib._00circulo2();
	this.instance_28.setTransform(330,540.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(121));

	// 00 indice de placa
	this.instance_29 = new lib._00indicedeplaca();
	this.instance_29.setTransform(244.8,540.8,1,1,0,0,0,-0.1,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(121));

	// 00 circulo1
	this.instance_30 = new lib._00circulo1();
	this.instance_30.setTransform(170.1,540.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(121));

	// 00 3,0
	this.instance_31 = new lib._0030();
	this.instance_31.setTransform(37,27.6,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(121));

	// 00 2,73
	this.instance_32 = new lib._00273();
	this.instance_32.setTransform(38,70);

	this.timeline.addTween(cjs.Tween.get(this.instance_32).wait(121));

	// 00 2,5
	this.instance_33 = new lib._0025();
	this.instance_33.setTransform(36.8,109.2,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(121));

	// 00 2,1
	this.instance_34 = new lib._0021();
	this.instance_34.setTransform(38,160.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_34).wait(121));

	// 00 1,5
	this.instance_35 = new lib._0015();
	this.instance_35.setTransform(35.4,215.8,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(121));

	// 00 1,0
	this.instance_36 = new lib._0010();
	this.instance_36.setTransform(35.7,274.5,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_36).wait(121));

	// 00 0,5
	this.instance_37 = new lib._0005();
	this.instance_37.setTransform(37,336,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_37).wait(121));

	// 00 0,0
	this.instance_38 = new lib._0000();
	this.instance_38.setTransform(31.5,395.5,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_38).wait(121));

	// 00 mascara (mask)
	var mask_8 = new cjs.Shape();
	mask_8._off = true;
	mask_8.graphics.p("EgnqAIdIAAr8MBYGAAAIAAL8g");
	mask_8.setTransform(310,54.1);

	// 00 flecha1
	this.instance_39 = new lib._00flecha1();
	this.instance_39.setTransform(337.9,70.3);

	this.instance_39.mask = mask_8;

	this.timeline.addTween(cjs.Tween.get(this.instance_39).wait(121));

	// 00 mascara (mask)
	var mask_9 = new cjs.Shape();
	mask_9._off = true;
	mask_9.graphics.p("EgnqAPKIAAr+MBYGAAAIAAL+g");
	mask_9.setTransform(310,97);

	// 00 flecha2
	this.instance_40 = new lib._00flecha2();
	this.instance_40.setTransform(337.9,160.6);

	this.instance_40.mask = mask_9;

	this.timeline.addTween(cjs.Tween.get(this.instance_40).wait(121));

	// 00 mascara (mask)
	var mask_10 = new cjs.Shape();
	mask_10._off = true;
	mask_10.graphics.p("EgD/AgWMAAAg7+IL8AAMAAAA7+g");
	mask_10.setTransform(51,207);

	// 00 carte2
	this.instance_41 = new lib._00carte2();
	this.instance_41.setTransform(63.5,222);

	this.instance_41.mask = mask_10;

	this.timeline.addTween(cjs.Tween.get(this.instance_41).wait(121));

	// 00 mascara (mask)
	var mask_11 = new cjs.Shape();
	mask_11._off = true;
	mask_11.graphics.p("EgnrAh9IAAr+MBYGAAAIAAL+g");
	mask_11.setTransform(310,217.4);

	// 00 carte1
	this.instance_42 = new lib._00carte1();
	this.instance_42.setTransform(338,397.5);

	this.instance_42.mask = mask_11;

	this.timeline.addTween(cjs.Tween.get(this.instance_42).wait(121));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(340,300.7,600,537.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;