(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 580,
	height: 360,
	fps: 30,
	color: "#FFFFFF",
	opacity: 0.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib._02puntoblanco5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,4.9,5);


(lib._02puntoblanco4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,5,4.9);


(lib._02puntoblanco3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgIAAgJQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAJgHAIQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,5,5);


(lib._02puntoblanco2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgIAHgIQAHgHAJAAQAKAAAHAHQAHAIAAAIQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.5,4.9,5);


(lib._02puntoblanco1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgJAHgHQAHgHAJAAQAKAAAHAHQAHAHAAAJQAAAKgHAHQgHAHgKAAQgJAAgHgHg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.4,4.9,4.9);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("Ah0DLQhVgrglhYQgmhYAehcQAihmBggxQBggxBkAiQBnAhAwBgQAxBgghBlQgdBZhQAyQhOAxhagLIhDA8g");
	this.shape.setTransform(26,28.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,57.9);


(lib.Path_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AiODDQhQg1gahcQgbhbAphYQAthiBlglQBjglBiAtQBiAuAlBlQAlBlguBfQgnBWhVAoQhUAohXgWIhKAzg");
	this.shape_1.setTransform(26,28.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,56.5);


(lib.Path_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B2F2F8").s().p("AhlDPQhXglgrhWQgshUAYheQAbhpBdg2QBdg3BmAaQBoAbA3BdQA3BcgbBnQgXBbhMA3QhLA2hagFIg/BAg");
	this.shape_2.setTransform(26,29.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,58.4);


(lib.Path_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B2F2F8").s().p("AhlDPQhYgmgqhWQgrhVAYheQAbhoBdg2QBcg3BnAbQBoAbA3BdQA2BdgbBmQgYBbhMA3QhLA2hagGIg/BAg");
	this.shape_3.setTransform(26,29.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.1,58.4);


(lib.Path_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7BD7F8").s().p("AhWEPQhlgkguhhQgvhhAlhkQAfhYBRgwQBQgvBZANIBFg6IAQBWQBUAuAjBZQAjBYghBcQgkBlhhAuQg3Abg4AAQgpAAgtgRg");
	this.shape_4.setTransform(26,28.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.2,57.6);


(lib._02franja = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgxFbIAAq1IBjAAIAAK1g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5,-34.7,10,69.5);


(lib._02562 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhDBgIB5jJIAOAKIh5DJgAAZBgQgEgEgDgGQgCgFAAgIIAAgpQAAgHACgGQADgGAEgEQAJgIANAAQANAAAJAIQAEAEADAGQACAGAAAHIAAApQAAAPgJAIQgJAJgNAAQgNAAgJgJgAAoAVQgCACAAAEIAAAyQAAAFACADQADACAEAAQAEAAADgCQADgDAAgFIAAgyQAAgEgDgCQgDgDgEAAQgEAAgDADgAg6gBQgGgCgEgFQgJgIAAgOIAAgqQAAgHADgGQACgFAEgFQAEgEAGgCQAFgCAHAAQAHAAAFACQAGACAEAEQAEAFADAFQACAGAAAHIAAAqQAAAOgJAIQgEAFgGACQgFABgHAAQgHAAgFgBgAg1hTQgDADAAAEIAAAyQAAAEADADQADACAEAAQAEAAADgCQACgDAAgEIAAgyQAAgEgCgDQgDgDgEAAQgEAAgDADg");
	this.shape.setTransform(20.5,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgzBmIAAgLQAAgLACgLQACgKAEgKQAFgLAIgLIATgbIALgNIAHgNIAFgLIABgLQAAgIgEgEQgDgDgFAAQgFAAgDAEQgDAEAAAHIAAATIgmAAIAAgYQAAgJADgJQADgIAGgGQAGgIAKgEQAJgEALAAQAOAAAKAEQAKAFAGAHQAHAHACAKQADAKAAALIgBARQgCAIgEAJQgEAIgHAIQgHAJgLALIgKAOIgGALIgCAJIgBAHIA2AAIAAAmg");
	this.shape_1.setTransform(5.8,-1.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgPAVIAAgpIAfAAIAAApg");
	this.shape_2.setTransform(-2.6,7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgUBkQgKgDgHgHQgHgHgEgKQgEgJAAgMIAAhnQAAgMAEgJQAEgKAHgHQAHgHAKgDQAKgEAKAAQANAAAJAEQAJAEAGAHQAHAHADAIQADAJAAAJIAAATIglAAIAAgPQAAgHgDgDQgDgEgGAAQgEAAgEADQgDAEAAAFIAAAvQAFgGAFgEQAGgEALAAQARAAAJAKQAFAFACAFQADAFAAAIIAAAnQAAAMgEAJQgEAKgHAHQgGAHgKADQgKAEgMAAQgLAAgJgEgAgHALQgDADAAAEIAAAmQAAAEADADQADADAEAAQAFAAADgDQADgDAAgEIAAgmQAAgEgDgDQgDgDgFAAQgEAAgDADg");
	this.shape_3.setTransform(-11.1,-0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgWBiQgJgFgHgGQgGgHgDgJQgEgHAAgKIAAgSIAnAAIAAAOQAAAHADADQADAEAGgBQADAAAEgCQADgEAAgFIAAguQAAgEgDgCQgDgCgEAAQgDAAgDACQgDABAAAEIglgFIAFhlIBcAAQgCAUgGAJQgEAJgMAAIgiAAIgDArQAGgIAGgDQAIgDAJgBQAPAAAIAKQAFAFADAHQABAGAAAHIAAA1QAAALgDAJQgFAJgHAGQgGAHgLADQgKAEgKAAQgMAAgJgEg");
	this.shape_4.setTransform(-23.2,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.1,-17,62.4,34.1);


(lib._01lineaoscura = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AP5E+Iv5kCIwGlIQgJgDgFgKQgFgJADgKQADgKAJgEQAKgFAJADIQFFJIP3EAQAKADAFAJQAGAJgDAKQgCAIgHAFQgHAFgIAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-104.8,-31.9,209.8,63.8);


(lib._01lineaclara = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AhBBeIwIiLQgKgBgGgJQgGgIABgLQABgKAJgGQAIgHAKACIQGCKISBgbQALgBAHAIQAIAHAAAKQAAALgHAIQgHAIgKAAIyFAdIgDgCg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-111.9,-9.7,224,19.4);


(lib.Group = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgERgMAAAgi/IAIAAMAAAAi/g");
	this.shape.setTransform(0.5,112);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1,224);


(lib.Group_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgERgMAAAgi/IAIAAMAAAAi/g");
	this.shape_1.setTransform(0.5,112);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1,224);


(lib.Group_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgERgMAAAgi/IAIAAMAAAAi/g");
	this.shape_2.setTransform(0.5,112);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1,224);


(lib._00vertical1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgOR0MAAAgjnIAdAAMAAAAjng");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-114,3,228);


(lib._00titulo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AATB+IgEgmIghAAIgFAmIg1AAIAsj7IBBAAIAsD7gAgMAtIAWAAIgKhag");
	this.shape.setTransform(91.4,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgZB+QgOgEgKgKQgKgIgFgOQgGgMAAgQIAAh7QAAgQAGgMQAFgNAKgKQAKgIAOgFQAOgGAPAAQAPAAANAFQAMAFAJAIQAJAKAFAMQAEALAAAQIAAAhIgzAAIAAgjQAAgGgEgFQgEgDgIAAQgGAAgEADQgEAFAAAGIAACEQAAAHAEAEQAEAEAGAAQAIAAAEgEQAEgEAAgHIAAgiIAzAAIAAAhQAAAPgEAMQgFAMgJAKQgJAIgMAFQgNAFgPAAQgPAAgOgGg");
	this.shape_1.setTransform(76.1,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASB+IgDgmIghAAIgFAmIg1AAIAsj7IBCAAIArD7gAgLAtIAUAAIgJhag");
	this.shape_2.setTransform(60.5,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag7B+IAAj7IA7AAIAADIIA8AAIAAAzg");
	this.shape_3.setTransform(46.4,-1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhDB+IAAj7IBQAAQAMAAAKADQAKAEAIAHQAHAHAEAKQAFALgBANIAAAyQABAOgFAIQgEALgHAHQgIAGgKAEQgKAEgMAAIgWAAIAABcgAgJgGIAMAAQALAAAAgMIAAgxQAAgMgLAAIgMAAg");
	this.shape_4.setTransform(31.7,-1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ag9B+IAAj7IB7AAIAAAxIhAAAIAAA0IAoAAIAAAqIgoAAIAAA4IBAAAIAAA0g");
	this.shape_5.setTransform(11.2,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhHB+IAAj7IBIAAQAQAAANAFQANAEAKAKQAJAJAFAMQAFANAAAPIAAB4QAAAPgFAMQgFAMgJAHQgKAJgNADQgNAFgQAAgAgNBOIAMAAQAGAAAEgFQAEgEABgKIAAh1QgBgJgEgFQgEgFgGAAIgMAAg");
	this.shape_6.setTransform(-4.5,-1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Ag9B+IAAj7IB6AAIAAAxIg/AAIAAA0IApAAIAAAqIgpAAIAAA4IA/AAIAAA0g");
	this.shape_7.setTransform(-25.3,-1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgZB+QgOgEgKgKQgKgIgFgOQgGgMAAgQIAAh7QAAgQAGgMQAFgNAKgKQAKgIAOgFQAOgGAPAAQAPAAANAFQAMAFAJAIQAJAKAFAMQAEALAAAQIAAAhIgzAAIAAgjQAAgGgEgFQgEgDgIAAQgGAAgEADQgEAFAAAGIAACEQAAAHAEAEQAEAEAGAAQAIAAAEgEQAEgEAAgHIAAgiIAzAAIAAAhQAAAPgEAMQgFAMgJAKQgJAIgMAFQgNAFgPAAQgPAAgOgGg");
	this.shape_8.setTransform(-40.7,-1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgcB+IAAj7IA5AAIAAD7g");
	this.shape_9.setTransform(-53,-1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhHB+IAAj7IBJAAQAPAAANAFQANAEAJAKQAKAJAFAMQAFANAAAPIAAB4QAAAPgFAMQgFAMgKAHQgJAJgNADQgNAFgPAAgAgMBOIALAAQAGAAAEgFQAFgEAAgKIAAh1QAAgJgFgFQgEgFgGAAIgLAAg");
	this.shape_10.setTransform(-65.4,-1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAbB+IgihdIgPgiIAAB/Ig0AAIAAj7IA0AAIAgBeIANAiIAAiAIAzAAIAAD7g");
	this.shape_11.setTransform(-82.5,-1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgiCgIAAj7IA4AAIAAD7gAgThpIANg2IApAAIgfA2g");
	this.shape_12.setTransform(-94.7,-4.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101.5,-20.7,203.1,42);


(lib._00leyenda = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAMBUIgCgZIgWAAIgDAZIgkAAIAeioIArAAIAeCogAgHAeIANAAIgGg8g");
	this.shape.setTransform(30.9,-0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgvBUIAAioIAwAAQAKAAAJAEQAJADAGAGQAGAGAEAIQADAJAAAKIAABQQAAAKgDAIQgEAIgGAGQgGAFgJADQgJACgKAAgAgIA1IAIAAQADAAADgEQADgDAAgHIAAhOQAAgGgDgDQgDgDgDAAIgIAAg");
	this.shape_1.setTransform(20.4,-0.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASBUIgXg+IgJgWIAABUIgjAAIAAioIAjAAIAVA/IAJAXIAAhWIAiAAIAACog");
	this.shape_2.setTransform(8.9,-0.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgoBUIAAioIBRAAIAAAiIgqAAIAAAjIAbAAIAAAbIgbAAIAAAlIAqAAIAAAjg");
	this.shape_3.setTransform(-1.6,-0.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgTBUIAAhGIgihiIAoAAIAOA9IAOg9IAnAAIgkBiIAABGg");
	this.shape_4.setTransform(-12,-0.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgoBUIAAioIBRAAIAAAiIgqAAIAAAjIAbAAIAAAbIgbAAIAAAlIAqAAIAAAjg");
	this.shape_5.setTransform(-22.1,-0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgnBUIAAioIAnAAIAACGIAoAAIAAAig");
	this.shape_6.setTransform(-31.3,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.2,-14.7,76.6,29.6);


(lib._00horizontal7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A41AFIAAgJMAxrAAAIAAAJg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib.Group_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_3.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib.Group_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_4.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib.Group_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_5.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib.Group_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_6.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib.Group_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("A41AEIAAgIMAxqAAAIAAAIg");
	this.shape_7.setTransform(159,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,318,1);


(lib._00horizontal1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A5DAPIAAgdMAyHAAAIAAAdg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-160.5,-1.5,321,3);


(lib._00CPC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgFgEQgFgFgCgGQgDgGAAgHIAAg4QAAgIADgGQACgGAFgEQAFgEAGgDQAHgCAFAAQAIAAAFACQAGADAEADQAEAEADAGQACAGAAAHIAAAPIgYAAIAAgQQAAgDgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAA8QAAAEACACQACACABAAQAEAAACgCQACgCAAgEIAAgPIAYAAIAAAPQAAAHgCAGQgDAFgEAFQgEAEgGACQgFACgIAAQgFAAgHgCg");
	this.shape.setTransform(7.5,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgfA6IAAh0IAlAAQAFAAAFADQAFABADADQAEAEACAEQACAFAAAGIAAAYQAAAGgCADQgCAFgEADQgDAEgFABIgKACIgJAAIAAAqgAgDgCIAEAAQAFAAAAgFIAAgYQAAgFgFAAIgEAAg");
	this.shape_1.setTransform(0.1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgFgEQgFgFgCgGQgDgGAAgHIAAg4QAAgIADgGQACgGAFgEQAFgEAGgDQAHgCAFAAQAIAAAFACQAGADAEADQAEAEADAGQACAGAAAHIAAAPIgYAAIAAgQQAAgDgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAA8QAAAEACACQACACABAAQAEAAACgCQACgCAAgEIAAgPIAYAAIAAAPQAAAHgCAGQgDAFgEAFQgEAEgGACQgFACgIAAQgFAAgHgCg");
	this.shape_2.setTransform(-7.3,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.1,-10.8,26.5,21.7);


(lib._00circulooscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AhKBLQgfgfAAgsQAAgrAfgfQAfgfArAAQAsAAAfAfQAfAfAAArQAAAsgfAfQgfAfgsAAQgrAAgfgfg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.7,-10.6,21.4,21.3);


(lib._00circuloclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AhKBLQgfgfAAgsQAAgrAfgfQAggfAqAAQAsAAAfAfQAfAfAAArQAAArgfAgQgfAfgsAAQgqAAgggfg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.7,-10.6,21.4,21.4);


(lib._00Baseline = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIARAAIAAASIgRAAIAAAbIAcAAIAAAXg");
	this.shape.setTransform(23.5,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAMA6IgPgrIgGgPIAAA6IgYAAIAAh0IAYAAIANAsIAHAPIAAg7IAYAAIAAB0g");
	this.shape_1.setTransform(16,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMA6IAAh0IAZAAIAAB0g");
	this.shape_2.setTransform(10,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IAbAAIAABdIAcAAIAAAXg");
	this.shape_3.setTransform(5.2,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIARAAIAAASIgRAAIAAAbIAdAAIAAAXg");
	this.shape_4.setTransform(-1.4,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgCgFQgCgGgBgIIAAgIIAYAAIAAAIQAAAEACADQACACAEAAQADAAACgCQACgCAAgFQAAgFgGgGIgJgLIgIgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQABgGAEgFQAFgFAGgCQAHgDAFAAQAIAAAFACQAGADAEAEQAEAEACAFQACAGAAAGIAAAIIgYAAIAAgJQAAgDgBgCQgDgCgDAAQAAAAgCACQgCACgBAEIACAGIADAGIAEAEIAFAHIAJAHIAHAHQADAEADAGQACAFAAAGQgBAHgCAGQgCAGgEAFQgFAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape_5.setTransform(-8.4,-0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAIA6IgCgRIgOAAIgCARIgZAAIAVh0IAdAAIAVB0gAgFAUIAJAAIgEgog");
	this.shape_6.setTransform(-15.7,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgfA6IAAh0IAmAAQAFAAAEACQAFACADACQAEADACAFQACAFAAAFIAAAQIgBAGIgCAFIgEAFIgGACQAHABADAFQADAFAAAGIAAASQAAAGgCAEQgCAFgEADQgDADgFABQgEABgFAAgAgDAlIAEAAQAFAAAAgGIAAgQQAAgGgFAAIgEAAgAgDgIIAEAAQAFAAAAgGIAAgRQAAgFgFAAIgEAAg");
	this.shape_7.setTransform(-23,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-28.7,-10.8,57.7,21.7);


(lib._00Aceitesesenciales = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgDgFQgCgGAAgIIAAgIIAYAAIAAAIQAAAEACADQACACAEAAQACAAACgCQADgCAAgFQgBgFgFgGIgIgLIgIgIIgIgGQgEgFgBgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAGgDAGAAQAHAAAGACQAFADAEAEQAEAEADAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgDgCgDAAQAAAAgDACQgCACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQADAEACAGQACAFABAGQAAAHgDAGQgCAGgEAFQgFAEgGACQgFADgIAAQgGAAgGgCg");
	this.shape.setTransform(53.9,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIASAAIAAASIgSAAIAAAbIAcAAIAAAXg");
	this.shape_1.setTransform(47.2,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IAbAAIAABdIAcAAIAAAXg");
	this.shape_2.setTransform(40.8,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAIA6IgCgRIgOAAIgCARIgZAAIAVh0IAdAAIAVB0gAgFAUIAJAAIgEgog");
	this.shape_3.setTransform(33.8,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgMA6IAAh0IAZAAIAAB0g");
	this.shape_4.setTransform(28.2,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgFgEQgFgFgCgGQgDgGAAgHIAAg4QAAgIADgGQACgGAFgEQAFgEAGgDQAHgCAFAAQAIAAAFACQAGADAEADQAEAEADAGQACAGAAAHIAAAPIgYAAIAAgQQAAgDgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAA8QAAAEACACQACACABAAQAEAAACgCQACgCAAgEIAAgPIAYAAIAAAPQAAAHgCAGQgDAFgEAFQgEAEgGACQgFACgIAAQgFAAgHgCg");
	this.shape_5.setTransform(22.6,-0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAMA6IgPgrIgGgPIAAA6IgZAAIAAh0IAZAAIANAsIAGAPIAAg7IAZAAIAAB0g");
	this.shape_6.setTransform(14.8,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIARAAIAAASIgRAAIAAAbIAdAAIAAAXg");
	this.shape_7.setTransform(7.5,-0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgCgFQgDgGAAgIIAAgIIAYAAIAAAIQAAAEACADQADACADAAQACAAADgCQACgCAAgFQAAgFgGgGIgJgLIgIgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAHgDAFAAQAIAAAFACQAFADAFAEQAEAEACAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgCgCgEAAQAAAAgCACQgDACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQAEAEABAGQADAFAAAGQAAAHgDAGQgCAGgEAFQgFAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape_8.setTransform(0.5,-0.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIARAAIAAASIgRAAIAAAbIAcAAIAAAXg");
	this.shape_9.setTransform(-6.3,-0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgDgFQgCgGAAgIIAAgIIAYAAIAAAIQAAAEACADQACACAEAAQACAAACgCQADgCAAgFQgBgFgFgGIgIgLIgJgIIgHgGQgEgFgBgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAGgDAGAAQAHAAAGACQAFADAEAEQAEAEADAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgDgCgDAAQAAAAgDACQgCACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQADAEACAGQACAFABAGQAAAHgDAGQgCAGgEAFQgFAEgGACQgFADgIAAQgGAAgGgCg");
	this.shape_10.setTransform(-15.8,-0.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIASAAIAAASIgSAAIAAAbIAcAAIAAAXg");
	this.shape_11.setTransform(-22.6,-0.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgMA6IAAhcIgTAAIAAgYIA/AAIAAAYIgTAAIAABcg");
	this.shape_12.setTransform(-29.4,-0.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgMA6IAAh0IAZAAIAAB0g");
	this.shape_13.setTransform(-34.8,-0.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIASAAIAAASIgSAAIAAAbIAdAAIAAAXg");
	this.shape_14.setTransform(-39.9,-0.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgFgEQgFgFgCgGQgDgGAAgHIAAg4QAAgIADgGQACgGAFgEQAFgEAGgDQAHgCAFAAQAIAAAFACQAGADAEADQAEAEADAGQACAGAAAHIAAAPIgYAAIAAgQQAAgDgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAA8QAAAEACACQACACABAAQAEAAACgCQACgCAAgEIAAgPIAYAAIAAAPQAAAHgCAGQgDAFgEAFQgEAEgGACQgFACgIAAQgFAAgHgCg");
	this.shape_15.setTransform(-47,-0.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAIA6IgCgRIgOAAIgCARIgZAAIAVh0IAdAAIAVB0gAgFAUIAJAAIgEgog");
	this.shape_16.setTransform(-54.3,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-59.9,-10.8,119.6,21.7);


(lib._006meses = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgCgFQgDgGAAgIIAAgIIAYAAIAAAIQAAAEACADQADACADAAQACAAADgCQACgCAAgFQAAgFgGgGIgIgLIgJgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAHgDAFAAQAIAAAFACQAFADAFAEQAEAEACAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgCgCgEAAQAAAAgCACQgDACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQAEAEABAGQADAFAAAGQAAAHgDAGQgCAGgEAFQgFAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape.setTransform(20.3,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIASAAIAAASIgSAAIAAAbIAcAAIAAAXg");
	this.shape_1.setTransform(13.5,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMA7QgHgCgEgEQgEgEgDgFQgCgGAAgIIAAgIIAYAAIAAAIQAAAEADADQABACAEAAQACAAACgCQACgCAAgFQAAgFgEgGIgJgLIgIgIIgIgGQgEgFgCgFQgCgEAAgHQAAgGACgGQACgGAEgFQAEgFAHgCQAGgDAHAAQAGAAAGACQAFADAEAEQAFAEACAFQACAGAAAGIAAAIIgXAAIAAgJQgBgDgCgCQgCgCgCAAQgBAAgDACQgCACAAAEIABAGIAEAGIADAEIAHAHIAIAHIAHAHQADAEACAGQACAFAAAGQAAAHgCAGQgCAGgFAFQgDAEgHACQgFADgIAAQgGAAgGgCg");
	this.shape_2.setTransform(6.5,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIASAAIAAASIgSAAIAAAbIAdAAIAAAXg");
	this.shape_3.setTransform(-0.3,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUA6IAAhNIgFAbIgIAyIgPAAIgJgyIgEgbIAABNIgYAAIAAh0IAkAAIAIAvIABATIADgTIAIgvIAjAAIAAB0g");
	this.shape_4.setTransform(-9,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgEgEQgEgFgCgFQgDgGAAgHIAAg7QAAgHADgGQACgGAEgEQAEgDAGgDQAGgCAFAAQAHAAAGACQAFADAEAEQAEAEABAFQACAFAAAFIAAALIgVAAIAAgJQAAgEgCgBQgCgCgEAAQgBAAgCACQgCABAAADIAAAcQADgDACgCQAEgDAGAAQAKAAAFAFQADADACADIABAIIAAAWQAAAHgCAGQgDAFgEAFQgEAEgFACQgGACgHAAQgGAAgFgCgAgDAGQgBAAAAABQgBABAAAAQAAABAAAAQAAABAAAAIAAAXQAAAAAAABQAAAAAAABQAAAAABABQAAABABAAQACACABgBQADABACgCQAAAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAIAAgXQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAgBAAAAQgCgCgDAAQgBAAgCACg");
	this.shape_5.setTransform(-20.3,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.9,-10.8,52,21.7);


(lib._0030 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgLA7QgGgCgEgEQgEgFgCgFQgDgFAAgIIAAg7QAAgHADgGQACgGAEgEQAEgEAGgCQAFgCAGAAQAHAAAGACQAFACAFAEQAEAEACAGQACAGAAAHIAAA7QAAAIgCAFQgCAFgEAFQgFAEgFACQgGACgHAAQgGAAgFgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQADACABAAQADAAACgCQAAAAAAgBQABAAAAgBQAAAAAAgBQABAAAAgBIAAg/QAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQgCgCgDAAQgBAAgDACg");
	this.shape.setTransform(5,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAPQAEAAACgDQACgCAAgHIgIAAIAAgWIASAAIAAAUQgBAEgBADIgEAHQgCACgCACQgEABgEAAg");
	this.shape_1.setTransform(-0.2,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNA6QgFgCgEgEQgDgEgCgEQgCgFAAgGIAAgMIAXAAIAAAJQAAAFABABQACACADAAQACAAACgCQACgBAAgEIAAgSQAAgEgFAAIgHAAIAAgRIAHAAQAFAAAAgGIAAgRQAAgDgCgBQgCgCgCAAQgGAAAAAHIAAAKIgXAAIAAgMQAAgFACgFQACgFADgEQAEgEAFgDQAGgCAHAAQAFAAAGACQAGACAEAEQAEAEADAFQACAFAAAHIAAAMQAAAHgEAGQgDAFgHABQAGAAAEAGQAEAEAAAJIAAAOQAAAGgCAFQgDAGgEAEQgEADgGACQgGACgFAAQgHAAgGgDg");
	this.shape_2.setTransform(-5.1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.6,-10.8,21.5,21.7);


(lib._003meses = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgEgEQgFgEgCgFQgCgGAAgIIAAgIIAXAAIAAAIQAAAEADADQACACADAAQADAAACgCQABgCAAgFQAAgFgEgGIgKgLIgIgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQABgGAFgFQAEgFAGgCQAHgDAFAAQAIAAAFACQAGADAEAEQADAEADAFQACAGAAAGIAAAIIgYAAIAAgJQAAgDgCgCQgBgCgEAAQAAAAgCACQgCACAAAEIABAGIADAGIAEAEIAFAHIAJAHIAHAHQAEAEACAGQABAFAAAGQAAAHgCAGQgCAGgFAFQgEAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape.setTransform(20.1,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgcA6IAAh0IA5AAIAAAYIgdAAIAAAYIARAAIAAASIgRAAIAAAbIAdAAIAAAXg");
	this.shape_1.setTransform(13.4,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMA7QgGgCgFgEQgEgEgCgFQgDgGAAgIIAAgIIAYAAIAAAIQAAAEACADQADACADAAQACAAADgCQACgCAAgFQAAgFgGgGIgIgLIgJgIIgHgGQgDgFgCgFQgDgEAAgHQAAgGADgGQACgGADgFQAFgFAGgCQAHgDAFAAQAIAAAFACQAFADAFAEQAEAEACAFQACAGAAAGIAAAIIgXAAIAAgJQAAgDgCgCQgCgCgEAAQAAAAgCACQgDACAAAEIABAGIAEAGIADAEIAGAHIAJAHIAHAHQAEAEABAGQADAFAAAGQAAAHgDAGQgCAGgEAFQgFAEgFACQgHADgHAAQgGAAgGgCg");
	this.shape_2.setTransform(6.3,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgbA6IAAh0IA3AAIAAAYIgcAAIAAAYIASAAIAAASIgSAAIAAAbIAcAAIAAAXg");
	this.shape_3.setTransform(-0.4,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUA6IAAhNIgFAbIgIAyIgPAAIgJgyIgEgbIAABNIgYAAIAAh0IAkAAIAIAvIABATIADgTIAIgvIAjAAIAAB0g");
	this.shape_4.setTransform(-9.1,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgNA6QgFgCgEgEQgDgEgCgEQgCgFAAgGIAAgMIAXAAIAAAJQAAAFABABQACACADAAQACAAACgCQACgBAAgEIAAgSQAAgEgFAAIgHAAIAAgRIAHAAQAFAAAAgGIAAgRQAAgDgCgBQgCgCgCAAQgGAAAAAHIAAAKIgXAAIAAgMQAAgFACgFQACgFADgEQAEgEAFgDQAGgCAHAAQAFAAAGACQAGACAEAEQAEAEADAFQACAFAAAHIAAAMQAAAHgEAGQgDAFgHABQAGAAAEAGQAEAEAAAJIAAAOQAAAGgCAFQgDAGgEAEQgEADgGACQgGACgFAAQgHAAgGgDg");
	this.shape_5.setTransform(-20.3,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.8,-10.8,51.7,21.7);


(lib._0025 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNA5QgFgCgEgEQgDgEgCgFQgCgFAAgFIAAgLIAXAAIAAAIQAAAEABACQACACADAAQACAAACgCQACgBAAgEIAAgaQAAgBAAgBQAAAAgBgBQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAgBgBQAAAAAAAAIgDABQAAAAgBAAQAAAAAAABQAAAAgBABQAAAAAAABIgWgDIADg7IA2AAQgBAMgEAFQgDAGgGAAIgUAAIgBAZQADgEADgDQAEgCAGAAQAJAAAFAGIAEAHQABAEAAADIAAAfQAAAHgCAFQgDAFgEAEQgEAEgGACQgGACgFAAQgHAAgGgDg");
	this.shape.setTransform(5,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAPQAEAAADgDQACgCAAgHIgJAAIAAgWIASAAIAAAUQABAEgCADIgEAHQgDACgBACQgEABgFAAg");
	this.shape_1.setTransform(0.1,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeA8IAAgGIABgNQABgGAEgGQACgHAEgGIAMgQIAGgHIAEgIIACgGIABgHQAAgEgCgCQgCgCgDAAQgBAAgDACQgBACAAAEIAAALIgXAAIAAgOIACgKQACgFADgEQAEgEAFgCQAGgDAGAAQAIAAAGADQAFACAEAFQAEAEACAGQABAFAAAHIAAAKIgEAKQgCAFgEADIgKAMIgHAJIgDAGIgBAFIgBAEIAgAAIAAAXg");
	this.shape_2.setTransform(-4.9,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.4,-10.8,21.1,21.7);


(lib._0020 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgFgCgEgEQgFgFgCgFQgCgFAAgIIAAg7QAAgHACgGQACgGAFgEQAEgEAFgCQAHgCAFAAQAHAAAGACQAGACADAEQAEAEADAGQACAGAAAHIAAA7QAAAIgCAFQgDAFgEAFQgDAEgGACQgGACgHAAQgFAAgHgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQACACACAAQADAAABgCQABAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBIAAg/QAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBgCgDAAQgCAAgCACg");
	this.shape.setTransform(5,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAPQAFAAACgDQACgCAAgHIgJAAIAAgWIASAAIAAAUQAAAEgBADIgEAHQgDACgCACQgDABgFAAg");
	this.shape_1.setTransform(-0.1,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeA8IAAgGIABgNQACgGADgGQACgHAFgGIALgQIAGgHIAEgIIADgGIAAgHQAAgEgCgCQgCgCgDAAQgCAAgCACQgBACAAAEIAAALIgWAAIAAgOIABgKQACgFADgEQAEgEAFgCQAGgDAGAAQAIAAAGADQAFACAEAFQAEAEABAGQACAFAAAHIgBAKIgDAKQgCAFgFADIgJAMIgHAJIgDAGIAAAFIgCAEIAfAAIAAAXg");
	this.shape_2.setTransform(-5.1,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.6,-10.8,21.6,21.7);


(lib._0015 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNA5QgFgCgEgEQgDgEgCgFQgCgFAAgFIAAgLIAXAAIAAAIQAAAEABACQACACADAAQACAAACgCQACgBAAgEIAAgaQAAgBAAgBQAAAAgBgBQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAgBgBQAAAAAAAAIgDABQAAAAgBAAQAAAAAAABQgBAAAAABQAAAAAAABIgWgDIADg7IA2AAQgBAMgEAFQgDAGgGAAIgUAAIgBAZQADgEADgDQAEgCAGAAQAJAAAFAGIAEAHQABAEAAADIAAAfQAAAHgCAFQgDAFgEAEQgEAEgGACQgGACgFAAQgHAAgGgDg");
	this.shape.setTransform(3.9,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAPQAFAAACgDQACgCAAgHIgJAAIAAgWIASAAIAAAUQAAAEgBADIgEAHQgDACgCACQgDABgFAAg");
	this.shape_1.setTransform(-1,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLA6IAAhoQAGAAADgEQACgDAAgFIAMAAIAAB0g");
	this.shape_2.setTransform(-4.8,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.2,-10.8,18.7,21.7);


(lib._0010 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgFgCgEgEQgFgFgBgFQgDgFAAgIIAAg7QAAgHADgGQABgGAFgEQAEgEAFgCQAGgCAGAAQAHAAAGACQAFACAEAEQAEAEADAGQACAGAAAHIAAA7QAAAIgCAFQgDAFgEAFQgEAEgFACQgGACgHAAQgGAAgGgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQADACABAAQADAAABgCQABAAAAgBQABAAAAgBQAAAAAAgBQABAAAAgBIAAg/QAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBgCgDAAQgBAAgDACg");
	this.shape.setTransform(3.9,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAPQAEAAADgDQACgCAAgHIgJAAIAAgWIATAAIAAAUQAAAEgCADIgEAHQgDACgCACQgDABgFAAg");
	this.shape_1.setTransform(-1.3,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLA6IAAhoQAGAAADgEQACgDAAgFIAMAAIAAB0g");
	this.shape_2.setTransform(-5.1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.4,-10.8,19.2,21.7);


(lib._0005 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNA5QgFgCgEgEQgDgEgCgFQgCgFAAgFIAAgLIAXAAIAAAIQAAAEABACQACACADAAQACAAACgCQACgBAAgEIAAgaQAAgBAAgBQgBAAAAgBQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAgBgBQAAAAAAAAIgDABQAAAAgBAAQAAAAAAABQAAAAgBABQAAAAAAABIgWgDIADg7IA2AAQgBAMgEAFQgDAGgGAAIgUAAIgBAZQADgEADgDQAEgCAGAAQAJAAAFAGIAEAHQABAEAAADIAAAfQAAAHgCAFQgDAFgEAEQgEAEgGACQgGACgFAAQgHAAgGgDg");
	this.shape.setTransform(5.2,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAPQAEAAACgDQACgCAAgHIgIAAIAAgWIASAAIAAAUQgBAEgBADIgEAHQgCACgCACQgEABgEAAg");
	this.shape_1.setTransform(0.3,5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMA7QgFgCgEgEQgFgFgCgFQgCgFAAgIIAAg7QAAgHACgGQACgGAFgEQAEgEAFgCQAHgCAFAAQAHAAAGACQAGACADAEQAEAEADAGQACAGAAAHIAAA7QAAAIgCAFQgDAFgEAFQgDAEgGACQgGACgHAAQgFAAgHgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQACACACAAQADAAABgCQABAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBIAAg/QAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBgCgDAAQgCAAgCACg");
	this.shape_2.setTransform(-4.9,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.6,-10.8,21.4,21.7);


(lib._0000 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMA7QgFgCgEgEQgFgFgCgFQgCgFAAgIIAAg7QAAgHACgGQACgGAFgEQAEgEAFgCQAHgCAFAAQAHAAAGACQAGACADAEQAEAEADAGQACAGAAAHIAAA7QAAAIgCAFQgDAFgEAFQgDAEgGACQgGACgHAAQgFAAgHgCgAgEgjQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAAA/QAAABAAAAQAAABAAAAQABABAAAAQAAABAAAAQACACACAAQADAAABgCQABAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBIAAg/QAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBgCgDAAQgCAAgCACg");
	this.shape.setTransform(0.1,-0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.6,-10.8,11.6,21.7);


(lib._02globooscuro2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AABBeIAAgmIg1AAIAAgcIA4h5IAjAAIAAB1IAOAAIAAAgIgOAAIAAAmgAgRAYIASAAIAAgtg");
	this.shape.setTransform(11.5,-3.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgiBeQAAgVAEgUQAEgWAGgUQAGgSAJgRQAFgSAJgNIg5AAIAAgmIBhAAIAAAQQgKAWgIAUQgHAUgFATQgGAVgCAWQgDAXAAAYg");
	this.shape_1.setTransform(1.8,-3.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-6.2,4.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTBeQgKgDgGgHQgHgGgDgJQgEgJAAgLIAAhhQAAgLAEgJQADgJAHgHQAGgGAKgEQAJgDAKAAQAMAAAIADQAKAEAGAGQAHAHADAJQAEAJAAALIAABhQAAALgEAJQgDAJgHAGQgGAHgKADQgIAEgMAAQgKAAgJgEgAgHg6QgCADAAAEIAABmQAAAEACADQAEADADAAQAFAAADgDQACgDAAgEIAAhmQAAgEgCgDQgDgCgFAAQgDAAgEACg");
	this.shape_3.setTransform(-14.5,-3.9);

	this.instance = new lib.Path();
	this.instance.setTransform(-1,-1,1,1,0,0,0,26,28.9);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-29.9,60,64);


(lib._02globooscuro1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AABBeIAAgmIg1AAIAAgdIA4h4IAjAAIAAB1IAOAAIAAAgIgOAAIAAAmgAgRAYIASAAIAAgtg");
	this.shape.setTransform(8.1,-2.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgSBeIAAipQAJAAAEgGQAEgFAAgHIAUAAIAAC7g");
	this.shape_1.setTransform(-1.3,-2.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-7.5,6.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTBeIAAipQAKAAAEgGQAEgFAAgHIAVAAIAAC7g");
	this.shape_3.setTransform(-13.5,-2.4);

	this.instance = new lib.Path_1();
	this.instance.setTransform(-1,-1,1,1,0,0,0,26,28.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-29.3,60,64);


(lib._02globoclaro2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgVBeQgJgEgGgGQgHgHgDgIQgDgIAAgIIAAgOIAjAAIAAAJQAAAHADADQADAEAHAAQAGAAACgEQADgDAAgFIAAgpQgEAGgGAEQgEAEgLAAQgQAAgIgKQgFgEgCgGQgDgEAAgIIAAgnQAAgLAEgJQADgJAHgHQAGgGAKgEQAJgDAKAAQALAAAJADQAJAEAHAGQAGAHAEAJQAEAJAAALIAABhQAAALgEAJQgEAJgHAGQgHAHgKADQgJAEgJAAQgMAAgJgEgAgHg6QgCACAAAEIAAAoQAAAEACADQAEACADAAQAFAAACgCQADgDAAgEIAAgoQAAgEgDgCQgCgDgFAAQgDAAgEADg");
	this.shape.setTransform(11,-4.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgTBfQgJgEgHgGQgHgHgDgJQgEgJAAgLIAAhhQAAgLAEgJQAEgJAGgGQAHgHAJgEQAKgEAJAAQAMAAAJAFQAIAEAGAGQAGAHADAIQADAHAAAJIAAARIgjAAIAAgNQAAgHgDgDQgDgDgFAAQgEAAgDADQgDADAAAFIAAAsQAEgFAFgEQAGgDAKgBQAQAAAJAJQAEAFACAFQADAFAAAHIAAAlQAAALgEAJQgEAJgGAHQgGAGgKAEQgJADgLAAQgKAAgJgDgAgGAKQgDACAAAFIAAAjQAAAFADACQACADAEAAQAFAAACgDQADgCAAgFIAAgjQAAgFgDgCQgCgCgFgBQgEABgCACg");
	this.shape_1.setTransform(-0.6,-4.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-8.8,4.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgTBeIAAipQAKAAAEgGQAEgFAAgHIAVAAIAAC7g");
	this.shape_3.setTransform(-14.9,-4.6);

	this.instance = new lib.Path_2();
	this.instance.setTransform(-1,-1,1,1,0,0,0,26,29.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-30.2,60,66);


(lib._02globoclaro1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgTBeIAAiqQAKAAAEgEQAEgGAAgHIAVAAIAAC7g");
	this.shape.setTransform(11,-3.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgVBeQgJgEgGgGQgGgHgDgIQgEgIAAgIIAAgOIAjAAIAAAJQAAAHADADQADAEAHAAQAGAAADgEQACgDAAgFIAAgpQgEAGgGAEQgFAEgKAAQgPAAgKgKQgEgEgCgGQgDgEAAgIIAAgnQAAgLAEgJQADgJAHgHQAHgGAJgEQAJgDAKAAQALAAAJADQAJAEAHAGQAHAHADAJQAEAJAAALIAABhQAAALgEAJQgEAJgHAGQgGAHgLADQgJAEgJAAQgMAAgJgEgAgHg6QgCACAAAEIAAAoQAAAEACADQAEACADAAQAFAAADgCQACgDAAgEIAAgoQAAgEgCgCQgDgDgFAAQgDAAgEADg");
	this.shape_1.setTransform(1.3,-3.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-7,5.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgSBeIAAiqQAJAAAEgEQAEgGAAgHIAUAAIAAC7g");
	this.shape_3.setTransform(-13,-3.3);

	this.instance = new lib.Path_3();
	this.instance.setTransform(-0.9,-0.9,1,1,0,0,0,26.1,29.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-30.2,60,66);


(lib._02globoazul = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgVBcQgJgEgGgGQgGgHgCgIQgDgIAAgIIAAgRIAkAAIAAANQAAAGACAEQADADAGAAQADAAADgDQADgDAAgFIAAgrQAAgFgDgBQgDgCgDAAQgCAAgEACQgCABgBADIgjgEIAFhfIBXAAQgCASgFAJQgFAIgLAAIggAAIgCApQAFgHAFgEQAIgDAJAAQANAAAJAJQAEAFACAGQACAGABAHIAAAyQgBAKgEAJQgDAIgHAGQgHAHgJADQgKADgJAAQgMAAgIgEg");
	this.shape.setTransform(12.3,3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgiBeQAAgVAEgVQAEgVAGgUQAGgSAJgRQAFgSAJgNIg5AAIAAgmIBhAAIAAARQgLAUgHAVQgIAUgEATQgFAWgDAWQgCAWAAAYg");
	this.shape_1.setTransform(1.5,2.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOAYQAGAAAEgEQAEgEAAgKIgOAAIAAgmIAdAAIAAAiQAAAHgCAFQgCAGgEAEQgFAEgDADQgGACgHAAg");
	this.shape_2.setTransform(-6.5,11.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgwBgIAAgKQAAgLACgJQACgKAEgKQAEgKAIgLIASgYIAKgNIAHgMIAEgLIABgLQAAgHgDgDQgDgDgFAAQgEAAgDADQgDAEAAAGIAAASIgkAAIAAgWQAAgIADgIQADgIAGgHQAFgGAJgEQAJgEAKAAQANAAAKAEQAJAEAGAHQAGAHADAJQACAJAAAKIgBARQgBAHgEAIQgEAIgHAHQgGAJgKALIgLANIgFAKIgCAIIgBAHIAzAAIAAAkg");
	this.shape_3.setTransform(-14.5,2.5);

	this.instance = new lib.Path_4();
	this.instance.setTransform(-0.9,-0.9,1,1,0,0,0,26.1,28.8);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.098)",2,2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.1,-29.7,60,64);


(lib._00vertical4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group();
	this.instance.setTransform(0,0,1,1,0,0,0,0.5,112);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-112,1,224);


(lib._00vertical3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_1();
	this.instance.setTransform(0,0,1,1,0,0,0,0.5,112);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-112,1,224);


(lib._00vertical2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_2();
	this.instance.setTransform(0,0,1,1,0,0,0,0.5,112);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-112,1,224);


(lib._00horizontal6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_3();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib._00horizontal5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_4();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib._00horizontal4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_5();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib._00horizontal3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_6();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


(lib._00horizontal2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.Group_7();
	this.instance.setTransform(0,0,1,1,0,0,0,159,0.5);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159,-0.5,318,1);


// stage content:
(lib.grafica_15_v2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// 02 56,2%
	this.instance = new lib._02562();
	this.instance.setTransform(502.2,226.2);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).wait(1).to({regY:-0.9,x:502.1,y:225.3,alpha:0.001},0).wait(1).to({x:501.6,alpha:0.006},0).wait(1).to({x:500.9,alpha:0.014},0).wait(1).to({x:499.7,alpha:0.025},0).wait(1).to({x:498.2,alpha:0.04},0).wait(1).to({x:496.3,alpha:0.06},0).wait(1).to({x:493.8,alpha:0.084},0).wait(1).to({x:490.9,alpha:0.114},0).wait(1).to({x:487.4,alpha:0.149},0).wait(1).to({x:483.3,alpha:0.191},0).wait(1).to({x:478.5,alpha:0.239},0).wait(1).to({x:473.2,alpha:0.293},0).wait(1).to({x:467.3,alpha:0.352},0).wait(1).to({x:460.9,alpha:0.416},0).wait(1).to({x:454.3,alpha:0.483},0).wait(1).to({x:447.5,alpha:0.551},0).wait(1).to({x:440.9,alpha:0.618},0).wait(1).to({x:434.7,alpha:0.681},0).wait(1).to({x:428.9,alpha:0.738},0).wait(1).to({x:423.8,alpha:0.79},0).wait(1).to({x:419.3,alpha:0.836},0).wait(1).to({x:415.4,alpha:0.875},0).wait(1).to({x:412.2,alpha:0.907},0).wait(1).to({x:409.5,alpha:0.934},0).wait(1).to({x:407.4,alpha:0.956},0).wait(1).to({x:405.7,alpha:0.973},0).wait(1).to({x:404.5,alpha:0.985},0).wait(1).to({x:403.6,alpha:0.994},0).wait(1).to({x:403.2,alpha:0.998},0).wait(1).to({regY:0,x:403,y:226.2,alpha:1},0).wait(18));

	// 02 franja
	this.instance_1 = new lib._02franja();
	this.instance_1.setTransform(364.5,223.5,1,0.016);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(62).to({_off:false},0).wait(1).to({scaleY:0.02},0).wait(1).to({scaleY:0.02},0).wait(1).to({scaleY:0.03},0).wait(1).to({scaleY:0.04},0).wait(1).to({scaleY:0.06},0).wait(1).to({scaleY:0.08},0).wait(1).to({scaleY:0.1},0).wait(1).to({scaleY:0.13},0).wait(1).to({scaleY:0.16},0).wait(1).to({scaleY:0.2},0).wait(1).to({scaleY:0.25},0).wait(1).to({scaleY:0.3},0).wait(1).to({scaleY:0.36},0).wait(1).to({scaleY:0.43},0).wait(1).to({scaleY:0.49},0).wait(1).to({scaleY:0.56},0).wait(1).to({scaleY:0.62},0).wait(1).to({scaleY:0.69},0).wait(1).to({scaleY:0.74},0).wait(1).to({scaleY:0.79},0).wait(1).to({scaleY:0.84},0).wait(1).to({scaleY:0.88},0).wait(1).to({scaleY:0.91},0).wait(1).to({scaleY:0.94},0).wait(1).to({scaleY:0.96},0).wait(1).to({scaleY:0.97},0).wait(1).to({scaleY:0.99},0).wait(1).to({scaleY:0.99},0).wait(1).to({scaleY:1},0).wait(1).to({scaleY:1},0).wait(18));

	// 02 globo claro 2
	this.instance_2 = new lib._02globoclaro2();
	this.instance_2.setTransform(324.9,161.6,0.018,0.018);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(62).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).wait(1).to({scaleX:0.29,scaleY:0.29},0).wait(1).to({scaleX:0.38,scaleY:0.38},0).wait(1).to({scaleX:0.47,scaleY:0.47},0).wait(1).to({scaleX:0.57,scaleY:0.57},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(28));

	// 02 globo claro 1
	this.instance_3 = new lib._02globoclaro1();
	this.instance_3.setTransform(218.9,129.6,0.018,0.018);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(52).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).wait(1).to({scaleX:0.29,scaleY:0.29},0).wait(1).to({scaleX:0.38,scaleY:0.38},0).wait(1).to({scaleX:0.47,scaleY:0.47},0).wait(1).to({scaleX:0.57,scaleY:0.57},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(38));

	// 02 globo oscuro 2
	this.instance_4 = new lib._02globooscuro2();
	this.instance_4.setTransform(329.1,232.4,0.018,0.018);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(52).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).wait(1).to({scaleX:0.29,scaleY:0.29},0).wait(1).to({scaleX:0.38,scaleY:0.38},0).wait(1).to({scaleX:0.47,scaleY:0.47},0).wait(1).to({scaleX:0.57,scaleY:0.57},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(38));

	// 02 globo oscuro 1
	this.instance_5 = new lib._02globooscuro1();
	this.instance_5.setTransform(224.5,199.9,0.02,0.02);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(39).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.38,scaleY:0.38},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.57,scaleY:0.57},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(51));

	// 02 globo azul
	this.instance_6 = new lib._02globoazul();
	this.instance_6.setTransform(93.8,137.6,0.02,0.02);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(25).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.38,scaleY:0.38},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.57,scaleY:0.57},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.89,scaleY:0.89},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(65));

	// 02 punto blanco 5
	this.instance_7 = new lib._02puntoblanco5();
	this.instance_7.setTransform(316.4,189.6,0.224,0.224);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(62).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(38));

	// 02 punto blanco 4
	this.instance_8 = new lib._02puntoblanco4();
	this.instance_8.setTransform(210.1,157.8,0.224,0.224);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(52).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(48));

	// 02 punto blanco 3
	this.instance_9 = new lib._02puntoblanco3();
	this.instance_9.setTransform(318.4,260.3,0.224,0.224);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(52).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(48));

	// 02 punto blanco 2
	this.instance_10 = new lib._02puntoblanco2();
	this.instance_10.setTransform(210.1,227,0.224,0.224);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(39).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1,y:227.1},0).wait(61));

	// 02 punto blanco 1
	this.instance_11 = new lib._02puntoblanco1();
	this.instance_11.setTransform(104,106.9,0.224,0.224);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(25).to({_off:false},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.27,scaleY:0.27},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.75,scaleY:0.75},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(75));

	// 00 mascara (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_39 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_graphics_40 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_41 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_42 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_graphics_43 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_graphics_44 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_graphics_45 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_46 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_graphics_47 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_48 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_49 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_graphics_50 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_51 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_52 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_53 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_54 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_55 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_56 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_57 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_58 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_59 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_60 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_graphics_61 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_62 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_63 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_graphics_64 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_65 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_graphics_66 = new cjs.Graphics().p("A5BX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_67 = new cjs.Graphics().p("A44X+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_68 = new cjs.Graphics().p("A4zX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_graphics_69 = new cjs.Graphics().p("A4yX+MAAAgjlMAyIAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(39).to({graphics:mask_graphics_39,x:-61.3,y:153.5}).wait(1).to({graphics:mask_graphics_40,x:-61,y:153.5}).wait(1).to({graphics:mask_graphics_41,x:-60,y:153.5}).wait(1).to({graphics:mask_graphics_42,x:-58.3,y:153.5}).wait(1).to({graphics:mask_graphics_43,x:-55.7,y:153.5}).wait(1).to({graphics:mask_graphics_44,x:-52.2,y:153.5}).wait(1).to({graphics:mask_graphics_45,x:-47.7,y:153.5}).wait(1).to({graphics:mask_graphics_46,x:-42.1,y:153.5}).wait(1).to({graphics:mask_graphics_47,x:-35.2,y:153.5}).wait(1).to({graphics:mask_graphics_48,x:-27,y:153.5}).wait(1).to({graphics:mask_graphics_49,x:-17.3,y:153.5}).wait(1).to({graphics:mask_graphics_50,x:-6.1,y:153.5}).wait(1).to({graphics:mask_graphics_51,x:6.6,y:153.5}).wait(1).to({graphics:mask_graphics_52,x:20.7,y:153.5}).wait(1).to({graphics:mask_graphics_53,x:35.7,y:153.5}).wait(1).to({graphics:mask_graphics_54,x:51.4,y:153.5}).wait(1).to({graphics:mask_graphics_55,x:67,y:153.5}).wait(1).to({graphics:mask_graphics_56,x:82.1,y:153.5}).wait(1).to({graphics:mask_graphics_57,x:96.1,y:153.5}).wait(1).to({graphics:mask_graphics_58,x:108.8,y:153.5}).wait(1).to({graphics:mask_graphics_59,x:120,y:153.5}).wait(1).to({graphics:mask_graphics_60,x:129.7,y:153.5}).wait(1).to({graphics:mask_graphics_61,x:138,y:153.5}).wait(1).to({graphics:mask_graphics_62,x:144.8,y:153.5}).wait(1).to({graphics:mask_graphics_63,x:150.5,y:153.5}).wait(1).to({graphics:mask_graphics_64,x:155,y:153.5}).wait(1).to({graphics:mask_graphics_65,x:158.4,y:153.5}).wait(1).to({graphics:mask_graphics_66,x:160.8,y:153.5}).wait(1).to({graphics:mask_graphics_67,x:161.6,y:153.5}).wait(1).to({graphics:mask_graphics_68,x:162.1,y:153.5}).wait(1).to({graphics:mask_graphics_69,x:162.3,y:153.5}).wait(41));

	// 01 linea clara
	this.instance_12 = new lib._01lineaclara();
	this.instance_12.setTransform(212.4,152.3);
	this.instance_12._off = true;

	this.instance_12.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(39).to({_off:false},0).wait(71));

	// 00 mascara (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_29 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_30 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_31 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_32 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_33 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_34 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_35 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_36 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_37 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_38 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_39 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_40 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_41 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_42 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_43 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_44 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_45 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_46 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_47 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_48 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_49 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_50 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_51 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_52 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_53 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_54 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_55 = new cjs.Graphics().p("A5EX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_56 = new cjs.Graphics().p("A5DX+MAAAgjlMAyIAAAMAAAAjlg");
	var mask_1_graphics_57 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");
	var mask_1_graphics_58 = new cjs.Graphics().p("A5DX+MAAAgjlMAyHAAAMAAAAjlg");
	var mask_1_graphics_59 = new cjs.Graphics().p("A5EX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(29).to({graphics:mask_1_graphics_29,x:-61.3,y:153.5}).wait(1).to({graphics:mask_1_graphics_30,x:-61,y:153.5}).wait(1).to({graphics:mask_1_graphics_31,x:-60.1,y:153.5}).wait(1).to({graphics:mask_1_graphics_32,x:-58.4,y:153.5}).wait(1).to({graphics:mask_1_graphics_33,x:-56,y:153.5}).wait(1).to({graphics:mask_1_graphics_34,x:-52.8,y:153.5}).wait(1).to({graphics:mask_1_graphics_35,x:-48.6,y:153.5}).wait(1).to({graphics:mask_1_graphics_36,x:-43.3,y:153.5}).wait(1).to({graphics:mask_1_graphics_37,x:-36.8,y:153.5}).wait(1).to({graphics:mask_1_graphics_38,x:-29.1,y:153.5}).wait(1).to({graphics:mask_1_graphics_39,x:-20,y:153.5}).wait(1).to({graphics:mask_1_graphics_40,x:-9.5,y:153.5}).wait(1).to({graphics:mask_1_graphics_41,x:2.4,y:153.5}).wait(1).to({graphics:mask_1_graphics_42,x:15.6,y:153.5}).wait(1).to({graphics:mask_1_graphics_43,x:29.7,y:153.5}).wait(1).to({graphics:mask_1_graphics_44,x:44.4,y:153.5}).wait(1).to({graphics:mask_1_graphics_45,x:59,y:153.5}).wait(1).to({graphics:mask_1_graphics_46,x:73.1,y:153.5}).wait(1).to({graphics:mask_1_graphics_47,x:86.3,y:153.5}).wait(1).to({graphics:mask_1_graphics_48,x:98.2,y:153.5}).wait(1).to({graphics:mask_1_graphics_49,x:108.7,y:153.5}).wait(1).to({graphics:mask_1_graphics_50,x:117.8,y:153.5}).wait(1).to({graphics:mask_1_graphics_51,x:125.5,y:153.5}).wait(1).to({graphics:mask_1_graphics_52,x:132,y:153.5}).wait(1).to({graphics:mask_1_graphics_53,x:137.3,y:153.5}).wait(1).to({graphics:mask_1_graphics_54,x:141.5,y:153.5}).wait(1).to({graphics:mask_1_graphics_55,x:144.7,y:153.5}).wait(1).to({graphics:mask_1_graphics_56,x:147.1,y:153.5}).wait(1).to({graphics:mask_1_graphics_57,x:148.8,y:153.5}).wait(1).to({graphics:mask_1_graphics_58,x:149.7,y:153.5}).wait(1).to({graphics:mask_1_graphics_59,x:150,y:153.5}).wait(51));

	// 01 linea oscura
	this.instance_13 = new lib._01lineaoscura();
	this.instance_13.setTransform(205.3,175.5);
	this.instance_13._off = true;

	this.instance_13.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(29).to({_off:false},0).wait(81));

	// 00 Aceites esenciales
	this.instance_14 = new lib._00Aceitesesenciales();
	this.instance_14.setTransform(488.8,71.8,1,1,0,0,0,-0.1,0.1);
	this.instance_14.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(1).to({regX:-0.3,regY:-0.6,x:488.6,y:71.1,alpha:0.001},0).wait(1).to({y:71.3,alpha:0.006},0).wait(1).to({y:71.6,alpha:0.013},0).wait(1).to({y:72,alpha:0.025},0).wait(1).to({y:72.6,alpha:0.04},0).wait(1).to({x:488.7,y:73.3,alpha:0.06},0).wait(1).to({y:74.3,alpha:0.085},0).wait(1).to({y:75.4,alpha:0.115},0).wait(1).to({x:488.8,y:76.8,alpha:0.151},0).wait(1).to({x:488.9,y:78.4,alpha:0.194},0).wait(1).to({x:489,y:80.3,alpha:0.243},0).wait(1).to({x:489.1,y:82.4,alpha:0.299},0).wait(1).to({x:489.2,y:84.8,alpha:0.362},0).wait(1).to({x:489.3,y:87.3,alpha:0.429},0).wait(1).to({x:489.4,y:90,alpha:0.5},0).wait(1).to({x:489.5,y:92.7,alpha:0.571},0).wait(1).to({x:489.6,y:95.4,alpha:0.641},0).wait(1).to({x:489.7,y:97.8,alpha:0.705},0).wait(1).to({x:489.8,y:100,alpha:0.764},0).wait(1).to({x:489.9,y:102,alpha:0.815},0).wait(1).to({x:490,y:103.7,alpha:0.86},0).wait(1).to({x:490.1,y:105.1,alpha:0.897},0).wait(1).to({y:106.2,alpha:0.927},0).wait(1).to({x:490.2,y:107.2,alpha:0.951},0).wait(1).to({y:107.9,alpha:0.97},0).wait(1).to({y:108.4,alpha:0.984},0).wait(1).to({y:108.7,alpha:0.993},0).wait(1).to({y:108.9,alpha:0.998},0).wait(1).to({regX:-0.1,regY:0.1,x:490.5,y:109.7,alpha:1},0).wait(81));

	// 00 circulo oscuro
	this.instance_15 = new lib._00circulooscuro();
	this.instance_15.setTransform(415.8,108.6,0.052,0.052);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.4,scaleY:0.4},0).wait(1).to({scaleX:0.46,scaleY:0.46},0).wait(1).to({scaleX:0.53,scaleY:0.53},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.66,scaleY:0.66},0).wait(1).to({scaleX:0.72,scaleY:0.72},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(81));

	// 00 CPC
	this.instance_16 = new lib._00CPC();
	this.instance_16.setTransform(442.2,43.1,1,1,0,0,0,0.1,0.1);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(2).to({_off:false},0).wait(1).to({regY:-0.6,y:42.4,alpha:0.001},0).wait(1).to({y:42.6,alpha:0.006},0).wait(1).to({y:42.9,alpha:0.013},0).wait(1).to({y:43.3,alpha:0.025},0).wait(1).to({x:442.3,y:43.9,alpha:0.04},0).wait(1).to({y:44.6,alpha:0.06},0).wait(1).to({y:45.6,alpha:0.085},0).wait(1).to({x:442.4,y:46.7,alpha:0.115},0).wait(1).to({x:442.5,y:48.1,alpha:0.151},0).wait(1).to({y:49.7,alpha:0.194},0).wait(1).to({x:442.6,y:51.6,alpha:0.243},0).wait(1).to({x:442.7,y:53.7,alpha:0.299},0).wait(1).to({x:442.8,y:56.1,alpha:0.362},0).wait(1).to({x:442.9,y:58.6,alpha:0.429},0).wait(1).to({x:443,y:61.3,alpha:0.5},0).wait(1).to({x:443.2,y:64,alpha:0.571},0).wait(1).to({x:443.3,y:66.7,alpha:0.641},0).wait(1).to({x:443.4,y:69.1,alpha:0.705},0).wait(1).to({x:443.5,y:71.3,alpha:0.764},0).wait(1).to({x:443.6,y:73.3,alpha:0.815},0).wait(1).to({x:443.7,y:75,alpha:0.86},0).wait(1).to({y:76.4,alpha:0.897},0).wait(1).to({x:443.8,y:77.5,alpha:0.927},0).wait(1).to({y:78.5,alpha:0.951},0).wait(1).to({y:79.2,alpha:0.97},0).wait(1).to({x:443.9,y:79.7,alpha:0.984},0).wait(1).to({y:80,alpha:0.993},0).wait(1).to({y:80.2,alpha:0.998},0).wait(1).to({regY:0.1,y:81,alpha:1},0).wait(79));

	// 00 circulo claro
	this.instance_17 = new lib._00circuloclaro();
	this.instance_17.setTransform(415.8,79.9,0.052,0.052);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(2).to({_off:false},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.28,scaleY:0.28},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.4,scaleY:0.4},0).wait(1).to({scaleX:0.46,scaleY:0.46},0).wait(1).to({scaleX:0.53,scaleY:0.53},0).wait(1).to({scaleX:0.59,scaleY:0.59},0).wait(1).to({scaleX:0.66,scaleY:0.66},0).wait(1).to({scaleX:0.72,scaleY:0.72},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(79));

	// 00 leyenda
	this.instance_18 = new lib._00leyenda();
	this.instance_18.setTransform(443.3,14.7);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(4).to({_off:false},0).wait(1).to({regX:0.4,regY:-0.8,x:443.7,y:14,alpha:0.001},0).wait(1).to({y:14.1,alpha:0.006},0).wait(1).to({y:14.4,alpha:0.013},0).wait(1).to({y:14.8,alpha:0.025},0).wait(1).to({x:443.8,y:15.4,alpha:0.04},0).wait(1).to({y:16.2,alpha:0.06},0).wait(1).to({y:17.1,alpha:0.085},0).wait(1).to({x:443.9,y:18.3,alpha:0.115},0).wait(1).to({x:444,y:19.6,alpha:0.151},0).wait(1).to({y:21.3,alpha:0.194},0).wait(1).to({x:444.1,y:23.1,alpha:0.243},0).wait(1).to({x:444.2,y:25.3,alpha:0.299},0).wait(1).to({x:444.3,y:27.6,alpha:0.362},0).wait(1).to({x:444.4,y:30.2,alpha:0.429},0).wait(1).to({x:444.5,y:32.9,alpha:0.5},0).wait(1).to({x:444.7,y:35.6,alpha:0.571},0).wait(1).to({x:444.8,y:38.2,alpha:0.641},0).wait(1).to({x:444.9,y:40.7,alpha:0.705},0).wait(1).to({x:445,y:42.9,alpha:0.764},0).wait(1).to({x:445.1,y:44.8,alpha:0.815},0).wait(1).to({x:445.2,y:46.5,alpha:0.86},0).wait(1).to({y:47.9,alpha:0.897},0).wait(1).to({x:445.3,y:49.1,alpha:0.927},0).wait(1).to({y:50,alpha:0.951},0).wait(1).to({y:50.7,alpha:0.97},0).wait(1).to({x:445.4,y:51.2,alpha:0.984},0).wait(1).to({y:51.6,alpha:0.993},0).wait(1).to({y:51.8,alpha:0.998},0).wait(1).to({regX:0,regY:0,x:445,y:52.7,alpha:1},0).wait(77));

	// 00 titulo
	this.instance_19 = new lib._00titulo();
	this.instance_19.setTransform(269.1,48.7,1,1,0,0,0,0.1,0.2);
	this.instance_19.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(1).to({regX:0.4,regY:-4.1,x:269.2,y:44.4,alpha:0.001},0).wait(1).to({x:268.5,alpha:0.006},0).wait(1).to({x:267.5,alpha:0.014},0).wait(1).to({x:265.9,alpha:0.025},0).wait(1).to({x:263.8,alpha:0.04},0).wait(1).to({x:261,alpha:0.06},0).wait(1).to({x:257.5,alpha:0.085},0).wait(1).to({x:253.3,alpha:0.115},0).wait(1).to({x:248.3,alpha:0.152},0).wait(1).to({x:242.3,alpha:0.195},0).wait(1).to({x:235.4,alpha:0.244},0).wait(1).to({x:227.6,alpha:0.301},0).wait(1).to({x:218.9,alpha:0.364},0).wait(1).to({x:209.4,alpha:0.431},0).wait(1).to({x:199.6,alpha:0.502},0).wait(1).to({x:189.7,alpha:0.574},0).wait(1).to({x:180.1,alpha:0.643},0).wait(1).to({x:171.1,alpha:0.708},0).wait(1).to({x:163,alpha:0.766},0).wait(1).to({x:155.9,alpha:0.817},0).wait(1).to({x:149.8,alpha:0.861},0).wait(1).to({x:144.7,alpha:0.898},0).wait(1).to({x:140.5,alpha:0.928},0).wait(1).to({x:137.2,alpha:0.952},0).wait(1).to({x:134.6,alpha:0.97},0).wait(1).to({x:132.7,alpha:0.984},0).wait(1).to({x:131.5,alpha:0.993},0).wait(1).to({x:130.7,alpha:0.998},0).wait(1).to({regX:0.1,regY:0.2,x:130.2,y:48.7,alpha:1},0).wait(81));

	// 00 mascara (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_12 = new cjs.Graphics().p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(12).to({graphics:mask_2_graphics_12,x:184.5,y:153.5}).wait(98));

	// 00 horizontal 7
	this.instance_20 = new lib._00horizontal7();
	this.instance_20.setTransform(-111,79.5);
	this.instance_20._off = true;

	this.instance_20.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(12).to({_off:false},0).wait(1).to({x:-110.6},0).wait(1).to({x:-109.1},0).wait(1).to({x:-106.6},0).wait(1).to({x:-102.8},0).wait(1).to({x:-97.7},0).wait(1).to({x:-91},0).wait(1).to({x:-82.6},0).wait(1).to({x:-72.4},0).wait(1).to({x:-60.1},0).wait(1).to({x:-45.6},0).wait(1).to({x:-28.8},0).wait(1).to({x:-9.7},0).wait(1).to({x:11.4},0).wait(1).to({x:33.9},0).wait(1).to({x:57.1},0).wait(1).to({x:80.2},0).wait(1).to({x:102},0).wait(1).to({x:122.1},0).wait(1).to({x:139.9},0).wait(1).to({x:155.4},0).wait(1).to({x:168.6},0).wait(1).to({x:179.6},0).wait(1).to({x:188.6},0).wait(1).to({x:195.7},0).wait(1).to({x:201.2},0).wait(1).to({x:205.2},0).wait(1).to({x:208},0).wait(1).to({x:209.5},0).wait(1).to({x:210},0).wait(69));

	// 00 mascara (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	var mask_3_graphics_10 = new cjs.Graphics().p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(10).to({graphics:mask_3_graphics_10,x:184.5,y:153.5}).wait(100));

	// 00 horizontal 6
	this.instance_21 = new lib._00horizontal6();
	this.instance_21.setTransform(-111,115.5);
	this.instance_21._off = true;

	this.instance_21.mask = mask_3;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(10).to({_off:false},0).wait(1).to({x:-110.6},0).wait(1).to({x:-109.1},0).wait(1).to({x:-106.6},0).wait(1).to({x:-102.8},0).wait(1).to({x:-97.7},0).wait(1).to({x:-91},0).wait(1).to({x:-82.6},0).wait(1).to({x:-72.4},0).wait(1).to({x:-60.1},0).wait(1).to({x:-45.6},0).wait(1).to({x:-28.8},0).wait(1).to({x:-9.7},0).wait(1).to({x:11.4},0).wait(1).to({x:33.9},0).wait(1).to({x:57.1},0).wait(1).to({x:80.2},0).wait(1).to({x:102},0).wait(1).to({x:122.1},0).wait(1).to({x:139.9},0).wait(1).to({x:155.4},0).wait(1).to({x:168.6},0).wait(1).to({x:179.6},0).wait(1).to({x:188.6},0).wait(1).to({x:195.7},0).wait(1).to({x:201.2},0).wait(1).to({x:205.2},0).wait(1).to({x:208},0).wait(1).to({x:209.5},0).wait(1).to({x:210},0).wait(71));

	// 00 mascara (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	var mask_4_graphics_8 = new cjs.Graphics().p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_4).to({graphics:null,x:0,y:0}).wait(8).to({graphics:mask_4_graphics_8,x:184.5,y:153.5}).wait(102));

	// 00 horizontal 5
	this.instance_22 = new lib._00horizontal5();
	this.instance_22.setTransform(-111,153.5);
	this.instance_22._off = true;

	this.instance_22.mask = mask_4;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(8).to({_off:false},0).wait(1).to({x:-110.6},0).wait(1).to({x:-109.1},0).wait(1).to({x:-106.6},0).wait(1).to({x:-102.8},0).wait(1).to({x:-97.7},0).wait(1).to({x:-91},0).wait(1).to({x:-82.6},0).wait(1).to({x:-72.4},0).wait(1).to({x:-60.1},0).wait(1).to({x:-45.6},0).wait(1).to({x:-28.8},0).wait(1).to({x:-9.7},0).wait(1).to({x:11.4},0).wait(1).to({x:33.9},0).wait(1).to({x:57.1},0).wait(1).to({x:80.2},0).wait(1).to({x:102},0).wait(1).to({x:122.1},0).wait(1).to({x:139.9},0).wait(1).to({x:155.4},0).wait(1).to({x:168.6},0).wait(1).to({x:179.6},0).wait(1).to({x:188.6},0).wait(1).to({x:195.7},0).wait(1).to({x:201.2},0).wait(1).to({x:205.2},0).wait(1).to({x:208},0).wait(1).to({x:209.5},0).wait(1).to({x:210},0).wait(73));

	// 00 mascara (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	var mask_5_graphics_6 = new cjs.Graphics().p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_5).to({graphics:null,x:0,y:0}).wait(6).to({graphics:mask_5_graphics_6,x:184.5,y:153.5}).wait(104));

	// 00 horizontal 4
	this.instance_23 = new lib._00horizontal4();
	this.instance_23.setTransform(-111,191.5);
	this.instance_23._off = true;

	this.instance_23.mask = mask_5;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(6).to({_off:false},0).wait(1).to({x:-110.6},0).wait(1).to({x:-109.1},0).wait(1).to({x:-106.6},0).wait(1).to({x:-102.8},0).wait(1).to({x:-97.7},0).wait(1).to({x:-91},0).wait(1).to({x:-82.6},0).wait(1).to({x:-72.4},0).wait(1).to({x:-60.1},0).wait(1).to({x:-45.6},0).wait(1).to({x:-28.8},0).wait(1).to({x:-9.7},0).wait(1).to({x:11.4},0).wait(1).to({x:33.9},0).wait(1).to({x:57.1},0).wait(1).to({x:80.2},0).wait(1).to({x:102},0).wait(1).to({x:122.1},0).wait(1).to({x:139.9},0).wait(1).to({x:155.4},0).wait(1).to({x:168.6},0).wait(1).to({x:179.6},0).wait(1).to({x:188.6},0).wait(1).to({x:195.7},0).wait(1).to({x:201.2},0).wait(1).to({x:205.2},0).wait(1).to({x:208},0).wait(1).to({x:209.5},0).wait(1).to({x:210},0).wait(75));

	// 00 mascara (mask)
	var mask_6 = new cjs.Shape();
	mask_6._off = true;
	var mask_6_graphics_4 = new cjs.Graphics().p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_6).to({graphics:null,x:0,y:0}).wait(4).to({graphics:mask_6_graphics_4,x:184.5,y:153.5}).wait(106));

	// 00 horizontal 3
	this.instance_24 = new lib._00horizontal3();
	this.instance_24.setTransform(-111,229.5);
	this.instance_24._off = true;

	this.instance_24.mask = mask_6;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(4).to({_off:false},0).wait(1).to({x:-110.6},0).wait(1).to({x:-109.1},0).wait(1).to({x:-106.6},0).wait(1).to({x:-102.8},0).wait(1).to({x:-97.7},0).wait(1).to({x:-91},0).wait(1).to({x:-82.6},0).wait(1).to({x:-72.4},0).wait(1).to({x:-60.1},0).wait(1).to({x:-45.6},0).wait(1).to({x:-28.8},0).wait(1).to({x:-9.7},0).wait(1).to({x:11.4},0).wait(1).to({x:33.9},0).wait(1).to({x:57.1},0).wait(1).to({x:80.2},0).wait(1).to({x:102},0).wait(1).to({x:122.1},0).wait(1).to({x:139.9},0).wait(1).to({x:155.4},0).wait(1).to({x:168.6},0).wait(1).to({x:179.6},0).wait(1).to({x:188.6},0).wait(1).to({x:195.7},0).wait(1).to({x:201.2},0).wait(1).to({x:205.2},0).wait(1).to({x:208},0).wait(1).to({x:209.5},0).wait(1).to({x:210},0).wait(77));

	// 00 mascara (mask)
	var mask_7 = new cjs.Shape();
	mask_7._off = true;
	var mask_7_graphics_2 = new cjs.Graphics().p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_7).to({graphics:null,x:0,y:0}).wait(2).to({graphics:mask_7_graphics_2,x:184.5,y:153.5}).wait(108));

	// 00 horizontal 2
	this.instance_25 = new lib._00horizontal2();
	this.instance_25.setTransform(-111,267.5);
	this.instance_25._off = true;

	this.instance_25.mask = mask_7;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(2).to({_off:false},0).wait(1).to({x:-110.6},0).wait(1).to({x:-109.1},0).wait(1).to({x:-106.6},0).wait(1).to({x:-102.8},0).wait(1).to({x:-97.7},0).wait(1).to({x:-91},0).wait(1).to({x:-82.6},0).wait(1).to({x:-72.4},0).wait(1).to({x:-60.1},0).wait(1).to({x:-45.6},0).wait(1).to({x:-28.8},0).wait(1).to({x:-9.7},0).wait(1).to({x:11.4},0).wait(1).to({x:33.9},0).wait(1).to({x:57.1},0).wait(1).to({x:80.2},0).wait(1).to({x:102},0).wait(1).to({x:122.1},0).wait(1).to({x:139.9},0).wait(1).to({x:155.4},0).wait(1).to({x:168.6},0).wait(1).to({x:179.6},0).wait(1).to({x:188.6},0).wait(1).to({x:195.7},0).wait(1).to({x:201.2},0).wait(1).to({x:205.2},0).wait(1).to({x:208},0).wait(1).to({x:209.5},0).wait(1).to({x:210},0).wait(79));

	// 00 mascara (mask)
	var mask_8 = new cjs.Shape();
	mask_8._off = true;
	mask_8.graphics.p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");
	mask_8.setTransform(184.5,153.5);

	// 00 horizontal 1
	this.instance_26 = new lib._00horizontal1();
	this.instance_26.setTransform(-112.5,305.5);

	this.instance_26.mask = mask_8;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(1).to({x:-112.1},0).wait(1).to({x:-110.6},0).wait(1).to({x:-108.1},0).wait(1).to({x:-104.3},0).wait(1).to({x:-99.2},0).wait(1).to({x:-92.5},0).wait(1).to({x:-84.1},0).wait(1).to({x:-73.9},0).wait(1).to({x:-61.6},0).wait(1).to({x:-47.1},0).wait(1).to({x:-30.3},0).wait(1).to({x:-11.2},0).wait(1).to({x:9.9},0).wait(1).to({x:32.4},0).wait(1).to({x:55.6},0).wait(1).to({x:78.7},0).wait(1).to({x:100.5},0).wait(1).to({x:120.6},0).wait(1).to({x:138.4},0).wait(1).to({x:153.9},0).wait(1).to({x:167.1},0).wait(1).to({x:178.1},0).wait(1).to({x:187.1},0).wait(1).to({x:194.2},0).wait(1).to({x:199.7},0).wait(1).to({x:203.7},0).wait(1).to({x:206.5},0).wait(1).to({x:208},0).wait(1).to({x:208.5},0).wait(81));

	// 00 mascara (mask)
	var mask_9 = new cjs.Shape();
	mask_9._off = true;
	var mask_9_graphics_9 = new cjs.Graphics().p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_9).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_9_graphics_9,x:184.5,y:153.5}).wait(101));

	// 00 vertical 4
	this.instance_27 = new lib._00vertical4();
	this.instance_27.setTransform(315.5,420);
	this.instance_27._off = true;

	this.instance_27.mask = mask_9;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(9).to({_off:false},0).wait(1).to({y:419.7},0).wait(1).to({y:418.6},0).wait(1).to({y:416.8},0).wait(1).to({y:414.1},0).wait(1).to({y:410.5},0).wait(1).to({y:405.8},0).wait(1).to({y:399.8},0).wait(1).to({y:392.6},0).wait(1).to({y:383.8},0).wait(1).to({y:373.5},0).wait(1).to({y:361.6},0).wait(1).to({y:348},0).wait(1).to({y:333.1},0).wait(1).to({y:317.1},0).wait(1).to({y:300.6},0).wait(1).to({y:284.2},0).wait(1).to({y:268.7},0).wait(1).to({y:254.4},0).wait(1).to({y:241.7},0).wait(1).to({y:230.7},0).wait(1).to({y:221.4},0).wait(1).to({y:213.6},0).wait(1).to({y:207.2},0).wait(1).to({y:202.2},0).wait(1).to({y:198.3},0).wait(1).to({y:195.4},0).wait(1).to({y:193.4},0).wait(1).to({y:192.3},0).wait(1).to({y:192},0).wait(72));

	// 00 mascara (mask)
	var mask_10 = new cjs.Shape();
	mask_10._off = true;
	var mask_10_graphics_6 = new cjs.Graphics().p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_10).to({graphics:null,x:0,y:0}).wait(6).to({graphics:mask_10_graphics_6,x:184.5,y:153.5}).wait(104));

	// 00 vertical 3
	this.instance_28 = new lib._00vertical3();
	this.instance_28.setTransform(209.5,420);
	this.instance_28._off = true;

	this.instance_28.mask = mask_10;

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(6).to({_off:false},0).wait(1).to({y:419.7},0).wait(1).to({y:418.6},0).wait(1).to({y:416.8},0).wait(1).to({y:414.1},0).wait(1).to({y:410.5},0).wait(1).to({y:405.8},0).wait(1).to({y:399.8},0).wait(1).to({y:392.6},0).wait(1).to({y:383.8},0).wait(1).to({y:373.5},0).wait(1).to({y:361.6},0).wait(1).to({y:348},0).wait(1).to({y:333.1},0).wait(1).to({y:317.1},0).wait(1).to({y:300.6},0).wait(1).to({y:284.2},0).wait(1).to({y:268.7},0).wait(1).to({y:254.4},0).wait(1).to({y:241.7},0).wait(1).to({y:230.7},0).wait(1).to({y:221.4},0).wait(1).to({y:213.6},0).wait(1).to({y:207.2},0).wait(1).to({y:202.2},0).wait(1).to({y:198.3},0).wait(1).to({y:195.4},0).wait(1).to({y:193.4},0).wait(1).to({y:192.3},0).wait(1).to({y:192},0).wait(75));

	// 00 mascara (mask)
	var mask_11 = new cjs.Shape();
	mask_11._off = true;
	var mask_11_graphics_3 = new cjs.Graphics().p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");

	this.timeline.addTween(cjs.Tween.get(mask_11).to({graphics:null,x:0,y:0}).wait(3).to({graphics:mask_11_graphics_3,x:184.5,y:153.5}).wait(107));

	// 00 vertical 2
	this.instance_29 = new lib._00vertical2();
	this.instance_29.setTransform(103.5,420);
	this.instance_29._off = true;

	this.instance_29.mask = mask_11;

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(3).to({_off:false},0).wait(1).to({y:419.7},0).wait(1).to({y:418.6},0).wait(1).to({y:416.8},0).wait(1).to({y:414.1},0).wait(1).to({y:410.5},0).wait(1).to({y:405.8},0).wait(1).to({y:399.8},0).wait(1).to({y:392.6},0).wait(1).to({y:383.8},0).wait(1).to({y:373.5},0).wait(1).to({y:361.6},0).wait(1).to({y:348},0).wait(1).to({y:333.1},0).wait(1).to({y:317.1},0).wait(1).to({y:300.6},0).wait(1).to({y:284.2},0).wait(1).to({y:268.7},0).wait(1).to({y:254.4},0).wait(1).to({y:241.7},0).wait(1).to({y:230.7},0).wait(1).to({y:221.4},0).wait(1).to({y:213.6},0).wait(1).to({y:207.2},0).wait(1).to({y:202.2},0).wait(1).to({y:198.3},0).wait(1).to({y:195.4},0).wait(1).to({y:193.4},0).wait(1).to({y:192.3},0).wait(1).to({y:192},0).wait(78));

	// 00 mascara (mask)
	var mask_12 = new cjs.Shape();
	mask_12._off = true;
	mask_12.graphics.p("A1UX+MAAAgjlMAyJAAAMAAAAjlg");
	mask_12.setTransform(184.5,153.5);

	// 00 vertical 1
	this.instance_30 = new lib._00vertical1();
	this.instance_30.setTransform(49.5,421);

	this.instance_30.mask = mask_12;

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(1).to({y:420.7},0).wait(1).to({y:419.6},0).wait(1).to({y:417.8},0).wait(1).to({y:415.1},0).wait(1).to({y:411.5},0).wait(1).to({y:406.8},0).wait(1).to({y:400.8},0).wait(1).to({y:393.6},0).wait(1).to({y:384.8},0).wait(1).to({y:374.5},0).wait(1).to({y:362.6},0).wait(1).to({y:349},0).wait(1).to({y:334.1},0).wait(1).to({y:318.1},0).wait(1).to({y:301.6},0).wait(1).to({y:285.2},0).wait(1).to({y:269.7},0).wait(1).to({y:255.4},0).wait(1).to({y:242.7},0).wait(1).to({y:231.7},0).wait(1).to({y:222.4},0).wait(1).to({y:214.6},0).wait(1).to({y:208.2},0).wait(1).to({y:203.2},0).wait(1).to({y:199.3},0).wait(1).to({y:196.4},0).wait(1).to({y:194.4},0).wait(1).to({y:193.3},0).wait(1).to({y:193},0).wait(81));

	// 00 6 meses
	this.instance_31 = new lib._006meses();
	this.instance_31.setTransform(315.6,349.1);
	this.instance_31.alpha = 0;
	this.instance_31._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(10).to({_off:false},0).wait(1).to({regX:0.1,regY:-0.6,x:315.7,y:348.5,alpha:0.001},0).wait(1).to({y:348.3,alpha:0.006},0).wait(1).to({y:348.1,alpha:0.014},0).wait(1).to({y:347.8,alpha:0.026},0).wait(1).to({y:347.3,alpha:0.042},0).wait(1).to({y:346.8,alpha:0.063},0).wait(1).to({y:346.1,alpha:0.089},0).wait(1).to({y:345.2,alpha:0.12},0).wait(1).to({y:344.1,alpha:0.158},0).wait(1).to({y:342.9,alpha:0.202},0).wait(1).to({y:341.5,alpha:0.253},0).wait(1).to({y:339.9,alpha:0.311},0).wait(1).to({y:338.2,alpha:0.375},0).wait(1).to({y:336.3,alpha:0.443},0).wait(1).to({y:334.3,alpha:0.514},0).wait(1).to({y:332.4,alpha:0.585},0).wait(1).to({y:330.5,alpha:0.653},0).wait(1).to({y:328.7,alpha:0.716},0).wait(1).to({y:327.2,alpha:0.772},0).wait(1).to({y:325.8,alpha:0.822},0).wait(1).to({y:324.6,alpha:0.865},0).wait(1).to({y:323.7,alpha:0.9},0).wait(1).to({y:322.8,alpha:0.929},0).wait(1).to({y:322.2,alpha:0.953},0).wait(1).to({y:321.7,alpha:0.971},0).wait(1).to({y:321.3,alpha:0.984},0).wait(1).to({y:321.1,alpha:0.993},0).wait(1).to({y:320.9,alpha:0.998},0).wait(1).to({regX:0,regY:0,x:315.6,y:321.5,alpha:1},0).wait(71));

	// 00 3 meses
	this.instance_32 = new lib._003meses();
	this.instance_32.setTransform(209.7,349.1);
	this.instance_32.alpha = 0;
	this.instance_32._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_32).wait(7).to({_off:false},0).wait(1).to({regY:-0.6,y:348.5,alpha:0.001},0).wait(1).to({y:348.3,alpha:0.006},0).wait(1).to({y:348.1,alpha:0.014},0).wait(1).to({y:347.8,alpha:0.026},0).wait(1).to({y:347.3,alpha:0.042},0).wait(1).to({y:346.8,alpha:0.063},0).wait(1).to({y:346.1,alpha:0.089},0).wait(1).to({y:345.2,alpha:0.12},0).wait(1).to({y:344.1,alpha:0.158},0).wait(1).to({y:342.9,alpha:0.202},0).wait(1).to({y:341.5,alpha:0.253},0).wait(1).to({y:339.9,alpha:0.311},0).wait(1).to({y:338.2,alpha:0.375},0).wait(1).to({y:336.3,alpha:0.443},0).wait(1).to({y:334.3,alpha:0.514},0).wait(1).to({y:332.4,alpha:0.585},0).wait(1).to({y:330.5,alpha:0.653},0).wait(1).to({y:328.7,alpha:0.716},0).wait(1).to({y:327.2,alpha:0.772},0).wait(1).to({y:325.8,alpha:0.822},0).wait(1).to({y:324.6,alpha:0.865},0).wait(1).to({y:323.7,alpha:0.9},0).wait(1).to({y:322.8,alpha:0.929},0).wait(1).to({y:322.2,alpha:0.953},0).wait(1).to({y:321.7,alpha:0.971},0).wait(1).to({y:321.3,alpha:0.984},0).wait(1).to({y:321.1,alpha:0.993},0).wait(1).to({y:320.9,alpha:0.998},0).wait(1).to({regY:0,y:321.5,alpha:1},0).wait(74));

	// 00 Baseline
	this.instance_33 = new lib._00Baseline();
	this.instance_33.setTransform(106.9,349.1);
	this.instance_33.alpha = 0;
	this.instance_33._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(4).to({_off:false},0).wait(1).to({regX:0.1,regY:-0.6,x:107,y:348.5,alpha:0.001},0).wait(1).to({y:348.3,alpha:0.006},0).wait(1).to({y:348.1,alpha:0.014},0).wait(1).to({y:347.8,alpha:0.026},0).wait(1).to({y:347.3,alpha:0.042},0).wait(1).to({y:346.8,alpha:0.063},0).wait(1).to({y:346.1,alpha:0.089},0).wait(1).to({y:345.2,alpha:0.12},0).wait(1).to({y:344.1,alpha:0.158},0).wait(1).to({y:342.9,alpha:0.202},0).wait(1).to({y:341.5,alpha:0.253},0).wait(1).to({y:339.9,alpha:0.311},0).wait(1).to({y:338.2,alpha:0.375},0).wait(1).to({y:336.3,alpha:0.443},0).wait(1).to({y:334.3,alpha:0.514},0).wait(1).to({y:332.4,alpha:0.585},0).wait(1).to({y:330.5,alpha:0.653},0).wait(1).to({y:328.7,alpha:0.716},0).wait(1).to({y:327.2,alpha:0.772},0).wait(1).to({y:325.8,alpha:0.822},0).wait(1).to({y:324.6,alpha:0.865},0).wait(1).to({y:323.7,alpha:0.9},0).wait(1).to({y:322.8,alpha:0.929},0).wait(1).to({y:322.2,alpha:0.953},0).wait(1).to({y:321.7,alpha:0.971},0).wait(1).to({y:321.3,alpha:0.984},0).wait(1).to({y:321.1,alpha:0.993},0).wait(1).to({y:320.9,alpha:0.998},0).wait(1).to({regX:0,regY:0,x:106.9,y:321.5,alpha:1},0).wait(77));

	// 00 3,0
	this.instance_34 = new lib._0030();
	this.instance_34.setTransform(32.9,79.6,0.051,0.051);
	this.instance_34._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_34).wait(12).to({_off:false},0).wait(1).to({regY:0.2,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42,y:79.7},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9,y:79.8},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,regY:0.1,scaleX:1,scaleY:1,y:79.6},0).wait(69));

	// 00 2,5
	this.instance_35 = new lib._0025();
	this.instance_35.setTransform(32.7,116.6,0.051,0.051);
	this.instance_35._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(10).to({_off:false},0).wait(1).to({regY:0.2,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42,y:116.7},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9,y:116.8},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,regY:0.1,scaleX:1,scaleY:1,y:116.6},0).wait(71));

	// 00 2,0
	this.instance_36 = new lib._0020();
	this.instance_36.setTransform(32.9,153.6,0.051,0.051);
	this.instance_36._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_36).wait(8).to({_off:false},0).wait(1).to({regY:0.2,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42,y:153.7},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9,y:153.8},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,regY:0.1,scaleX:1,scaleY:1,y:153.6},0).wait(73));

	// 00 1,5
	this.instance_37 = new lib._0015();
	this.instance_37.setTransform(31.5,190.6,0.051,0.051);
	this.instance_37._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_37).wait(6).to({_off:false},0).wait(1).to({regX:0.4,regY:0.3,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07,x:31.6},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25,y:190.7},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36,x:31.7},0).wait(1).to({scaleX:0.42,scaleY:0.42},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.61,scaleY:0.61,x:31.8,y:190.8},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83,x:31.9},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93,y:190.9},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,regY:0.1,scaleX:1,scaleY:1,x:31.5,y:190.6},0).wait(75));

	// 00 1,0
	this.instance_38 = new lib._0010();
	this.instance_38.setTransform(31.8,227.6,0.051,0.051);
	this.instance_38._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_38).wait(4).to({_off:false},0).wait(1).to({regX:0.3,regY:0.2,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25,x:31.9},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42,y:227.7},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.61,scaleY:0.61,x:32},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9,y:227.8},0).wait(1).to({scaleX:0.93,scaleY:0.93,x:32.1},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,regY:0.1,scaleX:1,scaleY:1,x:31.7,y:227.6},0).wait(77));

	// 00 0,5
	this.instance_39 = new lib._0005();
	this.instance_39.setTransform(32.9,264.6,0.051,0.051);
	this.instance_39._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_39).wait(2).to({_off:false},0).wait(1).to({regX:0.1,regY:0.2,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42,y:264.7},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.67,scaleY:0.67},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78,x:33},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9,y:264.8},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regY:0.1,scaleX:1,scaleY:1,x:32.9,y:264.6},0).wait(79));

	// 00 0,0
	this.instance_40 = new lib._0000();
	this.instance_40.setTransform(35,303.8,0.055,0.055);

	this.timeline.addTween(cjs.Tween.get(this.instance_40).wait(1).to({regX:0.1,regY:-0.6,scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.12,scaleY:0.12},0).wait(1).to({scaleX:0.14,scaleY:0.14,y:303.7},0).wait(1).to({scaleX:0.18,scaleY:0.18},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3,y:303.6},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42},0).wait(1).to({scaleX:0.48,scaleY:0.48,y:303.5},0).wait(1).to({scaleX:0.55,scaleY:0.55},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.67,scaleY:0.67,y:303.4},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78,x:35.1},0).wait(1).to({scaleX:0.83,scaleY:0.83,y:303.3},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97,y:303.2},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regY:0.1,scaleX:1,scaleY:1,x:34.9,y:303.9},0).wait(81));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(324.7,207.7,513.9,279.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;