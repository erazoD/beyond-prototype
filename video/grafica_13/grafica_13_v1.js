(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 510,
	height: 480,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib._02franja3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AjaRfMAAAgi9IG1AAMAAAAi9g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.9,-111.9,43.9,223.9);


(lib._02franja2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AjaDbIAAm1IG1AAIAAG1g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.9,-21.9,44,44);


(lib._02franja1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AjaChIAAlCIG1AAIAAFCg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.9,-16.2,44,32.4);


(lib._0211p = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag0B0IAAjkIAjAAIADAQQAKgTAUAAQAKAAAHAEQAHADAEAGQAFAGACAJQACAIAAAJIAABcQAAAJgCAHQgDAIgFAGQgEAGgGADQgHADgHAAQgJAAgHgDQgEgEgHgHIAABCgAgFhLQgDADAAAFIAABTQAAAFADADQADAEACAAQAEAAADgEQACgDAAgFIAAhTQAAgFgCgDQgDgDgEAAQgCAAgDADg");
	this.shape.setTransform(9.5,4.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgYB3IAAjWQAMAAAFgHQAFgHAAgIIAbAAIAADsg");
	this.shape_1.setTransform(-1.7,-1.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgYB3IAAjWQAMAAAFgHQAFgHAAgIIAbAAIAADsg");
	this.shape_2.setTransform(-11.1,-1.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-19.7,36,39.7);


(lib._01franja3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AjaRfMAAAgi9IG1AAMAAAAi9g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.9,-111.9,43.9,223.9);


(lib._01franja2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AjaHyIAAvjIG1AAIAAPjg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.9,-49.8,43.9,99.8);


(lib._01franja1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AjLDCIAAmDIGXAAIAAGDg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-20.4,-19.4,40.8,38.8);


(lib._0111v = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQBXIgnitIAsAAIALBNIABAaIADgaIAMhNIAoAAIgmCtg");
	this.shape.setTransform(9.5,2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgYB3IAAjWQAMAAAFgHQAFgHAAgIIAbAAIAADsg");
	this.shape_1.setTransform(-1.1,-1.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgYB3IAAjWQAMAAAFgHQAFgHAAgIIAbAAIAADsg");
	this.shape_2.setTransform(-10.5,-1.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.3,-19.7,34.8,39.7);


(lib._00titulo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgJAMIAAgYIATAAIAAAYg");
	this.shape.setTransform(119.1,4.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgFgEQgEgEgDgGQgCgGAAgHIAAgJIAZAAIAAAJQAAAEACACQADADADAAQACAAADgDQACgCAAgEQgBgHgEgFIgKgMIgJgIIgHgGIgGgLQgCgFAAgGQAAgHACgGQACgGAEgFQAFgFAGgDQAHgDAGAAQAIAAAGADQAFACAFAEQAEAFACAFQADAGAAAHIAAAIIgZAAIAAgJQAAgEgCgCQgCgCgEAAQAAAAgCACQgDACAAAEQAAAEABADIAEAFIADAGIAHAGIAIAIIAIAHIAGAKQACAGAAAGQAAAHgCAHQgDAGgEAFQgFAEgGADQgGADgIAAQgGAAgHgDg");
	this.shape_1.setTransform(113.9,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA6AAIAAAYIgdAAIAAAaIATAAIAAATIgTAAIAAAbIAdAAIAAAZg");
	this.shape_2.setTransform(106.8,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgFgEQgEgEgDgGQgCgGAAgHIAAgJIAZAAIAAAJQAAAEACACQADADADAAQACAAADgDQACgCAAgEQgBgHgEgFIgKgMIgIgIIgIgGIgGgLQgCgFAAgGQAAgHACgGQACgGAEgFQAFgFAGgDQAHgDAGAAQAIAAAGADQAFACAFAEQAEAFACAFQADAGAAAHIAAAIIgZAAIAAgJQAAgEgCgCQgCgCgEAAQAAAAgCACQgDACAAAEQAAAEABADIAEAFIADAGIAHAGIAIAIIAJAHIAFAKQACAGAAAGQAAAHgCAHQgDAGgEAFQgFAEgGADQgGADgIAAQgGAAgHgDg");
	this.shape_3.setTransform(99.5,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA7AAIAAAYIgeAAIAAAaIATAAIAAATIgTAAIAAAbIAeAAIAAAZg");
	this.shape_4.setTransform(92.4,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAVA9IAAhQIgFAcIgJA0IgQAAIgJg0IgEgcIAABQIgZAAIAAh5IAlAAIAJAwIABAUIADgUIAIgwIAlAAIAAB5g");
	this.shape_5.setTransform(83.4,-0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgMA9QgGgCgEgEQgEgFgDgFQgCgGAAgIIAAg+QAAgHADgGQACgGAEgEQAEgEAHgDQAFgCAGAAQAHAAAHADQAFACAEAEQADAFACAFQACAFAAAFIAAAMIgWAAIAAgJQgBgEgBgCQgCgCgEAAQgBAAgDACQgCACAAADIAAAdIAGgGQADgDAHAAQAKAAAHAGIADAGQACADAAAFIAAAXQAAAIgDAGQgCAFgDAFQgFAEgGACQgGADgHAAQgGAAgGgDgAgEAGQAAABAAAAQgBABAAAAQAAABAAAAQgBABAAABIAAAXQAAAAABABQAAABAAAAQAAABABAAQAAABAAAAQACACACAAQADAAACgCQAAAAAAgBQABAAAAgBQAAAAAAgBQABgBAAAAIAAgXQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgCgCgDAAQgCAAgCACg");
	this.shape_6.setTransform(71.6,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA7AAIAAAYIgeAAIAAAaIATAAIAAATIgTAAIAAAbIAeAAIAAAZg");
	this.shape_7.setTransform(61.7,-0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgNA9IAAhgIgUAAIAAgZIBDAAIAAAZIgUAAIAABgg");
	this.shape_8.setTransform(54.6,-0.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AANA9IgQgtIgHgQIAAA9IgZAAIAAh5IAZAAIAOAuIAHAPIAAg9IAZAAIAAB5g");
	this.shape_9.setTransform(46.8,-0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAJA9IgCgSIgPAAIgDASIgZAAIAVh5IAfAAIAVB5gAgFAVIAJAAIgEgqg");
	this.shape_10.setTransform(38.8,-0.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AALA9QAAAAAAAAQgBAAAAgBQAAAAgBAAQAAgBAAAAIgCgFIgCgGIAAgGIAAgXQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAgBgBAAQgBgDgCAAIgFAAIAAAxIgdAAIAAh5IAmAAIAKABQAEACAEAEQAFADACAFQACAFAAAGIAAAVQAAAFgDAEQgDAEgGABQAGADAEAFQACAEAAAFIAAAWIABALIADAKIAAAAgAgFgGIAFAAQAEAAAAgGIAAgUQAAgFgEAAIgFAAg");
	this.shape_11.setTransform(31.3,-0.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgLA8QgHgCgEgEQgGgFgCgFQgDgFAAgHIAAhdIAdAAIAABdQAAABAAAAQAAABAAABQABAAAAABQAAAAABABQAAAAABAAQAAAAABABQAAAAAAAAQAAAAAAAAQADAAACgBQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAABgBIAAhdIAbAAIAABdQAAAGgCAGQgDAFgFAFQgEAEgGACQgHADgHAAQgGAAgFgDg");
	this.shape_12.setTransform(23.2,-0.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgiA9IAAh5IAiAAQAIAAAGACQAGADAFAEQAFAFACAFQADAHgBAHIAAA5QABAHgDAHQgCAFgFAEQgFAEgGACQgGACgIAAgAgFAmIAFAAQADAAACgCQABgDAAgFIAAg3QAAgFgBgCQgCgCgDAAIgFAAg");
	this.shape_13.setTransform(15.2,-0.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgLA5IgLgDQgFgCgFgEIgJgHIgGgIIgGgLIgDgLIgBgLQAAgLAEgKIAGgKIAGgJQAIgIALgFIALgDIALgBQAMAAALAEIAKAGQAFADADAEIAHAJIAGAKQAEAKAAALIgBALIgDALQgFALgIAIQgDAEgFADQgFAEgFACQgLAEgMAAIgLgBgAgJgwIgKADQgJAEgGAHQgHAHgDAJQgEAJAAAJQAAAKAEAJQADAJAHAHQAGAHAJAEIAKADIAJABIAKgBIAKgDQAJgEAGgHQAHgHADgJQAEgJAAgKQAAgJgEgJQgDgJgHgHQgGgHgJgEQgJgEgLAAgAAUAaQgGAAgCgCQgDgCAAgFIAAgGQAAgDgBgCQgCgDgEAAIgIAAIAAAWIgKAAIAAgyIAUAAQAIAAAEAEQADAEAAAHIAAAAQAAAFgCADQgCACgFAAQAFAAACAEQADADAAAEIAAAFQAAABAAAAQAAABABAAQAAAAAAABQABAAABAAIAAAHgAgGgBIAIAAQADAAACgCQACgCAAgEIAAgCQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgCgCgDAAIgJAAg");
	this.shape_14.setTransform(2.2,-0.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IA6AAIAAAYIgeAAIAAAaIASAAIAAATIgSAAIAAAbIAeAAIAAAZg");
	this.shape_15.setTransform(-7.4,-0.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AANA9IgQgtIgHgQIAAA9IgZAAIAAh5IAZAAIAOAuIAHAPIAAg9IAZAAIAAB5g");
	this.shape_16.setTransform(-15.3,-0.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgNA9IAAh5IAbAAIAAB5g");
	this.shape_17.setTransform(-21.5,-0.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AALA9QAAAAAAAAQgBAAAAgBQAAAAgBAAQAAgBAAAAIgCgFIgCgGIAAgGIAAgXQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAgBgBAAQgBgDgCAAIgGAAIAAAxIgcAAIAAh5IAmAAIAKABQAEACAEAEQAFADABAFQADAFAAAGIAAAVQAAAFgDAEQgDAEgGABQAGADADAFQADAEAAAFIAAAWIABALIADAKIAAAAgAgGgGIAGAAQAEAAAAgGIAAgUQAAgFgEAAIgGAAg");
	this.shape_18.setTransform(-27.2,-0.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA7AAIAAAYIgeAAIAAAaIATAAIAAATIgTAAIAAAbIAeAAIAAAZg");
	this.shape_19.setTransform(-34.7,-0.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgNA9IAAhgIgUAAIAAgZIBDAAIAAAZIgUAAIAABgg");
	this.shape_20.setTransform(-41.8,-0.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgEgEQgFgEgDgGQgCgGAAgHIAAgJIAZAAIAAAJQAAAEACACQADADADAAQACAAACgDQACgCAAgEQABgHgFgFIgKgMIgIgIIgIgGIgGgLQgCgFAAgGQAAgHACgGQADgGAEgFQAEgFAGgDQAHgDAHAAQAHAAAFADQAHACADAEQAFAFACAFQACAGAAAHIAAAIIgYAAIAAgJQAAgEgCgCQgCgCgDAAQgBAAgDACQgCACAAAEQAAAEABADIAEAFIADAGIAHAGIAJAIIAIAHIAFAKQACAGAAAGQAAAHgDAHQgCAGgFAFQgEAEgGADQgGADgIAAQgGAAgHgDg");
	this.shape_21.setTransform(-48.9,-0.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgNA9IAAh5IAbAAIAAB5g");
	this.shape_22.setTransform(-54.6,-0.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IAcAAIAABgIAdAAIAAAZg");
	this.shape_23.setTransform(-59.7,-0.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgNA9IAAgzIgZhGIAcAAIALAsIAKgsIAcAAIgaBGIAAAzg");
	this.shape_24.setTransform(-69.7,-0.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IAcAAIAABgIAdAAIAAAZg");
	this.shape_25.setTransform(-79.3,-0.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAJA9IgCgSIgPAAIgDASIgZAAIAVh5IAfAAIAVB5gAgFAVIAJAAIgEgqg");
	this.shape_26.setTransform(-86.6,-0.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgNA9IAAhgIgUAAIAAgZIBDAAIAAAZIgUAAIAABgg");
	this.shape_27.setTransform(-93.4,-0.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AANA9IgQgtIgHgQIAAA9IgZAAIAAh5IAZAAIAOAuIAHAPIAAg9IAZAAIAAB5g");
	this.shape_28.setTransform(-101.2,-0.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IA5AAIAAAYIgdAAIAAAaIASAAIAAATIgSAAIAAAbIAdAAIAAAZg");
	this.shape_29.setTransform(-108.8,-0.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AghA9IAAh5IAiAAQAHAAAGACQAHADAEAEQAFAFACAFQADAHAAAHIAAA5QAAAHgDAHQgCAFgFAEQgEAEgHACQgGACgHAAgAgFAmIAFAAQACAAACgCQACgDAAgFIAAg3QAAgFgCgCQgCgCgCAAIgFAAg");
	this.shape_30.setTransform(-116.4,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-126.2,-11.1,252.6,22.5);


(lib._00titulo1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgFgEQgFgFgDgFQgCgGAAgIIAAg+QAAgHACgGQADgGAFgEQAFgEAGgCQAHgDAGAAQAHAAAGADQAHACAFAEQAFAEACAGQADAGAAAHIAAA+QAAAIgDAGQgCAFgFAFQgFAEgHACQgGADgHAAQgGAAgHgDgAgDglQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABIAABCQAAABAAAAQAAABABABQAAAAAAABQABAAAAABQABAAAAAAQABABAAAAQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAAAABgBQABAAAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAAAgBIAAhCQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAAAAAAAQAAAAgBABQAAAAgBAAQAAAAgBABg");
	this.shape.setTransform(201.1,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IAcAAIAABgIAdAAIAAAZg");
	this.shape_1.setTransform(194,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNA9IAAh5IAbAAIAAB5g");
	this.shape_2.setTransform(188.5,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAIA9IAAgvIgOAAIAAAvIgdAAIAAh5IAdAAIAAAzIAOAAIAAgzIAcAAIAAB5g");
	this.shape_3.setTransform(182.2,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgJAQQAEAAADgDQACgDAAgGIgJAAIAAgYIATAAIAAAVQAAAFgCADIgEAHQgDADgCABQgDABgFAAg");
	this.shape_4.setTransform(173.8,5.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgFgEQgEgFgEgFQgCgGAAgIIAAg+QAAgHACgGQAEgGAEgEQAFgEAGgCQAHgDAGAAQAHAAAHADQAGACAFAEQAFAEACAGQADAGAAAHIAAA+QAAAIgDAGQgCAFgFAFQgFAEgGACQgHADgHAAQgGAAgHgDgAgDglQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABIAABCQAAABAAAAQAAABABABQAAAAAAABQABAAAAABQAAAAABAAQABABAAAAQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAAAABgBQABAAAAAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBIAAhCQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAAAAAAAQAAAAgBABQAAAAgBAAQgBAAAAABg");
	this.shape_5.setTransform(168.3,-0.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AghA9IAAh5IAiAAQAHAAAGACQAHADAEAEQAFAFACAFQACAHAAAHIAAA5QAAAHgCAHQgCAFgFAEQgEAEgHACQgGACgHAAgAgFAmIAFAAQADAAABgCQACgDAAgFIAAg3QAAgFgCgCQgBgCgDAAIgFAAg");
	this.shape_6.setTransform(160.3,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAJA9IgCgSIgPAAIgDASIgZAAIAVh5IAfAAIAVB5gAgFAVIAJAAIgEgqg");
	this.shape_7.setTransform(152.4,-0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IAcAAIAABgIAdAAIAAAZg");
	this.shape_8.setTransform(145.6,-0.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IAcAAIAABgIAdAAIAAAZg");
	this.shape_9.setTransform(139,-0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgNA9IAAh5IAbAAIAAB5g");
	this.shape_10.setTransform(133.5,-0.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AggA9IAAh5IAmAAQAFAAAFACQAGACADADQADAEACAFQADAEAAAHIAAAYQAAAHgDADQgCAFgDADQgDAEgGACQgFACgFAAIgKAAIAAAsgAgEgCIAGAAQAEAAAAgGIAAgYQAAgFgEAAIgGAAg");
	this.shape_11.setTransform(127.9,-0.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA7AAIAAAYIgeAAIAAAaIATAAIAAATIgTAAIAAAbIAeAAIAAAZg");
	this.shape_12.setTransform(120.6,-0.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgLA9QgHgCgFgFQgFgEgDgGQgCgHAAgHIAAg7QAAgIACgGQADgGAFgEQAFgFAHgCQAHgDAFAAQAIAAAGACQAGADAEAEQAFAEACAGQACAGAAAHIAAAQIgZAAIAAgQQAAgDgCgCQgCgDgEAAQgBAAgCADQgCACAAADIAAA/QAAADACACQACACABAAQAEAAACgCQACgCAAgDIAAgRIAZAAIAAAQQAAAIgCAGQgCAFgFAFQgEAEgGACQgGADgIAAQgFAAgHgDg");
	this.shape_13.setTransform(113.1,-0.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA7AAIAAAYIgeAAIAAAaIATAAIAAATIgTAAIAAAbIAeAAIAAAZg");
	this.shape_14.setTransform(103.1,-0.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgiA9IAAh5IAiAAQAIAAAHACQAFADAFAEQAFAFACAFQACAHABAHIAAA5QgBAHgCAHQgCAFgFAEQgFAEgFACQgHACgIAAgAgFAmIAFAAQADAAACgCQABgDAAgFIAAg3QAAgFgBgCQgCgCgDAAIgFAAg");
	this.shape_15.setTransform(95.5,-0.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgEgEQgFgFgEgFQgCgGAAgIIAAg+QAAgHACgGQAEgGAFgEQAEgEAGgCQAHgDAGAAQAHAAAGADQAHACAFAEQAFAEACAGQADAGAAAHIAAA+QAAAIgDAGQgCAFgFAFQgFAEgHACQgGADgHAAQgGAAgHgDgAgDglQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABIAABCQAAABAAAAQAAABABABQAAAAAAABQABAAAAABQABAAAAAAQABABAAAAQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAAAABgBQABAAAAAAQABgBAAAAQAAgBABAAQAAgBAAgBQAAAAAAgBIAAhCQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAAAAAAAQAAAAgBABQAAAAgBAAQAAAAgBABg");
	this.shape_16.setTransform(84.8,-0.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgFgEQgEgEgDgGQgCgGAAgHIAAgJIAZAAIAAAJQAAAEACACQADADADAAQACAAADgDQACgCAAgEQgBgHgEgFIgKgMIgIgIIgIgGIgGgLQgCgFAAgGQAAgHACgGQACgGAEgFQAFgFAGgDQAHgDAGAAQAIAAAGADQAFACAFAEQAEAFACAFQADAGAAAHIAAAIIgZAAIAAgJQAAgEgCgCQgCgCgEAAQAAAAgCACQgDACAAAEQAAAEABADIAEAFIADAGIAHAGIAIAIIAJAHIAFAKQACAGAAAGQAAAHgCAHQgDAGgEAFQgFAEgGADQgGADgIAAQgGAAgHgDg");
	this.shape_17.setTransform(77,-0.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgMA8QgGgCgFgEQgEgFgDgFQgDgFAAgHIAAhdIAdAAIAABdQAAABAAAAQAAABAAABQABAAAAABQAAAAAAABQABAAABAAQAAAAABABQAAAAAAAAQAAAAAAAAQADAAACgBQAAgBABAAQAAgBAAAAQAAgBAAgBQABAAAAgBIAAhdIAbAAIAABdQAAAGgDAGQgCAFgEAFQgFAEgGACQgGADgIAAQgFAAgHgDg");
	this.shape_18.setTransform(69.2,-0.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IAcAAIAABgIAdAAIAAAZg");
	this.shape_19.setTransform(59.5,-0.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IA5AAIAAAYIgdAAIAAAaIASAAIAAATIgSAAIAAAbIAdAAIAAAZg");
	this.shape_20.setTransform(52.7,-0.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AANA9IgQgtIgHgQIAAA9IgZAAIAAh5IAZAAIAOAuIAHAPIAAg9IAZAAIAAB5g");
	this.shape_21.setTransform(42.2,-0.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgMA9QgHgCgFgEQgEgFgDgFQgDgGAAgIIAAg+QAAgHADgGQADgGAEgEQAFgEAHgCQAGgDAGAAQAHAAAHADQAGACAFAEQAEAEADAGQADAGAAAHIAAA+QAAAIgDAGQgDAFgEAFQgFAEgGACQgHADgHAAQgGAAgGgDgAgDglQgBAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAABCQAAABAAAAQAAABAAABQABAAAAABQAAAAABABQAAAAABAAQABABAAAAQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBIAAhCQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgBgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAAAAAAAQAAAAgBABQAAAAgBAAQgBAAAAABg");
	this.shape_22.setTransform(34,-0.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgLA9QgHgCgFgFQgFgEgDgGQgCgHAAgHIAAg7QAAgIACgGQADgGAFgEQAFgFAHgCQAHgDAFAAQAIAAAGACQAGADAEAEQAFAEACAGQACAGAAAHIAAAQIgZAAIAAgQQAAgDgCgCQgCgDgEAAQgBAAgCADQgCACAAADIAAA/QAAADACACQACACABAAQAEAAACgCQACgCAAgDIAAgRIAZAAIAAAQQAAAIgCAGQgCAFgFAFQgEAEgGACQgGADgIAAQgFAAgHgDg");
	this.shape_23.setTransform(26,-0.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgFgEQgFgEgCgGQgCgGAAgHIAAgJIAZAAIAAAJQAAAEACACQACADAEAAQACAAACgDQADgCAAgEQAAgHgFgFIgKgMIgIgIIgIgGIgGgLQgCgFAAgGQAAgHACgGQADgGADgFQAFgFAGgDQAHgDAGAAQAIAAAFADQAGACAFAEQAEAFACAFQADAGAAAHIAAAIIgZAAIAAgJQAAgEgCgCQgCgCgEAAQAAAAgDACQgCACAAAEQAAAEABADIAEAFIADAGIAHAGIAIAIIAJAHIAFAKQACAGAAAGQAAAHgDAHQgCAGgEAFQgFAEgGADQgGADgIAAQgGAAgHgDg");
	this.shape_24.setTransform(15.7,-0.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA7AAIAAAYIgeAAIAAAaIATAAIAAATIgTAAIAAAbIAeAAIAAAZg");
	this.shape_25.setTransform(8.6,-0.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IAcAAIAABgIAdAAIAAAZg");
	this.shape_26.setTransform(2,-0.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AggA9IAAh5IAnAAQAFAAAFABQAEACAEADQADADACAFQADAEAAAHIAAARIgBAFIgCAGIgEAEIgHADQAIABADAFQADAFAAAHIAAASQAAAGgDAFQgCAFgDADQgEADgEACQgFABgFAAgAgEAnIAGAAQAEAAAAgGIAAgSQAAgGgEAAIgGAAgAgEgIIAGAAQAEgBAAgFIAAgSQAAgFgEAAIgGAAg");
	this.shape_27.setTransform(-5.3,-0.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAJA9IgCgSIgPAAIgDASIgZAAIAVh5IAfAAIAVB5gAgFAVIAJAAIgEgqg");
	this.shape_28.setTransform(-13.1,-0.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AghA9IAAh5IAiAAQAHAAAGACQAHADAEAEQAFAFACAFQACAHAAAHIAAA5QAAAHgCAHQgCAFgFAEQgEAEgHACQgGACgHAAgAgFAmIAFAAQADAAABgCQACgDAAgFIAAg3QAAgFgCgCQgBgCgDAAIgFAAg");
	this.shape_29.setTransform(-20.6,-0.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgMA8QgGgCgEgEQgFgFgDgFQgDgFAAgHIAAhdIAdAAIAABdQAAABAAAAQAAABAAABQABAAAAABQAAAAAAABQABAAABAAQAAAAABABQAAAAAAAAQAAAAAAAAQADAAACgBQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAgBIAAhdIAcAAIAABdQAAAGgDAGQgCAFgFAFQgEAEgGACQgGADgIAAQgFAAgHgDg");
	this.shape_30.setTransform(-28.7,-0.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IAcAAIAABgIAdAAIAAAZg");
	this.shape_31.setTransform(-35.7,-0.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAJA9IgCgSIgPAAIgDASIgZAAIAVh5IAfAAIAVB5gAgFAVIAJAAIgEgqg");
	this.shape_32.setTransform(-43,-0.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgEgEQgGgEgCgGQgCgGAAgHIAAgJIAZAAIAAAJQAAAEACACQACADAEAAQADAAABgDQACgCAAgEQABgHgFgFIgKgMIgIgIIgIgGIgGgLQgCgFAAgGQAAgHACgGQACgGAFgFQAEgFAGgDQAHgDAHAAQAHAAAFADQAHACADAEQAFAFACAFQADAGgBAHIAAAIIgYAAIAAgJQAAgEgCgCQgCgCgDAAQgBAAgDACQgCACAAAEQAAAEABADIAEAFIADAGIAHAGIAJAIIAIAHIAFAKQACAGAAAGQAAAHgDAHQgCAGgFAFQgEAEgGADQgGADgIAAQgHAAgGgDg");
	this.shape_33.setTransform(-50.3,-0.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgFgEQgEgEgDgGQgCgGAAgHIAAgJIAZAAIAAAJQAAAEACACQADADADAAQACAAACgDQACgCABgEQAAgHgFgFIgKgMIgIgIIgIgGIgGgLQgCgFAAgGQAAgHACgGQADgGADgFQAFgFAGgDQAHgDAHAAQAHAAAFADQAHACADAEQAFAFACAFQACAGAAAHIAAAIIgYAAIAAgJQAAgEgCgCQgCgCgDAAQgBAAgDACQgCACAAAEQAAAEABADIAEAFIADAGIAHAGIAJAIIAIAHIAFAKQACAGAAAGQAAAHgDAHQgCAGgEAFQgFAEgGADQgGADgIAAQgGAAgHgDg");
	this.shape_34.setTransform(-60.5,-0.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgEgEQgFgFgEgFQgCgGAAgIIAAg+QAAgHACgGQAEgGAFgEQAEgEAGgCQAHgDAGAAQAHAAAGADQAHACAFAEQAEAEADAGQADAGAAAHIAAA+QAAAIgDAGQgDAFgEAFQgFAEgHACQgGADgHAAQgGAAgHgDgAgDglQAAAAgBABQAAAAgBABQAAAAAAABQAAAAAAABIAABCQAAABAAAAQAAABAAABQABAAAAABQABAAAAABQABAAAAAAQABABAAAAQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAAAABgBQABAAAAAAQABgBAAAAQAAgBABAAQAAgBAAgBQAAAAAAgBIAAhCQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAAAAAAAQAAAAgBABQAAAAgBAAQAAAAgBABg");
	this.shape_35.setTransform(-68.2,-0.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgNA9IAAhgIgUAAIAAgZIBDAAIAAAZIgUAAIAABgg");
	this.shape_36.setTransform(-75.7,-0.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA6AAIAAAYIgdAAIAAAaIATAAIAAATIgTAAIAAAbIAdAAIAAAZg");
	this.shape_37.setTransform(-82.6,-0.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgNA8QgGgCgEgEQgFgFgCgGQgCgFAAgHIAAgUIAaAAIAAAVQAAABAAAAQAAABAAABQABAAAAABQAAAAABABQAAAAABAAQABAAAAABQABAAAAAAQABAAAAAAQAAAAAAAAQABAAAAAAQABgBAAAAQABAAAAAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBIAAhdIAcAAIAABcQABAHgDAFQgDAGgEAFQgFAEgGACQgHADgGAAQgGAAgHgDg");
	this.shape_38.setTransform(-90.1,-0.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgMA8QgGgCgFgEQgEgFgDgFQgDgFAAgHIAAhdIAdAAIAABdQAAABAAAAQAAABAAABQABAAAAABQAAAAAAABQABAAABAAQAAAAABABQAAAAAAAAQAAAAAAAAQADAAACgBQAAgBABAAQAAgBAAAAQAAgBAAgBQABAAAAgBIAAhdIAbAAIAABdQAAAGgDAGQgCAFgEAFQgFAEgGACQgGADgIAAQgFAAgHgDg");
	this.shape_39.setTransform(-97.7,-0.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgNA9QgGgCgFgEQgEgEgDgGQgCgGAAgHIAAgJIAZAAIAAAJQAAAEACACQADADADAAQACAAACgDQACgCABgEQAAgHgFgFIgKgMIgIgIIgIgGIgGgLQgCgFAAgGQAAgHACgGQADgGADgFQAFgFAGgDQAHgDAHAAQAHAAAFADQAHACADAEQAFAFACAFQACAGAAAHIAAAIIgYAAIAAgJQAAgEgCgCQgCgCgDAAQgBAAgDACQgCACAAAEQAAAEABADIAEAFIADAGIAHAGIAJAIIAIAHIAFAKQACAGAAAGQAAAHgDAHQgCAGgEAFQgFAEgGADQgGADgIAAQgGAAgHgDg");
	this.shape_40.setTransform(-105.5,-0.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA7AAIAAAYIgeAAIAAAaIATAAIAAATIgTAAIAAAbIAeAAIAAAZg");
	this.shape_41.setTransform(-115.2,-0.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgiA9IAAh5IAiAAQAIAAAHACQAFADAFAEQAFAFACAFQACAHABAHIAAA5QgBAHgCAHQgCAFgFAEQgFAEgFACQgHACgIAAgAgFAmIAFAAQACAAADgCQABgDAAgFIAAg3QAAgFgBgCQgDgCgCAAIgFAAg");
	this.shape_42.setTransform(-122.8,-0.5);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgdA9IAAh5IA6AAIAAAYIgdAAIAAAaIATAAIAAATIgTAAIAAAbIAdAAIAAAZg");
	this.shape_43.setTransform(-132.9,-0.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgNA8QgGgCgEgEQgFgFgCgGQgCgFAAgHIAAgUIAaAAIAAAVQAAABAAAAQAAABAAABQABAAAAABQAAAAABABQAAAAABAAQABAAAAABQABAAAAAAQABAAAAAAQAAAAAAAAQABAAAAAAQABgBAAAAQABAAAAAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBIAAhdIAcAAIAABcQABAHgDAFQgDAGgEAFQgFAEgGACQgHADgGAAQgHAAgGgDg");
	this.shape_44.setTransform(-140.4,-0.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAJA9IgCgSIgPAAIgDASIgZAAIAVh5IAfAAIAVB5gAgFAVIAJAAIgEgqg");
	this.shape_45.setTransform(-147.8,-0.5);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgNA9IAAhgIgUAAIAAgZIBDAAIAAAZIgUAAIAABgg");
	this.shape_46.setTransform(-154.6,-0.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AANA9IgQgtIgHgQIAAA9IgZAAIAAh5IAZAAIAOAuIAHAPIAAg9IAZAAIAAB5g");
	this.shape_47.setTransform(-162.4,-0.5);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgcA9IAAh5IA5AAIAAAYIgdAAIAAAaIASAAIAAATIgSAAIAAAbIAdAAIAAAZg");
	this.shape_48.setTransform(-170,-0.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgLA9QgHgCgFgFQgFgEgDgGQgCgHAAgHIAAg7QAAgIACgGQADgGAFgEQAFgFAHgCQAHgDAFAAQAIAAAGACQAGADAEAEQAFAEACAGQACAGAAAHIAAAQIgZAAIAAgQQAAgDgCgCQgCgDgEAAQgBAAgCADQgCACAAADIAAA/QAAADACACQACACABAAQAEAAACgCQACgCAAgDIAAgRIAZAAIAAAQQAAAIgCAGQgCAFgFAFQgEAEgGACQgGADgIAAQgFAAgHgDg");
	this.shape_49.setTransform(-177.5,-0.5);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AALA9QAAAAAAAAQgBAAAAgBQAAAAgBAAQAAgBAAAAIgCgFIgCgGIAAgGIAAgXQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAgBgBAAQgBgDgCAAIgGAAIAAAxIgcAAIAAh5IAmAAIAKABQAEACAEAEQAFADABAFQADAFAAAGIAAAVQAAAFgDAEQgDAEgGABQAGADADAFQADAEAAAFIAAAWIABALIADAKIAAAAgAgGgGIAGAAQAEAAAAgGIAAgUQAAgFgEAAIgGAAg");
	this.shape_50.setTransform(-185.1,-0.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgMA9QgHgCgFgEQgEgFgDgFQgDgGAAgIIAAg+QAAgHADgGQADgGAEgEQAFgEAHgCQAGgDAGAAQAHAAAHADQAGACAFAEQAEAEADAGQADAGAAAHIAAA+QAAAIgDAGQgDAFgEAFQgFAEgGACQgHADgHAAQgGAAgGgDgAgDglQgBAAAAABQAAAAgBABQAAAAAAABQAAAAAAABIAABCQAAABAAAAQAAABAAABQABAAAAABQAAAAABABQAAAAABAAQABABAAAAQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBIAAhCQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgBgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAAAAAAAQAAAAgBABQAAAAgBAAQgBAAAAABg");
	this.shape_51.setTransform(-193.3,-0.5);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AggA9IAAh5IAmAAQAFAAAGACQAFACADADQADAEACAFQADAEAAAHIAAAYQAAAHgDADQgCAFgDADQgDAEgFACQgGACgFAAIgKAAIAAAsgAgEgCIAGAAQAEAAAAgGIAAgYQAAgFgEAAIgGAAg");
	this.shape_52.setTransform(-201,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-215.4,-11.1,431,22.5);


(lib._00seda2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("ABdB+QAAiQhfhBQgegUglgJQgSgGgMgBQgHAAABgHQAAgFAGAAQAPABATAFQAmAKAeAWQA6AmAaBCQATAzAABAQAAAGgGAAQgHAAAAgGg");
	this.shape.setTransform(0,-7.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AhbCgIg7g7IAAjJIA7g7IC3AAIA7A7IAADJIg7A7g");
	this.shape_1.setTransform(0,5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.1,-21,30.2,42);


(lib._00seda = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("ABPBrQAAh6hQg2QgagSgfgIQgPgEgLgBQgFAAAAgGQAAgFAGAAQAMABAQAFQAhAIAZASQBXA7AAB/QAAAFgGAAQgFAAAAgFg");
	this.shape.setTransform(0,-6.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AhNCHIgxgxIAAirIAxgxICbAAIAxAxIAACrIgxAxg");
	this.shape_1.setTransform(0,4.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.7,-17.7,25.6,35.6);


(lib._00punto3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AglAmQgQgQAAgWQAAgVAQgQQAQgQAVAAQAWAAAQAQQAQAQAAAVQAAAWgQAQQgQAQgWAAQgVAAgQgQg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.4,-5.4,11,11);


(lib._00punto2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AglAmQgQgQAAgWQAAgVAQgQQAQgQAVAAQAWAAAQAQQAQAQAAAVQAAAWgQAQQgQAQgWAAQgVAAgQgQg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.4,-5.4,10.9,11);


(lib._00punto1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AglAmQgQgQAAgWQAAgVAQgQQAQgQAVAAQAWAAAQAQQAQAQAAAVQAAAWgQAQQgQAQgWAAQgVAAgQgQg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.4,-5.4,10.9,11);


(lib._00listerine = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AhGBQIAAifICNAAIAACfg");
	this.shape.setTransform(0,4.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AghArIAAgGIAFAAIAAgDIgFAAIAAhNIBDAAIAABNIgFAAIAAADIAFAAIAAAGg");
	this.shape_1.setTransform(0,-17.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7BD7F8").s().p("AhNCyIgWgWIAAgzIAMgQIAAinIgMgLIAAg1IAggaIAsAAIAAgJIAvAAIAAAJIAtAAIAfAaIAAA1IgMALIAACnIAMAQIAAAzIgVAWg");
	this.shape_2.setTransform(0,4.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10,-22.2,20.1,44.4);


(lib._00cepillo3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AgYBGIAAhpIAigiIAPAAIAAALIgLAAIgbAcIAABkg");
	this.shape.setTransform(-6,-11.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AgiDRIAAl1IAYgsIAtAAIAAGhg");
	this.shape_1.setTransform(-5.4,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7BD7F8").s().p("AgwAJQgDAAgDgDQgDgCAAgEQAAgCADgDQADgCADAAIBhAAQAEAAACACQADADAAACQAAAEgDACQgCADgEAAg");
	this.shape_2.setTransform(3.1,5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7BD7F8").s().p("AgwAJQgDAAgDgDQgDgCAAgEQAAgCADgDQADgDADAAIBhAAQADAAADADQADADAAACQAAAEgDACQgCADgEAAg");
	this.shape_3.setTransform(3.1,0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7BD7F8").s().p("AgwAJQgDgBgDgCQgDgDAAgDQAAgCADgDQADgDADABIBhAAQAEgBACADQADADAAACQAAADgDADQgCACgEABg");
	this.shape_4.setTransform(3.1,-4.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7BD7F8").s().p("AgwAIQgDAAgDgCQgDgDAAgDQAAgCADgDQADgDADAAIBhAAQAEAAACADQADADAAACQAAADgDADQgCACgEAAg");
	this.shape_5.setTransform(3.1,-8.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#7BD7F8").s().p("AgwAJQgJAAAAgJQAAgCADgDQADgDADAAIBhAAQAEAAACADQADADAAACQAAAJgJAAg");
	this.shape_6.setTransform(3.1,-13.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#7BD7F8").s().p("AgwAJQgDAAgDgDQgDgCAAgEQAAgCADgDQADgDADAAIBhAAQADAAADADQADADAAACQAAAEgDACQgCADgEAAg");
	this.shape_7.setTransform(3.1,7.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#7BD7F8").s().p("AgwAJQgDAAgDgDQgDgCAAgEQAAgCADgDQADgDADAAIBhAAQADAAADADQADADAAACQAAAEgDACQgCADgEAAg");
	this.shape_8.setTransform(3.1,2.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#7BD7F8").s().p("AgwAJQgDAAgDgDQgDgCAAgEQAAgCADgDQADgDADAAIBhAAQADAAADADQADADAAACQAAAEgDACQgCADgEAAg");
	this.shape_9.setTransform(3.1,-1.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#7BD7F8").s().p("AgwAJQgDgBgDgCQgDgDAAgDQAAgCADgDQADgDADABIBhAAQAEgBACADQADADAAACQAAADgDADQgCACgEABg");
	this.shape_10.setTransform(3.1,-6.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#7BD7F8").s().p("AgwAIQgDAAgDgCQgDgDAAgDQAAgCADgDQADgDADAAIBhAAQAEAAACADQADADAAACQAAADgDADQgCACgEAAg");
	this.shape_11.setTransform(3.1,-10.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#7BD7F8").s().p("AgwAIQgDABgDgDQgDgCAAgEQAAgCADgDQADgCADgBIBhAAQAEABACACQADADAAACQAAAEgDACQgCADgEgBg");
	this.shape_12.setTransform(3.1,-15.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.9,-21,17.9,42);


(lib._00cepillo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgcBTIAAh8IAngpIASAAIAAANIgNAAIggAhIAAB3g");
	this.shape.setTransform(-7.3,-14.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgmD0IAAm7IANgsIBAAAIAAHng");
	this.shape_1.setTransform(-5.4,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_2.setTransform(3.5,5.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_3.setTransform(3.5,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_4.setTransform(3.5,-5.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_5.setTransform(3.5,-10.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_6.setTransform(3.5,-16.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_7.setTransform(3.5,8.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_8.setTransform(3.5,2.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_9.setTransform(3.5,-2.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_10.setTransform(3.5,-8.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_11.setTransform(3.5,-13.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#256379").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQACADAAADQAAAEgCADQgEADgEAAg");
	this.shape_12.setTransform(3.5,-18.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.3,-24.5,20.7,49);


(lib._00cepillo1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2F2F8").s().p("AgcBTIAAh8IAngpIASAAIAAANIgNAAIgfAiIAAB2g");
	this.shape.setTransform(-6.9,-14.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B2F2F8").s().p("AgsD0IAAm4IAhgvIA3AAIAAHng");
	this.shape_1.setTransform(-6.3,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_2.setTransform(3.9,5.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_3.setTransform(3.9,0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_4.setTransform(3.9,-5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_5.setTransform(3.9,-10.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_6.setTransform(3.9,-15.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_7.setTransform(3.9,8.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_8.setTransform(3.9,3.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_9.setTransform(3.9,-2.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_10.setTransform(3.9,-7.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_11.setTransform(3.9,-13.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#B2F2F8").s().p("Ag4AKQgFAAgDgDQgEgDABgEQgBgDAEgDQADgDAFAAIByAAQAEAAAEADQADADAAADQAAAEgDADQgEADgEAAg");
	this.shape_12.setTransform(3.9,-18.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.8,-24.5,21.6,49);


(lib._00carte2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Egh0AAFIAAgJMBDpAAAIAAAJg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-216.5,-0.5,433,1);


(lib._00carte1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgEUeMAAAgo7IAJAAMAAAAo7g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-131,1,262);


(lib._00100 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgOBGQgHgDgFgFQgFgFgCgGQgDgGAAgJIAAhHQAAgIADgHQACgHAFgFQAFgFAHgCQAHgCAHgBQAIABAHACQAHACAFAFQAFAFACAHQADAHAAAIIAABHQAAAJgDAGQgCAGgFAFQgFAFgHADQgHACgIAAQgHAAgHgCgAgEgrQgDADAAADIAABLQAAADADACQACACACAAQADAAACgCQACgCAAgDIAAhLQAAgDgCgDQgCgBgDAAQgCAAgCABg");
	this.shape.setTransform(7.2,-0.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOBGQgHgDgFgFQgFgFgCgGQgDgGAAgJIAAhHQAAgIADgHQACgHAFgFQAFgFAHgCQAHgCAHgBQAIABAHACQAHACAFAFQAFAFACAHQADAHAAAIIAABHQAAAJgDAGQgCAGgFAFQgFAFgHADQgHACgIAAQgHAAgHgCgAgEgrQgDADAAADIAABLQAAADADACQACACACAAQADAAACgCQACgCAAgDIAAhLQAAgDgCgDQgCgBgDAAQgCAAgCABg");
	this.shape_1.setTransform(-1.6,-0.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNBFIAAh9QAHAAADgDQADgFAAgEIAOAAIAACJg");
	this.shape_2.setTransform(-8.8,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.6,-12.4,27.4,25);


(lib._0075 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPBEQgHgDgEgFQgEgEgCgGQgDgGAAgGIAAgNIAbAAIAAAKQAAAEACADQACACAEAAQACAAADgCQACgCAAgEIAAgfQAAgEgCgBQgDgBgCgBQgBAAgDACQAAAAgBAAQAAAAAAABQAAAAgBABQAAAAAAABIgagDIAEhGIBAAAQgCAOgEAGQgDAHgIgBIgYAAIgBAeQAEgFADgCQAGgDAGAAQALAAAGAHQADADABAFQACAEAAAFIAAAlQAAAIgDAGQgDAHgFAEQgFAEgHACQgHADgGAAQgJAAgGgDg");
	this.shape.setTransform(4,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgZBGQABgQACgQQADgQAFgOQAEgNAGgNQAEgNAHgKIgqAAIAAgcIBHAAIAAAMIgNAgQgFAPgEANQgEAQgCAQQgCAQAAATg");
	this.shape_1.setTransform(-3.9,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-12.5,20.5,25.1);


(lib._0050 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgOBGQgHgDgFgFQgFgFgCgGQgDgGAAgJIAAhHQAAgIADgHQACgHAFgFQAFgFAHgCQAHgCAHgBQAIABAHACQAHACAFAFQAFAFACAHQADAHAAAIIAABHQAAAJgDAGQgCAGgFAFQgFAFgHADQgHACgIAAQgHAAgHgCgAgEgrQgDADAAADIAABLQAAADADACQACACACAAQADAAACgCQACgCAAgDIAAhLQAAgDgCgDQgCgBgDAAQgCAAgCABg");
	this.shape.setTransform(4.2,-0.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgPBEQgHgDgEgEQgEgFgCgGQgDgGAAgGIAAgNIAbAAIAAAKQAAAFACACQACACAEAAQACAAADgCQACgCAAgEIAAgfQAAgEgCgBQgDgBgCAAQgBAAgDABQAAAAgBAAQAAAAAAABQgBAAAAABQAAAAAAABIgagDIAEhGIBAAAQgCANgEAHQgDAGgIAAIgYAAIgBAeQAEgFADgDQAGgCAGAAQALAAAGAHQADAEABAEQACAFAAAEIAAAlQAAAIgDAGQgDAHgFAEQgFAFgHACQgHACgGAAQgJAAgGgDg");
	this.shape_1.setTransform(-4.4,-0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.5,-12.4,21.3,25);


(lib._0025 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPBEQgHgDgEgEQgEgFgCgGQgDgGAAgGIAAgNIAbAAIAAAKQAAAFACACQACACAEAAQACAAADgCQACgCAAgEIAAgfQAAgEgCgBQgDgBgCAAQgBAAgDABQAAAAgBAAQAAAAAAABQgBAAAAABQAAAAAAABIgagDIAEhGIBAAAQgCANgEAHQgDAGgIAAIgYAAIgBAeQAEgFADgDQAGgCAGAAQALAAAGAHQADAEABAEQACAFAAAEIAAAlQAAAIgDAGQgDAHgFAEQgFAFgHACQgHACgGAAQgJAAgGgDg");
	this.shape.setTransform(4.2,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgjBHIAAgHQgBgIACgHIAEgPQADgHAGgIIAOgTIAHgJIAFgJIADgIIABgHQAAgGgCgCQgDgCgEAAQgCAAgCACQgCADgBAFIAAANIgaAAIAAgRQAAgGACgGQACgGAEgEQAEgFAHgDQAHgDAHAAQAJAAAIADQAGADAFAFQAFAFACAHQABAHAAAHIAAAMIgFAMIgHALQgFAGgIAIIgIAKIgDAHIgBAGIAAAGIAlAAIAAAag");
	this.shape_1.setTransform(-4.1,-0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.3,-12.4,20.8,25);


(lib._000 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgOBGQgHgDgFgFQgFgFgCgGQgDgGAAgJIAAhHQAAgIADgHQACgHAFgFQAFgFAHgCQAHgCAHgBQAIABAHACQAHACAFAFQAFAFACAHQADAHAAAIIAABHQAAAJgDAGQgCAGgFAFQgFAFgHADQgHACgIAAQgHAAgHgCgAgEgrQgDADAAADIAABLQAAADADACQACACACAAQADAAACgCQACgCAAgDIAAhLQAAgDgCgDQgCgBgDAAQgCAAgCABg");
	this.shape.setTransform(0.1,-0.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.3,-12.4,13,25);


(lib._0032 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AgLAqIAAgeIgdAAIAAgYIAdAAIAAgdIAXAAIAAAdIAdAAIAAAYIgdAAIAAAeg");
	this.shape.setTransform(0.1,-1.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.7,-16.2,13.8,32.7);


(lib._0031 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AgLAqIAAgeIgdAAIAAgYIAdAAIAAgdIAXAAIAAAdIAdAAIAAAYIgdAAIAAAeg");
	this.shape.setTransform(0.1,-1.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.7,-16.2,13.8,32.7);


(lib._002 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgLAqIAAgeIgdAAIAAgYIAdAAIAAgdIAXAAIAAAdIAdAAIAAAYIgdAAIAAAeg");
	this.shape.setTransform(0.1,-1.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.7,-16.2,13.8,32.7);


// stage content:
(lib.grafica_13_v1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_85 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(85).call(this.frame_85).wait(1));

	// 02 mascara3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_46 = new cjs.Graphics().p("A+EZiMAAAgwTMBEkAAAMAAAAwTg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(46).to({graphics:mask_graphics_46,x:246.5,y:163.5}).wait(40));

	// 02 franja3
	this.instance = new lib._02franja3();
	this.instance.setTransform(424.2,439.9);
	this.instance._off = true;

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(46).to({_off:false},0).wait(1).to({y:439.6},0).wait(1).to({y:438.5},0).wait(1).to({y:436.7},0).wait(1).to({y:434.1},0).wait(1).to({y:430.5},0).wait(1).to({y:425.9},0).wait(1).to({y:420.1},0).wait(1).to({y:413.2},0).wait(1).to({y:404.9},0).wait(1).to({y:395.2},0).wait(1).to({y:384.1},0).wait(1).to({y:371.6},0).wait(1).to({y:357.8},0).wait(1).to({y:343},0).wait(1).to({y:327.6},0).wait(1).to({y:312.1},0).wait(1).to({y:296.9},0).wait(1).to({y:282.7},0).wait(1).to({y:269.8},0).wait(1).to({y:258.3},0).wait(1).to({y:248.3},0).wait(1).to({y:239.9},0).wait(1).to({y:233},0).wait(1).to({y:227.4},0).wait(1).to({y:223.1},0).wait(1).to({y:219.9},0).wait(1).to({y:217.7},0).wait(1).to({y:216.4},0).wait(1).to({y:216.1},0).wait(11));

	// 02 mascara2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_44 = new cjs.Graphics().p("A+EZiMAAAgwTMBEkAAAMAAAAwTg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(44).to({graphics:mask_1_graphics_44,x:246.5,y:163.5}).wait(42));

	// 02 franja2
	this.instance_1 = new lib._02franja2();
	this.instance_1.setTransform(368.9,350);
	this.instance_1._off = true;

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(44).to({_off:false},0).wait(1).to({y:349.9},0).wait(1).to({y:349.7},0).wait(1).to({y:349.3},0).wait(1).to({y:348.8},0).wait(1).to({y:348.1},0).wait(1).to({y:347.2},0).wait(1).to({y:346.1},0).wait(1).to({x:368.8,y:344.7},0).wait(1).to({y:343.1},0).wait(1).to({y:341.2},0).wait(1).to({y:339},0).wait(1).to({x:368.7,y:336.5},0).wait(1).to({y:333.8},0).wait(1).to({y:330.9},0).wait(1).to({x:368.6,y:327.9},0).wait(1).to({y:324.8},0).wait(1).to({x:368.5,y:321.9},0).wait(1).to({y:319.1},0).wait(1).to({y:316.5},0).wait(1).to({y:314.3},0).wait(1).to({x:368.4,y:312.3},0).wait(1).to({y:310.7},0).wait(1).to({y:309.3},0).wait(1).to({y:308.2},0).wait(1).to({y:307.4},0).wait(1).to({y:306.7},0).wait(1).to({y:306.3},0).wait(1).to({y:306.1},0).wait(1).to({y:306},0).wait(13));

	// 02 mascara1 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_42 = new cjs.Graphics().p("A+EZiMAAAgwTMBEkAAAMAAAAwTg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_2_graphics_42,x:246.5,y:163.5}).wait(44));

	// 02 franja1
	this.instance_2 = new lib._02franja1();
	this.instance_2.setTransform(312.6,344.2);
	this.instance_2._off = true;

	this.instance_2.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(42).to({_off:false},0).wait(2).to({y:344},0).wait(1).to({y:343.7},0).wait(1).to({y:343.4},0).wait(1).to({y:342.8},0).wait(1).to({y:342.2},0).wait(1).to({y:341.3},0).wait(1).to({y:340.3},0).wait(1).to({y:339.1},0).wait(1).to({y:337.7},0).wait(1).to({y:336.1},0).wait(1).to({y:334.3},0).wait(1).to({y:332.3},0).wait(1).to({y:330.2},0).wait(1).to({y:327.9},0).wait(1).to({y:325.7},0).wait(1).to({y:323.5},0).wait(1).to({y:321.5},0).wait(1).to({y:319.6},0).wait(1).to({y:317.9},0).wait(1).to({y:316.5},0).wait(1).to({y:315.3},0).wait(1).to({y:314.2},0).wait(1).to({y:313.4},0).wait(1).to({y:312.8},0).wait(1).to({y:312.4},0).wait(1).to({y:312},0).wait(1).to({y:311.9},0).wait(1).to({y:311.8},0).wait(15));

	// 02 11p
	this.instance_3 = new lib._0211p();
	this.instance_3.setTransform(372.9,360.9,0.033,0.033);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(42).to({_off:false},0).wait(1).to({regX:0.6,regY:1.6,scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05,x:373},0).wait(1).to({scaleX:0.06,scaleY:0.06,y:361},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.12,scaleY:0.12,y:361.1},0).wait(1).to({scaleX:0.15,scaleY:0.15},0).wait(1).to({scaleX:0.19,scaleY:0.19,y:361.2},0).wait(1).to({scaleX:0.23,scaleY:0.23,x:373.1},0).wait(1).to({scaleX:0.28,scaleY:0.28,y:361.3},0).wait(1).to({scaleX:0.33,scaleY:0.33,y:361.4},0).wait(1).to({scaleX:0.39,scaleY:0.39,x:373.2,y:361.5},0).wait(1).to({scaleX:0.46,scaleY:0.46,y:361.6},0).wait(1).to({scaleX:0.53,scaleY:0.53,y:361.7},0).wait(1).to({scaleX:0.59,scaleY:0.59,x:373.3,y:361.8},0).wait(1).to({scaleX:0.66,scaleY:0.66,y:361.9},0).wait(1).to({scaleX:0.72,scaleY:0.72,x:373.4,y:362},0).wait(1).to({scaleX:0.77,scaleY:0.77,y:362.1},0).wait(1).to({scaleX:0.82,scaleY:0.82,y:362.2},0).wait(1).to({scaleX:0.87,scaleY:0.87,y:362.3},0).wait(1).to({scaleX:0.9,scaleY:0.9,x:373.5},0).wait(1).to({scaleX:0.93,scaleY:0.93,y:362.4},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99,y:362.5},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,regY:0.1,scaleX:1,scaleY:1,x:373,y:360.9},0).wait(15));

	// 02 mascara8 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	var mask_3_graphics_23 = new cjs.Graphics().p("A+EZiMAAAgwTMBEkAAAMAAAAwTg");

	this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(23).to({graphics:mask_3_graphics_23,x:246.5,y:163.5}).wait(63));

	// 01 franja3
	this.instance_4 = new lib._01franja3();
	this.instance_4.setTransform(212.7,439.9);
	this.instance_4._off = true;

	this.instance_4.mask = mask_3;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(23).to({_off:false},0).wait(1).to({y:439.6},0).wait(1).to({y:438.5},0).wait(1).to({y:436.6},0).wait(1).to({y:433.9},0).wait(1).to({y:430.1},0).wait(1).to({y:425.2},0).wait(1).to({y:419.1},0).wait(1).to({y:411.7},0).wait(1).to({y:402.8},0).wait(1).to({y:392.4},0).wait(1).to({y:380.4},0).wait(1).to({y:366.8},0).wait(1).to({y:352},0).wait(1).to({y:336.4},0).wait(1).to({y:320.4},0).wait(1).to({y:304.7},0).wait(1).to({y:289.8},0).wait(1).to({y:276.2},0).wait(1).to({y:264},0).wait(1).to({y:253.5},0).wait(1).to({y:244.5},0).wait(1).to({y:237},0).wait(1).to({y:230.9},0).wait(1).to({y:226},0).wait(1).to({y:222.2},0).wait(1).to({y:219.4},0).wait(1).to({y:217.5},0).wait(1).to({y:216.4},0).wait(1).to({y:216.1},0).wait(34));

	// 02 mascara7 (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	var mask_4_graphics_21 = new cjs.Graphics().p("A+EZiMAAAgwTMBEkAAAMAAAAwTg");

	this.timeline.addTween(cjs.Tween.get(mask_4).to({graphics:null,x:0,y:0}).wait(21).to({graphics:mask_4_graphics_21,x:246.5,y:163.5}).wait(65));

	// 01 franja2
	this.instance_5 = new lib._01franja2();
	this.instance_5.setTransform(158.8,377.9);
	this.instance_5._off = true;

	this.instance_5.mask = mask_4;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(21).to({_off:false},0).wait(1).to({y:377.7},0).wait(1).to({y:377.2},0).wait(1).to({y:376.4},0).wait(1).to({y:375.2},0).wait(1).to({y:373.5},0).wait(1).to({y:371.3},0).wait(1).to({y:368.6},0).wait(1).to({y:365.3},0).wait(1).to({y:361.3},0).wait(1).to({y:356.7},0).wait(1).to({y:351.3},0).wait(1).to({y:345.3},0).wait(1).to({y:338.7},0).wait(1).to({y:331.7},0).wait(1).to({y:324.6},0).wait(1).to({y:317.6},0).wait(1).to({y:311},0).wait(1).to({y:304.9},0).wait(1).to({y:299.5},0).wait(1).to({y:294.8},0).wait(1).to({y:290.8},0).wait(1).to({y:287.4},0).wait(1).to({y:284.7},0).wait(1).to({y:282.5},0).wait(1).to({y:280.8},0).wait(1).to({y:279.6},0).wait(1).to({y:278.7},0).wait(1).to({y:278.3},0).wait(1).to({y:278.1},0).wait(36));

	// 02 mascara6 (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	var mask_5_graphics_19 = new cjs.Graphics().p("A+EZiMAAAgwTMBEkAAAMAAAAwTg");

	this.timeline.addTween(cjs.Tween.get(mask_5).to({graphics:null,x:0,y:0}).wait(19).to({graphics:mask_5_graphics_19,x:246.5,y:163.5}).wait(67));

	// 01 franja1
	this.instance_6 = new lib._01franja1();
	this.instance_6.setTransform(103.2,347.4);
	this.instance_6._off = true;

	this.instance_6.mask = mask_5;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(19).to({_off:false},0).wait(1).to({x:103.1,y:347.3},0).wait(1).to({y:347.2},0).wait(1).to({y:346.8},0).wait(1).to({y:346.3},0).wait(1).to({y:345.7},0).wait(1).to({y:344.8},0).wait(1).to({y:343.7},0).wait(1).to({y:342.4},0).wait(1).to({y:340.9},0).wait(1).to({y:339},0).wait(1).to({y:336.9},0).wait(1).to({x:103,y:334.5},0).wait(1).to({y:331.9},0).wait(1).to({y:329.2},0).wait(1).to({y:326.3},0).wait(1).to({x:102.9,y:323.6},0).wait(1).to({y:320.9},0).wait(1).to({y:318.5},0).wait(1).to({y:316.4},0).wait(1).to({y:314.6},0).wait(1).to({x:102.8,y:313},0).wait(1).to({y:311.6},0).wait(1).to({y:310.6},0).wait(1).to({y:309.7},0).wait(1).to({y:309},0).wait(1).to({y:308.5},0).wait(1).to({y:308.2},0).wait(1).to({y:308},0).wait(39));

	// 01 11v
	this.instance_7 = new lib._0111v();
	this.instance_7.setTransform(159.2,360.9,0.033,0.033);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(19).to({_off:false},0).wait(1).to({regX:1,regY:-1.1,scaleX:0.03,scaleY:0.03,x:159.3,y:360.8},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.12,scaleY:0.12,y:360.7},0).wait(1).to({scaleX:0.16,scaleY:0.16,x:159.4},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.24,scaleY:0.24,x:159.5,y:360.6},0).wait(1).to({scaleX:0.29,scaleY:0.29},0).wait(1).to({scaleX:0.35,scaleY:0.35,x:159.6,y:360.5},0).wait(1).to({scaleX:0.41,scaleY:0.41,y:360.4},0).wait(1).to({scaleX:0.48,scaleY:0.48,x:159.7,y:360.3},0).wait(1).to({scaleX:0.55,scaleY:0.55,x:159.8},0).wait(1).to({scaleX:0.62,scaleY:0.62,y:360.2},0).wait(1).to({scaleX:0.68,scaleY:0.68,x:159.9,y:360.1},0).wait(1).to({scaleX:0.74,scaleY:0.74,x:160},0).wait(1).to({scaleX:0.79,scaleY:0.79,y:360},0).wait(1).to({scaleX:0.84,scaleY:0.84,x:160.1},0).wait(1).to({scaleX:0.88,scaleY:0.88,y:359.9},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94,x:160.2,y:359.8},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0.1,regY:0.1,scaleX:1,scaleY:1,x:159.2,y:360.9},0).wait(38));

	// 00 listerine
	this.instance_8 = new lib._00listerine();
	this.instance_8.setTransform(402.8,419.9,0.065,0.065,-90);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(47).to({_off:false},0).wait(1).to({scaleX:0.07,scaleY:0.07,rotation:-89.9},0).wait(1).to({scaleX:0.07,scaleY:0.07,rotation:-89.4},0).wait(1).to({scaleX:0.08,scaleY:0.08,rotation:-88.6},0).wait(1).to({scaleX:0.09,scaleY:0.09,rotation:-87.5},0).wait(1).to({scaleX:0.11,scaleY:0.11,rotation:-85.9},0).wait(1).to({scaleX:0.13,scaleY:0.13,rotation:-83.9},0).wait(1).to({scaleX:0.15,scaleY:0.15,rotation:-81.4},0).wait(1).to({scaleX:0.19,scaleY:0.19,rotation:-78.4},0).wait(1).to({scaleX:0.22,scaleY:0.22,rotation:-74.8},0).wait(1).to({scaleX:0.27,scaleY:0.27,rotation:-70.5},0).wait(1).to({scaleX:0.32,scaleY:0.32,rotation:-65.7},0).wait(1).to({scaleX:0.37,scaleY:0.37,rotation:-60.3},0).wait(1).to({scaleX:0.44,scaleY:0.44,rotation:-54.4},0).wait(1).to({scaleX:0.5,scaleY:0.5,rotation:-48.1},0).wait(1).to({scaleX:0.57,scaleY:0.57,rotation:-41.8},0).wait(1).to({scaleX:0.63,scaleY:0.63,rotation:-35.6},0).wait(1).to({scaleX:0.69,scaleY:0.69,rotation:-29.7},0).wait(1).to({scaleX:0.75,scaleY:0.75,rotation:-24.3},0).wait(1).to({scaleX:0.8,scaleY:0.8,rotation:-19.4},0).wait(1).to({scaleX:0.84,scaleY:0.84,rotation:-15.2},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:-11.6},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:-8.6},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:-6.1},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-4.1},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-2.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.4},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-0.6},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.1},0).wait(1).to({scaleX:1,scaleY:1,rotation:0},0).wait(10));

	// 00 +32
	this.instance_9 = new lib._0032();
	this.instance_9.setTransform(383.2,422.5,0.033,0.033,180);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(45).to({_off:false},0).wait(1).to({regX:0.1,regY:-1.2,scaleX:0.04,scaleY:0.04,rotation:179.7},0).wait(1).to({scaleX:0.04,scaleY:0.04,rotation:178.8},0).wait(1).to({scaleX:0.05,scaleY:0.05,rotation:177.3},0).wait(1).to({scaleX:0.06,scaleY:0.06,rotation:175},0).wait(1).to({scaleX:0.08,scaleY:0.08,rotation:171.9,y:422.6},0).wait(1).to({scaleX:0.1,scaleY:0.1,rotation:167.9},0).wait(1).to({scaleX:0.13,scaleY:0.13,rotation:162.9,x:383.3},0).wait(1).to({scaleX:0.16,scaleY:0.16,rotation:156.8},0).wait(1).to({scaleX:0.2,scaleY:0.2,rotation:149.5,y:422.7},0).wait(1).to({scaleX:0.24,scaleY:0.24,rotation:141.1,x:383.4},0).wait(1).to({scaleX:0.3,scaleY:0.3,rotation:131.4,x:383.5},0).wait(1).to({scaleX:0.35,scaleY:0.35,rotation:120.5,x:383.6},0).wait(1).to({scaleX:0.42,scaleY:0.42,rotation:108.7,x:383.7},0).wait(1).to({scaleX:0.48,scaleY:0.48,rotation:96.3,x:383.8,y:422.6},0).wait(1).to({scaleX:0.55,scaleY:0.55,rotation:83.6,x:383.9,y:422.5},0).wait(1).to({scaleX:0.62,scaleY:0.62,rotation:71.2,y:422.3},0).wait(1).to({scaleX:0.68,scaleY:0.68,rotation:59.4,x:384,y:422.1},0).wait(1).to({scaleX:0.74,scaleY:0.74,rotation:48.6,x:383.9,y:421.9},0).wait(1).to({scaleX:0.79,scaleY:0.79,rotation:38.9,y:421.8},0).wait(1).to({scaleX:0.84,scaleY:0.84,rotation:30.4,x:383.8,y:421.7},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:23.2,x:383.7,y:421.6},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:17.1,x:383.6,y:421.5},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:12.1,y:421.4},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:8.1,x:383.5,y:421.3},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:5,x:383.4},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:2.7},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:1.2,x:383.3},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.3},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,rotation:0,x:383.2,y:422.5},0).wait(12));

	// 00 seda
	this.instance_10 = new lib._00seda();
	this.instance_10.setTransform(358.9,420.1,0.051,0.051,-90);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(43).to({_off:false},0).wait(1).to({scaleX:0.05,scaleY:0.05,rotation:-89.9},0).wait(1).to({scaleX:0.06,scaleY:0.06,rotation:-89.4},0).wait(1).to({scaleX:0.07,scaleY:0.07,rotation:-88.6},0).wait(1).to({scaleX:0.08,scaleY:0.08,rotation:-87.5},0).wait(1).to({scaleX:0.09,scaleY:0.09,rotation:-85.9},0).wait(1).to({scaleX:0.12,scaleY:0.12,rotation:-83.9},0).wait(1).to({scaleX:0.14,scaleY:0.14,rotation:-81.4},0).wait(1).to({scaleX:0.17,scaleY:0.17,rotation:-78.4},0).wait(1).to({scaleX:0.21,scaleY:0.21,rotation:-74.8},0).wait(1).to({scaleX:0.26,scaleY:0.26,rotation:-70.5},0).wait(1).to({scaleX:0.31,scaleY:0.31,rotation:-65.7},0).wait(1).to({scaleX:0.36,scaleY:0.36,rotation:-60.3},0).wait(1).to({scaleX:0.43,scaleY:0.43,rotation:-54.4},0).wait(1).to({scaleX:0.49,scaleY:0.49,rotation:-48.1},0).wait(1).to({scaleX:0.56,scaleY:0.56,rotation:-41.8},0).wait(1).to({scaleX:0.63,scaleY:0.63,rotation:-35.6},0).wait(1).to({scaleX:0.69,scaleY:0.69,rotation:-29.7},0).wait(1).to({scaleX:0.74,scaleY:0.74,rotation:-24.3},0).wait(1).to({scaleX:0.8,scaleY:0.8,rotation:-19.4},0).wait(1).to({scaleX:0.84,scaleY:0.84,rotation:-15.2},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:-11.6},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:-8.6},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:-6.1},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-4.1},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-2.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.4},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-0.6},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.1},0).wait(1).to({scaleX:1,scaleY:1,rotation:0},0).wait(14));

	// 00 +31
	this.instance_11 = new lib._0031();
	this.instance_11.setTransform(333.2,422.5,0.033,0.033,180);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(41).to({_off:false},0).wait(1).to({regX:0.1,regY:-1.2,scaleX:0.04,scaleY:0.04,rotation:179.7},0).wait(1).to({scaleX:0.04,scaleY:0.04,rotation:178.8},0).wait(1).to({scaleX:0.05,scaleY:0.05,rotation:177.3},0).wait(1).to({scaleX:0.06,scaleY:0.06,rotation:175},0).wait(1).to({scaleX:0.08,scaleY:0.08,rotation:171.9,y:422.6},0).wait(1).to({scaleX:0.1,scaleY:0.1,rotation:167.9},0).wait(1).to({scaleX:0.13,scaleY:0.13,rotation:162.9},0).wait(1).to({scaleX:0.16,scaleY:0.16,rotation:156.8},0).wait(1).to({scaleX:0.2,scaleY:0.2,rotation:149.5,x:333.3,y:422.7},0).wait(1).to({scaleX:0.24,scaleY:0.24,rotation:141.1,x:333.4},0).wait(1).to({scaleX:0.3,scaleY:0.3,rotation:131.4},0).wait(1).to({scaleX:0.35,scaleY:0.35,rotation:120.5,x:333.5},0).wait(1).to({scaleX:0.42,scaleY:0.42,rotation:108.7,x:333.6},0).wait(1).to({scaleX:0.48,scaleY:0.48,rotation:96.3,x:333.8,y:422.6},0).wait(1).to({scaleX:0.55,scaleY:0.55,rotation:83.6,y:422.5},0).wait(1).to({scaleX:0.62,scaleY:0.62,rotation:71.2,x:333.9,y:422.3},0).wait(1).to({scaleX:0.68,scaleY:0.68,rotation:59.4,y:422.1},0).wait(1).to({scaleX:0.74,scaleY:0.74,rotation:48.6,y:421.9},0).wait(1).to({scaleX:0.79,scaleY:0.79,rotation:38.9,x:333.8,y:421.8},0).wait(1).to({scaleX:0.84,scaleY:0.84,rotation:30.4,x:333.7,y:421.7},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:23.2,y:421.6},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:17.1,x:333.6,y:421.5},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:12.1,x:333.5,y:421.4},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:8.1,x:333.4,y:421.3},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:5},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:2.7,x:333.3},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:1.2},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.3},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,rotation:0,x:333.2,y:422.5},0).wait(16));

	// 00 cepillo3
	this.instance_12 = new lib._00cepillo3();
	this.instance_12.setTransform(312.9,425,0.061,0.061,-90);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(39).to({_off:false},0).wait(1).to({scaleX:0.06,scaleY:0.06,rotation:-89.9},0).wait(1).to({scaleX:0.07,scaleY:0.07,rotation:-89.4},0).wait(1).to({scaleX:0.08,scaleY:0.08,rotation:-88.6},0).wait(1).to({scaleX:0.09,scaleY:0.09,rotation:-87.5},0).wait(1).to({scaleX:0.1,scaleY:0.1,rotation:-85.9},0).wait(1).to({scaleX:0.13,scaleY:0.13,rotation:-83.9},0).wait(1).to({scaleX:0.15,scaleY:0.15,rotation:-81.4},0).wait(1).to({scaleX:0.18,scaleY:0.18,rotation:-78.4},0).wait(1).to({scaleX:0.22,scaleY:0.22,rotation:-74.8},0).wait(1).to({scaleX:0.26,scaleY:0.26,rotation:-70.5},0).wait(1).to({scaleX:0.32,scaleY:0.32,rotation:-65.7},0).wait(1).to({scaleX:0.37,scaleY:0.37,rotation:-60.3},0).wait(1).to({scaleX:0.43,scaleY:0.43,rotation:-54.4},0).wait(1).to({scaleX:0.5,scaleY:0.5,rotation:-48.1},0).wait(1).to({scaleX:0.56,scaleY:0.56,rotation:-41.8},0).wait(1).to({scaleX:0.63,scaleY:0.63,rotation:-35.6},0).wait(1).to({scaleX:0.69,scaleY:0.69,rotation:-29.7},0).wait(1).to({scaleX:0.75,scaleY:0.75,rotation:-24.3},0).wait(1).to({scaleX:0.8,scaleY:0.8,rotation:-19.4},0).wait(1).to({scaleX:0.84,scaleY:0.84,rotation:-15.2},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:-11.6},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:-8.6},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:-6.1},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-4.1},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-2.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.4},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-0.6},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.1},0).wait(1).to({scaleX:1,scaleY:1,rotation:0},0).wait(18));

	// 00 punto3
	this.instance_13 = new lib._00punto3();
	this.instance_13.setTransform(298.2,394.9,0.101,0.101);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(39).to({_off:false},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.18,scaleY:0.18},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.29,scaleY:0.29},0).wait(1).to({scaleX:0.33,scaleY:0.33},0).wait(1).to({scaleX:0.38,scaleY:0.38},0).wait(1).to({scaleX:0.44,scaleY:0.44},0).wait(1).to({scaleX:0.5,scaleY:0.5},0).wait(1).to({scaleX:0.56,scaleY:0.56},0).wait(1).to({scaleX:0.63,scaleY:0.63},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.79,scaleY:0.79},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(18));

	// 00 seda2
	this.instance_14 = new lib._00seda2();
	this.instance_14.setTransform(251.5,420.2,0.036,0.036,-90);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(36).to({_off:false},0).wait(1).to({scaleX:0.04,scaleY:0.04,rotation:-89.9},0).wait(1).to({scaleX:0.04,scaleY:0.04,rotation:-89.4},0).wait(1).to({scaleX:0.05,scaleY:0.05,rotation:-88.7},0).wait(1).to({scaleX:0.06,scaleY:0.06,rotation:-87.6},0).wait(1).to({scaleX:0.08,scaleY:0.08,rotation:-86.1},0).wait(1).to({scaleX:0.1,scaleY:0.1,rotation:-84.2},0).wait(1).to({scaleX:0.12,scaleY:0.12,rotation:-81.8},0).wait(1).to({scaleX:0.16,scaleY:0.16,rotation:-78.9},0).wait(1).to({scaleX:0.19,scaleY:0.19,rotation:-75.5},0).wait(1).to({scaleX:0.24,scaleY:0.24,rotation:-71.5},0).wait(1).to({scaleX:0.28,scaleY:0.28,rotation:-66.9},0).wait(1).to({scaleX:0.34,scaleY:0.34,rotation:-61.7},0).wait(1).to({scaleX:0.4,scaleY:0.4,rotation:-56},0).wait(1).to({scaleX:0.47,scaleY:0.47,rotation:-49.9},0).wait(1).to({scaleX:0.53,scaleY:0.53,rotation:-43.7},0).wait(1).to({scaleX:0.6,scaleY:0.6,rotation:-37.5},0).wait(1).to({scaleX:0.66,scaleY:0.66,rotation:-31.5},0).wait(1).to({scaleX:0.72,scaleY:0.72,rotation:-25.8},0).wait(1).to({scaleX:0.78,scaleY:0.78,rotation:-20.8},0).wait(1).to({scaleX:0.83,scaleY:0.83,rotation:-16.3},0).wait(1).to({scaleX:0.87,scaleY:0.87,rotation:-12.5},0).wait(1).to({scaleX:0.9,scaleY:0.9,rotation:-9.2},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:-6.5},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:-4.4},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-2.7},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-1.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-0.6},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.2},0).wait(1).to({scaleX:1,scaleY:1,rotation:0},0).wait(21));

	// 00 +2
	this.instance_15 = new lib._002();
	this.instance_15.setTransform(221.9,423.3,0.036,0.036,180);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(34).to({_off:false},0).wait(1).to({regX:0.1,regY:-1.2,scaleX:0.04,scaleY:0.04,rotation:179.7},0).wait(1).to({scaleX:0.04,scaleY:0.04,rotation:178.9},0).wait(1).to({scaleX:0.05,scaleY:0.05,rotation:177.4},0).wait(1).to({scaleX:0.06,scaleY:0.06,rotation:175.2},0).wait(1).to({scaleX:0.08,scaleY:0.08,rotation:172.2,y:423.4},0).wait(1).to({scaleX:0.1,scaleY:0.1,rotation:168.4},0).wait(1).to({scaleX:0.12,scaleY:0.12,rotation:163.6,x:222},0).wait(1).to({scaleX:0.15,scaleY:0.15,rotation:157.8},0).wait(1).to({scaleX:0.19,scaleY:0.19,rotation:151,y:423.5},0).wait(1).to({scaleX:0.23,scaleY:0.23,rotation:142.9,x:222.1},0).wait(1).to({scaleX:0.28,scaleY:0.28,rotation:133.7,x:222.2},0).wait(1).to({scaleX:0.34,scaleY:0.34,rotation:123.4,x:222.3},0).wait(1).to({scaleX:0.4,scaleY:0.4,rotation:112,x:222.4},0).wait(1).to({scaleX:0.47,scaleY:0.47,rotation:99.9,x:222.5,y:423.4},0).wait(1).to({scaleX:0.53,scaleY:0.53,rotation:87.4,x:222.6,y:423.3},0).wait(1).to({scaleX:0.6,scaleY:0.6,rotation:74.9,y:423.1},0).wait(1).to({scaleX:0.66,scaleY:0.66,rotation:62.9,x:222.7,y:423},0).wait(1).to({scaleX:0.72,scaleY:0.72,rotation:51.7,y:422.8},0).wait(1).to({scaleX:0.78,scaleY:0.78,rotation:41.6,x:222.6,y:422.6},0).wait(1).to({scaleX:0.83,scaleY:0.83,rotation:32.6,x:222.5,y:422.5},0).wait(1).to({scaleX:0.87,scaleY:0.87,rotation:24.9,y:422.4},0).wait(1).to({scaleX:0.9,scaleY:0.9,rotation:18.4,x:222.4,y:422.3},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:13,x:222.3,y:422.2},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:8.7,x:222.2,y:422.1},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:5.4,x:222.1},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:2.9},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:1.3},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.3,x:222},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,rotation:0,x:221.9,y:423.3},0).wait(23));

	// 00 cepillo2
	this.instance_16 = new lib._00cepillo2();
	this.instance_16.setTransform(197.4,426.5,0.053,0.053,-90);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(32).to({_off:false},0).wait(1).to({scaleX:0.05,scaleY:0.05,rotation:-89.9},0).wait(1).to({scaleX:0.06,scaleY:0.06,rotation:-89.4},0).wait(1).to({scaleX:0.07,scaleY:0.07,rotation:-88.7},0).wait(1).to({scaleX:0.08,scaleY:0.08,rotation:-87.6},0).wait(1).to({scaleX:0.09,scaleY:0.09,rotation:-86.1},0).wait(1).to({scaleX:0.11,scaleY:0.11,rotation:-84.2},0).wait(1).to({scaleX:0.14,scaleY:0.14,rotation:-81.8},0).wait(1).to({scaleX:0.17,scaleY:0.17,rotation:-78.9},0).wait(1).to({scaleX:0.21,scaleY:0.21,rotation:-75.5},0).wait(1).to({scaleX:0.25,scaleY:0.25,rotation:-71.5},0).wait(1).to({scaleX:0.3,scaleY:0.3,rotation:-66.9},0).wait(1).to({scaleX:0.35,scaleY:0.35,rotation:-61.7},0).wait(1).to({scaleX:0.41,scaleY:0.41,rotation:-56},0).wait(1).to({scaleX:0.47,scaleY:0.47,rotation:-49.9},0).wait(1).to({scaleX:0.54,scaleY:0.54,rotation:-43.7},0).wait(1).to({scaleX:0.61,scaleY:0.61,rotation:-37.5},0).wait(1).to({scaleX:0.67,scaleY:0.67,rotation:-31.5},0).wait(1).to({scaleX:0.73,scaleY:0.73,rotation:-25.8},0).wait(1).to({scaleX:0.78,scaleY:0.78,rotation:-20.8},0).wait(1).to({scaleX:0.83,scaleY:0.83,rotation:-16.3},0).wait(1).to({scaleX:0.87,scaleY:0.87,rotation:-12.5},0).wait(1).to({scaleX:0.9,scaleY:0.9,rotation:-9.2},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:-6.5},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:-4.4},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-2.7},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-0.6},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.2},0).wait(1).to({scaleX:1,scaleY:1,rotation:0},0).wait(25));

	// 00 punto2
	this.instance_17 = new lib._00punto2();
	this.instance_17.setTransform(179.8,394.9,0.101,0.101);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(32).to({_off:false},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.18,scaleY:0.18},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.29,scaleY:0.29},0).wait(1).to({scaleX:0.33,scaleY:0.33},0).wait(1).to({scaleX:0.38,scaleY:0.38},0).wait(1).to({scaleX:0.44,scaleY:0.44},0).wait(1).to({scaleX:0.5,scaleY:0.5},0).wait(1).to({scaleX:0.56,scaleY:0.56},0).wait(1).to({scaleX:0.63,scaleY:0.63},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.79,scaleY:0.79},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(25));

	// 00 cepillo1
	this.instance_18 = new lib._00cepillo1();
	this.instance_18.setTransform(124.8,425.5,0.051,0.051,-90);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(29).to({_off:false},0).wait(1).to({scaleX:0.05,scaleY:0.05,rotation:-89.9},0).wait(1).to({scaleX:0.06,scaleY:0.06,rotation:-89.4},0).wait(1).to({scaleX:0.07,scaleY:0.07,rotation:-88.7},0).wait(1).to({scaleX:0.08,scaleY:0.08,rotation:-87.6},0).wait(1).to({scaleX:0.09,scaleY:0.09,rotation:-86.1},0).wait(1).to({scaleX:0.11,scaleY:0.11,rotation:-84.2},0).wait(1).to({scaleX:0.14,scaleY:0.14,rotation:-81.8},0).wait(1).to({scaleX:0.17,scaleY:0.17,rotation:-78.9},0).wait(1).to({scaleX:0.2,scaleY:0.2,rotation:-75.5},0).wait(1).to({scaleX:0.25,scaleY:0.25,rotation:-71.5},0).wait(1).to({scaleX:0.3,scaleY:0.3,rotation:-66.9},0).wait(1).to({scaleX:0.35,scaleY:0.35,rotation:-61.7},0).wait(1).to({scaleX:0.41,scaleY:0.41,rotation:-56},0).wait(1).to({scaleX:0.47,scaleY:0.47,rotation:-49.9},0).wait(1).to({scaleX:0.54,scaleY:0.54,rotation:-43.7},0).wait(1).to({scaleX:0.61,scaleY:0.61,rotation:-37.5},0).wait(1).to({scaleX:0.67,scaleY:0.67,rotation:-31.5},0).wait(1).to({scaleX:0.73,scaleY:0.73,rotation:-25.8},0).wait(1).to({scaleX:0.78,scaleY:0.78,rotation:-20.8},0).wait(1).to({scaleX:0.83,scaleY:0.83,rotation:-16.3},0).wait(1).to({scaleX:0.87,scaleY:0.87,rotation:-12.5},0).wait(1).to({scaleX:0.9,scaleY:0.9,rotation:-9.2},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:-6.5},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:-4.4},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-2.7},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-1.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-0.6},0).wait(1).to({scaleX:1,scaleY:1,rotation:-0.2},0).wait(1).to({scaleX:1,scaleY:1,rotation:0},0).wait(28));

	// 00 punto1
	this.instance_19 = new lib._00punto1();
	this.instance_19.setTransform(108.6,394.9,0.11,0.11);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(29).to({_off:false},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.12,scaleY:0.12},0).wait(1).to({scaleX:0.12,scaleY:0.12},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.15,scaleY:0.15},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.19,scaleY:0.19},0).wait(1).to({scaleX:0.22,scaleY:0.22},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.29,scaleY:0.29},0).wait(1).to({scaleX:0.34,scaleY:0.34},0).wait(1).to({scaleX:0.39,scaleY:0.39},0).wait(1).to({scaleX:0.45,scaleY:0.45},0).wait(1).to({scaleX:0.51,scaleY:0.51},0).wait(1).to({scaleX:0.57,scaleY:0.57},0).wait(1).to({scaleX:0.63,scaleY:0.63},0).wait(1).to({scaleX:0.69,scaleY:0.69},0).wait(1).to({scaleX:0.74,scaleY:0.74},0).wait(1).to({scaleX:0.8,scaleY:0.8},0).wait(1).to({scaleX:0.84,scaleY:0.84},0).wait(1).to({scaleX:0.88,scaleY:0.88},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.94,scaleY:0.94},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(28));

	// 00 titulo2
	this.instance_20 = new lib._00titulo2();
	this.instance_20.setTransform(274.4,30.3);
	this.instance_20.alpha = 0;
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(7).to({_off:false},0).wait(1).to({regX:0.1,regY:-0.5,x:274.5,y:29.8,alpha:0.001},0).wait(1).to({y:29.9,alpha:0.006},0).wait(1).to({y:30.1,alpha:0.014},0).wait(1).to({y:30.5,alpha:0.026},0).wait(1).to({y:30.9,alpha:0.042},0).wait(1).to({y:31.5,alpha:0.063},0).wait(1).to({y:32.2,alpha:0.09},0).wait(1).to({y:33.1,alpha:0.121},0).wait(1).to({y:34.1,alpha:0.16},0).wait(1).to({y:35.3,alpha:0.204},0).wait(1).to({y:36.7,alpha:0.256},0).wait(1).to({y:38.3,alpha:0.314},0).wait(1).to({y:40,alpha:0.378},0).wait(1).to({y:41.9,alpha:0.446},0).wait(1).to({y:43.8,alpha:0.517},0).wait(1).to({y:45.7,alpha:0.587},0).wait(1).to({y:47.6,alpha:0.655},0).wait(1).to({y:49.3,alpha:0.717},0).wait(1).to({y:50.8,alpha:0.774},0).wait(1).to({y:52.1,alpha:0.823},0).wait(1).to({y:53.3,alpha:0.865},0).wait(1).to({y:54.2,alpha:0.901},0).wait(1).to({y:55,alpha:0.93},0).wait(1).to({y:55.7,alpha:0.953},0).wait(1).to({y:56.2,alpha:0.971},0).wait(1).to({y:56.5,alpha:0.984},0).wait(1).to({y:56.8,alpha:0.993},0).wait(1).to({y:56.9,alpha:0.998},0).wait(1).to({regX:0,regY:0,x:274.4,y:57.5,alpha:1},0).wait(50));

	// 00 titulo1
	this.instance_21 = new lib._00titulo1();
	this.instance_21.setTransform(274.4,8.4);
	this.instance_21.alpha = 0;
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(9).to({_off:false},0).wait(1).to({regX:0.1,regY:0.2,x:274.5,y:8.6,alpha:0.001},0).wait(1).to({y:8.7,alpha:0.006},0).wait(1).to({y:8.9,alpha:0.014},0).wait(1).to({y:9.3,alpha:0.026},0).wait(1).to({y:9.7,alpha:0.042},0).wait(1).to({y:10.3,alpha:0.063},0).wait(1).to({y:11,alpha:0.09},0).wait(1).to({y:11.9,alpha:0.121},0).wait(1).to({y:12.9,alpha:0.16},0).wait(1).to({y:14.1,alpha:0.204},0).wait(1).to({y:15.5,alpha:0.256},0).wait(1).to({y:17.1,alpha:0.314},0).wait(1).to({y:18.8,alpha:0.378},0).wait(1).to({y:20.7,alpha:0.446},0).wait(1).to({y:22.6,alpha:0.517},0).wait(1).to({y:24.5,alpha:0.587},0).wait(1).to({y:26.4,alpha:0.655},0).wait(1).to({y:28.1,alpha:0.717},0).wait(1).to({y:29.6,alpha:0.774},0).wait(1).to({y:30.9,alpha:0.823},0).wait(1).to({y:32.1,alpha:0.865},0).wait(1).to({y:33,alpha:0.901},0).wait(1).to({y:33.8,alpha:0.93},0).wait(1).to({y:34.5,alpha:0.953},0).wait(1).to({y:35,alpha:0.971},0).wait(1).to({y:35.3,alpha:0.984},0).wait(1).to({y:35.6,alpha:0.993},0).wait(1).to({y:35.7,alpha:0.998},0).wait(1).to({regX:0,regY:0,x:274.4,y:35.6,alpha:1},0).wait(48));

	// 01 mascara5 (mask)
	var mask_6 = new cjs.Shape();
	mask_6._off = true;
	mask_6.graphics.p("A+EZoMAAAgwUMBEkAAAMAAAAwUg");
	mask_6.setTransform(246.5,164);

	// 00 carte2
	this.instance_22 = new lib._00carte2();
	this.instance_22.setTransform(-162.5,327.5);

	this.instance_22.mask = mask_6;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(1).to({x:-161.9},0).wait(1).to({x:-160},0).wait(1).to({x:-156.6},0).wait(1).to({x:-151.7},0).wait(1).to({x:-145.1},0).wait(1).to({x:-136.5},0).wait(1).to({x:-125.8},0).wait(1).to({x:-112.8},0).wait(1).to({x:-97.3},0).wait(1).to({x:-79.2},0).wait(1).to({x:-58.2},0).wait(1).to({x:-34.4},0).wait(1).to({x:-8},0).wait(1).to({x:20.5},0).wait(1).to({x:50.5},0).wait(1).to({x:80.9},0).wait(1).to({x:110.7},0).wait(1).to({x:138.8},0).wait(1).to({x:164.5},0).wait(1).to({x:187.3},0).wait(1).to({x:207},0).wait(1).to({x:223.6},0).wait(1).to({x:237.3},0).wait(1).to({x:248.3},0).wait(1).to({x:256.8},0).wait(1).to({x:263.1},0).wait(1).to({x:267.3},0).wait(1).to({x:269.7},0).wait(1).to({x:270.5},0).wait(57));

	// 01 mascara4 (mask)
	var mask_7 = new cjs.Shape();
	mask_7._off = true;
	mask_7.graphics.p("A+EZoMAAAgwUMBEkAAAMAAAAwUg");
	mask_7.setTransform(246.5,164);

	// 00 carte1
	this.instance_23 = new lib._00carte1();
	this.instance_23.setTransform(54.5,459);

	this.instance_23.mask = mask_7;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(1).to({y:458.6},0).wait(1).to({y:457.5},0).wait(1).to({y:455.4},0).wait(1).to({y:452.5},0).wait(1).to({y:448.4},0).wait(1).to({y:443.3},0).wait(1).to({y:436.8},0).wait(1).to({y:428.9},0).wait(1).to({y:419.6},0).wait(1).to({y:408.6},0).wait(1).to({y:395.9},0).wait(1).to({y:381.5},0).wait(1).to({y:365.5},0).wait(1).to({y:348.2},0).wait(1).to({y:330.1},0).wait(1).to({y:311.7},0).wait(1).to({y:293.7},0).wait(1).to({y:276.7},0).wait(1).to({y:261.1},0).wait(1).to({y:247.3},0).wait(1).to({y:235.4},0).wait(1).to({y:225.4},0).wait(1).to({y:217.1},0).wait(1).to({y:210.4},0).wait(1).to({y:205.3},0).wait(1).to({y:201.5},0).wait(1).to({y:198.9},0).wait(1).to({y:197.5},0).wait(1).to({y:197},0).wait(57));

	// 00 100
	this.instance_24 = new lib._00100();
	this.instance_24.setTransform(33.8,80,0.044,0.044);
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(8).to({_off:false},0).wait(1).to({regX:0.3,regY:-0.6,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09,x:33.9},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.13,scaleY:0.13,y:79.9},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.29,scaleY:0.29,y:79.8},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.41,scaleY:0.41},0).wait(1).to({scaleX:0.48,scaleY:0.48,x:34,y:79.7},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.67,scaleY:0.67,y:79.6},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.79,scaleY:0.79,x:34.1},0).wait(1).to({scaleX:0.83,scaleY:0.83,y:79.5},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97,y:79.4},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:33.8,y:80},0).wait(49));

	// 00 75
	this.instance_25 = new lib._0075();
	this.instance_25.setTransform(37.3,142.9,0.048,0.048);
	this.instance_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(6).to({_off:false},0).wait(1).to({regY:-0.5,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17,y:142.8},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.35,scaleY:0.35,y:142.7},0).wait(1).to({scaleX:0.41,scaleY:0.41},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.55,scaleY:0.55},0).wait(1).to({scaleX:0.61,scaleY:0.61,y:142.6},0).wait(1).to({scaleX:0.68,scaleY:0.68},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.79,scaleY:0.79,y:142.5},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96,y:142.4},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regY:0,scaleX:1,scaleY:1,y:142.9},0).wait(51));

	// 00 50
	this.instance_26 = new lib._0050();
	this.instance_26.setTransform(36.8,201.3,0.048,0.048);
	this.instance_26._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(4).to({_off:false},0).wait(1).to({regY:-0.6,scaleX:0.05,scaleY:0.05,y:201.2},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.25,scaleY:0.25,y:201.1},0).wait(1).to({scaleX:0.3,scaleY:0.3},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.41,scaleY:0.41,y:201},0).wait(1).to({scaleX:0.48,scaleY:0.48},0).wait(1).to({scaleX:0.55,scaleY:0.55,y:200.9},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.68,scaleY:0.68},0).wait(1).to({scaleX:0.73,scaleY:0.73,y:200.8},0).wait(1).to({scaleX:0.79,scaleY:0.79},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.91,scaleY:0.91,y:200.7},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regY:0,scaleX:1,scaleY:1,y:201.3},0).wait(53));

	// 00 25
	this.instance_27 = new lib._0025();
	this.instance_27.setTransform(37.1,261.6,0.044,0.044);
	this.instance_27._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(2).to({_off:false},0).wait(1).to({regY:-0.6,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.13,scaleY:0.13,y:261.5},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.29,scaleY:0.29,y:261.4},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.41,scaleY:0.41},0).wait(1).to({scaleX:0.48,scaleY:0.48,y:261.3},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.67,scaleY:0.67,y:261.2},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.79,scaleY:0.79},0).wait(1).to({scaleX:0.83,scaleY:0.83,y:261.1},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97,y:261},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regY:0,scaleX:1,scaleY:1,y:261.6},0).wait(55));

	// 00 0
	this.instance_28 = new lib._000();
	this.instance_28.setTransform(41,317.4,0.044,0.044);

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(1).to({regX:0.1,regY:-0.6,scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.13,scaleY:0.13,y:317.3},0).wait(1).to({scaleX:0.16,scaleY:0.16},0).wait(1).to({scaleX:0.2,scaleY:0.2},0).wait(1).to({scaleX:0.24,scaleY:0.24},0).wait(1).to({scaleX:0.29,scaleY:0.29,y:317.2},0).wait(1).to({scaleX:0.35,scaleY:0.35},0).wait(1).to({scaleX:0.41,scaleY:0.41},0).wait(1).to({scaleX:0.48,scaleY:0.48,y:317.1},0).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.67,scaleY:0.67,y:317},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.79,scaleY:0.79,x:41.1},0).wait(1).to({scaleX:0.83,scaleY:0.83,y:316.9},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.91,scaleY:0.91},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.96,scaleY:0.96},0).wait(1).to({scaleX:0.97,scaleY:0.97,y:316.8},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:41,y:317.4},0).wait(57));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(295.7,556.9,14.4,11.2);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;