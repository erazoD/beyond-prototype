(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 640,
	height: 570,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib._02seda = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8CD1E6").s().p("ACACvQgDgCAAgEQAAjCiAhXQgpgbgxgNQgYgGgRgCQgDAAgCgDQgCgCAAgEQAAgJAJABQATACAaAHQAyAOApAcQBOA0AiBYQAaBFAABWQAAAEgCACQgDADgDAAQgDAAgDgDg");
	this.shape.setTransform(0,-10.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C9EBE").s().p("AgpAqQgRgRAAgZQAAgXARgRQASgSAXAAQAYAAASASQARARAAAXQAAAZgRARQgSARgYAAQgXAAgSgRg");
	this.shape_1.setTransform(0,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#8CD1E6").s().p("AhkBmQgrgqAAg8QAAg6ArgrQAqgqA6AAQA7AAArAqQAqArAAA6QAAA8gqAqQgrAqg7AAQg6AAgqgqg");
	this.shape_2.setTransform(0,6.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ah6DWIhPhNIAAkRIBPhOID2AAIBOBOIAAERIhOBNg");
	this.shape_3.setTransform(0,6.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-20.2,-28.1,40.5,56.3);


(lib._02franjaoscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AiyRLMAAAgiVIFlAAMAAAAiVg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-110,36,220);


(lib._02franjaclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AiyWAMAAAgr/IFlAAMAAAAr/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-140.8,35.9,281.7);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBgAABEBEQBDBEAABeQAABfhDBEQhEBEhgAAQheAAhEhEg");
	this.shape.setTransform(26.5,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3.4,0,46.3,46.3);


(lib.Path_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBfAABEBEQBEBEAABeQAABfhEBEQhEBEhfAAQheAAhEhEg");
	this.shape_1.setTransform(23.2,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,46.3,46.4);


(lib._02cepillo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8CD1E6").s().p("AgmBwIAAinIA1g3IAYAAIAAARIgRAAIgrAtIAACgg");
	this.shape.setTransform(-9,-18.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("Ag7FOIAApSIAvhJIBIAAIAAKbg");
	this.shape_1.setTransform(-8.6,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgFQADgEAHAAICZAAQAHAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_2.setTransform(5.5,8.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgFAFgDQADgFAHAAICZAAQAHAAAEAFQAEADAAAFQAAAFgEAFQgEAEgHAAg");
	this.shape_3.setTransform(5.5,0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_4.setTransform(5.5,-6.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_5.setTransform(5.5,-13.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgHAAg");
	this.shape_6.setTransform(5.5,-20.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgFAFgEQADgEAHAAICZAAQAHAAAEAEQAEAEAAAFQAAAFgEAFQgEAEgHAAg");
	this.shape_7.setTransform(5.5,11.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgHAAg");
	this.shape_8.setTransform(5.5,4.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgFQADgEAHAAICZAAQAHAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_9.setTransform(5.5,-2.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgFAAgFQAAgEAFgEQADgFAHAAICZAAQAHAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgHAAg");
	this.shape_10.setTransform(5.5,-10);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgEAFgFQADgEAHAAICZAAQAHAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgHAAg");
	this.shape_11.setTransform(5.5,-17.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhMAOQgHAAgDgEQgFgEAAgGQAAgFAFgEQADgEAHAAICZAAQAHAAAEAEQAEAEAAAFQAAAGgEAEQgEAEgHAAg");
	this.shape_12.setTransform(5.5,-24.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.6,-33.5,29.4,67);


(lib._02 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPA5IAAgoIgnAAIAAghIAnAAIAAgnIAfAAIAAAnIAnAAIAAAhIgnAAIAAAog");
	this.shape.setTransform(0.1,-1.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.3,-21.1,17,42.5);


(lib._01franjaoscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AiyTdMAAAgm5IFlAAMAAAAm5g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-124.5,35.9,249.1);


(lib._01franjaclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AiyY1MAAAgxpIFlAAMAAAAxpg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.9,-158.9,35.9,317.8);


(lib.Path_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBgAABEBEQBDBEAABeQAABfhDBEQhEBEhgAAQheAAhEhEg");
	this.shape_2.setTransform(26.5,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3.4,0,46.3,46.3);


(lib.Path_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7BD7F8").s().p("AiiCjQhEhEAAhfQAAheBEhEQBEhEBeAAQBfAABEBEQBEBEAABeQAABfhEBEQhEBEhfAAQheAAhEhEg");
	this.shape_3.setTransform(23.2,23.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,46.3,46.4);


(lib._01cepillo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8CD1E6").s().p("AgmBwIAAioIA1g2IAYAAIAAARIgRAAIgrAtIAACgg");
	this.shape.setTransform(-9.3,-18.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("Ag7FOIAApQIArhLIBLAAIAAKbg");
	this.shape_1.setTransform(-8.4,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgFAFgDQAEgFAGAAICaAAQAGAAAEAFQAEADAAAFQAAAFgEAFQgEAEgGAAg");
	this.shape_2.setTransform(5.2,8.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_3.setTransform(5.2,1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_4.setTransform(5.2,-6.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_5.setTransform(5.2,-13.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_6.setTransform(5.2,-20.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_7.setTransform(5.2,11.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_8.setTransform(5.2,4.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_9.setTransform(5.2,-2.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgFAAgFQAAgEAFgEQAEgFAGAAICaAAQAGAAAEAFQAEAEAAAEQAAAFgEAFQgEAEgGAAg");
	this.shape_10.setTransform(5.2,-9.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_11.setTransform(5.2,-17);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhMAOQgGAAgEgEQgFgEAAgGQAAgEAFgFQAEgEAGAAICaAAQAGAAAEAEQAEAFAAAEQAAAGgEAEQgEAEgGAAg");
	this.shape_12.setTransform(5.2,-24.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.4,-33.5,28.9,67);


(lib._00indicedeplaca = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AAJBBIgCgUIgQAAIgCAUIgbAAIAWiBIAhAAIAXCBgAgFAXIAKAAIgFgug");
	this.shape.setTransform(47,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AgMBBQgHgCgFgFQgGgFgCgGQgEgHAAgIIAAg/QAAgIAEgGQACgHAGgFQAFgFAHgCQAHgDAHAAQAHAAAHADQAGACAFAEQAEAFADAHQADAGAAAHIAAARIgbAAIAAgRQAAgEgDgBQgCgDgDAAQgCAAgDADQgCABAAAEIAABDQAAAEACACQADACACAAQADAAACgCQADgCAAgEIAAgRIAbAAIAAARQAAAHgDAGQgDAHgEAFQgFAEgGACQgHADgHAAQgHAAgHgDg");
	this.shape_1.setTransform(39.1,-0.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7BD7F8").s().p("AAJBBIgBgUIgQAAIgDAUIgbAAIAWiBIAhAAIAWCBgAgFAXIAKAAIgFgug");
	this.shape_2.setTransform(31.1,-0.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7BD7F8").s().p("AgeBBIAAiBIAeAAIAABnIAfAAIAAAag");
	this.shape_3.setTransform(23.8,-0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7BD7F8").s().p("AgiBBIAAiBIAoAAQAHAAAFACQAFACAEAEQADADADAGQACAFAAAHIAAAaQAAAGgCAEQgDAFgDAEQgEAEgFACQgFABgHABIgKAAIAAAvgAgEgCIAGAAQAFgBAAgFIAAgaQAAgGgFAAIgGAAg");
	this.shape_4.setTransform(16.2,-0.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7BD7F8").s().p("AgeBBIAAiBIA+AAIAAAaIggAAIAAAbIATAAIAAAUIgTAAIAAAdIAgAAIAAAbg");
	this.shape_5.setTransform(5.6,-0.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#7BD7F8").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_6.setTransform(-2.5,-0.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#7BD7F8").s().p("AgeBBIAAiBIA+AAIAAAaIghAAIAAAbIAVAAIAAAUIgVAAIAAAdIAhAAIAAAbg");
	this.shape_7.setTransform(-13.2,-0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#7BD7F8").s().p("AgMBBQgIgCgFgFQgFgFgDgGQgCgHAAgIIAAg/QAAgIACgGQADgHAFgFQAFgFAIgCQAHgDAGAAQAJAAAGADQAGACAFAEQAEAFADAHQADAGgBAHIAAARIgaAAIAAgRQAAgEgCgBQgCgDgFAAQgCAAgCADQgCABAAAEIAABDQAAAEACACQACACACAAQAFAAACgCQACgCAAgEIAAgRIAaAAIAAARQABAHgDAGQgDAHgEAFQgFAEgGACQgGADgJAAQgGAAgHgDg");
	this.shape_8.setTransform(-21.1,-0.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#7BD7F8").s().p("AgNBBIAAiBIAbAAIAACBg");
	this.shape_9.setTransform(-27.5,-0.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#7BD7F8").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_10.setTransform(-33.9,-0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#7BD7F8").s().p("AANBBIgQgwIgIgRIAABBIgaAAIAAiBIAaAAIAQAxIAHAQIAAhBIAaAAIAACBg");
	this.shape_11.setTransform(-42.7,-0.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#7BD7F8").s().p("AgRBSIAAiBIAcAAIAACBgAgJg2IAHgbIAUAAIgQAbg");
	this.shape_12.setTransform(-49,-2.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.4,-11.6,106.7,23.7);


(lib._00indicedegingivitis = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgOBBQgGgCgFgEQgFgFgDgGQgCgGAAgIIAAgKIAaAAIAAAKQAAAEADADQACADAEAAQADAAACgDQACgCAAgFQAAgHgFgGIgKgMIgJgJIgJgHIgGgLQgDgFABgHQgBgHADgHQADgHAEgFQAFgFAGgDQAIgDAHAAQAHAAAGADQAHACAEAFQAEAEADAGQADAHAAAHIAAAIIgbAAIAAgJQAAgEgCgCQgCgDgDAAQgCAAgCADQgCACgBAEQABAEABADIAEAGIADAGIAHAGIAKAJIAIAHIAGALQACAGAAAHQAAAIgCAGQgDAHgEAFQgGAFgGADQgHADgIAAQgHAAgHgDg");
	this.shape.setTransform(94.8,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgOBBIAAiBIAdAAIAACBg");
	this.shape_1.setTransform(88.7,-0.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOBBIAAhnIgVAAIAAgaIBHAAIAAAaIgVAAIAABng");
	this.shape_2.setTransform(82.7,-0.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgNBBIAAiBIAbAAIAACBg");
	this.shape_3.setTransform(76.8,-0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#256379").s().p("AgNBBIgaiBIAeAAIAJBOIAMhOIAcAAIgaCBg");
	this.shape_4.setTransform(70.6,-0.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#256379").s().p("AgNBBIAAiBIAbAAIAACBg");
	this.shape_5.setTransform(64.4,-0.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#256379").s().p("AgRBBQgGgCgFgFQgEgEgDgGQgCgGAAgHIAAhFQABgGACgGQADgGAFgFQAFgEAHgDQAHgDAHAAQAIAAAGADQAIADAEAEQAFAFADAGQADAFAAAHIAAATIgeAAIAAgTQAAgDgBgCQgCgCgEAAQgCAAgCACQgCACAAADIAABFQAAADADACQACACABAAQAEAAACgCQACgCAAgDIAAgQIgIAAIAAgTIAmAAIAABBIgRAAIgEgLQgDAHgHADQgGAEgEAAQgIAAgGgDg");
	this.shape_6.setTransform(58,-0.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#256379").s().p("AANBBIgQgwIgIgRIAABBIgaAAIAAiBIAaAAIAQAxIAHAQIAAhBIAaAAIAACBg");
	this.shape_7.setTransform(49.2,-0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#256379").s().p("AgNBBIAAiBIAbAAIAACBg");
	this.shape_8.setTransform(42.6,-0.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#256379").s().p("AgRBBQgHgCgEgFQgEgEgDgGQgCgGAAgHIAAhFQABgGACgGQADgGAFgFQAFgEAHgDQAHgDAHAAQAIAAAHADQAGADAFAEQAFAFADAGQACAFAAAHIAAATIgdAAIAAgTQABgDgCgCQgCgCgEAAQgCAAgCACQgCACAAADIAABFQAAADADACQACACABAAQAEAAACgCQADgCgBgDIAAgQIgIAAIAAgTIAlAAIAABBIgQAAIgEgLQgDAHgHADQgFAEgFAAQgIAAgGgDg");
	this.shape_9.setTransform(36.2,-0.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#256379").s().p("AgeBBIAAiBIA+AAIAAAaIghAAIAAAbIAVAAIAAAUIgVAAIAAAdIAhAAIAAAbg");
	this.shape_10.setTransform(25.5,-0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#256379").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_11.setTransform(17.4,-0.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#256379").s().p("AgNBBQgHgCgFgFQgGgEgCgHQgDgGAAgIIAAhCQAAgIADgGQACgGAGgEQAFgFAHgCQAGgDAHAAQAHAAAHADQAHACAFAFQAFAEADAGQADAGAAAIIAABCQAAAIgDAGQgDAHgFAEQgFAFgHACQgHADgHAAQgHAAgGgDgAgDgoQgDACAAADIAABGQAAADADACQACACABAAQAAAAABAAQABAAAAgBQABAAAAAAQABAAAAgBQACgCAAgDIAAhGQAAgDgCgCQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAgCABg");
	this.shape_12.setTransform(6,-0.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#256379").s().p("AgOBBIAAiBIAdAAIAACBg");
	this.shape_13.setTransform(-0.4,-0.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#256379").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_14.setTransform(-6.8,-0.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#256379").s().p("AgeBBIAAiBIA+AAIAAAaIggAAIAAAbIATAAIAAAUIgTAAIAAAdIAgAAIAAAbg");
	this.shape_15.setTransform(-14.7,-0.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#256379").s().p("AAWBBIAAhWIgFAeIgKA4IgRAAIgJg4IgFgeIAABWIgaAAIAAiBIAnAAIAKAzIABAVIADgVIAJgzIAnAAIAACBg");
	this.shape_16.setTransform(-24.4,-0.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#256379").s().p("AgNBBQgHgCgFgFQgGgEgCgHQgDgGAAgIIAAhCQAAgIADgGQACgGAGgEQAFgFAHgCQAGgDAHAAQAHAAAHADQAHACAFAFQAFAEADAGQADAGAAAIIAABCQAAAIgDAGQgDAHgFAEQgFAFgHACQgHADgHAAQgHAAgGgDgAgDgoQgDACAAADIAABGQAAADADACQACACABAAQAAAAABAAQABAAAAgBQABAAAAAAQABAAAAgBQACgCAAgDIAAhGQAAgDgCgCQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAgCABg");
	this.shape_17.setTransform(-34.5,-0.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#256379").s().p("AAMBBQAAAAgBAAQAAAAgBAAQAAgBAAAAQgBAAAAgBIgCgFIgBgGIgBgHIAAgYIgBgFQgCgCgCAAIgGAAIAAAzIgeAAIAAiBIAoAAQAFAAAGACQAFACAEADQAEAEADAFQACAFAAAHIAAAWQAAAGgDAEQgDAEgGACQAGADADAEQAEAFAAAFIAAAYIABALIACAKIAAABgAgGgHIAGAAQAEABAAgHIAAgVQAAgGgEAAIgGAAg");
	this.shape_18.setTransform(-42.7,-0.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#256379").s().p("AgiBBIAAiBIApAAQAGAAAFACQAFACAEAEQAEADACAGQACAFAAAHIAAAaQAAAGgCAEQgCAFgEAEQgEAEgFACQgFABgGABIgLAAIAAAvgAgEgCIAFAAQAGgBAAgFIAAgaQAAgGgGAAIgFAAg");
	this.shape_19.setTransform(-51,-0.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#256379").s().p("AgfBBIAAiBIA+AAIAAAaIggAAIAAAbIAVAAIAAAUIgVAAIAAAdIAgAAIAAAbg");
	this.shape_20.setTransform(-61.6,-0.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#256379").s().p("AgMBBQgHgCgGgFQgFgFgDgGQgDgHAAgIIAAg/QAAgIADgGQADgHAFgFQAGgFAHgCQAHgDAGAAQAIAAAHADQAGACAFAEQAEAFADAHQACAGAAAHIAAARIgaAAIAAgRQAAgEgDgBQgBgDgFAAQgCAAgCADQgCABAAAEIAABDQAAAEACACQACACACAAQAFAAABgCQADgCAAgEIAAgRIAaAAIAAARQAAAHgCAGQgDAHgEAFQgFAEgGACQgHADgIAAQgGAAgHgDg");
	this.shape_21.setTransform(-69.5,-0.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#256379").s().p("AgOBBIAAiBIAcAAIAACBg");
	this.shape_22.setTransform(-75.9,-0.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#256379").s().p("AgkBBIAAiBIAkAAQAJAAAGACQAHADAFAFQAFAFACAGQADAGAAAJIAAA8QAAAIgDAGQgCAGgFAEQgFAEgHADQgGACgJAAgAgGAoIAGAAQACAAACgCQADgCAAgGIAAg7QAAgFgDgCQgCgDgCAAIgGAAg");
	this.shape_23.setTransform(-82.3,-0.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#256379").s().p("AANBBIgQgwIgIgRIAABBIgaAAIAAiBIAaAAIAQAxIAHAQIAAhBIAaAAIAACBg");
	this.shape_24.setTransform(-91.1,-0.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#256379").s().p("AgRBSIAAiBIAcAAIAACBgAgJg2IAHgbIAUAAIgQAbg");
	this.shape_25.setTransform(-97.4,-2.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101.8,-11.6,202.8,23.7);


(lib._00flecha2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgZgdIAzAdIgzAeg");
	this.shape.setTransform(279.3,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgOAEIAAgHIAcAAIAAAHg");
	this.shape_1.setTransform(274.7,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("EApBAAEIAAgHIBFAAIAAAHgEAm0AAEIAAgHIBHAAIAAAHgEAkoAAEIAAgHIBGAAIAAAHgEAidAAEIAAgHIBGAAIAAAHgEAgQAAEIAAgHIBGAAIAAAHgAeFAEIAAgHIBGAAIAAAHgAb4AEIAAgHIBGAAIAAAHgAZtAEIAAgHIBFAAIAAAHgAXgAEIAAgHIBHAAIAAAHgAVVAEIAAgHIBFAAIAAAHgATJAEIAAgHIBGAAIAAAHgAQ8AEIAAgHIBGAAIAAAHgAOxAEIAAgHIBGAAIAAAHgAMkAEIAAgHIBGAAIAAAHgAKZAEIAAgHIBGAAIAAAHgAIMAEIAAgHIBGAAIAAAHgAGBAEIAAgHIBFAAIAAAHgAD0AEIAAgHIBHAAIAAAHgABpAEIAAgHIBFAAIAAAHgAgiAEIAAgHIBFAAIAAAHgAiuAEIAAgHIBGAAIAAAHgAk5AEIAAgHIBGAAIAAAHgAnGAEIAAgHIBGAAIAAAHgApRAEIAAgHIBGAAIAAAHgAreAEIAAgHIBGAAIAAAHgAtpAEIAAgHIBFAAIAAAHgAv2AEIAAgHIBHAAIAAAHgAyBAEIAAgHIBFAAIAAAHgA0OAEIAAgHIBHAAIAAAHgA2aAEIAAgHIBGAAIAAAHgA4lAEIAAgHIBGAAIAAAHgA6yAEIAAgHIBGAAIAAAHgA89AEIAAgHIBGAAIAAAHgA/KAEIAAgHIBGAAIAAAHgEghVAAEIAAgHIBFAAIAAAHgEgjiAAEIAAgHIBHAAIAAAHgEgltAAEIAAgHIBFAAIAAAHgEgn6AAEIAAgHIBHAAIAAAHgEgqGAAEIAAgHIBGAAIAAAHg");
	this.shape_2.setTransform(-2.3,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("AgOAEIAAgHIAdAAIAAAHg");
	this.shape_3.setTransform(-280.3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-281.8,-3,563.8,6.1);


(lib._00flecha1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AgZgdIAzAdIgzAeg");
	this.shape.setTransform(279.3,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7BD7F8").s().p("AgOAFIAAgIIAcAAIAAAIg");
	this.shape_1.setTransform(274.7,0.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7BD7F8").s().p("EApBAAFIAAgIIBFAAIAAAIgEAm0AAFIAAgIIBHAAIAAAIgEAkoAAFIAAgIIBGAAIAAAIgEAidAAFIAAgIIBGAAIAAAIgEAgQAAFIAAgIIBGAAIAAAIgAeFAFIAAgIIBGAAIAAAIgAb4AFIAAgIIBGAAIAAAIgAZtAFIAAgIIBFAAIAAAIgAXgAFIAAgIIBHAAIAAAIgAVVAFIAAgIIBFAAIAAAIgATJAFIAAgIIBGAAIAAAIgAQ8AFIAAgIIBGAAIAAAIgAOxAFIAAgIIBGAAIAAAIgAMkAFIAAgIIBGAAIAAAIgAKZAFIAAgIIBGAAIAAAIgAIMAFIAAgIIBGAAIAAAIgAGBAFIAAgIIBFAAIAAAIgAD0AFIAAgIIBHAAIAAAIgABpAFIAAgIIBFAAIAAAIgAgiAFIAAgIIBFAAIAAAIgAiuAFIAAgIIBGAAIAAAIgAk5AFIAAgIIBGAAIAAAIgAnGAFIAAgIIBGAAIAAAIgApRAFIAAgIIBGAAIAAAIgAreAFIAAgIIBGAAIAAAIgAtpAFIAAgIIBFAAIAAAIgAv2AFIAAgIIBHAAIAAAIgAyBAFIAAgIIBFAAIAAAIgA0OAFIAAgIIBHAAIAAAIgA2aAFIAAgIIBGAAIAAAIgA4lAFIAAgIIBGAAIAAAIgA6yAFIAAgIIBGAAIAAAIgA89AFIAAgIIBGAAIAAAIgA/KAFIAAgIIBGAAIAAAIgEghVAAFIAAgIIBFAAIAAAIgEgjiAAFIAAgIIBHAAIAAAIgEgltAAFIAAgIIBFAAIAAAIgEgn6AAFIAAgIIBHAAIAAAIgEgqGAAFIAAgIIBGAAIAAAIg");
	this.shape_2.setTransform(-2.3,0.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7BD7F8").s().p("AgOAFIAAgIIAdAAIAAAIg");
	this.shape_3.setTransform(-280.3,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-281.8,-3,563.8,6.2);


(lib._00circulo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AhZBaQglgmAAg0QAAgzAlgmQAmglAzAAQA0AAAmAlQAlAmAAAzQAAA0glAmQgmAlg0AAQgzAAgmglg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.7,-12.7,25.5,25.5);


(lib._00circulo1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7BD7F8").s().p("AhZBaQglgmAAg0QAAgzAlgmQAmglAzAAQA0AAAmAlQAlAmAAAzQAAA0glAmQgmAlg0AAQgzAAgmglg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.7,-12.7,25.5,25.5);


(lib._00carte2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgEd/MAAAg79IAJAAMAAAA79g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-192,1,384);


(lib._00carte1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgsCAAFIAAgJMBYFAAAIAAAJg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-282,-0.5,564,1);


(lib._0030 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgNBBQgGgCgFgFQgFgEgDgGQgCgHAAgHIAAhDQAAgIACgGQADgGAFgFQAFgEAGgCQAGgDAHAAQAIAAAGADQAHACAEAEQAFAFACAGQACAGABAIIAABDQgBAHgCAHQgCAGgFAEQgEAFgHACQgGADgIAAQgHAAgGgDgAgEgnQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABIAABGQAAADACACQACACACAAQADAAACgCQABgCABgDIAAhGQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgDAAQgCAAgCACg");
	this.shape.setTransform(5.5,-0.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAUAAIAAAXQAAAFgBAEQgBADgDADIgGAFQgEACgFAAg");
	this.shape_1.setTransform(-0.2,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgOBBQgGgCgEgFQgEgEgCgGQgDgGABgFIAAgNIAZAAIAAAKQAAAFACACQACACADAAQACAAACgCQACgCAAgEIAAgUQABgGgGAAIgIAAIAAgTIAIAAQAGAAgBgFIAAgTQAAgDgCgCQgCgCgCAAQgHgBAAAJIAAALIgZAAIAAgOQgBgFADgGQACgFAEgFQAEgEAGgDQAGgDAIAAQAGAAAGACQAHADAEAEQAFAFADAFQACAGAAAHIAAAOQAAAIgEAGQgDAGgIABQAHABAEAFQAEAFAAAKIAAAPQAAAHgCAGQgDAGgFAEQgEAFgHACQgGACgGAAQgIAAgGgDg");
	this.shape_2.setTransform(-5.7,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.5,-11.7,23.4,23.7);


(lib._00273 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgOBBQgGgDgEgEQgEgEgCgGQgCgGgBgFIAAgNIAaAAIAAAKQAAAFACABQACADADAAQACAAADgDQACgBAAgEIAAgUQgBgFgFgBIgIAAIAAgSIAIAAQAFAAABgGIAAgTQAAgEgCgCQgDgCgCAAQgHAAAAAJIAAAKIgaAAIAAgMQABgGACgGQACgFAEgFQAEgEAGgDQAGgDAIAAQAGAAAGADQAHACAFAEQAFAEACAGQACAGABAHIAAANQAAAJgFAGQgEAGgHABQAHAAAEAHQAFAFAAAIIAAAQQgBAHgCAGQgCAGgFAEQgFAEgHACQgGADgGAAQgIAAgGgDg");
	this.shape.setTransform(8.6,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgXBBQAAgOADgPQACgPAFgNQAEgMAFgMQAEgNAGgIIgnAAIAAgbIBDAAIAAAMQgIAOgFAOQgFAPgDAMQgEAPgCAPIgCAgg");
	this.shape_1.setTransform(1.1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQgBAFgBAEQgCADgDADIgFAFQgDACgGAAg");
	this.shape_2.setTransform(-4.4,5.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AghBDIAAgHIABgPIAFgNQADgHAFgIIAMgRIAHgIIAFgJIACgHIABgHQAAgFgCgCQgCgCgEAAQgCAAgCACQgCADAAAEIAAAMIgZAAIAAgPQAAgGACgGQACgFAEgEQAEgFAGgDQAGgCAHAAQAJAAAGACQAHADAEAFQAEAFACAGQACAGAAAIIgBALIgEAKQgCAHgFADIgMAOIgHAJIgDAHIgBAGIAAAFIAiAAIAAAZg");
	this.shape_3.setTransform(-10,-0.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7BD7F8").s().p("Ah+B/Qg1g1AAhKQAAhJA1g1QA1g1BJAAQBKAAA1A1QA1A1AABJQAABKg1A1Qg1A1hKAAQhJAAg1g1g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18,-18,36.1,36.1);


(lib._0025 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgOBAQgGgDgEgEQgEgFgCgFQgDgGAAgFIAAgNIAaAAIAAAJQAAAFACACQACADADgBQADABACgDQACgCAAgEIAAgdQAAgDgCgBQgDgBgCAAQAAAAAAAAQAAAAgBAAQAAABgBAAQgBAAAAAAQgDABAAACIgYgDIAEhCIA7AAQgBAOgEAFQgDAHgHAAIgWAAIgCAbQAEgEADgDQAFgCAHAAQAJAAAFAHQADADACAEQABAEABAEIAAAiQgBAHgCAHQgDAFgEAFQgFAEgGACQgHACgGAAQgIAAgGgCg");
	this.shape.setTransform(5.5,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQgBAFgBAEQgCADgDADIgFAFQgDACgGAAg");
	this.shape_1.setTransform(0.1,5.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AghBDIAAgHIABgPIAFgNQADgHAFgIIAMgRIAHgIIAFgJIACgHIABgHQAAgFgCgCQgCgCgEAAQgCAAgCACQgCADAAAEIAAAMIgZAAIAAgPQAAgGACgGQACgFAEgEQAEgFAGgDQAGgCAHAAQAJAAAGACQAHADAEAFQAEAFACAGQACAGAAAIIgBALIgEAKQgCAHgFADIgMAOIgHAJIgDAHIgBAGIAAAFIAiAAIAAAZg");
	this.shape_2.setTransform(-5.5,-0.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.3,-11.7,22.9,23.7);


(lib._0021 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMBBIAAh1QAGAAADgDQADgEAAgFIANAAIAACBg");
	this.shape.setTransform(5.1,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQAAAFgCAEQgCADgCADIgGAFQgDACgGAAg");
	this.shape_1.setTransform(0.9,5.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AghBDIAAgHIABgPIAFgNQADgHAFgIIAMgRIAHgIIAFgJIACgHIABgHQAAgFgCgCQgCgCgEAAQgCAAgCACQgCADAAAEIAAAMIgZAAIAAgPQAAgGACgGQACgFAEgEQAEgFAGgDQAGgCAHAAQAJAAAGACQAHADAEAFQAEAFACAGQACAGAAAIIgBALIgEAKQgCAHgFADIgMAOIgHAJIgDAHIgBAGIAAAFIAiAAIAAAZg");
	this.shape_2.setTransform(-4.7,-0.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#256379").s().p("Ah+B/Qg1g1AAhKQAAhKA1g0QA1g1BJAAQBKAAA1A1QA1A0AABKQAABKg1A1Qg1A1hKAAQhJAAg1g1g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18,-18,36.1,36.1);


(lib._0015 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgOA/QgGgCgEgFQgEgEgCgGQgDgFAAgGIAAgMIAaAAIAAAKQAAAEACACQACADADAAQACAAADgDQABgCAAgDIAAgeQAAgDgBgBQgDgBgCAAQAAAAAAAAQAAAAgBAAQAAABgBAAQgBAAAAAAQgDABAAACIgYgDIADhBIA8AAQgBAMgEAHQgDAFgHAAIgWAAIgCAcQAEgFADgCQAFgCAHAAQAJAAAFAGQAEADABAFQABAEABAEIAAAjQgBAGgCAGQgDAHgEADQgFAFgGACQgHACgGAAQgIAAgGgDg");
	this.shape.setTransform(4.3,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQgBAFgBAEQgCADgDADIgFAFQgDACgGAAg");
	this.shape_1.setTransform(-1.2,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgMBBIAAh1QAGAAADgDQADgEAAgFIANAAIAACBg");
	this.shape_2.setTransform(-5.4,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10,-11.7,20.3,23.7);


(lib._0010 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgNBBQgGgCgFgFQgFgEgCgGQgDgHAAgHIAAhDQAAgIADgGQACgGAFgFQAFgEAGgCQAGgDAHAAQAIAAAGADQAGACAFAEQAEAFADAGQADAGgBAIIAABDQABAHgDAHQgDAGgEAEQgFAFgGACQgGADgIAAQgHAAgGgDgAgEgnQgBAAAAABQAAAAAAABQgBAAAAABQAAAAAAABIAABGQAAADACACQABACADAAQADAAACgCQABgCAAgDIAAhGQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgDAAQgDAAgBACg");
	this.shape.setTransform(4.3,-0.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQAAAFgCAEQgBADgDADIgGAFQgEACgFAAg");
	this.shape_1.setTransform(-1.5,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgMBBIAAh1QAGAAADgDQADgEAAgFIANAAIAACBg");
	this.shape_2.setTransform(-5.7,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.3,-11.7,20.9,23.7);


(lib._0005 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgOA/QgGgCgEgFQgEgEgCgGQgCgFgBgGIAAgMIAaAAIAAAKQAAAEACACQACADADAAQACAAACgDQADgCAAgDIAAgeQAAgDgDgBQgCgBgCAAQAAAAAAAAQAAAAgBAAQAAABgBAAQAAAAgBAAQgCABgBACIgYgDIAEhBIA7AAQgBAMgEAHQgDAFgHAAIgWAAIgCAcQAEgFADgCQAFgCAGAAQAKAAAGAGQACADACAFQACAEAAAEIAAAjQAAAGgDAGQgCAHgFADQgFAFgHACQgGACgGAAQgIAAgGgDg");
	this.shape.setTransform(5.8,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#256379").s().p("AgKARQAFAAADgDQACgDAAgHIgKAAIAAgaIAVAAIAAAXQAAAFgCAEQgBADgDADIgGAFQgEACgFAAg");
	this.shape_1.setTransform(0.3,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#256379").s().p("AgNBBQgHgCgEgFQgFgEgDgGQgCgHAAgHIAAhDQAAgIACgGQADgGAFgFQAEgEAHgCQAGgDAHAAQAIAAAGADQAHACAEAEQAFAFACAGQACAGABAIIAABDQgBAHgCAHQgCAGgFAEQgEAFgHACQgGADgIAAQgHAAgGgDgAgEgnQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABIAABGQAAADACACQACACACAAQADAAACgCQABgCABgDIAAhGQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgDAAQgCAAgCACg");
	this.shape_2.setTransform(-5.4,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.5,-11.7,23.4,23.7);


(lib._0000 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#256379").s().p("AgNBBQgGgCgFgFQgFgEgDgGQgCgHAAgHIAAhDQAAgIACgGQADgGAFgFQAFgEAGgCQAGgDAHAAQAIAAAGADQAHACAEAEQAFAFACAGQACAGAAAIIAABDQAAAHgCAHQgCAGgFAEQgEAFgHACQgGADgIAAQgHAAgGgDgAgEgnQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABIAABGQAAADACACQACACACAAQADAAACgCQABgCABgDIAAhGQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgDAAQgCAAgCACg");
	this.shape.setTransform(0.1,-0.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,-11.7,12.5,23.7);


(lib._02circulooscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRBWIAAiaQAJAAAEgFQADgFAAgGIASAAIAACqg");
	this.shape.setTransform(12.4,-0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRBWQgJgDgGgFQgHgGgDgIQgDgHAAgKIAAgVQAAgJADgHQAEgIAHgCQgHgDgEgHQgDgIAAgJIAAgTQAAgKADgHQADgIAHgGQAGgFAJgDQAIgDAJAAQAKAAAJADQAIADAGAFQAGAGAEAIQADAHAAAKIAAATQAAAJgEAIQgEAHgHADQAHACAEAIQAEAHAAAJIAAAVQAAAKgDAHQgDAIgHAGQgGAFgIADQgJADgKAAQgJAAgIgDgAgGAKQgDADAAAEIAAAdQAAAFADACQADADADAAQAEAAADgDQADgCAAgFIAAgdQAAgEgDgDQgDgCgEAAQgDAAgDACgAgGg1QgDADAAAFIAAAbQAAAEADADQADADADAAQAEAAADgDQADgDAAgEIAAgbQAAgFgDgDQgDgCgEAAQgDAAgDACg");
	this.shape_1.setTransform(3.5,-0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgDAEgEACQgGACgGAAg");
	this.shape_2.setTransform(-4,7.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgQBWIAAiaQAIAAADgFQAFgFAAgGIASAAIAACqg");
	this.shape_3.setTransform(-9.5,-0.8);

	this.instance = new lib.Path();
	this.instance.setTransform(1.7,-1.6,1,1,0,0,0,26.5,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.169)",-3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.4,-24.8,55,54);


(lib._02circuloclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgfBWQABgTADgTQAEgUAFgSQAGgQAHgQQAFgQAIgMIgzAAIAAgiIBXAAIAAAPQgJATgHATQgGASgFARQgFAUgCATQgCAUAAAXg");
	this.shape.setTransform(12,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgTBWQgIgEgFgGQgGgGgCgHQgDgHAAgIIAAgRIAhAAIAAAOQAAAFADADQADADAEAAQADAAADgDQACgCABgFIAAgaQAAgIgIAAIgKAAIAAgZIAKAAQAIAAAAgHIAAgZQgBgFgCgDQgDgCgDAAQgKAAAAALIAAAOIghAAIAAgRQAAgIADgHQACgIAGgFQAFgGAIgEQAIgDALAAQAIAAAJADQAIADAHAFQAGAGADAIQADAHAAAKIAAARQAAALgFAIQgGAIgJACQAJABAFAHQAGAHAAAMIAAAUQAAAKgDAHQgDAIgGAGQgHAFgIADQgJADgIAAQgLAAgIgDg");
	this.shape_1.setTransform(1.8,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgDAEgEACQgGACgGAAg");
	this.shape_2.setTransform(-5.3,7.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgsBXIAAgJQAAgKACgIQACgJAEgJQAEgJAGgKIARgXIAJgLIAGgLIAEgKIABgKQAAgGgDgDQgDgDgFAAQgDAAgCAEQgDADAAAFIAAARIghAAIAAgUQAAgIADgHQACgIAGgFQAFgGAIgEQAIgDAJAAQAMAAAJADQAIAEAGAGQAFAGACAJQADAIAAAJIgCAPQgBAHgDAHQgEAIgGAGIgPARIgJANIgFAJIgCAHIAAAHIAuAAIAAAgg");
	this.shape_3.setTransform(-12.6,-1.1);

	this.instance = new lib.Path_1();
	this.instance.setTransform(-1.4,-1.4,1,1,0,0,0,23.2,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.227)",3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.6,-24.6,54,54);


(lib._01circulooscuro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AABBWIAAgjIgwAAIAAgaIAyhtIAgAAIAABqIANAAIAAAdIgNAAIAAAjgAgQAWIARAAIAAgpg");
	this.shape.setTransform(14.3,-1.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRBWQgJgDgGgGQgGgGgDgIQgDgIAAgLIAAhYQAAgKADgIQADgIAGgGQAGgGAJgDQAIgDAJAAQAKAAAJADQAIADAGAGQAGAGADAIQADAIAAAKIAABYQAAALgDAIQgDAIgGAGQgGAGgIADQgJADgKAAQgJAAgIgDgAgGg0QgCACAAAEIAABcQAAAEACADQADADADAAQAEAAADgDQACgDAAgEIAAhcQAAgEgCgCQgDgDgEAAQgDAAgDADg");
	this.shape_1.setTransform(3.7,-1.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgEAEgDACQgFACgHAAg");
	this.shape_2.setTransform(-3.8,6.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgsBXIAAgJQAAgKACgIQACgJAEgJQAEgJAGgKIARgXIAJgLIAGgLIAEgKIABgKQAAgGgDgDQgDgDgFAAQgDAAgCAEQgDADAAAFIAAARIghAAIAAgUQAAgIADgHQACgIAGgFQAFgGAIgEQAIgDAJAAQAMAAAJADQAIAEAGAGQAFAGACAJQADAIAAAJIgCAPQgBAHgDAHQgEAIgGAGIgPARIgJANIgFAJIgCAHIAAAHIAuAAIAAAgg");
	this.shape_3.setTransform(-11.1,-1.8);

	this.instance = new lib.Path_2();
	this.instance.setTransform(1.7,-1.6,1,1,0,0,0,26.5,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.169)",-3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.4,-24.8,55,54);


(lib._01circuloclaro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRBVIAAiZQAJAAAEgFQADgFAAgHIASAAIAACqg");
	this.shape.setTransform(10.8,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRBWQgJgDgGgGQgGgGgDgIQgDgJAAgKIAAhYQAAgKADgIQAEgIAFgGQAHgGAIgDQAJgDAIAAQAKAAAIADQAJAEAFAGQAFAFADAIQADAHAAAIIAAAQIggAAIAAgNQAAgGgDgDQgDgCgFAAQgDAAgDACQgCADAAAFIAAAoQADgFAFgEQAFgDAJAAQAPAAAIAIQAEAEACAFQACAEAAAHIAAAhQAAAKgDAJQgEAIgFAGQgGAGgJADQgJADgJAAQgJAAgIgDgAgGAJQgDACABAEIAAAgQgBAEADADQADACADAAQAEAAADgCQACgDAAgEIAAggQAAgEgCgCQgDgCgEAAQgDAAgDACg");
	this.shape_1.setTransform(2.2,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAWQAGAAAEgEQADgEAAgJIgNAAIAAgiIAbAAIAAAfQAAAGgCAFQgCAFgEAEQgDAEgEACQgGACgGAAg");
	this.shape_2.setTransform(-5.3,7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgsBXIAAgJQAAgKACgIQACgJAEgJQAEgJAGgKIARgXIAJgLIAGgLIAEgKIABgKQAAgGgDgDQgDgDgFAAQgDAAgCAEQgDADAAAFIAAARIghAAIAAgUQAAgIADgHQACgIAGgFQAFgGAIgEQAIgDAJAAQAMAAAJADQAIAEAGAGQAFAGACAJQADAIAAAJIgCAPQgBAHgDAHQgEAIgGAGIgPARIgJANIgFAJIgCAHIAAAHIAuAAIAAAgg");
	this.shape_3.setTransform(-12.6,-1.2);

	this.instance = new lib.Path_3();
	this.instance.setTransform(-1.4,-1.5,1,1,0,0,0,23.2,23.2);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,0.227)",3,3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.6,-24.7,54,54);


// stage content:
(lib.grafica_10_v1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_69 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(69).call(this.frame_69).wait(1));

	// 02 circulo oscuro
	this.instance = new lib._02circulooscuro();
	this.instance.setTransform(376.7,190.2,0.022,0.022);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.26,scaleY:0.26},0).wait(1).to({scaleX:0.31,scaleY:0.31},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.43,scaleY:0.43},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.55,scaleY:0.55},0).wait(1).to({scaleX:0.62,scaleY:0.62},0).wait(1).to({scaleX:0.68,scaleY:0.68},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(34));

	// 02 mascara (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_4 = new cjs.Graphics().p("EgffAgWMAAAg18MBNCAAAMAAAA18g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(4).to({graphics:mask_graphics_4,x:291.6,y:207}).wait(66));

	// 02 franja oscuro
	this.instance_1 = new lib._02franjaoscuro();
	this.instance_1.setTransform(354.6,524.1);
	this.instance_1._off = true;

	this.instance_1.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).wait(1).to({y:523.7},0).wait(1).to({y:522.7},0).wait(1).to({y:521},0).wait(1).to({y:518.4},0).wait(1).to({y:514.9},0).wait(1).to({y:510.5},0).wait(1).to({y:505},0).wait(1).to({y:498.4},0).wait(1).to({y:490.7},0).wait(1).to({y:481.6},0).wait(1).to({y:471.3},0).wait(1).to({y:459.8},0).wait(1).to({y:447.1},0).wait(1).to({y:433.5},0).wait(1).to({y:419.3},0).wait(1).to({y:404.8},0).wait(1).to({y:390.6},0).wait(1).to({y:376.9},0).wait(1).to({y:364.2},0).wait(1).to({y:352.6},0).wait(1).to({y:342.4},0).wait(1).to({y:333.5},0).wait(1).to({y:325.9},0).wait(1).to({y:319.6},0).wait(1).to({y:314.5},0).wait(1).to({y:310.5},0).wait(1).to({y:307.6},0).wait(1).to({y:305.6},0).wait(1).to({y:304.4},0).wait(1).to({y:304.1},0).wait(36));

	// 02 circulo claro
	this.instance_2 = new lib._02circuloclaro();
	this.instance_2.setTransform(284.7,120.9,0.022,0.022);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.26,scaleY:0.26},0).wait(1).to({scaleX:0.31,scaleY:0.31},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.43,scaleY:0.43},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.55,scaleY:0.55},0).wait(1).to({scaleX:0.62,scaleY:0.62},0).wait(1).to({scaleX:0.68,scaleY:0.68},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(38));

	// 02 mascara (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("EgffAgWMAAAg18MBNCAAAMAAAA18g");
	mask_1.setTransform(291.6,207);

	// 02 franja claro
	this.instance_3 = new lib._02franjaclaro();
	this.instance_3.setTransform(301.2,554.9);

	this.instance_3.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({y:554.5},0).wait(1).to({y:553.2},0).wait(1).to({y:550.9},0).wait(1).to({y:547.7},0).wait(1).to({y:543.2},0).wait(1).to({y:537.6},0).wait(1).to({y:530.6},0).wait(1).to({y:522.1},0).wait(1).to({y:512.1},0).wait(1).to({y:500.6},0).wait(1).to({y:487.4},0).wait(1).to({y:472.6},0).wait(1).to({y:456.3},0).wait(1).to({y:438.9},0).wait(1).to({y:420.7},0).wait(1).to({y:402.2},0).wait(1).to({y:384},0).wait(1).to({y:366.5},0).wait(1).to({y:350.2},0).wait(1).to({y:335.4},0).wait(1).to({y:322.3},0).wait(1).to({y:310.9},0).wait(1).to({y:301.2},0).wait(1).to({y:293.1},0).wait(1).to({y:286.6},0).wait(1).to({y:281.5},0).wait(1).to({y:277.7},0).wait(1).to({y:275.2},0).wait(1).to({y:273.7},0).wait(1).to({y:273.2},0).wait(40));

	// 02 seda
	this.instance_4 = new lib._02seda();
	this.instance_4.setTransform(365.9,462.9,0.021,0.021);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.26,scaleY:0.26},0).wait(1).to({scaleX:0.31,scaleY:0.31},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.55,scaleY:0.55},0).wait(1).to({scaleX:0.62,scaleY:0.62},0).wait(1).to({scaleX:0.68,scaleY:0.68},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(36));

	// 02 +
	this.instance_5 = new lib._02();
	this.instance_5.setTransform(326.4,467.1,0.028,0.028);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(2).to({_off:false},0).wait(1).to({regX:0.1,regY:-1.6,scaleX:0.03,scaleY:0.03,y:467},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.05,scaleY:0.05},0).wait(1).to({scaleX:0.07,scaleY:0.07},0).wait(1).to({scaleX:0.09,scaleY:0.09,y:466.9},0).wait(1).to({scaleX:0.11,scaleY:0.11},0).wait(1).to({scaleX:0.14,scaleY:0.14,y:466.8},0).wait(1).to({scaleX:0.18,scaleY:0.18},0).wait(1).to({scaleX:0.22,scaleY:0.22,y:466.7},0).wait(1).to({scaleX:0.26,scaleY:0.26,x:326.5},0).wait(1).to({scaleX:0.31,scaleY:0.31,y:466.6},0).wait(1).to({scaleX:0.37,scaleY:0.37,y:466.5},0).wait(1).to({scaleX:0.43,scaleY:0.43,y:466.4},0).wait(1).to({scaleX:0.49,scaleY:0.49,y:466.3},0).wait(1).to({scaleX:0.56,scaleY:0.56,y:466.2},0).wait(1).to({scaleX:0.62,scaleY:0.62,y:466.1},0).wait(1).to({scaleX:0.68,scaleY:0.68,y:466},0).wait(1).to({scaleX:0.73,scaleY:0.73,y:465.9},0).wait(1).to({scaleX:0.79,scaleY:0.79,y:465.8},0).wait(1).to({scaleX:0.83,scaleY:0.83,y:465.7},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9,y:465.6},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95,y:465.5},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({regY:0.1,scaleX:1,scaleY:1,x:326.4,y:467.1},0).wait(38));

	// 02 cepillo
	this.instance_6 = new lib._02cepillo();
	this.instance_6.setTransform(292.7,470.5,0.018,0.018);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.02,scaleY:0.02},0).wait(1).to({scaleX:0.03,scaleY:0.03},0).wait(1).to({scaleX:0.04,scaleY:0.04},0).wait(1).to({scaleX:0.06,scaleY:0.06},0).wait(1).to({scaleX:0.08,scaleY:0.08},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).wait(1).to({scaleX:0.13,scaleY:0.13},0).wait(1).to({scaleX:0.17,scaleY:0.17},0).wait(1).to({scaleX:0.21,scaleY:0.21},0).wait(1).to({scaleX:0.25,scaleY:0.25},0).wait(1).to({scaleX:0.31,scaleY:0.31},0).wait(1).to({scaleX:0.36,scaleY:0.36},0).wait(1).to({scaleX:0.42,scaleY:0.42},0).wait(1).to({scaleX:0.49,scaleY:0.49},0).wait(1).to({scaleX:0.55,scaleY:0.55},0).wait(1).to({scaleX:0.61,scaleY:0.61},0).wait(1).to({scaleX:0.68,scaleY:0.68},0).wait(1).to({scaleX:0.73,scaleY:0.73},0).wait(1).to({scaleX:0.78,scaleY:0.78},0).wait(1).to({scaleX:0.83,scaleY:0.83},0).wait(1).to({scaleX:0.87,scaleY:0.87},0).wait(1).to({scaleX:0.9,scaleY:0.9},0).wait(1).to({scaleX:0.93,scaleY:0.93},0).wait(1).to({scaleX:0.95,scaleY:0.95},0).wait(1).to({scaleX:0.97,scaleY:0.97},0).wait(1).to({scaleX:0.98,scaleY:0.98},0).wait(1).to({scaleX:0.99,scaleY:0.99},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(40));

	// 01 circulo oscuro
	this.instance_7 = new lib._01circulooscuro();
	this.instance_7.setTransform(206.8,166);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(70));

	// 01 mascara (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("EgffAgWMAAAg18MBNCAAAMAAAA18g");
	mask_2.setTransform(291.6,207);

	// 01 franja oscuro
	this.instance_8 = new lib._01franjaoscuro();
	this.instance_8.setTransform(184.7,289.5);

	this.instance_8.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(70));

	// 01 circulo claro
	this.instance_9 = new lib._01circuloclaro();
	this.instance_9.setTransform(114.8,93.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(70));

	// 01 mascara (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("EgffAgWMAAAg18MBNCAAAMAAAA18g");
	mask_3.setTransform(291.6,207);

	// 01 franja claro
	this.instance_10 = new lib._01franjaclaro();
	this.instance_10.setTransform(131.3,255.2);

	this.instance_10.mask = mask_3;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(70));

	// 01 cepillo
	this.instance_11 = new lib._01cepillo();
	this.instance_11.setTransform(160.4,469.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(70));

	// 00 indice de gingivitis
	this.instance_12 = new lib._00indicedegingivitis();
	this.instance_12.setTransform(454.2,540.8,1,1,0,0,0,-0.5,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(70));

	// 00 circulo2
	this.instance_13 = new lib._00circulo2();
	this.instance_13.setTransform(330,540.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(70));

	// 00 indice de placa
	this.instance_14 = new lib._00indicedeplaca();
	this.instance_14.setTransform(244.8,540.8,1,1,0,0,0,-0.1,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(70));

	// 00 circulo1
	this.instance_15 = new lib._00circulo1();
	this.instance_15.setTransform(170.1,540.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(70));

	// 00 3,0
	this.instance_16 = new lib._0030();
	this.instance_16.setTransform(37,27.6,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(70));

	// 00 2,73
	this.instance_17 = new lib._00273();
	this.instance_17.setTransform(38,70);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(70));

	// 00 2,5
	this.instance_18 = new lib._0025();
	this.instance_18.setTransform(36.8,109.2,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(70));

	// 00 2,1
	this.instance_19 = new lib._0021();
	this.instance_19.setTransform(38,160.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(70));

	// 00 1,5
	this.instance_20 = new lib._0015();
	this.instance_20.setTransform(35.4,215.8,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(70));

	// 00 1,0
	this.instance_21 = new lib._0010();
	this.instance_21.setTransform(35.7,274.5,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(70));

	// 00 0,5
	this.instance_22 = new lib._0005();
	this.instance_22.setTransform(37,336,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(70));

	// 00 0,0
	this.instance_23 = new lib._0000();
	this.instance_23.setTransform(31.5,395.5,1,1,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(70));

	// 00 mascara (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	mask_4.graphics.p("EgnqAIdIAAr8MBYGAAAIAAL8g");
	mask_4.setTransform(310,54.1);

	// 00 flecha1
	this.instance_24 = new lib._00flecha1();
	this.instance_24.setTransform(337.9,70.3);

	this.instance_24.mask = mask_4;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(70));

	// 00 mascara (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	mask_5.graphics.p("EgnqAPKIAAr+MBYGAAAIAAL+g");
	mask_5.setTransform(310,97);

	// 00 flecha2
	this.instance_25 = new lib._00flecha2();
	this.instance_25.setTransform(337.9,160.6);

	this.instance_25.mask = mask_5;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(70));

	// 00 mascara (mask)
	var mask_6 = new cjs.Shape();
	mask_6._off = true;
	mask_6.graphics.p("EgD/AgWMAAAg7+IL8AAMAAAA7+g");
	mask_6.setTransform(51,207);

	// 00 carte2
	this.instance_26 = new lib._00carte2();
	this.instance_26.setTransform(63.5,222);

	this.instance_26.mask = mask_6;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(70));

	// 00 mascara (mask)
	var mask_7 = new cjs.Shape();
	mask_7._off = true;
	mask_7.graphics.p("EgnrAh9IAAr+MBYGAAAIAAL+g");
	mask_7.setTransform(310,217.4);

	// 00 carte1
	this.instance_27 = new lib._00carte1();
	this.instance_27.setTransform(338,397.5);

	this.instance_27.mask = mask_7;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(70));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(340,300.7,600,537.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;